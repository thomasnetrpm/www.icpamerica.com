<?
# EXECUTE YOUR CSS
$css = 'home';

# IMPORT YOUR BASE TEMPLATE
include 'manager/home.php';
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<?
if(isset($_POST['login'])) {			
	//check if the login information is valid
	$_POST = sanitize($_POST);
	$client = $_POST;
	settype($client,'object');
	if(Client::checkLogin($client)==1) {
		$_SESSION['logged_in'] = true;
	    $clients = Client::getInfo($client);
		$_SESSION['client_id']	 = $clients->fldClientID;
		$_SESSION['client_type'] = $clients->fldClientType;
		$_SESSION['client_firstname'] = $clients->fldClientFirstName;
		$xclient = session_id();

		$condition = "fldTempCartClientID='$xclient'";
		if(TempCart::countTempcartbyCondition($condition)>=1) {
			//change the client id
			TempCart::updateTempcartClient($_SESSION['client_id'],$xclient);
			$links = $root.'billing-info.html';
			header("Location: $links");					
		} else {
			// $links = $root . 'account-information.html';
			$links = $root;
			error_log('redirecting to ' . $links);
			header("Location: $links");			
		}
	} else {		
		$error = "Invalid username or password";
	}
}
?>

<form action="" method="post" id="header-panel">

	<img src="temp/slogan/home-slogan.png" alt="">
	<?php
	if (!$_SESSION['logged_in']) {
		// echo "logged out";
		?>
		<fieldset>
			<h3>Sign In to your Account</h3>
			<? if(isset($error)): ?>
				<div class="alert alert-error">
		    		<button type="button" class="close" data-dismiss="alert">&times;</button>
		    		<?=$error?>
		    	</div>
			<? endif; ?>
			<div class="user-field">
				<input type="text" name="username" placeholder="Username" required>
				<input type="password" name="password" placeholder="Password" required>
			</div>

			<ul class="unstyled clearfix">
				<li class="pull-left">
					<input type="checkbox"> Remember Me
				</li>
				<li class="pull-right">
					<button type="submit" class="btn_submit" name="login"> <b class="icon-lock icon-white"></b> Sign In</button>	
				</li>
			</ul>

			<ul class="user-access">
				<li><a href="<?=$root?>forgot-password.html">Forgot your ID or Password?</a></li>
				<!-- <li><a href="<?=$root?>registration.html">New Customer? Click here to create an account.</a></li> -->
			</ul>

		</fieldset>
		<?php
	} else {
		// echo "logged in";
		?>
		<?php
	}
	?>
</form>


<article class="product-module-wrap">
	<section class="product-module">
		<ul class="unstyled clearfix product-list">
			<?php
			// Get Top 9 Featured Category
			$category = Category::getFeaturedCategory(9);
			$iCat = 1;
			foreach ($category as $cat) {
				$categoryID 	= $cat->fldCategoryID;
				$categoryName 	= $cat->fldCategoryName;
				$categoryURL 	= $cat->fldCategoryURL;
				$categoryImage 	= $cat->fldCategoryImage;
				$isURL 			= $cat->fldCategoryIsUrl;

				if ($isURL==1) {
					$link = $categoryURL;
				} else {
					$link = $root.'products/'.$categoryURL;
				}

				$outsideLink = array(9, 11, 14);
				if (in_array($categoryID, $outsideLink)) { $target = "target='_blank'"; }
				?>
				<li>
					<a href="<?=$link?>" <?=$target?> >
					<h2><?=$categoryName?></h2>
					<img src="<?=$root?>uploads/category/_320_<?=$categoryImage?>" width="336" height="150" alt="<?=$categoryName?>" style="border-left: solid 1px #CCC;">
					</a>
				</li>
				<?php
				if ($iCat == '6') { // Show inbetween info
					?>
				</ul>
			</section>
		</article>

		<article class="company-info" style='height:auto;'>
			<section>
				<h1><span class="greened" style='color:#fff;'>ICP America, Inc</span> The Leading Industrial Computer Supplier</h1>
				<h2>Serving the Industrial Market for more than 25 years.</h2>
				<p>In an industry that is constantly re-inventing itself, ICP America has remained one step ahead of the competition for more than 25 years.  Originally founded as a product development company, ICP America quickly made a name for itself with computer innovated designs.  <a href="<?=$root?>about_us/default.html">Read More</a></p>
				<!--HubSpot Call-to-Action Code -->
<span class="hs-cta-wrapper" id="hs-cta-wrapper-8672f315-74b1-4c13-b952-4acad009ee86">
    <span class="hs-cta-node hs-cta-8672f315-74b1-4c13-b952-4acad009ee86" id="hs-cta-8672f315-74b1-4c13-b952-4acad009ee86">
        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/8672f315-74b1-4c13-b952-4acad009ee86" ><img class="hs-cta-img" id="hs-cta-img-8672f315-74b1-4c13-b952-4acad009ee86" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/8672f315-74b1-4c13-b952-4acad009ee86.png"  alt="Want to be an ICP America Partner?"/></a>
    </span>
    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
    <script type="text/javascript">
        hbspt.cta.load(549477, '8672f315-74b1-4c13-b952-4acad009ee86');
    </script>
</span>
<!-- end HubSpot Call-to-Action Code -->

			</section>
		</article>

		<article>
			<section>
				<ul class="unstyled clearfix product-list">
					<?php
				}
			$iCat++;
			}
			?>
		</ul>
	</section>
</article>




<? endblock(); ?>



<? # CSS & JAVASCRIPT PLUGINS BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<? endblock(); ?>