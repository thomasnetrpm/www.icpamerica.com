<?

class HomePageGallery{
     
    function addHomePageGallery($gallery,$image){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($gallery->name);
		$links = addslashes($gallery->links);
		
		
        $query="INSERT INTO ".$table_prefix."_tblHomePageGallery SET ".           			
			"fldHomePageGalleryImage='$image',". 
			"fldHomePageGalleryPosition='$gallery->positions',". 			
			"fldHomePageGalleryLinks='$gallery->links',". 
			"fldHomePageGalleryName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateHomePageGallery($gallery,$image) {
		 global $adb;
		 global $table_prefix;
		 

		$name = addslashes($gallery->name);
		$links = addslashes($gallery->links);
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblHomePageGallery SET ".
				"fldHomePageGalleryPosition='$gallery->positions',". 			
				"fldHomePageGalleryLinks='$gallery->links',". 
				"fldHomePageGalleryName='$name'". 	
		    " WHERE fldHomePageGalleryID=$gallery->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblHomePageGallery SET ".
				"fldHomePageGalleryImage='$image',". 
				"fldHomePageGalleryPosition='$gallery->positions',". 			
				"fldHomePageGalleryLinks='$gallery->links',". 
				"fldHomePageGalleryName='$name'". 		
		    " WHERE fldHomePageGalleryID=$gallery->Id";
			
		}
           
		
	    $adb->query($query);
        return true;
	}
	
	function updateHomePageGalleryPositions($positions,$project_id) {
		 global $adb;
		 global $table_prefix;
		 
			$query="UPDATE ".$table_prefix."_tblHomePageGallery SET ".
			"fldHomePageGalleryPosition='$positions'". 	
		    " WHERE fldHomePageGalleryID='$project_id'";
		$adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePageGallery ORDER BY fldHomePageGalleryPosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePageGalleryORDER BY fldHomePageGalleryPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllHomePageGallery() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePageGallery ORDER BY fldHomePageGalleryID DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countHomePageGallery() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePageGallery";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findHomePageGallery($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblHomePageGallery WHERE fldHomePageGalleryID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteHomePageGallery($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblHomePageGallery WHERE fldHomePageGalleryID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
