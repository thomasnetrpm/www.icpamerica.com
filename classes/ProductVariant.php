<?php
class ProductVariant{
     
// OPTION

    function addProductOption($option){
        global $adb;
		global $table_prefix;
		
		$product_id 	= $option->product_id;
		$product_option = addslashes(trim($option->product_option));
		
        $query = "INSERT INTO ".$table_prefix."_tblProductOption SET ".
			"product_id='$product_id',".
			"product_option='$product_option',".
			"status='1',".
            "date_added=NOW() ";
        $adb->query($query);
        return mysql_insert_id();
    }

	function displayAllOptionByProductID($product_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductOption WHERE product_id='$product_id' ORDER BY product_option DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
    function findOption($option_id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProductOption WHERE product_option_id='$option_id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

	function countOption($product_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductOption WHERE product_id='$product_id' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}


	function removeOrderingInformation($orderinfo_id) {
		global $adb;
		global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProductOption WHERE product_option_id='$orderinfo_id'";
		$adb->query($query);
        return true;
	}


	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductOption ORDER BY product_id";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	

// VARIANT -----------------------------------------------------------------------------------------------------------------------

	function findAllVariantByOption($product_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductVariant WHERE product_id='$product_id' ORDER BY product_option_id ASC, product_variant_id DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function findAllVariantByOption2($option_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductVariant WHERE product_option_id='$option_id' ORDER BY product_option_id ASC, product_variant_id DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

    function findVariant($variant_id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProductVariant WHERE product_variant_id='$variant_id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

	function addVariant($variant) {
		global $adb;
		global $table_prefix;
		
		$product_id 	= $variant->product_id;
		$option_id 		= $variant->product_option_id;
		$variant_name 	= addslashes(trim($variant->variant));
		$variant_description = addslashes(trim($variant->variant_description));
		$price 			= $variant->variant_price;

        $query = "INSERT INTO ".$table_prefix."_tblProductVariant SET ".
			"product_id='$product_id',".
			"product_option_id='$option_id',".
			"product_variant='$variant_name',".
			"product_variant_description='$variant_description',".
			"price='$price',".
			"status='1',".
            "date_added=NOW() ";
        $adb->query($query);
        return mysql_insert_id();
	}


	function updateVariant($variant) {
		global $adb;
		global $table_prefix;
		
		$variant_id 	= $variant->variant_id;
		// $variant_name 	= addslashes(trim($variant->variant));
		// $variant_description = addslashes(trim($variant->variant_description));
		$price 			= $variant->variant_price;

        $query = "UPDATE ".$table_prefix."_tblProductVariant SET ".
			// "product_variant='$variant_name', ".
			// "product_variant_description='$variant_description', ".
			"price='$price' ".
            "WHERE product_variant_id='$variant_id' ";
        $adb->query($query);
        return mysql_insert_id();
	}


	function updateAllVariantPrice($variant) {
		global $adb;
		global $table_prefix;
		
		$price 			= $variant->variant_price;
		$variant_name 	= addslashes(trim($variant->variant_name));

        $query = "UPDATE ".$table_prefix."_tblProductVariant SET ".
			"price='$price' ".
            "WHERE product_variant='$variant_name' ";
        $adb->query($query);
        return mysql_insert_id();
	}


	function removeVariant($variant_id) {
		global $adb;
		global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProductVariant WHERE product_variant_id='$variant_id'";
        $adb->query($query);
        return true;
	}

	function countProductOption($orderinfo_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProductVariant WHERE product_option_id='$orderinfo_id'";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

/*
    function addProducts($products,$image){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($products->content);		
		$name = addslashes($products->name);
		$url = addslashes($products->url);
		$code = addslashes($products->code);
		$overview = addslashes($products->overview);
		$features = addslashes($products->features);
		$techspec = addslashes($products->techspec);
		$orderinfo = addslashes($products->orderinfo);
		$information = addslashes($products->information);
		$metatitle = addslashes($products->meta_title);
		
        $query="INSERT INTO ".$table_prefix."_tblProducts SET ".           			
			"fldProductsCategoryID='$products->category_id',". 			
			"fldProductsName='$name',".
			"fldProductsURL='$url',".
			"fldProductsCode='$code',".
			"fldProductsAvailability='$products->isAvailable',". 
			"fldProductsFeatured='$products->isFeatured',". 
			"isNew='$products->isNew',". 
			"isSpecialDeal='$products->isSpecialDeal',". 
			"fldProductsPosition='$products->position',". 
			"fldProductsPrice1='$products->price1',".
			"fldProductsPrice1Text='$products->price1text',".
			"fldProductsPrice2='$products->price2',".
			"fldProductsPrice2Text='$products->price2text',".
			"fldProductsPrice3='$products->price3',".
			"fldProductsPrice3Text='$products->price3text',".
			"fldProductsWeight='$products->weight',". 				
			"fldProductsInformation='$information',".
			"fldProductsOverview='$overview',".
			"fldProductsFeatures='$features',".
			"fldProductsTechspecs='$techspec',".
			"fldProductsOrderinfo='$orderinfo',".
			"fldProductsImage='$image',".
			"fldProductMetaTitle='$metatitle',".
            "fldProductsDescription='$content'";
        $adb->query($query);
        return mysql_insert_id();
    }

	function updateProducts($products, $image, $pdf) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($products->content);		
		$name = addslashes($products->name);
		$code = addslashes($products->code);
		$url = addslashes($products->url);
		$overview = addslashes($products->overview);
		$features = addslashes($products->features);
		$techspec = addslashes($products->techspec);
		$orderinfo = addslashes($products->orderinfo);
		$information = addslashes($products->information);
		$metatitle = addslashes($products->meta_title);
		
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsCategoryID='$products->category_id',". 			
			"fldProductsName='$name',".
			"fldProductsCode='$code',".
			"fldProductsURL='$url',".
			"fldProductsPosition='$products->position',". 
			"fldProductsAvailability='$products->isAvailable',". 
			"fldProductsFeatured='$products->isFeatured',".
			"isNew='$products->isNew',". 
			"isSpecialDeal='$products->isSpecialDeal',". 
			"fldProductsPrice1='$products->price1',".
			"fldProductsPrice1Text='$products->price1text',".
			"fldProductsPrice2='$products->price2',".
			"fldProductsPrice2Text='$products->price2text',".
			"fldProductsPrice3='$products->price3',".
			"fldProductsPrice3Text='$products->price3text',".
			"fldProductsWeight='$products->weight',";
		if($image != "") { $query .= "fldProductsImage='$image', "; }
		if($pdf != "") { $query .= "fldProductsPDF='$pdf', "; }
		$query .= "fldProductsOverview='$overview',".
			"fldProductsFeatures='$features',".
			"fldProductsTechspecs='$techspec',".
			"fldProductsOrderinfo='$orderinfo',".
			"fldProductsInformation='$information',".
			"fldProductMetaTitle='$metatitle',".
            "fldProductsDescription='$content'".
            " WHERE fldProductsId=$products->Id";
	    $adb->query($query);
        return true;
	}

	function updateMainCategory($categoryID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsMainCategoryID='$categoryID', ".
        	"fldProductsSubcategoryID= null, ".
        	"fldProductsSubcategory2ID= null ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function updateSubCategory($subcategoryID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsSubcategoryID='$subcategoryID', ".
        	"fldProductsSubcategory2ID= null ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function updateSubCategory2($subcategory2ID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsSubcategory2ID='$subcategory2ID' ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBySubcategory($subcatID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsSubcategoryID='$subcatID' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllBySubcategory2($subcat2ID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsSubcategory2ID='$subcat2ID' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllProducts() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts ORDER BY fldProductsName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countProducts() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	function countProductsBySubcategory($subcatID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts 
				WHERE fldProductsSubcategoryID='$subcatID' 
				AND (fldProductsSubcategory2ID is NULL OR fldProductsSubcategory2ID='0') 
				ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}


    function findProducts($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function findProductByName($productname){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE LCASE(fldProductsName)='$productname'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function findProductByCode($productcode){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE LCASE(fldProductsCode)='$productcode'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function deleteProducts($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProducts WHERE fldProductsId='$id'";
        $adb->query($query);
        return true;
    }
    
	function addProductImages($productID, $filename, $order) {
		global $adb;
		global $table_prefix;
		
		$query = "INSERT INTO ".$table_prefix."_tblProductImage 
					SET fldProductId='".$productID."', 
					fldProductImage='".$filename."', 
					fldProductImagePosition='".$order."' ";
		$result = $adb->query($query);
		return true;
	}
	
	function displayAllProductImages($productID) {
		global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblProductImage 
					WHERE fldProductId='".$productID."' 
					ORDER BY fldProductImagePosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function deleteProductImage($productImageID) {
		global $adb;
		global $table_prefix;
		
		$query = "DELETE FROM ".$table_prefix."_tblProductImage 
				WHERE fldProductImageID='".$productImageID."' ";
		$result = $adb->query($query);
		return true;
	}
   
	function displayAllByCondition($catID, $condition) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' $condition ";
		$query .= "ORDER BY fldProductsPosition ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countAllByCondition($catID, $condition) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' $condition ";
		$query .= "ORDER BY fldProductsPosition ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function findProductByURL($url){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsURL= '$url' ";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    // select the id to use for your insert statement
    function getNextProductID(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT coalesce(max(fldProductsId)+1,1) AS ID FROM cms_tblProducts";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    // select next value during insert
    function insertNewProduct(){
        global $adb;
		global $table_prefix;
		
        $query = "INSERT INTO `cms_tblProducts`(fldProductsId, fldProductsName)
        	SELECT coalesce(max(fldProductsId)+1,1) AS ID, 'New Product' FROM cms_tblProducts";
        $result=$adb->query($query);
        return mysql_insert_id();
    }

    // Display All isNew
	function displayAllIsNew($count) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isNew='1' ORDER BY fldProductsName LIMIT 0, $count";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countProductsIsNew() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isNew='1' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    // Display All isSpecialDeal
	function displayAllIsSpecialDeal($count) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isSpecialDeal='1' ORDER BY fldProductsName LIMIT 0, $count";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countProductsIsSpecialDeal() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isSpecialDeal='1' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function updateProductsByPosition($position, $product_id) {
		 global $adb;
		 global $table_prefix;
		 			
		$query ="UPDATE ".$table_prefix."_tblProducts SET ".	
			"fldProductsPosition='$position' ".
            "WHERE fldProductsId='$product_id'";
	    $adb->query($query);
        return true;
	}

	// SEARCH BY KEYWORD
	function countProductByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%$keyword%' ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		return $result->db_num_rows();
	}
	
	function displayProductByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%$keyword%' ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	// DELETE
	function deleteDatasheet($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsPDF = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}

	function deleteUsermanual($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsUserManual = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}

	function deleteQuickguide($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsQuickGuide = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}
*/
}
?>
