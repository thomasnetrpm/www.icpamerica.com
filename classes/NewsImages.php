<?

class NewsImages{
     
    function addNewsImages($news,$image){
        global $adb;
		global $table_prefix;
		

        $query="INSERT INTO ".$table_prefix."_tblNewsImages SET ".           			
			"fldNewsId='$news->news_id',". 	
			"fldNewsImagesImage='$image'"; 
        $adb->query($query);
        return true;
    }
	
	function updateNewsImages($news,$image) {
		 global $adb;
		 global $table_prefix;
		 
		if($image != "") {
			$query="UPDATE ".$table_prefix."_tblNewsImages SET ".
			"fldNewsImagesImage='$image'". 	
		    " WHERE fldNewsImagesID=$news->Id";
			
		}
           
		
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsImages";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($projectid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsImages WHERE fldNewsId='$projectid'";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllNewsImages() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsImages";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countNewsImages() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsImages";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findNewsImages($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblNewsImages WHERE fldNewsImagesID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteNewsImages($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblNewsImages WHERE fldNewsImagesID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
