<?
class TeamCategory{
     
	function countCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeamCategory";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeamCategory ORDER BY fldTCID ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function findCategory($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTeamCategory WHERE fldTCID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function addTeamCategory($team){
        global $adb;
		global $table_prefix;
		
		$name = addslashes($team->name);
		
        $query = "INSERT INTO ".$table_prefix."_tblTeamCategory SET ".           			
			"fldTCName='$name' ";
		$adb->query($query);
        return true;
    }

	function updateTeamCategory($team) {
		global $adb;
		global $table_prefix;
		 
		$name = addslashes($team->name);
		
		$query  = "UPDATE ".$table_prefix."_tblTeamCategory SET ".
        	"fldTCName='$name' ".
	        "WHERE fldTCID=$team->Id ";
	    $adb->query($query);
        return true;
	}
    
	function findTeamCategory($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTeamCategory WHERE fldTCID='$id' ";
        $result=$adb->query($query);
        return $result->fetch_object();
    }



/*

	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function displayAllTeam($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamId!='$id'";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function findTeamfirst(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTeam LIMIT 1 ORDER BY Id";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteTeam($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblTeam WHERE fldTeamId='$id'";
        $adb->query($query);
        return true;
    }
    
	function displayAllByCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam ORDER BY fldTeamCategory, fldTeamId ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
*/
}
?>
