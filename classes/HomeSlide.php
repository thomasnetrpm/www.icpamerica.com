<?

class HomeSlide{
     
    function addHomeSlide($home,$image){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($home->name);
		
        $query="INSERT INTO ".$table_prefix."_tblHomeSlide SET ".           			
			"fldHomeSlideImage='$image',". 
			"fldHomeSlideLinks='$home->url',".
			"fldHomeSlidePosition='$home->position',". 
			"fldHomeSlideName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateHomeSlide($home,$image) {
		 global $adb;
		 global $table_prefix;
		 

		$name = addslashes($home->name);

		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblHomeSlide SET ".
           "fldHomeSlideLinks='$home->url', ".
		   "fldHomeSlidePosition='$home->position', ".
		   "fldHomeSlideName='$name', ".
		   "fldHomeSlideImageLink='$home->link' ".
		   "WHERE fldHomeSlideID=$home->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblHomeSlide SET ".
			"fldHomeSlideImage='$image', ".
			"fldHomeSlidePosition='$home->position', ".
			"fldHomeSlideLinks='$home->url', ".
			"fldHomeSlideName='$name', ".
			"fldHomeSlideImageLink='$home->link' ".
			"WHERE fldHomeSlideID=$home->Id";
		}
           
	    $adb->query($query);
        return true;
	}
	
	
	function updateHomeSlidePositions($positions,$project_id) {
		 global $adb;
		 global $table_prefix;
		 
			$query="UPDATE ".$table_prefix."_tblHomeSlide SET ".
			"fldHomeSlidePosition='$positions'". 	
		    " WHERE fldHomeSlideID='$project_id'";

		$adb->query($query);
        return true;
	}
	
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomeSlide ORDER BY fldHomeSlidePosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomeSlide ORDER BY fldHomeSlidePosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countHomeSlide() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomeSlide";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findHomeSlide($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblHomeSlide WHERE fldHomeSlideID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteHomeSlide($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblHomeSlide WHERE fldHomeSlideID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
