<?

class Podcast{
     
    function addPodcast($podcast,$file){
        global $adb;
		global $table_prefix;
		
		
		$name = addslashes($podcast->name);
		$guest_name = addslashes($podcast->guest_name);
		
		
        $query="INSERT INTO ".$table_prefix."_tblPodcast SET ".           							
			"fldPodcastName='$name',".		
			"fldPodcastFile='$file',".
			"fldPodcastGuest='$podcast->fldPodcastGuest',".		
			"fldPodcastGuestName='$guest_name',".		
			"fldPodcastDate='$podcast->date'";            
        $adb->query($query);
        return true;
    }
	
	function updatePodcast($podcast,$file) {
		 global $adb;
		 global $table_prefix;
		 
		
		$name = addslashes($podcast->name);
		$guest_name = addslashes($podcast->guest_name);
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblPodcast SET ".
         	"fldPodcastName='$name',".		
			"fldPodcastGuest='$podcast->fldPodcastGuest',".		
			"fldPodcastGuestName='$guest_name',".
			"fldPodcastDate='$podcast->date'".
        
            " WHERE fldPodcastId=$podcast->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblPodcast SET ".
        		"fldPodcastName='$name',".		
			"fldPodcastFile='$file',".
			"fldPodcastGuest='$podcast->fldPodcastGuest',".		
			"fldPodcastGuestName='$guest_name',".
			"fldPodcastDate='$podcast->date'".

            " WHERE fldPodcastId=$podcast->Id";
		}
	    $adb->query($query);
		
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast ORDER BY fldPodcastDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast ORDER BY fldPodcastDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBydate($dateFrom,$dateTo) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast WHERE fldPodcastDate BETWEEN '$dateFrom' AND '$dateTo' ORDER BY fldPodcastDate DESC";		
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBydateGuest($dateFrom,$dateTo) {
		global $adb;
		global $table_prefix;
		$guest = "1";
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast WHERE fldPodcastDate BETWEEN '$dateFrom' AND '$dateTo' AND fldPodcastGuest='$guest' ORDER BY fldPodcastDate DESC";		
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayFirst4() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast ORDER BY fldPodcastDate DESC LIMIT 4";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllPodcast() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast ORDER BY fldPodcastName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countPodcast() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPodcast";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findPodcastByPosition($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPodcast WHERE fldPodcastCategory='$id' ORDER BY fldPodcastDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findPodcast($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPodcast WHERE fldPodcastId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deletePodcast($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblPodcast WHERE fldPodcastId='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
