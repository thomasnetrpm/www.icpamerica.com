<?php
class Subcategory{
     
    function addSubcategory($subcat, $image){
        global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$categoryID = addslashes($subcat->cid);
		$meta_title = addslashes($subcat->meta_title);
		$meta_keywords = addslashes($subcat->meta_keywords);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
        $query = "INSERT INTO ".$table_prefix."_tblSubcategory SET ".           			
			"fldSubcategoryName='$name',".
			"fldSubcategoryDescription='$description',".
			"fldCategoryID='$categoryID',".
			"fldSubcategoryURL='$url',".
			"fldSubcategoryImage='$image',".
			"fldSubcategoryMetaTitle='$meta_title',".
			"fldSubcategoryMetaKeywods='$meta_keywords',".
			"fldSubcategoryMetaDescription='$meta_description',".
			"fldSubcategoryPosition='$position'"; 						
        $adb->query($query);
        return true;
    }

	function updateSubcategory($subcat, $image) {
		global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$meta_title = addslashes($subcat->meta_title);
		$meta_keywords = addslashes($subcat->meta_keywords);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
		$query  = "UPDATE ".$table_prefix."_tblSubcategory SET ".
			"fldSubcategoryName='$name', ".
			"fldSubcategoryDescription='$description',".
			"fldSubcategoryURL='$url',";
		if ($image != '') { $query .= "fldSubcategoryImage='$image', "; }
		if ($meta_description != '') { $query .= "fldSubcategoryMetaDescription='$meta_description', "; }
		$query .= "fldSubcategoryMetaTitle='$meta_title', ".
			"fldSubcategoryPosition='$position' ".
			"WHERE fldSubcategoryID=$subcat->Id ";
		// die();
	    $adb->query($query);
        return true;
	}

  	function findSubcategory($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldSubcategoryID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
    
	function findAll($categoryID, $pg) {
		global $adb;
		global $table_prefix;
		
		// $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='".$categoryID."' ";
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='".$categoryID."' AND subcategoryID='' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

    function findByName($subcategoryname){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE LCASE(fldSubcategoryName)='$subcategoryname'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

  //   function findByURL($subcategory_url){
  //       global $adb;
		// global $table_prefix;
		
  //       $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldSubcategoryURL = '$subcategory_url'";
  //       $result=$adb->query($query);
  //       return $result->fetch_object();
  //   }

    function findByURL($url){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldSubcategoryURL = '$url'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

	function displayAll($categoryID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='".$categoryID."' AND subcategoryID='' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}


	function displayAllNotHidden($categoryID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory 
			WHERE fldCategoryID='".$categoryID."' AND subcategoryID='' 
			AND fldSubcategoryIsHidden IS NULL 
			ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}


	function countSubcategory($categoryID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='".$categoryID."' AND subcategoryID='' ";		
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	function updateSubcategoryOrder($subcategoryID, $orderID) {
		global $adb;
		global $table_prefix;

		$query = "UPDATE ".$table_prefix."_tblSubcategory SET ".
			"fldSubcategoryPosition='$orderID' ".
			"WHERE fldSubcategoryID=$subcategoryID";
	    $adb->query($query);
        return true;
	}

    function deleteSubcategory($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblSubcategory WHERE fldSubcategoryID='$id'";
        $adb->query($query);
        return true;
    }


// ---------------------------------------------------------- 
    function addSubcategory2($subcat, $image){
        global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$categoryID = addslashes($subcat->cid);
		$subcategoryID = addslashes($subcat->scid);
		$meta_title = addslashes($subcat->meta_title);
		$meta_keywords = addslashes($subcat->meta_keywords);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
        $query = "INSERT INTO ".$table_prefix."_tblSubcategory SET ".           			
			"fldSubcategoryName='$name',".
			"fldSubcategoryDescription='$description',".
			"fldCategoryID='$categoryID',".
			"subcategoryID='$subcategoryID',".
			"fldSubcategoryURL='$url',".
			"fldSubcategoryImage='$image',".
			"fldSubcategoryMetaTitle='$meta_title',".
			"fldSubcategoryMetaKeywods='$meta_keywords',".
			"fldSubcategoryMetaDescription='$meta_description',".
			"fldSubcategoryPosition='$position'";
        $adb->query($query);
        return true;
    }

	function updateSubcategory2($subcat, $image) {
		global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$meta_title = addslashes($subcat->meta_title);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
		$query  = "UPDATE ".$table_prefix."_tblSubcategory SET ".
			"fldSubcategoryName='$name', ".
			"fldSubcategoryDescription='$description',".
			"fldSubcategoryURL='$url', ";
		if ($image != '') { $query .= "fldSubcategoryImage='$image', "; }
		if ($meta_description != '') { $query .= "fldSubcategoryMetaDescription='$meta_description', "; }
		$query .= "fldSubcategoryMetaTitle='$meta_title', ".
			"fldSubcategoryPosition='$position' ".
			"WHERE fldSubcategoryID=$subcat->Id ";
	    $adb->query($query);
        return true;
	}

	function countSubcategory2($categoryID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$categoryID."' AND subcategory2ID IS NULL ";
		// $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$categoryID."' ";		
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function findAll2($subcategoryID, $pg) {
		global $adb;
		global $table_prefix;
		
 		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$subcategoryID."' AND subcategory2ID IS NULL ORDER BY fldSubcategoryPosition ASC ";
		// $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$subcategoryID."' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAll2($subcategoryID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$subcategoryID."' AND subcategory2ID IS NULL ORDER BY fldSubcategoryPosition ASC ";
		// $query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategoryID='".$subcategoryID."' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}


	function findByID($catID, $cond1) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='$catID' AND $cond1 ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countByID($catID, $cond1) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='$catID' AND $cond1 ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function countNullByID($catID, $cond1) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE fldCategoryID='$catID' AND (fldSubcategoryURL = '' OR fldSubcategoryURL IS NULL) AND $cond1 ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function updateSubcategoryByPosition($position, $subcat_id) {
		 global $adb;
		 global $table_prefix;
		 			
		$query = "UPDATE ".$table_prefix."_tblSubcategory SET ".	
			"fldSubcategoryPosition='$position' ".
            "WHERE fldSubcategoryID='$subcat_id'";
	    $adb->query($query);
        return true;
	}

	function updateSubcategory2ByPosition($position, $subcat2_id) {
		global $adb;
		global $table_prefix;

		$query = "UPDATE ".$table_prefix."_tblSubcategory SET ".
			"fldSubcategoryPosition='$position' ".
			"WHERE fldSubcategoryID=$subcat2_id";
	    $adb->query($query);
        return true;
	}


// Sub-category 3
	function countSubcategory3($subcategory2_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategory2ID='".$subcategory2_id."' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function findAll3($subcategory2_id, $pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategory2ID='".$subcategory2_id."' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAll3($subcategory2_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSubcategory WHERE subcategory2ID='".$subcategory2_id."' ORDER BY fldSubcategoryPosition ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function updateSubcategory3($subcat, $image) {
		global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$meta_title = addslashes($subcat->meta_title);
		$meta_keywords = addslashes($subcat->meta_keywords);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
		$query  = "UPDATE ".$table_prefix."_tblSubcategory SET ".
			"fldSubcategoryName='$name', ".
			"fldSubcategoryDescription='$description',".
			"fldSubcategoryURL='$url', ";
		if ($image != '') { $query .= "fldSubcategoryImage='$image', "; }
		$query .= "fldSubcategoryMetaTitle='$meta_title', ".
			"fldSubcategoryPosition='$position' ".
			"WHERE fldSubcategoryID=$subcat->Id ";
		// echo $query;
		// die();
	    $adb->query($query);
        return true;
	}

    function addSubcategory3($subcat, $image){
        global $adb;
		global $table_prefix;

		$name = addslashes($subcat->name);
		$description = addslashes($subcat->description);
		$url = addslashes($subcat->url);
		$categoryID = addslashes($subcat->cid);
		$subcategoryID = addslashes($subcat->scid);
		$subcategory2ID = addslashes($subcat->sc2id);
		$meta_title = addslashes($subcat->meta_title);
		$meta_keywords = addslashes($subcat->meta_keywords);
		$meta_description = addslashes($subcat->meta_description);
		$position = addslashes($subcat->position);
		
        $query = "INSERT INTO ".$table_prefix."_tblSubcategory SET ".           			
			"fldSubcategoryName='$name',".
			"fldSubcategoryDescription='$description',".
			"fldCategoryID='$categoryID',".
			"subcategoryID='$subcategoryID',".
			"subcategory2ID='$subcategory2ID',".
			"fldSubcategoryURL='$url',".
			"fldSubcategoryImage='$image',".
			"fldSubcategoryMetaTitle='$meta_title',".
			"fldSubcategoryMetaKeywods='$meta_keywords',".
			"fldSubcategoryMetaDescription='$meta_description',".
			"fldSubcategoryPosition='$position'";
        $adb->query($query);
        return true;
    }



}
?>
