<?

class Products{
     
    function addProducts($products,$image){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($products->content);		
		$name = addslashes($products->name);
		$url = addslashes($products->url);
		$code = addslashes($products->code);
		$overview = addslashes($products->overview);
		$features = addslashes($products->features);
		$techspec = addslashes($products->techspec);
		$orderinfo = addslashes($products->orderinfo);
		$information = addslashes($products->information);
		$metatitle = addslashes($products->meta_title);
		
        $query="INSERT INTO ".$table_prefix."_tblProducts SET ".           			
			"fldProductsCategoryID='$products->category_id',". 			
			"fldProductsName='$name',".
			"fldProductsURL='$url',".
			"fldProductsCode='$code',".
			"fldProductsAvailability='$products->isAvailable',". 
			"fldProductsFeatured='$products->isFeatured',". 
			"isNew='$products->isNew',". 
			"isSpecialDeal='$products->isSpecialDeal',". 
			"fldProductsPosition='$products->position',". 
			"fldProductsPrice1='$products->price1',".
			"fldProductsPrice1Text='$products->price1text',".
			"fldProductsPrice2='$products->price2',".
			"fldProductsPrice2Text='$products->price2text',".
			"fldProductsPrice3='$products->price3',".
			"fldProductsPrice3Text='$products->price3text',".
			"fldProductsWeight='$products->weight',". 				
			"fldProductsInformation='$information',".
			"fldProductsOverview='$overview',".
			"fldProductsFeatures='$features',".
			"fldProductsTechspecs='$techspec',".
			"fldProductsOrderinfo='$orderinfo',".
			"fldProductsImage='$image',".
			"fldProductMetaTitle='$metatitle',".
            "fldProductsDescription='$content'";
        $adb->query($query);
        return mysql_insert_id();
    }

	function updateProducts($products, $image, $pdf) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($products->content);		
		$name = addslashes($products->name);
		$code = addslashes($products->code);
		$url = addslashes($products->url);
		$overview = addslashes($products->overview);
		$features = addslashes($products->features);
		$techspec = addslashes($products->techspec);
		$orderinfo = addslashes($products->orderinfo);
		$information = addslashes($products->information);
		$metatitle = addslashes($products->meta_title);
		
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsCategoryID='$products->category_id',". 			
			"fldProductsName='$name',".
			"fldProductsCode='$code',".
			"fldProductsURL='$url',".
			"fldProductsPosition='$products->position',". 
			"fldProductsAvailability='$products->isAvailable',". 
			"fldProductsFeatured='$products->isFeatured',".
			"isNew='$products->isNew',". 
			"isSpecialDeal='$products->isSpecialDeal',". 
			"fldProductsPrice1='$products->price1',".
			"fldProductsPrice1Text='$products->price1text',".
			"fldProductsPrice2='$products->price2',".
			"fldProductsPrice2Text='$products->price2text',".
			"fldProductsPrice3='$products->price3',".
			"fldProductsPrice3Text='$products->price3text',".
			"fldProductsWeight='$products->weight',";
		if($image != "") { $query .= "fldProductsImage='$image', "; }
		if($pdf != "") { $query .= "fldProductsPDF='$pdf', "; }
		$query .= "fldProductsOverview='$overview',".
			"fldProductsFeatures='$features',".
			"fldProductsTechspecs='$techspec',".
			"fldProductsOrderinfo='$orderinfo',".
			"fldProductsInformation='$information',".
			"fldProductMetaTitle='$metatitle',".
            "fldProductsDescription='$content'".
            " WHERE fldProductsId=$products->Id";
		
	    $adb->query($query);
        return true;
	}

	function updateMainCategory($categoryID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsMainCategoryID='$categoryID', ".
        	"fldProductsSubcategoryID= null, ".
        	"fldProductsSubcategory2ID= null ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function updateSubCategory($subcategoryID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsSubcategoryID='$subcategoryID', ".
        	"fldProductsSubcategory2ID= null ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function updateSubCategory2($subcategory2ID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsSubcategory2ID='$subcategory2ID' ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function updateSubCategory3($subcategory3ID, $productID) {
		 global $adb;
		 global $table_prefix;
		 
		$query = "UPDATE ".$table_prefix."_tblProducts SET ".
        	"fldProductsSubcategory3ID='$subcategory3ID' ".
            "WHERE fldProductsId=$productID";
	    $adb->query($query);
        return true;
	}

	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts ORDER BY fldProductsName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllByCategory($category_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsCategoryID='$category_id' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBySubcategory($subcatID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsSubcategoryID='$subcatID' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllBySubcategory2($subcat2ID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsSubcategory2ID='$subcat2ID' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllProducts() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts ORDER BY fldProductsName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countProducts() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	function countProductsBySubcategory($subcatID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts 
				WHERE fldProductsSubcategoryID='$subcatID' 
				AND (fldProductsSubcategory2ID is NULL OR fldProductsSubcategory2ID='0') 
				ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function countProductsBySubcategory2($subcat2ID) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsSubcategory2ID='$subcat2ID' ORDER BY fldProductsPosition";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function findProducts($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function findProductByName($productname){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE LCASE(fldProductsName)='$productname'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function findProductByCode($productcode){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE LCASE(fldProductsCode)='$productcode'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function deleteProducts($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProducts WHERE fldProductsId='$id'";
        $adb->query($query);
        return true;
    }
    
	function addProductImages($productID, $filename, $order) {
		global $adb;
		global $table_prefix;
		
		$query = "INSERT INTO ".$table_prefix."_tblProductImage 
					SET fldProductId='".$productID."', 
					fldProductImage='".$filename."', 
					fldProductImagePosition='".$order."' ";
		$result = $adb->query($query);
		return true;
	}
	
	function displayAllProductImages($productID) {
		global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblProductImage 
					WHERE fldProductId='".$productID."' 
					ORDER BY fldProductImagePosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function deleteProductImage($productImageID) {
		global $adb;
		global $table_prefix;
		
		$query = "DELETE FROM ".$table_prefix."_tblProductImage 
				WHERE fldProductImageID='".$productImageID."' ";
		$result = $adb->query($query);
		return true;
	}
   
// ---------------------------------------
/*
	function displayAllByCondition($catID, $subcatID1, $subcatID2, $subcatID3, $condition) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' ";
		if ($subcatID1!='') { $query .= "AND fldProductsSubcategoryID ='$catID' "; }
		if ($subcatID2!='') { $query .= "AND fldProductsSubcategoryID ='$catID' "; }
		if ($subcatID3!='') { $query .= "AND fldProductsSubcategoryID ='$catID' "; }

		$query .= "ORDER BY fldProductsPosition ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

*/
	function displayAllByCondition($catID, $condition) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' $condition ";
		$query .= "ORDER BY fldProductsPosition ";
		// echo $query.'<br>';
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllByCondition2($catID, $condition, $pg) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' $condition ";
		$query .= "ORDER BY fldProductsPosition ";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countAllByCondition($catID, $condition) {
		global $adb;
		global $table_prefix;
		
		$query  = "SELECT * FROM ".$table_prefix."_tblProducts 
					WHERE fldProductsMainCategoryID ='$catID' $condition ";
		$query .= "ORDER BY fldProductsPosition ";
		// echo $query.'<br>';
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function findProductByURL($url){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsURL= '$url' ";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    // select the id to use for your insert statement
    function getNextProductID(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT coalesce(max(fldProductsId)+1,1) AS ID FROM cms_tblProducts";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    // select next value during insert
    function insertNewProduct(){
        global $adb;
		global $table_prefix;
		
        $query = "INSERT INTO `cms_tblProducts`(fldProductsId, fldProductsName)
        	SELECT coalesce(max(fldProductsId)+1,1) AS ID, 'New Product' FROM cms_tblProducts";
        $result=$adb->query($query);
        return mysql_insert_id();
    }

    // Display All isNew
	function displayAllIsNew($count) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isNew='1' ORDER BY fldProductsName LIMIT 0, $count";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countProductsIsNew() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isNew='1' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    // Display All isSpecialDeal
	function displayAllIsSpecialDeal($count) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isSpecialDeal='1' ORDER BY fldProductsName LIMIT 0, $count";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countProductsIsSpecialDeal() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE isSpecialDeal='1' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

    function updateProductsByPosition($position, $product_id) {
		 global $adb;
		 global $table_prefix;
		 			
		$query ="UPDATE ".$table_prefix."_tblProducts SET ".	
			"fldProductsPosition='$position' ".
            "WHERE fldProductsId='$product_id'";
	    $adb->query($query);
        return true;
	}

	// SEARCH BY KEYWORD
	function countProductByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%$keyword%' ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		return $result->db_num_rows();
	}
	
	function displayProductByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%$keyword%' ORDER BY fldProductsName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	// DELETE
	function deleteDatasheet($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsPDF = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}

	function deleteUsermanual($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsUserManual = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}

	function deleteQuickguide($productID) {
		global $adb;
		global $table_prefix;
		
		$query = "UPDATE ".$table_prefix."_tblProducts 
			SET fldProductsQuickGuide = null 
			WHERE fldProductsId='".$productID."' ";
		$result = $adb->query($query);
		return true;
	}

	function updateMetaDescription($products) {
		global $adb;
		global $table_prefix;
		 
		$metadescription = addslashes($products->meta_description);
		
		$query = "UPDATE ".$table_prefix."_tblProducts SET fldProductMetaDescription='$metadescription' WHERE fldProductsId=$products->Id";
	    $adb->query($query);
        return true;
	}

}
?>
