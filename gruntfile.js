module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        sourceMap: true,
      },
      my_target: {
        files: {
          'assets/rpm-js/production.min.js': ['assets/rpm-js/plugins.js', 'assets/rpm-js/main.js'],
          //'-hubspot/files/js/production.min.js': ['js/plugins.js', 'js/main.js']
        }
      }
    },
    watch: {
      options: {
        livereload: true,
      },
      scripts: {
        files: ['js/*.js'],
        tasks: ['uglify'],
        options: {
          spawn: false,
        },
      },
      css: {
        files: ['*.scss', 'assets/rpm-css/**/*.scss', 'assets/rpm-css/**/**/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false,
        }
      },
      html: {
        files: ['*.html'],
      }
    },
    sass: {
      dist: {
        options: {
          //outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          'assets/css/rpm-responsive-style.css': 'style.scss',
          //'-hubspot/styles/style.css': 'style.scss'
        }
      }
    },
  });

  // 3. Where we tell Grunt we plan to use this plug-in.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');

  // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('default', ['uglify', 'sass', 'watch']);

};
