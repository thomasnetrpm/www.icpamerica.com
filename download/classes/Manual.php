<?
   
class Manual{
   
    function addManual($manual,$file){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($manual->name);
		
	
        $query="INSERT INTO ".$table_prefix."_tblManual SET ".
            "fldManualTitle='$name',".
			"fldManualsFile='$file'";				
        $adb->query($query);
        return true;
    }
	
	function updateManual($manual,$file) {
		 global $adb;
		 global $table_prefix;
		 
		
		$name = addslashes($manual->name);
		
		if($file != "") { 
			$query="UPDATE ".$table_prefix."_tblManual SET ".
				 "fldManualTitle='$name',".
				"fldManualsFile='$file'".
           		 " WHERE fldManualID=$manual->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblManual SET ".
				 "fldManualTitle='$name'".
           		 " WHERE fldManualID=$manual->Id";
		}
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblManual";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblManual ORDER BY fldManualID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countManual() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblManual";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findManual(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblManual ORDER BY fldManualID DESC LIMIT 1";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteManual($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblManual WHERE fldManualID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
