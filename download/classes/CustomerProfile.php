<?
  
class CustomerProfile{
   
    
    function addCustomerProfile($customer){
        global $adb;
		global $table_prefix;
		
		$firstname = addslashes($customer->firstname);
		$lastname = addslashes($customer->lastname);
		$username = addslashes($customer->username);
		$password = addslashes($customer->password);
		$date = date('Y-m-d');
		
        $query="INSERT INTO ".$table_prefix."_tblCustomerProfile SET ".
			"fldCustomerProfileUsername='$username',". 
			"fldCustomerProfilePassword='$password',". 
			"fldCustomerProfileLastname='$lastname',". 
			"fldCustomerProfileFirstName='$firstname',". 
			"fldCustomerProfileEmail='$customer->email',".			
			"fldCustomerProfileRegDate='$date'";             
        $adb->query($query);
        return mysql_insert_id();
    }
	
	function updateCustomerProfile($customer) {
		 global $adb;
		 global $table_prefix;
		
		$firstname = addslashes($customer->firstname);
		$lastname = addslashes($customer->lastname);
		$username = addslashes($customer->username);

			$query="UPDATE ".$table_prefix."_tblCustomerProfile SET ".
			 "fldCustomerProfileUsername='$username',". 
			"fldCustomerProfileLastname='$lastname',". 
			"fldCustomerProfileFirstName='$firstname',". 
			"fldCustomerProfileEmail='$customer->email'".			
      
            " WHERE fldCustomerProfileID=$customer->Id";
	    $adb->query($query);
        return true;
	}
	
	
	
	
	function changePassword($customer) {
		 global $adb;
		 global $table_prefix;
		 
		$password = $customer->password;
			$query="UPDATE ".$table_prefix."_tblCustomerProfile SET ".          
			"fldCustomerProfileUsername='$customer->username',".
			"fldCustomerProfilePassword='$password'".              
            " WHERE fldCustomerProfileID=$customer->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile ORDER BY fldCustomerProfileFirstName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile ORDER BY fldCustomerProfileFirstName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countCustomerProfile() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function countCustomerProfileEmail($email) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileEmail='$email'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function countCustomerProfileUsername($username) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileUsername='$username'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function checkLogin($customer) {	
		global $adb;
		global $table_prefix;
		
		$username  = $customer->username;
		$password  = $customer->password;

		$query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileUsername='$username' AND fldCustomerProfilePassword='$password'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function getInfo($customer){
        global $adb;
		global $table_prefix;
		
		$username  = $customer->username;
		$password  = $customer->password;
		
       $query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileUsername='$username' AND fldCustomerProfilePassword='$password'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findCustomerProfile($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findCustomerProfileByEmail($email){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileEmail='$email'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	
	
	   
    function deleteCustomerProfile($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblCustomerProfile WHERE fldCustomerProfileID='$id'";
        $adb->query($query);
        return true;
    }
    
    
}
?>
