<?
class Category{
     
    function addCategory($category, $image){
        global $adb;
		global $table_prefix;

		$name = addslashes($category->name);
		$url = addslashes($category->url);
		$description = addslashes($category->description);
		$position = addslashes($category->position);
		$meta_title = addslashes($category->meta_title);
		$meta_keywords = addslashes($category->meta_keywords);
		$meta_description = addslashes($category->meta_description);
		$isfeatured = $category->isFeatured;
		
        $query="INSERT INTO ".$table_prefix."_tblCategory SET ".           			
			"fldCategoryImage='$image',".
			"fldCategoryURL='$url',".
			"fldCategoryPosition='$position',".
			"fldCategoryDescription='$description',".
			"fldCategoryMetaTitle='$meta_title',".
			"fldCategoryMetaKeywods='$meta_keywords',".
			"fldCategoryMetaDescription='$meta_description',".
			"fldCategoryIsFeatured='$isfeatured',".
			"fldCategoryName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateCategory($category, $image) {
		global $adb;
		global $table_prefix;

		$name = addslashes($category->name);
		$url = addslashes($category->url);
		$description = addslashes($category->description);
		$position = addslashes($category->position);
		$meta_title = addslashes($category->meta_title);
		$meta_keywords = addslashes($category->meta_keywords);
		$meta_description = addslashes($category->meta_description);
		$isfeatured = $category->isFeatured;
		
		$query  = "UPDATE ".$table_prefix."_tblCategory SET ";
		if ($image != '') { $query .= "fldCategoryImage='$image',"; }
		$query .= "fldCategoryPosition='$position',".
			"fldCategoryURL='$url',".
			"fldCategoryDescription='$description',".
			"fldCategoryMetaTitle='$meta_title',". 	
			"fldCategoryMetaKeywods='$meta_keywords',".
			"fldCategoryMetaDescription='$meta_description',".
			"fldCategoryIsFeatured='$isfeatured',".
			"fldCategoryName='$name'".
			" WHERE fldCategoryID=$category->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		// $query = "SELECT * FROM ".$table_prefix."_tblCategory";
		$query = "SELECT * FROM ".$table_prefix."_tblCategory ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	

	function findAllNotHidden($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory 
			WHERE fldCategoryIsHidden IS NULL 
			ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}


	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll2() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryIsHidden IS NULL ORDER BY fldCategoryPosition ASC";
		// $query = "SELECT * FROM ".$table_prefix."_tblCategory ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllCategoryNotHidden() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory 
			WHERE fldCategoryIsHidden IS NULL 
			ORDER BY fldCategoryPosition ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function getFeaturedCategory($count) {
		global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryIsFeatured='1' ORDER BY fldCategoryPosition ASC LIMIT $count ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	

	function countCategoryNotHidden() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryIsHidden IS NULL ";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}


    function findCategory($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function findByName($category_name){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE LCASE(fldCategoryName)='$category_name'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

  //   function findByURL($category_url){
  //       global $adb;
		// global $table_prefix;
		
  //       $query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryURL = '$category_url'";
  //       $result=$adb->query($query);
  //       return $result->fetch_object();
  //   }

    function findByURL($url){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCategory WHERE fldCategoryURL = '$url'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

    function deleteCategory($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblCategory WHERE fldCategoryID='$id'";
        $adb->query($query);
        return true;
    }
    
	function updateCategoryOrder($categoryID, $orderID) {
		global $adb;
		global $table_prefix;

		$query = "UPDATE ".$table_prefix."_tblCategory SET ".
			"fldCategoryPosition='$orderID' ".
			"WHERE fldCategoryID=$categoryID";
	    $adb->query($query);
        return true;
	}
   
    function updateCategoryByPosition($position, $category_id) {
		 global $adb;
		 global $table_prefix;
		 			
		$query = "UPDATE ".$table_prefix."_tblCategory SET ".	
			"fldCategoryPosition='$position' ".
            "WHERE fldCategoryID='$category_id'";
	    $adb->query($query);
        return true;
	}

}
?>
