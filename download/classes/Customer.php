<?
  
class Customer{
   
    
    function addCustomer($customer){
        global $adb;
		global $table_prefix;
		
		$firstname = addslashes($customer->firstname);
		$lastname = addslashes($customer->lastname);
		$username = addslashes($customer->username);
		$password = addslashes($customer->password);
		$date = date('Y-m-d');
		$status = "New";
		$optStatus = 1;
        $query="INSERT INTO ".$table_prefix."_tblCustomer SET ".
			"fldCustomerType='$customer->customer_type',". 
			"fldCustomerStatus='$status',". 
			"fldCustomerUsername='$username',". 
			"fldCustomerPassword='$password',". 
			"fldCustomerLastname='$lastname',". 
			"fldCustomerFirstName='$firstname',". 
			"fldCustomerEmail='$customer->email',".	
			"fldCustomerOptStatus='$optStatus',".	
			"fldCustomerRegDate='$date'";             
        $adb->query($query);
        return mysql_insert_id();
    }
	
	function addCustomerByDashboard($customer){
        global $adb;
		global $table_prefix;
		
		$username = addslashes($customer->username);
		$password = addslashes($customer->password);
		$date = date('Y-m-d');
		$status = "New";
		$optStatus = 1;

        $query="INSERT INTO ".$table_prefix."_tblCustomer SET ".
			"fldCustomerType='$customer->customer_type',". 
			"fldCustomerUsername='$username',". 
			"fldCustomerPassword='$password',". 
			"fldCustomerStatus='$status',". 
			"fldCustomerCustomerID='$customer->customer_id',".	
			"fldCustomerOptStatus='$optStatus',".	
			"fldCustomerRegDate='$date'";             
        $adb->query($query);
        return mysql_insert_id();
    }
	
	function updateCustomer($customer) {
		 global $adb;
		 global $table_prefix;
		
		$firstname = addslashes($customer->firstname);
		$lastname = addslashes($customer->lastname);
		$username = addslashes($customer->username);

			$query="UPDATE ".$table_prefix."_tblCustomer SET ".
			 "fldCustomerType='$customer->customer_type',". 
			 "fldCustomerUsername='$username',". 
			"fldCustomerLastname='$lastname',". 
			"fldCustomerFirstName='$firstname',". 
			"fldCustomerEmail='$customer->email'".			
      
            " WHERE fldCustomerID=$customer->Id";
	    $adb->query($query);
        return true;
	}
	
	function updateCustomerDashboard($customer) {
		 global $adb;
		 global $table_prefix;
		
		$firstname = addslashes($customer->firstname);
		$lastname = addslashes($customer->lastname);
	

			$query="UPDATE ".$table_prefix."_tblCustomer SET ".	
			"fldCustomerLastname='$lastname',". 
			"fldCustomerFirstName='$firstname',". 
			"fldCustomerEmail='$customer->email'".			      
            " WHERE fldCustomerID=$customer->Id";
	    $adb->query($query);
        return true;
	}
	
	function updateOPT($customer_id,$opt) {
		 global $adb;
		 global $table_prefix;
				
			$query="UPDATE ".$table_prefix."_tblCustomer SET ".
			 "fldCustomerOptStatus='$opt'". 
            " WHERE fldCustomerID=$customer_id";
	    $adb->query($query);
        return true;
	}
	
	
	
	
	function changePassword($customer) {
		 global $adb;
		 global $table_prefix;
		 
		$password = $customer->password;
			$query="UPDATE ".$table_prefix."_tblCustomer SET ".          
			"fldCustomerUsername='$customer->username',".
			"fldCustomerPassword='$password'".              
            " WHERE fldCustomerID=$customer->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer ORDER BY fldCustomerFirstName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer ORDER BY fldCustomerFirstName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllUser($customer_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerCustomerID='$customer_id' ORDER BY fldCustomerFirstName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBySearch($search,$customer_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE MATCH (fldCustomerFirstName,fldCustomerLastname, fldCustomerEmail) AGAINST ('".$search."' IN BOOLEAN MODE) AND fldCustomerCustomerID='$customer_id'";
		//echo $query;
		//$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerCustomerID='$customer_id' ORDER BY fldCustomerFirstName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function countCustomer() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function countCustomerEmail($email) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerEmail='$email'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function countCustomerUsername($username) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerUsername='$username'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function checkLogin($customer) {	
		global $adb;
		global $table_prefix;
		
		$username  = $customer->username;
		$password  = $customer->password;
		$optStatus = 1;
		$query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerUsername='$username' AND fldCustomerPassword='$password' AND fldCustomerOptStatus='$optStatus'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function getInfo($customer){
        global $adb;
		global $table_prefix;
		
		$username  = $customer->username;
		$password  = $customer->password;
		$optStatus = 1;
		
       $query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerUsername='$username' AND fldCustomerPassword='$password' AND fldCustomerOptStatus='$optStatus'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findCustomer($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findCustomerByEmail($email){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCustomer WHERE fldCustomerEmail='$email'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	
	
	   
    function deleteCustomer($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblCustomer WHERE fldCustomerID='$id'";
        $adb->query($query);
        return true;
    }
    
    
}
?>
