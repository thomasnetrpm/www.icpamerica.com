<?

class Gallery{
     
    function addGallery($gallery,$image){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($gallery->name);
		
        $query="INSERT INTO ".$table_prefix."_tblGallery SET ".           			
			"fldGalleryImage='$image',". 
			"fldGalleryCategoryId='$gallery->category_id',".
			"fldGalleryPosition='$gallery->position',".
			"fldGalleryName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateGallery($gallery,$image) {
		 global $adb;
		 global $table_prefix;
		 

		$name = addslashes($gallery->name);
		
		 if($image != "") {
		
			$query="UPDATE ".$table_prefix."_tblGallery SET ".
           "fldGalleryName='$name',". 		
		   "fldGalleryPosition='$gallery->position',".
		   "fldGalleryImage='$image'".
		    " WHERE fldGalleryID=$gallery->Id";
		 } else {
			 $query="UPDATE ".$table_prefix."_tblGallery SET ".
           "fldGalleryName='$name',". 					
		   "fldGalleryPosition='$gallery->position'".
		    " WHERE fldGalleryID=$gallery->Id";
		 }

	    $adb->query($query);
        return true;
	}
    
	function findAll($pg,$category_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGallery WHERE fldGalleryCategoryId='$category_id' ORDER BY fldGalleryPosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGallery ORDER BY fldGalleryPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllGallery() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGallery ORDER BY fldGalleryPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countGallery($category_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGallery WHERE fldGalleryCategoryId='$category_id'";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findGallery($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblGallery WHERE fldGalleryID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteGallery($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblGallery WHERE fldGalleryID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
