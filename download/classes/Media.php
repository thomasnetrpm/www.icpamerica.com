<?

class Media{
     
    function addMedia($media,$file){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($media->content);		
		$name = addslashes($media->name);
		
		
        $query="INSERT INTO ".$table_prefix."_tblMedia SET ".           			
				
			"fldMediaName='$name',".		
			"fldMediaFile='$file',".
			"fldMediaDate='$media->date',".
            "fldMediaDescription='$content'";
        $adb->query($query);
        return true;
    }
	
	function updateMedia($media,$file) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($media->content);		
		$name = addslashes($media->name);
		
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblMedia SET ".
         	"fldMediaName='$name',".		
			"fldMediaDate='$media->date',".
            "fldMediaDescription='$content'".
            " WHERE fldMediaId=$media->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblMedia SET ".
        		"fldMediaName='$name',".		
			"fldMediaFile='$file',".
			"fldMediaDate='$media->date',".
            "fldMediaDescription='$content'".
            " WHERE fldMediaId=$media->Id";
		}
	    $adb->query($query);
		
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblMedia ORDER BY fldMediaDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblMedia ORDER BY fldMediaDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayFirst4() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblMedia ORDER BY fldMediaDate DESC LIMIT 4";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllMedia() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblMedia ORDER BY fldMediaName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countMedia() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblMedia";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findMediaByPosition($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblMedia WHERE fldMediaCategory='$id' ORDER BY fldMediaDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findMedia($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblMedia WHERE fldMediaId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteMedia($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblMedia WHERE fldMediaId='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
