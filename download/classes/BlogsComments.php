<?
   
class BlogsComments{
   
    function addBlogs($blogs){
        global $adb;
		global $table_prefix;
		
	
		$name = addslashes($blogs->name);
		$website = addslashes($blogs->website);
		$comments = addslashes($blogs->comments);
		$date = date('Y-m-d');
        $query="INSERT INTO ".$table_prefix."_tblBlogsComments SET ".
            "fldBlogsID='$blogs->Id',".
			"fldBlogsCommentsName='$name',".
			"fldBlogsCommentsDate='$date',".
			"fldBlogsCommentsWebsite='$website',".	
			"fldBlogsCommentsEmail='$blogs->email',".					
            "fldBlogsComments='$comments'";
        $adb->query($query);
        return true;
    }
	
		
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogsComments ORDER BY fldBlogsCommentsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
		
	function countBlogsComments($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogsComments WHERE fldBlogsID='$id'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findBlogs($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblBlogsComments WHERE fldBlogsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteBlogs($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblBlogsComments WHERE fldBlogsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
