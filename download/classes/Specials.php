<?
   
class Specials{
   
    function addSpecials($specials){
        global $adb;
		global $table_prefix;
		
		$notes = addslashes($specials->notes);
		$name = addslashes($specials->name);
		$description = addslashes($specials->description);
		
	
        $query="INSERT INTO ".$table_prefix."_tblSpecials SET ".
            "fldSpecialsNotes='$notes',".
			"fldSpecialsName='$name',".
			"fldSpecialssDescription='$description'";				
        $adb->query($query);
        return true;
    }
	
	function updateSpecials($specials) {
		 global $adb;
		 global $table_prefix;
		 
		$notes = addslashes($specials->notes);
		$name = addslashes($specials->name);
		$description = addslashes($specials->description);
		
		
		
			$query="UPDATE ".$table_prefix."_tblSpecials SET ".
				   "fldSpecialsNotes='$notes',".
					"fldSpecialsName='$name',".
					"fldSpecialssDescription='$description'";	
           		 " WHERE fldSpecialsID=$specials->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSpecials";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSpecials ORDER BY fldSpecialsID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countSpecials() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblSpecials";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findSpecials($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblSpecials WHERE fldSpecialsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteSpecials($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblSpecials WHERE fldSpecialsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
