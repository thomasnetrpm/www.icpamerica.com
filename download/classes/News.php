<?
   
class News{
   
    function addNews($news, $categoryid, $pdf){
        global $adb;
		global $table_prefix;
		
		$name 	= addslashes($news->name);
		$description = addslashes($news->description);
		$url 	= $news->url;
		$date 	= $news->date_news;
		
        $query="INSERT INTO ".$table_prefix."_tblNews SET ".
            "fldNewsTitle='$name',".
			"fldNewsCategoryID='$categoryid',".
			"fldNewsDescription='$description',".
			"fldNewsURL='$url',".
			"fldNewsPDF='$pdf',".
            "fldNewsDate='$date'";
        $adb->query($query);
        return true;
    }
	
	function updateNews($news, $categoryid, $pdf) {
		 global $adb;
		 global $table_prefix;
		 
		$name 	= addslashes($news->name);
		$description = addslashes($news->description);
		$url 	= $news->url;
		$date 	= $news->date_news;

		/*
		if($image == "") { 
			$query="UPDATE ".$table_prefix."_tblNews SET ".
				"fldNewsTitle='$name', ".
				"fldNewsSEOURL='$url', ".
				"fldNewsDescription='$description', ".					
	            "fldNewsDate='$news->date_news', ".
	            "fldNewsMetaTitle='$metatitle', ".
	            "fldNewsMetaDescription='$metadesc', ".
	            "fldNewsMetaKeywords='$metakeywords' ".
           		"WHERE fldNewsID=$news->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblNews SET ".
				"fldNewsTitle='$name', ".
				"fldNewsSEOURL='$url', ".
				"fldNewsDescription='$description', ".
				"fldNewsImage='$image', ".
	            "fldNewsDate='$news->date_news', ".
	            "fldNewsMetaTitle='$metatitle', ".
	            "fldNewsMetaDescription='$metadesc', ".
	            "fldNewsMetaKeywords='$metakeywords' ".
           		"WHERE fldNewsID=$news->Id";
		}
		*/
		$query = "UPDATE ".$table_prefix."_tblNews SET ".
			"fldNewsTitle='$name', ".
			"fldNewsCategoryID='$categoryid',".
			"fldNewsDescription='$description', ".
			"fldNewsURL='$url', ".
			"fldNewsPDF='$pdf',".
	        "fldNewsDate='$date' ".
	   		"WHERE fldNewsID=$news->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg, $categoryid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsCategoryID='$categoryid' ORDER BY fldNewsDate DESC";
		// $query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($newsid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsID != '$newsid' ORDER BY fldNewsDate DESC";

		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllByCategory($categoryid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsCategoryID='$categoryid' ORDER BY fldNewsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllThumb() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews  ORDER BY fldNewsDate DESC";

		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllNews($start,$end) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT $start,$end";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllLatest() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT 3";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countNews($categoryid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsCategoryID='$categoryid' ";
		// $query = "SELECT * FROM ".$table_prefix."_tblNews";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	  function displayLatestNews(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
    function findNews($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findNewsLatest(){
        global $adb;
		global $table_prefix;
		
        echo $query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT 1";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteNews($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblNews WHERE fldNewsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
