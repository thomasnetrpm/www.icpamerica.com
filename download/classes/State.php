<?php

class State{
        
   	function findAll() {
        global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblState ORDER BY fldStateName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function updateState($state) {
		 global $adb;
		 global $table_prefix;
		 		
			$query="UPDATE ".$table_prefix."_tblState SET ".
				"fldStateTax='$state->tax'".				           
           		 " WHERE fldStateID='$state->Id'";
	    $adb->query($query);
        return true;
	}
	
	function findState($state){
        global $adb;
		global $table_prefix;

        $query = "SELECT * FROM ".$table_prefix."_tblState WHERE fldStateID='$state'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
}
?>
