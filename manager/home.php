<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";

// $pageID 	= 1; // Default page ID for Index page
$pageID 	= 19; // Default page ID for Index page

$content = Pages::findPages($pageID);
$pageName 	= $content->fldPagesName;
$pageDesc 	= $content->fldPagesDescription;

$pageMetatitle	= $content->fldPagesMetaTitle;
$pageMetadesc	= $content->fldPagesMetaDescription;
$pageMetakey	= $content->fldPagesMetaKeywords;
?>
<!doctype html>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<?php if ($pageMetaAuthor) { ?>
<meta name="author" content="<?=$pageMetaAuthor?>">
<? } ?>
<?php if ($pageMetakey) { ?>
<meta name="keyword" content="<?=$pageMetakey?>">
<? } ?>
<?php if ($pageMetadesc) { ?>
<meta name="description" content="<?=$pageMetadesc?>">
<? } ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="p:domain_verify" content="623ced875a19bc850e391b4b1fd72cb0"/>
<meta name="google-site-verification" content="N0VOxtoKRAJp--0ZBs85b4pDJzCme9_mfDioBVz4l2E" />
<link rel="stylesheet" type="text/css" media="all" href="<?=$root?>assets/css/print.css">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<link href="<?=$root?>assets/css/rpm-responsive-style.css?v=refresh" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<?=$root?>assets/js/modernizr.js"></script>
<?=emptyblock('head');?>
</head>
<body class="home-page">
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"QcIFg1awAe00w8", domain:"icpamerica.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<!-- End Alexa Certify Javascript -->
<div class="wrap-body">
<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<!--<div class="search-module">
	<form action="<?=$root?>search/default.html" method="post" id="search-panel" 
    style="height: 100%;padding: 2em 1em;background-color:rgba(0,0,0,.3);" >
		
		<input type="text" name="top_keyword" value="" style="width: 73%;padding: 20px;margin-right: 2%;" placeholder="Type your keywords...">
		<input type="submit" name="top_search" value="Search" style="width: 24%;margin-top:-8px;">
		<script>
	  (function() {
	    var cx = '016610365802457252862:tgc3nfpxgoe';
	    var gcse = document.createElement('script');
	    gcse.type = 'text/javascript';
	    gcse.async = true;
	    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(gcse, s);
	  })();
	</script>
	<gcse:search></gcse:search>
	</form>
</div> -->
<div class="wrap header clearfix">
	<form action="<?=$root?>search/default.html" method="post" id="search-panel">
		<div class="inner-wrap">
			<div class="social-wrap">
					<a href="http://www.facebook.com/icpa1" target="_blank"><img src="assets/images/icon-facebook.png" width="24" height="24" alt="FB"></a> 
					<a href="http://www.linkedin.com/company/icp-america-inc." target="_blank"><img src="assets/images/icon-linkedin.png" width="24" height="24" alt="LI"></a> 
					<a href="https://twitter.com/icpamericaInc" target="_blank"><img src="assets/images/icon-twitter.png" width="24" height="24" alt="TT"></a> 
	        <a href="https://plus.google.com/100124929247060898549" rel="publisher" target="_blank"><img src="<?=$root?>assets/images/icon-google_plus.png" width="24" alt=""></a>
	    </div>

			<a class="m-menu" href="#"><img src="assets/images/ico-nav.svg" alt=""></a>
					<!-- <div class="broken-search" >
                    <input type="text" class="search-input" name="top_keyword" value="" placeholder="Type your keywords...">
                    <input class="submit-input" type="submit" name="top_search" value="&nbsp;">
                </div> -->
       <div class="google-search" >
                <script>
                  (function() {
                    var cx = '005015634715982447058:ak1wwfvnu9i';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                  })();
                </script>
                <gcse:search></gcse:search>
            </div>
			<div class="utility-nav">
					<a class="utility-newsletter" href="http://info.icpamerica.com/sign-up-for-our-newsletter">Sign Up For Our Newsletter</a>
					<a class="utility-phone" href="tel:1-877-293-2000">1-877-293-2000</a>
					<a class="utility-item" href="<?=$root?>news/default.html">News</a>
					<a class="utility-item" href="<?=$root?>support/default.html">Support</a>
					<a class="utility-item" href="<?=$root?>partners/default.html">Partners</a>
			</div>
		</div><!-- inner-wrap end -->
	</form>
	<header class="site-header">
		<div class="inner-wrap">
			<a id="hdr-logo" href="<?=$root?>" title="go back to Homepage"></a>
			<button type="button" class="media-btn btn">
				<i class="icon-align-justify"></i>
			</button>

			<?php include "includes/pages/top-nav.php"; ?>
		</div><!-- inner-wrap end -->
	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<div class="inner-wrap">
		<div class="si-content">

			<h1 class="si-header">The Only Choice For Your Industrial Computer Supplier</h1>

			<p class="si-subtext">We provide a diverse array of computer chassis, digital signage, and medical products, in addition to maintaining a full division for embedded technology.</p>

		<!-- 	<img class="header-panel-img" src="temp/slogan/home-slogan.png" alt=""> -->

				<div id="home-page-ctas">
					<!--HubSpot Call-to-Action Code -->
					<span class="hs-cta-wrapper" id="hs-cta-wrapper-6308b47c-a49d-49b3-872a-ef6f1298ccc2">
					    <span class="hs-cta-node hs-cta-6308b47c-a49d-49b3-872a-ef6f1298ccc2" id="hs-cta-6308b47c-a49d-49b3-872a-ef6f1298ccc2">
					        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
					        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/6308b47c-a49d-49b3-872a-ef6f1298ccc2"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-6308b47c-a49d-49b3-872a-ef6f1298ccc2" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/6308b47c-a49d-49b3-872a-ef6f1298ccc2.png"  alt="Learn About Our Marine Case Study"/></a>
					    </span>
					    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					    <script type="text/javascript">
					        hbspt.cta.load(549477, '6308b47c-a49d-49b3-872a-ef6f1298ccc2', {});
					    </script>
					</span>
					<!-- end HubSpot Call-to-Action Code -->

					<!--HubSpot Call-to-Action Code -->
					<span class="hs-cta-wrapper" id="hs-cta-wrapper-ac9ab259-d914-48e5-9ebe-3c043546e168">
					    <span class="hs-cta-node hs-cta-ac9ab259-d914-48e5-9ebe-3c043546e168" id="hs-cta-ac9ab259-d914-48e5-9ebe-3c043546e168">
					        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
					        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/ac9ab259-d914-48e5-9ebe-3c043546e168"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-ac9ab259-d914-48e5-9ebe-3c043546e168" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/ac9ab259-d914-48e5-9ebe-3c043546e168.png"  alt="View Our Resource Library"/></a>
					    </span>
					    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
					    <script type="text/javascript">
					        hbspt.cta.load(549477, 'ac9ab259-d914-48e5-9ebe-3c043546e168', {});
					    </script>
					</span>
					<!-- end HubSpot Call-to-Action Code -->


				</div>


					<!--[if lte IE 8]>
				<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
				<![endif]--> 
				<!-- <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
				<script>
				  hbspt.forms.create({ 
				    portalId: '549477',
				    formId: '815b9ebc-bb83-448c-95c4-ea4f9a743f30'
				  });
				</script> -->
		</div>
	</div>
	<?=emptyblock('content');?>
</div>
<!--/ CONTENT -->
<?php include "includes/footer.php"; ?>
<? /*
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
*/ ?>
<!--/ FOOTER -->
<div class="bg-panel"></div>
</div>
<!--/ BACKGROUND PANEL  -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<script src="<?=$root?>assets/js/placeholder.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];  
    _gaq.push(['_setAccount', 'UA-48231208-1']);  
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

<!-- Mobile Nav -->
<script>
//Responsive Navigation
$(document).ready(function() {
  $searchLink = $('.search-link'),
  $siteSearch = $('.search-module');

  $searchLink.click(function(e) {
    e.preventDefault();
    $searchLink.toggleClass('active');
    $siteSearch.toggleClass('active');
  });
});

$(".m-menu").click(function(){
    $('.menunav ul').toggleClass("m-show");
  });
</script>



<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/549477.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol + "//www.webtraxs.com/trxscript.php' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
_trxid = "icpamerica";
webTraxs();
</script>
<noscript><img src="http://www.webtraxs.com/webtraxs.php?id=icpamerica&st=img" alt=""></noscript>
<!-- Crazy Egg Code Start -->
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0025/7608.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<!-- Crazy Egg Code End -->
</body>
</html>