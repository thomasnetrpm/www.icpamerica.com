<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";

$pageID 	= 1; // Default page ID for Index page

$content = Pages::findPages($pageID);
$pageName 	= $content->fldPagesName;
$pageDesc 	= $content->fldPagesDescription;

$pageMetatitle	= $content->fldPagesMetaTitle;
$pageMetadesc	= $content->fldPagesMetaDescription;
$pageMetakey	= $content->fldPagesMetaKeywords;
?>
<!DOCTYPE HTML>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<meta name="description" content="<?=$pageMetadesc?>">
<meta name="keyword" content="<?=$pageMetakey?>">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?=emptyblock('head');?>
<script src="<?=$root?>assets/js/modernizr.js"></script>
</head>
<body>
<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix">
	<form action="" method="post" id="search-panel">
		<ul class="unstyled clearfix">
			<li>Email: info@icpamerica.com &nbsp;&nbsp; Call: 1-800-293-2000</li>
			<li><a href="#signin">Sign Up</a> <a href="#login">Login</a></li>
			<li><input type="text" name="q" placeholder="Type your keywords..."><input type="submit" name="search" value="&nbsp;"></li>
			<li><a href="#"><img src="assets/images/icon-facebook.png" width="24" height="24" alt="FB"></a> <a href="#"><img src="assets/images/icon-linkedin.png" width="24" height="24" alt="LI"></a> <a href="#"><img src="assets/images/icon-twitter.png" width="24" height="24" alt="TT"></a> <a href="#"><img src="assets/images/icon-google_plus.png" width="24" height="24" alt="G+"></a></li>
		</ul>
	</form>
	<header>
		<a id="hdr-logo" href="#" title="go back to Homepage"></a>
		<button type="button" class="media-btn btn">
			<i class="icon-align-justify"></i>
		</button>

		<?php include "includes/pages/top-nav.php"; ?>

	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<?=emptyblock('content');?> <!-- Code in content block found in index.php -->
</div>
<!--/ CONTENT -->
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
<!--/ FOOTER -->
<div class="bg-panel"></div>
<!--/ BACKGROUND PANEL  -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<?=emptyblock('script');?>
</body>
</html>