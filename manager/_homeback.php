<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";

$pageID 	= 1; // Default page ID for Index page

$content = Pages::findPages($pageID);
$pageName 	= $content->fldPagesName;
$pageDesc 	= $content->fldPagesDescription;

$pageMetatitle	= $content->fldPagesMetaTitle;
$pageMetadesc	= $content->fldPagesMetaDescription;
$pageMetakey	= $content->fldPagesMetaKeywords;
?>
<!doctype html>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<meta name="description" content="<?=$pageMetadesc?>">
<meta name="keyword" content="<?=$pageMetakey?>">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?=emptyblock('head');?>
<script src="<?=$root?>assets/js/modernizr.js"></script>
</head>
<body>
<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix">
	<form action="" method="post" id="search-panel">
		<ul class="unstyled clearfix">
            <? // if(!isset($_SESSION['client_id'])) { ?>
            <? if(!isset($_SESSION['logged_in'])) { ?>
                <li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-800-293-2000</li>
                <!-- <li><a href="<?=$root?>registration.html">Sign Up</a> <a href="<?=$root?>login.html">Login</a></li>-->
			<? } else { ?>
            	<li><a href='<?=$root?>logout.php'>Logout</a></li>
				<li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-800-293-2000</li>
                <li>

		    <div class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account</a>
			    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			    	<div><a href="<?=$root?>account-information.html">My Account</a></div>
			    	<div><a href="<?=$root?>account-history.html">Purchase History</a></div>
			    </div>
		    </div>

                </li>
            <? } ?>            
			<li><input type="text" name="q" placeholder="Type your keywords..."><input type="submit" name="search" value="&nbsp;"></li>
			<li>
				<a href="http://www.facebook.com/icpa1" target="_blank"><img src="assets/images/icon-facebook.png" width="24" height="24" alt="FB"></a> 
				<a href="#" target="_blank"><img src="assets/images/icon-linkedin.png" width="24" height="24" alt="LI"></a> 
				<a href="http://twitter.com/icpamerica" target="_blank"><img src="assets/images/icon-twitter.png" width="24" height="24" alt="TT"></a> 
				<a href="#" target="_blank"><img src="assets/images/icon-google_plus.png" width="24" height="24" alt="G+"></a>
			</li>
		</ul>
	</form>
	<header>
		<a id="hdr-logo" href="<?=$root?>" title="go back to Homepage"></a>
		<button type="button" class="media-btn btn">
			<i class="icon-align-justify"></i>
		</button>

		<?php include "includes/pages/top-nav.php"; ?>

	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<?=emptyblock('content');?>
</div>
<!--/ CONTENT -->
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
<!--/ FOOTER -->
<div class="bg-panel"></div>
<!--/ BACKGROUND PANEL  -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<script src="<?=$root?>assets/js/placeholder.js"></script>
<?=emptyblock('script');?>
<?php
// echo 'client_id: '.$_SESSION['client_id'].'<br>';
// echo 'logged_in: '.$_SESSION['logged_in'].'<br>';
?>
</body>
</html>