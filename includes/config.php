<?
ob_start();
session_start();
?>
<?php 
include   "classes/Database.php";
include   "classes/Connection.php";
include_once "includes/bootstrap.php";    
include   "classes/Pages.php";
include   "classes/Products.php";
include   "classes/Category.php";
include   "classes/Client.php";
include   "classes/Subcategory.php";
include   "classes/Projects.php";
include   "classes/ProjectsImages.php";
include   "classes/News.php";
include   "classes/Contact.php";
include   "classes/State.php";
include   "classes/Country.php";
include   "classes/HomeSlide.php";
include   "classes/NewsImages.php";
include   "classes/TempCart.php";
include_once "includes/Pagination.php";   
include  "includes/security.funcs.inc";

include   "classes/Billing.php";
include   "classes/Shipping.php";
include   "classes/Cart.php";
include 'classes/ProductVariant.php';
?>