<section class="resources-module">
  <div class="inner-wrap">
    <h2>Additional Resources</h2>
    <div class="rows-of-3">


    <!--HubSpot Call-to-Action Code -->
<span class="hs-cta-wrapper" id="hs-cta-wrapper-f7a1ccbb-54d5-4f6d-a36c-bec209180d98">
    <span class="hs-cta-node hs-cta-f7a1ccbb-54d5-4f6d-a36c-bec209180d98" id="hs-cta-f7a1ccbb-54d5-4f6d-a36c-bec209180d98">
        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/f7a1ccbb-54d5-4f6d-a36c-bec209180d98"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-f7a1ccbb-54d5-4f6d-a36c-bec209180d98" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/f7a1ccbb-54d5-4f6d-a36c-bec209180d98.png"  alt="Custom Design Project Study Marine Computer Download Case Study"/></a>
    </span>
    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
    <script type="text/javascript">
        hbspt.cta.load(549477, 'f7a1ccbb-54d5-4f6d-a36c-bec209180d98', {});
    </script>
</span>
<!-- end HubSpot Call-to-Action Code -->



<!--HubSpot Call-to-Action Code -->
<span class="hs-cta-wrapper" id="hs-cta-wrapper-b4835637-6653-4f09-bd01-d97dfa6783ae">
    <span class="hs-cta-node hs-cta-b4835637-6653-4f09-bd01-d97dfa6783ae" id="hs-cta-b4835637-6653-4f09-bd01-d97dfa6783ae">
        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/b4835637-6653-4f09-bd01-d97dfa6783ae"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-b4835637-6653-4f09-bd01-d97dfa6783ae" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/b4835637-6653-4f09-bd01-d97dfa6783ae.png"  alt="How to Choose the Best Rugged Display Download eBook"/></a>
    </span>
    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
    <script type="text/javascript">
        hbspt.cta.load(549477, 'b4835637-6653-4f09-bd01-d97dfa6783ae', {});
    </script>
</span>
<!-- end HubSpot Call-to-Action Code -->


<!--HubSpot Call-to-Action Code -->
<span class="hs-cta-wrapper" id="hs-cta-wrapper-4a8cdbb1-d351-4c04-9348-082739e0c12d">
    <span class="hs-cta-node hs-cta-4a8cdbb1-d351-4c04-9348-082739e0c12d" id="hs-cta-4a8cdbb1-d351-4c04-9348-082739e0c12d">
        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
        <a href="http://cta-redirect.hubspot.com/cta/redirect/549477/4a8cdbb1-d351-4c04-9348-082739e0c12d"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-4a8cdbb1-d351-4c04-9348-082739e0c12d" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/549477/4a8cdbb1-d351-4c04-9348-082739e0c12d.png"  alt="How to Choose the Best Single Board Computer Download eBook"/></a>
    </span>
    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
    <script type="text/javascript">
        hbspt.cta.load(549477, '4a8cdbb1-d351-4c04-9348-082739e0c12d', {});
    </script>
</span>
<!-- end HubSpot Call-to-Action Code -->



    </div>
  
  </div>
</section>


<?php if ($pageID == 19): ?>

    <article>

        <section class="deals-section rows-of-3">
            <div class="ds-bucket">
                <h3><a href="http://www.icpamerica.com/products/new/default.html">New Products</a></h3>
                <a href="http://www.icpamerica.com/products/new/default.html">see more</a>
            </div>
            <div class="ds-bucket">
                <h3><a href="http://www.icpamerica.com/products/special-deal/default.html">Special Deals</a></h3>
                <a href="http://www.icpamerica.com/products/power_supply/atx/ace_a130b.html">ACE-A130B</a>
                <a href="http://www.icpamerica.com/products/power_supply/other/ace_a406a_rs.html">ACE-A406A-RS</a>
                <a href="http://www.icpamerica.com/products/special-deal/default.html">see more</a>
            </div>
            <div class="ds-bucket">
                <h3><a href="http://info.icpamerica.com/contact-us">Contact Us</a></h3>
                <p>ICP America’s Sales Department is available to assist you in selecting the right product for your project. To speak with a member of our Sales Team please call Monday-Friday 7:00AM – 4:30PM.</p>
                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td>Call Us today</td>
                            <td>: 1-877-293-2000</td>
                        </tr>
                        <tr>
                            <td>From Outside US</td>
                            <td>: 760-598-2176</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>: info@icpamerica.com</td>
                        </tr>
                        <tr>
                            <td>Sales</td>
                            <td>: sales@icpamerica.com</td>
                        </tr>
                        <tr>
                            <td>Technical Support</td>
                            <td>: support@icpamerica.com</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

        
        <section class="category-links rows-of-4">
            <div class="c-bucket">
                <a href="<?=$root?>">ICP America Industrial Computer</a>
                <a href="<?=$root?>about_us/default.html">About ICP America</a>
                <a href="http://info.icpamerica.com/contact-us">Contact Us</a>
                <a href="<?=$root?>news/default.html">Industrial Computer News<br />from ICP America</a>
                <a href="<?=$root?>support/default.html">Support</a>
            </div>
            <div class="c-bucket">
                <a href="<?=$root?>partners/default.html">Partners</a></li>
                <a href="<?=$root?>services/default.html">Industrial Computer Services<br />from ICP America</a></li>
                <a href="<?=$root?>custom_quote/default.html">Request a Custom Quote</a></li>
                <a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></li>
            </div>
            <div class="c-bucket">
                 <a href="<?=$root?>faqs/default.html">FAQs</a></li>
                    <a href="<?=$root?>products/default.html">Industrial Computer Products</a>
                    <a href="<?=$root?>products/single_board_computers/default.html">Single Board Computers</a></li>
                    <a href="<?=$root?>products/LCD_products/default.html">LCD Products & Panel PC</a></li>
                    <a href="<?=$root?>products/chassis/default.html">Computer Chassis</a></li>
                    <a href="<?=$root?>products/power_supply/default.html">Power Supplies</a></li>
            </div>
            <div class="c-bucket">
                    <a href="<?=$root?>products/medical_products/default.html">Medical Products</a></li>
                    <a href="<?=$root?>products/accessories/default.html">Computer Accessories</a></li>
                    <a href="http://www.mcsi1.com/" target="_blank">Embedded Division</a></li>
                    <a href="http://www.icpads.com/" target="_blank">Digital Signage Division</a></li>
                    <a href="http://www.icpmobile.com/" target="_blank">ICP Mobile</a></li>
            </div>
        </section>

    </article>
    

   <?php else: ?> 
<?php endif ?>



<!-- 
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
-->
<?php
// Get Footer
$footerID = 20;
$ftr = Pages::findPages($footerID);
$footerContent = stripslashes($ftr->fldPagesDescription)
?>

<div class="wrap footer">
	<footer class="clearfix">
		<?=$footerContent?>
	</footer>
</div>