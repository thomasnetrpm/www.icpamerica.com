<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
?>
<?php
$filename = basename($_SERVER['PHP_SELF']);
if (isset($_REQUEST['top_keyword'])) {
	$search = trim($_REQUEST['top_keyword']);
	$search = str_replace(' ', '-', (strtolower($search)));

	$_SESSION['search'] = $search;
	$search_url = $root.'search/'.$search.'';
	header("Location: ".$search_url."");
	die();
}

if (isset($_REQUEST['kw'])) {
	$kw = trim($_REQUEST['kw']);
	$_SESSION['search'] = $kw;
}

$search = ($_SESSION['search'])? $_SESSION['search']: '';

$keyword = $search;
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>

    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li class="active">Search <span class="divider">:</span></li>
	    <li class="active"><b><?=$keyword?></b></li>
    </ul>

		<ul class="unstyled clearfix">
		<?php
		$countProducts = Pages::countPageByKeyword($keyword);

		if ($countProducts == 0) {
			echo 'No Product/s found.';
		} else {
			$page = Pages::displayPageByKeyword($keyword);
			foreach ($page as $pg) {

				$pg_id 		= $pg->_id;
				$pg_name 	= $pg->_name;
				$pg_url 	= $pg->_url;

				if ($pg_url=='') { $pg_url = str_replace(" ", "_", strtolower($pg_name))."/default.html"; }
				else { $pg_url = 'products/'.$pg_url; }
				$pg_overview 		= $pg->_overview;
				$pg_overview = (strlen($pg_overview)>200)? substr($pg->_overview, 0, 200).' ...': $pg_overview;
				?>
				<li style="padding:10px 0;">
					<b><a href="<?=$root.$pg_url?>"><?=$pg_name?></a></b>
					<br>
					<a href="<?=$root.$pg_url?>"><font color="green"><?=$root.$pg_url?></font></a>
					<?=strip_tags($pg_overview, '<p><a><img><br>')?>
				</li>
				<?php
			}
		}
		?>

		</ul>


	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>
<? endblock(); ?>