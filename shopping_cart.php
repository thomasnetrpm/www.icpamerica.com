<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
include 'classes/Coupon.php';
?>
<?
$date = date('Y-m-d');

if ($_SESSION['http_referer']=='') {
	$_SESSION['http_referer'] = $_SERVER['HTTP_REFERER'];
}

if(isset($_SESSION['client_id'])) {
	$client_id = $_SESSION['client_id'];
} else {
	$client_id = session_id();
	$_SESSION['client_id'] = $client_id;
}

if (isset($_REQUEST['delete'])) {
	$delete = $_REQUEST['delete'];
	Tempcart::deleteTempcart($delete);

	$links = $root . 'shopping-cart.html';
	header("Location: $links");
	exit();
}


if (isset($_POST['continue'])) {
	$goTo = $_POST['from_page'];

	header("Location: $goTo");
	exit();
}

if (isset($_POST['update'])) {

	if(isset($_POST['qty']))
	{
		foreach($_POST['qty'] as $qty)
		{
			if($qty == 0){$qty=1;}
			list ($key,$cartid) = each ($_POST['cartId']);
			Tempcart::updateTempcart($qty,$cartid);
		}
	}
}

if(isset($_POST['checkout'])) {
	// echo 'session_id: '.session_id();
	// echo 'client_id: '.$_SESSION['client_id'];
	if ($_POST['subtotal']>=0 && $_POST['subtotal']<100) {
		$error = "Subtotal did not meet minimum amount of $100";
	} else {
		if(isset($_SESSION['client_id'])) {
			if ($_SESSION['client_id']==session_id()) {
				$links = $root . 'login.html';
			} else {
				$links = $root . 'billing-info.html';
			}
		} //else {
			//$links = $root . 'registration.html';
		//}

		header("Location: $links");
		exit();
	}
}

if (isset($_REQUEST['id'])) { //Add to cart from product category page
	$product_id = $_REQUEST['id'];
	$qty 		= $_REQUEST['product_qty'];
	$name 		= $_REQUEST['product_name'];
	$price 		= $_REQUEST['product_price'];
	$price_text	= $_REQUEST['product_price_text'];
	
	//get the products information
	$products = Products::findProducts($product_id);

	$condition = "fldTempCartClientID='$client_id' AND fldTempCartProductID='$product_id'";

	 if(Tempcart::countTempcartbyCondition($condition)==1) {
	// 	//update the quantity of the products
	 	$temp_cart = Tempcart::displayTempcart($condition);
	 	$qty = $qty + $temp_cart->fldTempCartQuantity;
	 	Tempcart::updateTempcart($qty,$temp_cart->fldTempCartID);
	 } else {			
		//save the products to temporary cart
		$_POST = sanitize($_POST);
		$tempcart = $_POST;
		settype($tempcart,'object');
		$tempcart->product_id 	= $product_id;
		$tempcart->quantity 	= $qty;
		$tempcart->product_name = $name;
		$tempcart->price 		= $price;
		$tempcart->price_text	= $price_text;
		$tempcart->client_id 	= $client_id;

		//$tempcart->options = $options;
		Tempcart::addTempcart($tempcart); 
	  }	
			
}

if (isset($_POST['product_id'])) { //Add to cart from product details page
	
	$product_id	= $_POST['product_id'];
	$name 		= $_POST['product_name'];
	$price 		= $_POST['product_price'];
	$price_text = $_POST['product_price_text'];
	$quantity 	= $_POST['product_qty'];

	// echo 'prod details page quantity'.$quantity;
	// die();
	if ($quantity >= 1) {
		$qty = $quantity;
	} else {
		$qty = 1;
	}
	
	//get the products information
	$products = Products::findProducts($product_id);
	
	///code for options
	//$option =  var_dump($_POST['options']);
	if(isset($_POST['options']))
	{
		foreach($_POST['options'] as $option1)
		{
			$array = array_values($option1);
			$options .= $array[0] . ', ';
		}	
	}
	

	$options = substr($options,0,strlen($options)-2);
 	//end code for options	
		 
	// $condition = "fldTempCartClientID='$client_id' AND fldTempCartProductID='$product_id' ";
	$condition = "fldTempCartClientID='$client_id' AND fldTempCartProductID='$product_id' AND fldTempCartDate='$date' ";
	// echo $condition;
	if (Tempcart::countTempcartbyCondition($condition)==1) {
	// 	//update the quantiy of the products
	 	$temp_cart = Tempcart::displayTempcart($condition);
	 	$qty = $qty + $temp_cart->fldTempCartQuantity;
	 	Tempcart::updateTempcart($qty,$temp_cart->fldTempCartID);
	 } else {			
		//save the products to tempporary cart
		$_POST = sanitize($_POST);
		$tempcart = $_POST;
		settype($tempcart,'object');
		$tempcart->product_id  	= $product_id;
		$tempcart->quantity 	= $qty;
		$tempcart->product_name = $name;
		$tempcart->price 		= $price;
		$tempcart->price_text	= $price_text;
		$tempcart->client_id 	= $client_id;
		$tempcart->options 		= $options;

		//$tempcart->options = $options;
		Tempcart::addTempcart($tempcart); 
	  }	
}


?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li>Shopping Cart</li>
    </ul>

    <hgroup>
      <h2>Shopping Cart</h2>
    </hgroup>


  		<? //require_once('includes/shopping_cart_content.php');?>
       


		<form method="post" action="shopping-cart.html">
	    <table class="table table-bordered cart-order-list">
	    	<thead>
	    		<tr class="cart-hdr">
						<th class="hdr-panel5"> <i class="icon-trash icon-white"></i> </th>
		    		<th class="hdr-panel1">Item Name</th>
		    		<th class="hdr-panel2">Price</th>
		    		<th class="hdr-panel3">QTY</th>
		    		<th class="hdr-panel4">Item Total</th>
	    		</tr>
	    	</thead>
	    	<tbody>
                <?
					$date = date('Y-m-d');
					$condition = "fldTempCartClientID='$client_id' AND fldTempCartDate='$date'";	

					$cart = Tempcart::findTempcartByCondition($condition);
					$total = 0;
					if(empty($cart)) {
			  	?>
	    		<tr>
	    			<td class="tp5" colspan=5>Shopping Cart is Empty</td>
	    		</tr>
              	<?			
					} else {
					foreach($cart as $carts) { 
						$products = Products::findProducts($carts->fldTempCartProductID);

						$subtotal = $carts->fldTempCartProductPrice * $carts->fldTempCartQuantity;
						$total = $total + $subtotal;

						$productURL = $products->fldProductsURL;
						$corporate_price_text = $products->fldProductsPrice2Text;

						$productPriceText = $carts->fldTempCartProductPriceText;

						// Product Option + Variant
						$variant_id = $carts->fldTempCartProductVariant;
						if (!empty($variant_id)) {
							$variant = ProductVariant::findVariant($variant_id);
							$variant_name = $variant->product_variant;
							$option_id = $variant->product_option_id;
							$option = ProductVariant::findOption($option_id);
							// $option_name = $option->product_option;
							$option_name = ($option->product_option=="Product Options")? "": $option->product_option.' | ';
						}

				?>
	    		<tr>
	    			<td class="tp5"> 
	    				<a href="<?=$root?>shopping-cart-delete-<?=$carts->fldTempCartID?>.html">
	    					<i class="icon-remove icon-red" onClick="return confirm(&quot;Are you sure you want to completely remove this product from the Shopping Cart?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting this product.\n&quot;)"></i>
	    				</a> 
	    			</td>
	    			<td class="tp1">
	    				<?php // Show no image to accessories
	    				if (strtolower($option->product_option)!='accessories') {
	    					?>
		    				<a href="<?=$root?>products/<?=$productURL?>">
		    				<img src="<?=$root?>uploads/product_image/<?=$carts->fldTempCartProductID?>/_75_<?=$products->fldProductsImage?>" alt="" border=0 align="left"  width="75">
	    					<?
	    				} else {
	    					?>
		    				<img src="<?=$root?>uploads/product_image/thumbblank.jpg" alt="" border=0 align="left"  width="75">
		    				<a href="<?=$root?>products/<?=$productURL?>">
	    					<?
	    				}
	    				?>
	    				&nbsp;&nbsp;&nbsp;
	    				<?=stripslashes($carts->fldTempCartProductName)?></a>
	    				<?php if ($productPriceText) { echo "<br>&nbsp;&nbsp;&nbsp;<b>** ".$productPriceText." **</b>"; }?>
	    				<?php if ($variant_id) { 
	    					// echo "<br>&nbsp;&nbsp;&nbsp; ".$option_name." | ".$variant_name." "; 
	    					echo "<br>&nbsp;&nbsp;&nbsp; ".$option_name.$variant_name." "; 
					        if ($_SESSION['client_type']==2) { // Corporate
								echo "&nbsp;&nbsp;&nbsp;<b>** ".$corporate_price_text." **</b>";
					        }
	    				}?>
	    				
	    			</td>
<!-- 	    			<td class="tp2"><?//=number_format($carts->fldTempCartProductPrice,2)?></td> -->
	    			<td class="tp2"><?=$carts->fldTempCartProductPrice?></td>
	    			<td class="tp3">
	    				<input type=text name="qty[]" value="<?=$carts->fldTempCartQuantity?>" maxlength=4 size=3 style="text-align:center" onchange="checkForDecimal(this.value)" style="width:10px !Important;">
	    				<input type="hidden" name="cartId[]" value="<?=$carts->fldTempCartID?>" />
	    			</td>
	    			<td class="tp4"><?=number_format($subtotal,2)?></td>
	    		</tr>
	                <? } ?>
                <? } ?>
			</tbody>
	    	<tfoot>
	    		<tr class="cart-ftr">
	    			<td colspan="4" align="right">Sub Total</td>
	    			<td class="total-cart-price">$ <?=number_format($total,2)?></td>
	    		</tr>
	    		<?php
	    		/*
				if (isset($_POST['coupon_code'])) {
					if ($coupon_price > 0) {
						$coupon_amount = $coupon_price;
					} elseif ($coupon_percent > 0) {
						$coupon_amount = $total * ($coupon_percent / 100);
					} elseif ($coupon_freeship > 0) {
						// Get shipping amount
						echo 'get shipping amount<br>';
					}
					// $saveCoupon = TempCart::
				}
				$grandTotal = $total - $coupon_amount;
	    		*/
	    		?>
 	    		<?php /* <tr>
	    			<td colspan="2" align="right">Discount Code <br> <small>*You can only use one discount code at a time</small></td>
	    			<td colspan="2"> <input type="text" name="coupon_code" value="<?=$coupon_code?>"> </td>
	    			<td class="total-cart-price">$ <?=number_format($coupon_amount,2)?></td>
	    		</tr>
	    		<tr>
	    			<td colspan="4" align="right"><strong>Grand Total</strong></td>
	    			<td class="total-cart-price payment">$ <?=number_format($grandTotal,2)?></td>
	    		</tr> */ ?>
	    		<tr>
	    			<td colspan="5" class="cart-functions">
					<?php
					if ($_SESSION['from_page']) {
						$from_page = $_SESSION['from_page'];
					} else {
						if ($_POST['from_page']) {
							$from_page = $_POST['from_page'];
						} else {
							$from_page = $_SERVER['HTTP_REFERER'];
							$_SESSION['from_page'] = $_SERVER['HTTP_REFERER'];
						}
					}
					// echo 'HTTP_REFERER: '.$_SERVER['HTTP_REFERER'].'<br>';
					// echo '_POST from_page: '.$_POST['from_page'].'<br>';
					// echo '_SESSION from_page: '.$_SESSION['from_page'].'<br>';
					//echo 'from_page: '.$from_page.'<br>';
					?>
	    				<div class="btn-group">
							<input type="hidden" name="from_page" value="<?=$from_page?>">
		    				<button name="continue" type="submit" id="continue" class="btn btn-small">Continue Shopping</button>
		    				<button name="update" type="submit" id="update" class="btn btn-small">Update Shopping Cart</button>
		    				<input type="hidden" name="subtotal" value="<?=$total?>">
		    				<button name="checkout" type="submit" id="checkout" class="btn btn-small btn-success">Checkout Item(s)</button>
	    				</div>
	    			</td>
	    		</tr>
	    	</tfoot>
	    </table>
        </form>
		<? if(isset($error)): ?>
			<div class="alert alert-error">
	    		<button type="button" class="close" data-dismiss="alert">&times;</button>
	    		<?=$error?>
	    	</div>
		<? endif; ?>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>


<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('headercodes') ?>
<? endblock() ?>

<? startblock('extracodes') ?>
<? endblock() ?>
