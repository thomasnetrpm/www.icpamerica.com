<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
?>
<?php
if (isset($_REQUEST['top_search']) && $_REQUEST['top_keyword']!='') {
	$keyword = trim($_REQUEST['top_keyword']);
} else {
	header("Location: ".$root."products/default.html");
	exit();
}
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li><a href="<?=$root?>products/default.html">Products</a> <span class="divider">/</span></li>
	    <li class="active">Search</li>
    </ul>

		<h2>Product/s with <i><?=$keyword?></i></h2>

		<ul class="unstyled clearfix product-listing">
		<?php
		$countProducts = Products::countProductByKeyword($keyword);

		if ($countProducts == 0) {
			echo 'No Product/s found.';
		} else {
			$product = Products::displayProductByKeyword($keyword);
			foreach ($product as $prodR) {
				$productID 			= $prodR->fldProductsId;
				$productName 		= $prodR->fldProductsName;
				$productImage 		= $prodR->fldProductsImage;
				$productCode 		= $prodR->fldProductsCode;
				$productURL 		= $prodR->fldProductsURL;
				$productPDF 		= $prodR->fldProductsPDF;
				$productOverview 	= $prodR->fldProductsOverview;
				$productInfo 		= $prodR->fldProductsInformation;
				$productDesc 		= $prodR->fldProductsDescription;

				// Tiered Pricing
				if ($_SESSION['client_type'] == 1) { $productPrice = $prodR->fldProductsPrice1; $productPriceText = $prodR->fldProductsPrice1Text; }
				elseif ($_SESSION['client_type'] == 2) { $productPrice = $prodR->fldProductsPrice2; $productPriceText = $prodR->fldProductsPrice2Text; }
				elseif ($_SESSION['client_type'] == 3) { $productPrice = $prodR->fldProductsPrice3; $productPriceText = $prodR->fldProductsPrice3Text; }
				?>
				<li>
	    			<form action="<?=$root?>shopping-cart.html" method='POST'>
						<table width="100%">
							<tbody>
								
								<tr>
								<td class="product-img">
									<a href="<?=$root?>products/<?=$productURL?>">
									<img src="<?=$root?>uploads/product_image/<?=$productID?>/_190_<?=$productImage?>" alt="">
									</a>
								</td>
								<td class="product-desc">
									<h4><a href="<?=$root?>products/<?=$productURL?>"><?=$productCode?></a></h4>
									<?=$productOverview?>
								</td>
								<td class="product-action">
									<? if($_SESSION['logged_in']!=1) { ?>
										<h4>sign up</h4>
										<a href="<?=$root?>login.html" class="login">login</a>
										<a href="<?=$root?>products/<?=$productURL?>" class="view">view</a>
									<? } else { ?>
				    					<?php
				    					// Check for Text before Price
				    					if ($productPriceText!='') { // Show Price Text
				    						?>
											<h4><?=$productPriceText?></h4>
				    						<?php
				    						$productPrice = "0.00";
				    					} else { // Show Price
				    						?>
											<!-- <h4>$ <?//=$productPrice?></h4> -->
				    						<?php
				    					}
				    					?>
					    				<input type='hidden' name='product_id' value='<?=$productID?>'>
					    				<input type='hidden' name='product_name' value='<?=$productName?>'>
					    				<input type='hidden' name='product_price' value='<?=$productPrice?>'>
					    				<input type='hidden' name='product_price_text' value='<?=$productPriceText?>'>
										<?php /* <button type="submit" class="add2cart" name="add2cart">add to cart</button> */ ?>
										<a href="<?=$root?>products/<?=$productURL?>" class="view">view</a>
									<? } ?>
								</td>
								</tr>
						
							</tbody>
						</table>
					</form>
				</li>
				<?php
			}
		}
		?>

		</ul>


	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>
<? endblock(); ?>