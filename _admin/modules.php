<? include '_manager/box.header/base.php' ?>

	<? startblock('section') ?> 
      <div id="container">
      	<h2>Modules Management</h2>
        <span>Welcome to your Modules Manager</span>

        <div class="hr-clear" style="margin-bottom:20px;"><!-- Clear Section --></div>        

        <div id="modules">
          <div id="module_box" class="col1">
            <?php
            // echo 'adminLevel for '.$_SESSION['admin_id'].' :'.$adminLevel.'<br>';
            if ($adminLevel!=1) {
              $adm = Administrator::findAdministrator($_SESSION['admin_id']);

              $customerAccess   = $adm->mod_cust;
              $discountAccess   = $adm->mod_disc;
              $productAccess    = $adm->mod_prod;
              $taxAccess        = $adm->mod_tax;
              $prodCatAccess    = $adm->mod_prodcat;
              $transactionAccess= $adm->mod_trans;
              $contactUsAccess  = $adm->mod_cont;
              $newsAccess  = $adm->mod_news;
            }
            ?>

            <ul>

              <?php if (($adminLevel!=1 && $customerAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_client/dashboard.php" rel="shadowbox;width=1110;height=700" title="Customer Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Customer <p><span>Manage your Customer</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $transactionAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_order/dashboard.php" rel="shadowbox;width=1110;height=700" title="Orders Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Orders <p><span>Manage your Orders</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $contactUsAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_contact/dashboard.php" rel="shadowbox;width=1110;height=700" title="Contact Us Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Contact Us <p><span>Manage your Contact Us Information</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $prodCatAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog clear"><a href="<?=$ROOT_URL?>_admin/_modules/mods_category/dashboard.php" rel="shadowbox;width=1110;height=700" title="Product Category Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Product Category <p><span>Manage your Product Category</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $productAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/category_dashboard.php" rel="shadowbox;width=1110;height=700" title="Product Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Products <p><span>Manage your Products</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $discountAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_coupon/dashboard.php" rel="shadowbox;width=1110;height=700" title="Coupon Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Discount Coupon <p><span>Manage your Coupons</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $taxAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_tax/dashboard.php" rel="shadowbox;width=1110;height=700" title="Tax Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> Tax <p><span>Manage your Tax</span></p></a></li>
              <?php } ?>

              <?php if (($adminLevel!=1 && $newsAccess==1) || ($adminLevel==1)) { ?>
              <li class="blog"><a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard_category.php" rel="shadowbox;width=1110;height=700" title="News Module"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/advanced.png" width="32" height="32" alt=""> News <p><span>Manage your News</span></p></a></li>
              <?php } ?>

            </ul>
           
          </div>
        </div>
        <div class="hr-clear"><!-- Clear Section --></div>
        

      </div>
  <? endblock() //End of Section ?>



<? startblock('headercodes') ?> 
<link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" />
<link rel="stylesheet" type="text/css" href="<?=$ROOT_URL?>_admin/_assets/shadowbox/shadowbox.css">
<? endblock() //End of Header Codes ?>

<? startblock('extracodes') ?> 
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/shadowbox/shadowbox.js"></script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h2, h3');

	Shadowbox.init({
		language:'en',
		players:['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv'],
		modal:true
	});
</script>
<? endblock() //End of Extra Codes ?>
