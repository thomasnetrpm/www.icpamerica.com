<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Pages.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include   "../../functions/myFunctions.php";
include_once('thumbnail/thumbnail_images.class.php');
			
if(isset($_POST['submit'])) {

  $_POST = sanitize($_POST);
  $pages = $_POST;
  settype($pages,'object');
  Pages::updatePages($pages); 
  $success = "Page Successfully Saved!";

  $updates = 'Update Page Content';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

$page = Pages::findPages($_REQUEST['id']);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/page.css" /> 
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/tiny.mods.js"></script>
</head>

<body>
  <? if(isset($success)) { ?>
  	<div class="alert"> <?=$success?> </div>
  <? } ?>
  
  <form id="form_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
  
    <fieldset>
      <legend>Modify Page</legend>
      <ul>
        <li><label for="page_name"> Page Name: </label>
          <input type="text" id="page_name" name="name" value="<?=stripslashes($page->fldPagesName)?>"></li>
        <?php if ($_REQUEST['id']==19) { // Show Home page two textbox ?>

          <li><label for="page.editor"> Banner1 Text: </label>
            <textarea cols="" rows="" id="page.banner1" name="custom1"><?=stripslashes($page->fldPagesContent1)?></textarea></li>
          <li><label for="page.editor"> Banner2 Text: </label>
            <textarea cols="" rows="" id="page.banner2" name="custom2"><?=stripslashes($page->fldPagesContent2)?></textarea></li>

        <?php } else { ?>

          <li><label for="page.editor"> Content Manager: </label>
            <textarea cols="" rows="" id="page.editor" name="content"><?=stripslashes($page->fldPagesDescription)?></textarea></li>

        <?php } ?>

      </ul>
    </fieldset>

<?php /*
    <fieldset>
      <legend>Meta Information</legend>
      <ul>
        <li><label for="meta_title"> Meta Title </label>
          <textarea cols="80" rows="10" id="meta_title" name="meta_title"><?=stripslashes($page->fldPagesMetaTitle)?></textarea></li>
         <li><label for="meta_keywords"> Meta Keywords </label>
          <textarea cols="80" rows="10" id="meta_keywords" name="meta_keywords"><?=stripslashes($page->fldPagesMetaKeywords)?></textarea></li> 
        <li><label for="meta_description"> Meta Description </label>
          <textarea cols="80" rows="10" id="meta_description" name="meta_description"><?=stripslashes($page->fldPagesMetaDescription)?></textarea></li>
      </ul>
    </fieldset>
*/ ?>

<?php /*    <div class="iecol1 center"><button type="submit">Save Page</button> &nbsp; <button type="reset">Clear Page</button></div> */ ?>
    
    <ul class="submission">
      <input type="hidden" name="Id" value="<?=$page->fldPagesID?>">
    	<li><button type="submit" name="submit">Save Page</button></li>
      <li><button type="reset">Clear Page</button></li>
    </ul>
    
    
  </form>

</body>
</html>