<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Pages.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include   "../../functions/myFunctions.php";
include_once('thumbnail/thumbnail_images.class.php');
$ROOT_URL = 'http://www.icpamerica.com/';

			
if(isset($_POST['submit'])) {

  $_POST = sanitize($_POST);
  $pages = $_POST;
  settype($pages,'object');
  $ctr = "";

		include ("upload_class.php");
		$max_size = 1024*250;
		$my_upload = new file_upload;
							
		if($_FILES['banner1']['name'] != '') {
			$size = getimagesize($_FILES['banner1']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 890){
						$banner_error1 = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['banner1']['tmp_name'];
					$my_upload->the_file = $_FILES['banner1']['name'];
					$my_upload->http_error = $_FILES['banner1']['error'];
					$my_upload->replace = 'y';

					$new_name = "Banner1".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$banner1_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($banner1_name);
						$banner1 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $banner1_name;
						$obj_img->PathImgNew = $save_path."_75_".$banner1;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$banner1 = "";
		}

		if($_FILES['banner2']['name'] != '') {
			$size = getimagesize($_FILES['banner2']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 890){
						$banner_error2 = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['banner2']['tmp_name'];
					$my_upload->the_file = $_FILES['banner2']['name'];
					$my_upload->http_error = $_FILES['banner2']['error'];
					$my_upload->replace = 'y';

					$new_name = "Banner2".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$banner2_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($banner2_name);
						$banner2 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $banner2_name;
						$obj_img->PathImgNew = $save_path."_75_".$banner2;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$banner2 = "";
		}

		if($_FILES['productimage1']['name'] != '') {
			$size = getimagesize($_FILES['productimage1']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 345){
						$productimage1_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage1']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage1']['name'];
					$my_upload->http_error = $_FILES['productimage1']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage1".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage1_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage1_name);
						$productimage1 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage1_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage1;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage1 = "";
		}

		if($_FILES['productimage2']['name'] != '') {
			$size = getimagesize($_FILES['productimage2']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 345){
						$productimage2_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage2']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage2']['name'];
					$my_upload->http_error = $_FILES['productimage2']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage2".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage2_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage2_name);
						$productimage2 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage2_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage2;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage2 = "";
		}

		if($_FILES['productimage3']['name'] != '') {
			$size = getimagesize($_FILES['productimage3']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 315){
						$productimage3_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage3']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage3']['name'];
					$my_upload->http_error = $_FILES['productimage3']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage3".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage3_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage3_name);
						$productimage3 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage3_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage3;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage3 = "";
		}

		if($_FILES['productimage4']['name'] != '') {
			$size = getimagesize($_FILES['productimage4']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 315){
						$productimage4_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage4']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage4']['name'];
					$my_upload->http_error = $_FILES['productimage4']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage4".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage4_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage4_name);
						$productimage4 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage4_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage4;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage4 = "";
		}

		if($_FILES['productimage5']['name'] != '') {
			$size = getimagesize($_FILES['productimage5']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 315){
						$productimage5_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage5']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage5']['name'];
					$my_upload->http_error = $_FILES['productimage5']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage5".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage5_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage5_name);
						$productimage5 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage5_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage5;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage5 = "";
		}

		if($_FILES['productimage6']['name'] != '') {
			$size = getimagesize($_FILES['productimage6']['tmp_name']);
				$width  = $size[0];
				$height = $size[1];
					if($width != 345){
						$productimage6_error = "Invalid Image Dimension";
						$ctr = 1;
					}
					$save_path = "../../../uploads/pages/";
					$my_upload->upload_dir = $save_path;
					$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
					$my_upload->max_length_filename = 50;
					$my_upload->rename_file = true;

					$my_upload->the_temp_file = $_FILES['productimage6']['tmp_name'];
					$my_upload->the_file = $_FILES['productimage6']['name'];
					$my_upload->http_error = $_FILES['productimage6']['error'];
					$my_upload->replace = 'y';

					$new_name = "productimage6".'_'.GetSID(7);
					if ($my_upload->upload($new_name)) {
						$productimage6_name = $my_upload->upload_dir.$my_upload->file_copy;
						$info = $my_upload->get_uploaded_file_info($productimage6_name);
						$productimage6 = $my_upload->file_copy;
						
						//create a thumbnail 75 width
						$obj_img = new thumbnail_images();
						$obj_img->PathImgOld = $productimage6_name;
						$obj_img->PathImgNew = $save_path."_75_".$productimage6;
						$obj_img->NewWidth = 75;
						if (!$obj_img->create_thumbnail_images()) { echo "error"; }
					}
		} else {
			$productimage6 = "";
		}


	if($ctr ==""){	
		echo $_POST['banner1'];
		  Pages::updatePages($pages, $banner1, $banner2, $productimage1, $productimage2, $productimage3, $productimage4, $productimage5, $productimage6); 
		  $success = "Page Successfully Saved!";
		
		  $updates = 'Update Page Content';
		  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
}

$page = Pages::findPages($_REQUEST['id']);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/page.css" /> 
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/tiny.mods.js"></script>
</head>

<body>
  <? if(isset($success)) { ?>
  	<div class="alert"> <?=$success?> </div>
  <? } ?>
  
  <form id="form_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
  
    <fieldset>
      <legend>Modify Page</legend>
      <ul>
        <li><label for="page_name"> Page Name: </label>
          <input type="text" id="page_name" name="name" value="<?=stripslashes($page->fldPagesName)?>"></li>
        <?php if ($_REQUEST['id']==19) { // Show Home page two textbox ?>

          <li><label for="page.editor"> Banner1 Text: </label>
            <textarea cols="" rows="" id="page.banner1" name="custom1"><?=stripslashes($page->fldPagesContent1)?></textarea></li>
          <li><label for="page.editor"> Banner2 Text: </label>
            <textarea cols="" rows="" id="page.banner2" name="custom2"><?=stripslashes($page->fldPagesContent2)?></textarea></li>
		<? }else if ($_REQUEST['id']==21) { ?>
			
			<li>
				<fieldset>
					<legend>Banner Image ( <small class="noticed">types: .jpg, .png | max-size: 2MB | dimension: 890px x 220px )</small></legend>
					<input type="file" style="width:auto;" name="banner1"/>	<?=$pages->fldBanner1?>	
					<? if($page->fldBanner1 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner1?>">
		            <? } ?>	
					<? if(isset($banner_error1)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$banner_error1?></b></div>
				 	<? } ?>		
				</fieldset>

				<fieldset style="width:47.5%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname1" value="<?=stripslashes($page->fldProductName1)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl1" value="<?=stripslashes($page->fldProductUrl1)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 345px x 296px )</small>
					<input type="file" style="width:auto;" name="productimage1"/>	
					<? if($page->fldProductImage1 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage1?>">
		            <? } ?>	
					<? if(isset($productimage1_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage1_error?></b></div>
				 	<? } ?>	
					Description		
					<textarea style="width:97% !important;" name="productdescription1"><?=stripslashes($page->fldProductDescription1)?></textarea>
				</fieldset>

				<fieldset style="width:47.5%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname2" value="<?=stripslashes($page->fldProductName2)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl2" value="<?=stripslashes($page->fldProductUrl2)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 345px x 296px )</small>
					<input type="file" style="width:auto;" name="productimage2"/>	
					<? if($page->fldProductImage2 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage2?>">
		            <? } ?>	
					<? if(isset($productimage2_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage2_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription2"><?=stripslashes($page->fldProductDescription2)?></textarea>
				</fieldset>

				<fieldset>
					<legend>Banner Image ( <small class="noticed">types: .jpg, .png | max-size: 2MB | dimension: 890px x 220px )</small></legend>
					<input type="file" style="width:auto;" name="banner2"/>	
					<? if($page->fldBanner2 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner2?>">
		            <? } ?>	
					<? if(isset($banner_error2)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$banner_error2?></b></div>
				 	<? } ?>
					<br/>					<br/>
					Url<input type="text" name="url" value="<?=stripslashes($page->fldUrl)?>"/>				
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname3" value="<?=stripslashes($page->fldProductName3)?>"/>
					Url
					<input type="text" style="width:97%;" name="producturl3" value="<?=stripslashes($page->fldProductUrl3)?>"/>		
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage3"/>	
					<? if($page->fldProductImage3 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage3?>">
		            <? } ?>	
					<? if(isset($productimage3_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage3_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription3"><?=stripslashes($page->fldProductDescription3)?></textarea>
				</fieldset>
	
				<fieldset style="width:30.8%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname4" value="<?=stripslashes($page->fldProductName4)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl4" value="<?=stripslashes($page->fldProductUrl4)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage4"/>	
					<? if($page->fldProductImage4 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage4?>">
		            <? } ?>	
					<? if(isset($productimage4_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage4_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription4"><?=stripslashes($page->fldProductDescription4)?></textarea>
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname5" value="<?=stripslashes($page->fldProductName5)?>"/>
					Url
					<input type="text" style="width:97%;" name="producturl5" value="<?=stripslashes($page->fldProductUrl5)?>"/>		
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage5"/>	
					<? if($page->fldProductImage5 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage5?>">
		            <? } ?>	
					<? if(isset($productimage5_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage5_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription5"><?=stripslashes($page->fldProductDescription5)?></textarea>
				</fieldset>
			</li>

        <? }else if ($_REQUEST['id']==22) { ?>
			
			<li>
				<!--
				<fieldset>
					<legend>Banner Image ( <small class="noticed">types: .jpg, .png | max-size: 2MB | dimension: 890px x 220px )</small></legend>
					<input type="file" style="width:auto;" name="banner1"/>	<?=$pages->fldBanner1?>	
					<? if($page->fldBanner1 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner1?>">
		            <? } ?>	
					<? if(isset($banner_error1)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$banner_error1?></b></div>
				 	<? } ?>		
				</fieldset>
				-->
				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname6" value="<?=stripslashes($page->fldProductName6)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl6" value="<?=stripslashes($page->fldProductUrl6)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 345px x 296px )</small>
					<input type="file" style="width:auto;" name="productimage6"/>	
					<? if($page->fldProductImage6 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage6?>">
		            <? } ?>	
					<? if(isset($productimage6_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage6_error?></b></div>
				 	<? } ?>	
					Description		
					<textarea style="width:97% !important;" name="productdescription6"><?=stripslashes($page->fldProductDescription6)?></textarea>
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname1" value="<?=stripslashes($page->fldProductName1)?>"/>
					Url
					<input type="text" style="width:97%;" name="producturl1" value="<?=stripslashes($page->fldProductUrl1)?>"/>		
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 345px x 296px )</small>
					<input type="file" style="width:auto;" name="productimage1"/>	
					<? if($page->fldProductImage1 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage1?>">
		            <? } ?>	
					<? if(isset($productimage1_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage1_error?></b></div>
				 	<? } ?>	
					Description		
					<textarea style="width:97% !important;" name="productdescription1"><?=stripslashes($page->fldProductDescription1)?></textarea>
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname2" value="<?=stripslashes($page->fldProductName2)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl2" value="<?=stripslashes($page->fldProductUrl2)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 345px x 296px )</small>
					<input type="file" style="width:auto;" name="productimage2"/>	
					<? if($page->fldProductImage2 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage2?>">
		            <? } ?>	
					<? if(isset($productimage2_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage2_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription2"><?=stripslashes($page->fldProductDescription2)?></textarea>
				</fieldset>

				<fieldset>
					<legend>Banner Image ( <small class="noticed">types: .jpg, .png | max-size: 2MB | dimension: 890px x 220px )</small></legend>
					<input type="file" style="width:auto;" name="banner2"/>	
					<? if($page->fldBanner2 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner2?>">
		            <? } ?>	
					<? if(isset($banner_error2)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$banner_error2?></b></div>
				 	<? } ?>
					<br/>					<br/>
					Url<input type="text" name="url" value="<?=stripslashes($page->fldUrl)?>"/>				
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname3" value="<?=stripslashes($page->fldProductName3)?>"/>
					Url
					<input type="text" style="width:97%;" name="producturl3" value="<?=stripslashes($page->fldProductUrl3)?>"/>		
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage3"/>	
					<? if($page->fldProductImage3 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage3?>">
		            <? } ?>	
					<? if(isset($productimage3_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage3_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription3"><?=stripslashes($page->fldProductDescription3)?></textarea>
				</fieldset>
	
				<fieldset style="width:30.8%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname4" value="<?=stripslashes($page->fldProductName4)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl4" value="<?=stripslashes($page->fldProductUrl4)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage4"/>	
					<? if($page->fldProductImage4 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage4?>">
		            <? } ?>	
					<? if(isset($productimage4_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage4_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription4"><?=stripslashes($page->fldProductDescription4)?></textarea>
				</fieldset>

				<fieldset style="width:30.7%; display:inline-block; vertical-align:top;">
					<legend>Product</legend>
					Name
					<input type="text" style="width:97%;" name="productname5" value="<?=stripslashes($page->fldProductName5)?>"/>	
					Url
					<input type="text" style="width:97%;" name="producturl5" value="<?=stripslashes($page->fldProductUrl5)?>"/>	
					Image <small class="noticed">( types: .jpg, .png | max-size: 2MB | dimension: 315px x 245px )</small>
					<input type="file" style="width:auto;" name="productimage5"/>	
					<? if($page->fldProductImage5 != "") { ?>
		            	<br>
		            	<img style="width:100%;" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage5?>">
		            <? } ?>	
					<? if(isset($productimage5_error)) { ?>      
						<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$productimage5_error?></b></div>
				 	<? } ?>
					Description	
					<textarea style="width:97% !important;" name="productdescription5"><?=stripslashes($page->fldProductDescription5)?></textarea>
				</fieldset>
			</li>

        <?php } else { ?>

          <li><label for="page.editor"> Content Manager: </label>
            <textarea cols="" rows="" id="page.editor" name="content"><?=stripslashes($page->fldPagesDescription)?></textarea></li>

        <?php } ?>

      </ul>
    </fieldset>

<?php /*
    <fieldset>
      <legend>Meta Information</legend>
      <ul>
        <li><label for="meta_title"> Meta Title </label>
          <textarea cols="80" rows="10" id="meta_title" name="meta_title"><?=stripslashes($page->fldPagesMetaTitle)?></textarea></li>
         <li><label for="meta_keywords"> Meta Keywords </label>
          <textarea cols="80" rows="10" id="meta_keywords" name="meta_keywords"><?=stripslashes($page->fldPagesMetaKeywords)?></textarea></li> 
        <li><label for="meta_description"> Meta Description </label>
          <textarea cols="80" rows="10" id="meta_description" name="meta_description"><?=stripslashes($page->fldPagesMetaDescription)?></textarea></li>
      </ul>
    </fieldset>
*/ ?>

<?php /*    <div class="iecol1 center"><button type="submit">Save Page</button> &nbsp; <button type="reset">Clear Page</button></div> */ ?>
    
    <ul class="submission">
      <input type="hidden" name="Id" value="<?=$page->fldPagesID?>">
    	<li><button type="submit" name="submit">Save Page</button></li>
      <li><button type="reset">Clear Page</button></li>
    </ul>
    
    
  </form>

</body>
</html>