<? 	
if(isset($_POST['update'])) {
  $_POST = sanitize($_POST);
  $admin = $_POST;
  settype($admin,'object');
  Administrator::updateAdministratorSite($admin); 
  $success = "Administrator Successfully Changed!";
}

$administrator = Administrator::findAdministrator($_REQUEST['id']);
?>					
<? if(isset($success)) { ?>	
      <div class="alert"> <?=$success?> </div>
<? } ?>      
        
        <form id="profile_page" action="<? $PHP_SELF; ?>" method="post">
        
        <fieldset class="col1" style="width:47%;">
          <legend>Personal Profile</legend>
          <ul>
            <li><label> Full Name: </label>
              <input type="text" id="field[]" name="real_name" value="<?=$administrator->fldAdministratorRealName?>"></li>
            <li><label> Username: </label>
              <input type="text" id="field[]" name="username" value="<?=$administrator->fldAdministratorusername?>"></li>
            <li><label> Password: </label>
              <input type="text" id="field[]" name="password"></li>
            <li><label> Email: </label>
              <input type="text" id="field[]" name="email" value="<?=$administrator->fldAdministratorEmail?>"></li>
            <li><label> Phone: </label>
              <input type="text" id="field[]" name="phone" value="<?=$administrator->fldAdministratorPhone?>"></li>
            <?php
            if ($adminLevel==1) {
              ?>
              <label>Is Super Admin?: <input type="checkbox" name="superadmin" style="width:10px; margin-right:10px; float:left;" value="1" <?=($administrator->fldAdministratorLevel==1)? "checked": "";?> ></label>
              <?php
            }
            ?>
          </ul>
        </fieldset>
        
        <fieldset class="col2" style="width:47%;">
          <legend>Contact &amp; Networks Info</legend>
          <ul>
            <li><label> Site Name: </label>
              <input type="text" id="field[]" name="sitename" value="<?=$administrator->fldAdministratorSitename?>"></li>
            <li><label> Blog: </label>
              <input type="text" id="field[]" name="blog" value="<?=$administrator->fldAdministratorBlog?>"></li>
            <li><label> Facebook: </label>
              <input type="text" id="field[]" name="facebook" value="<?=$administrator->fldAdministratorFaceBook?>"></li>
            <li><label> Twitter: </label>
              <input type="text" id="field[]" name="twitter" value="<?=$administrator->fldAdministratorTwitter?>"></li>
            <li><label> LinkedIn: </label>
              <input type="text" id="field[]" name="linkedin" value="<?=$administrator->fldAdministratorLinkedin?>"></li>
          </ul>
        </fieldset>
        
        <div class="hr-clear"></div>
        <?php
        if ($adminLevel==1) {
          ?>
          <fieldset style="width:1045px;">
            <legend>Module Access</legend>
            <ul class="inline-list">
              <li><input type="checkbox" name="mod_cust" value="1" <?=($administrator->mod_cust==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;"> Customer</li>
              <li><input type="checkbox" name="mod_trans" value="1" <?=($administrator->mod_trans==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;"> Orders</li>
              <li><input type="checkbox" name="mod_cont" value="1" <?=($administrator->mod_cont==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > Contact Us</li>
              <li><input type="checkbox" name="mod_prodcat" value="1" <?=($administrator->mod_prodcat==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > Product Category</li>
              
							<li><input type="checkbox" name="mod_prod" value="1" <?=($administrator->mod_prod==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > Products</li>
              <li><input type="checkbox" name="mod_disc" value="1" <?=($administrator->mod_disc==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > Discount Coupon</li>
              <li><input type="checkbox" name="mod_tax" value="1" <?=($administrator->mod_tax==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > Tax</li>
              <li><input type="checkbox" name="mod_news" value="1" <?=($administrator->mod_news==1)? "checked": "";?> style="width:10px; margin-right:10px; float:left;" > News</li>
            </ul>
          </fieldset>
          <?php
        }
        ?>

        <div class="hr-clear"></div>

        <fieldset style="width:1045px;">
          <legend>About Yourself/Company</legend>
          <ul>
            <li><label> Biographical Info: </label>
              <textarea cols="" rows="" style="display:block;width:900px;max-width:900px;max-height:150px;height:150px;" name="about"><?=stripslashes($administrator->fldAdministratorAbout)?></textarea></li>
          </ul>
        </fieldset>

        <div class="iecol1 center">
       
        	<input type="hidden" name="Id" value="<?=$administrator->fldAdministratorID?>" />
        	<button type="submit" name="update">Update Profile</button> &nbsp; <button type="reset">Clear Profile</button></div>
       
        </form>
