<? 
if(isset($_POST['submit'])) {
  $_POST = sanitize($_POST);
  $admin = $_POST;
  settype($admin,'object');
  
  Administrator::addAdministratorSite($admin); 
  $success = "Administrator Successfully Saved!";
}
?>
<? if(isset($success)) { ?>	
      <div class="alert"> <?=$success?> </div>
<? } ?>
        
        <form id="profile_page" action="<? $PHP_SELF; ?>" method="post">
        
        <fieldset class="col1" style="width:47%;">
          <legend>Personal Profile</legend>
          <ul>
            <li><label> Full Name: </label>
              <input type="text" id="field[]" name="real_name" value=""></li>
            <li><label> Username: </label>
              <input type="text" id="field[]" name="username" value=""></li>
            <li><label> Password: </label>
              <input type="text" id="field[]" name="password"></li>
            <li><label> Email: </label>
              <input type="text" id="field[]" name="email" value=""></li>
            <li><label> Phone: </label>
              <input type="text" id="field[]" name="phone" value=""></li>
            <?php
            if ($adminLevel==1) {
              ?>
              <label>Is Super Admin?: <input type="checkbox" style="width:10px; margin-right:10px; float:left;" name="superadmin" value="1"></label>
              <?php
            }
            ?>
          </ul>
        </fieldset>
        
        <fieldset class="col2" style="width:47%;">
          <legend>Contact &amp; Networks Info</legend>
          <ul>
            <li><label> Site Name: </label>
              <input type="text" id="field[]" name="sitename" value=""></li>
            <li><label> Blog: </label>
              <input type="text" id="field[]" name="blog" value=""></li>
            <li><label> Facebook: </label>
              <input type="text" id="field[]" name="facebook" value=""></li>
            <li><label> Twitter: </label>
              <input type="text" id="field[]" name="twitter" value=""></li>
            <li><label> LinkedIn: </label>
              <input type="text" id="field[]" name="linkedin"  value=""></li>
          </ul>
        </fieldset>
        
        <div class="hr-clear"></div>

        <?php
        if ($adminLevel==1) {
          ?>
          <fieldset style="width:1045px;">
            <legend>Module Access</legend>
            <ul class="inline-list">
              <li><input type="checkbox" name="mod_cust" value="1" style="width:10px; margin-right:10px; float:left;"> Customer</li>
              <li><input type="checkbox" name="mod_trans" value="1" style="width:10px; margin-right:10px; float:left;"> Orders</li>
              <li><input type="checkbox" name="mod_cont" value="1" style="width:10px; margin-right:10px; float:left;"> Contact Us</li>
              <li><input type="checkbox" name="mod_prodcat" value="1" style="width:10px; margin-right:10px; float:left;"> Product Category</li>
              <li><input type="checkbox" name="mod_prod" value="1" style="width:10px; margin-right:10px; float:left;"> Products</li>
              <li><input type="checkbox" name="mod_disc" value="1" style="width:10px; margin-right:10px; float:left;"> Discount Coupon</li>
              <li><input type="checkbox" name="mod_tax" value="1" style="width:10px; margin-right:10px; float:left;"> Tax</li>
              <li><input type="checkbox" name="mod_news" value="1" style="width:10px; margin-right:10px; float:left;"> News</li>
            </ul>
          </fieldset>
          <?php
        }
        ?>

        <div class="hr-clear"></div>

        <fieldset style="width:1045px;">
          <legend>About Yourself/Company</legend>
          <ul>
            <li><label> Biographical Info: </label>
              <textarea cols="" rows="" name="about"></textarea></li>
          </ul>
        </fieldset>

        <div class="iecol1 center">
       
	        <button type="submit" name="submit">Save Profile</button> &nbsp; <button type="reset">Clear Profile</button></div>
           
        </form>
