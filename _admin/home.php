<? include '_manager/box.header/base.php' ?>
<? 
  require_once('functions/myFunctions.php');
  include_once('thumbnail/thumbnail_images.class.php');
?>

<?
	if(isset($_POST['submit'])) {
		 	
	include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['banner']['name'] != '') {
				$my_upload->upload_dir = "../banner_image/"; // "files" is the folder for the uploaded files (you have to create this folder)
				$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
			// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
				$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
				$my_upload->rename_file = true;
			
				$my_upload->the_temp_file = $_FILES['banner']['tmp_name'];
				$my_upload->the_file = $_FILES['banner']['name'];
				$my_upload->http_error = $_FILES['banner']['error'];
				$my_upload->replace = 'y';
				//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
				//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename
			
				$new_name = 'Banner'.GetSID(7);
				//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
				if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
					$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
					$info = $my_upload->get_uploaded_file_info($full_path_name);
					$full_path = $my_upload->file_copy;
					// ... or do something like insert the filename to the database
					
					//create a thumbnail 99 width
							$obj_img = new thumbnail_images();
							$obj_img->PathImgOld = "../banner_image/".$full_path;
							$obj_img->PathImgNew = "../banner_image/_1280_".$full_path;
							$obj_img->NewWidth = 1280;
				
									//$obj_img->NewHeight = 99;
							if (!$obj_img->create_thumbnail_images()) { echo "error"; }
														
				}					
	
		} else {
				$full_path = "";
		}
			
			$_POST = sanitize($_POST);
		    $page = $_POST;
		    settype($page,'object');
			if($page->Id == "") { 
				HomePage::addHomePage($page,$full_path); 
			} else {
				HomePage::updateHomePage($page,$full_path); 
			}
			$success = "Page Successfully Saved!";
			
			$updates = 'Add New Page Content';
			AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
?>

	<? startblock('section') ?> 
      <div id="container">
      	<h2>Home Page Management</h2>
        <span>Welcome to your Page Manager</span>
        
        <div class="hr-clear"><!-- Clear Section --></div>
        
        <div id="page_tabs">                    
          <ul id="pt_selectors">
            <li id="pt_btab"><a href="<?=$ROOT_URL?>_admin/page.php">View All Page</a></li>
          </ul>
          
          <div id="pt_ftab">
          		<? if(isset($success)) { ?>
            <div class="alert"> Page Successfully Saved! </div>
        <? } ?>    
		<?
			$homepage = HomePage::findHomePage(1);			
		?>
            <form id="form_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
            
              <fieldset>
                <legend>Home Page</legend>
                <ul>
                  <li><label for="page_name"> Registration: </label>
                   <textarea cols="80" rows="10" name="registration"><?=stripslashes($homepage->fldHomePageRegistration)?></textarea></li>
                   <li><label for="page_name"> Schedule: </label>
                   <textarea cols="80" rows="10" name="schedule"><?=stripslashes($homepage->fldHomePageSchedule)?></textarea></li>
                   <li><label for="page_name"> Course Description: </label>
                   <textarea cols="80" rows="10" name="course"><?=stripslashes($homepage->fldHomePageCourse)?></textarea></li>
               	
                    
                  <li><label for="page.editor"> Content Manager: </label>
                    <textarea cols="80" rows="10" id="page.editor" name="content"><?=stripslashes($homepage->fldHomePageDescription)?></textarea></li>
                  
                   <li><label for="page_name"> Banner: (1280 width x 495 height)</label>
                   <input type="file" name="banner">
                   	<? if($homepage->fldHomePageBanner != "") { ?>
                    	<br>
						<img src="<?=$ROOT_URL?>banner_image/_1280_<?=$homepage->fldHomePageBanner?>" width="800">
                    <? } ?>
                   </li>
                 
                </ul>
              </fieldset>
              
              
             <input type="hidden" name="Id" value="<?=$homepage->fldHomePageID?>">
              
              <div class="iecol1 center"><button type="submit" name="submit">Save Page</button> &nbsp; <button type="reset">Clear Page</button></div>
              
            </form>
          </div>
        </div>
        <!-- /End Page Tabs -->
      </div>
  <? endblock() //End of Section ?>



<? startblock('headercodes') ?> 
<link rel="stylesheet" type="text/css" href="<?=$ROOT_URL?>_admin/_assets/css/page.css">
<? endblock() //End of Header Codes ?>

<? startblock('extracodes') ?> 
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/tiny.mods.js"></script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h2');
</script>
<? endblock() //End of Extra Codes ?>
