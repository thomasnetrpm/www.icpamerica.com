<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Contact.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

if(isset($_POST['submit'])) {
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$email = $_POST['email'];
	
	if($firstname == "" || $lastname == ""){$name_error = "Please enter name";$ctr=1;}
	
	if($ctr=="") { 
	 $_POST = sanitize($_POST);
      $contact = $_POST;
	  settype($contact,'object');
	  Contact::addContact($contact); 
	  $success = "Contacts Successfully Saved!";
	  
	   $updates = 'Add new contacts content';
  	  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_event/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_contact/dashboard.php">Back</a></li>
    </ul>    
</div>    
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post">
    <h3>ACM Contacts</h3>
    <fieldset style="width:1050px;">
      <legend>Contacts Panel</legend>
      <ul>
        <li>
          <label for="title">First Name</label>
           <input type="text" id="title" name="firstname">   
           <? if(isset($name_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$name_error?></b></div>
           <? } ?>
        </li>
        <li>
          <label for="title">Last Name</label>
           <input type="text" id="title" name="lastname">   
           <? if(isset($name_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$name_error?></b></div>
           <? } ?>
        </li>
        <li>
          <label for="name">Company name</label>
           <input type="text" id="company" name="company">   
        </li>
        <li>
          <label for="phone">Phone</label>
           <input type="text" id="phone" name="phone">         
        </li>
        <li>
          <label for="fax">Fax</label>
           <input type="text" id="fax" name="fax">         
        </li>
        <li>
          <label for="email">Email</label>
           <input type="text" id="email" name="email">   
        </li>
        <li>
          <label for="subject">Address 1</label>
           <input type="text" id="address1" name="address1">         
        </li>  
        <li>
          <label for="subject">Address 2</label>
           <input type="text" id="address2" name="address2">         
        </li>  
        <li>
          <label for="city">City </label>
           <input type="text" id="city" name="city">         
        </li> 
         <li>
          <label for="state">State </label>
           <input type="text" id="state" name="state">         
        </li>  
         <li>
          <label for="zip">ZIP</label>
           <input type="text" id="zip" name="zip">         
        </li>       

         <li>
          <label for="zip">How Did You Find Us?</label>
           <input type="text" id="find" name="find">
        </li>       
         <li>
          <label for="zip">Inquiry / Comment</label>
           <input type="text" id="comment" name="comment">
        </li>       
<!--
          <li>
          <label for="interest">Email Updates</label>
			<input type="checkbox" name="email_updates" value="Yes" width="10px;">
        </li>
-->  
      </ul>
    </fieldset>
    <ul class="submission">
      <li><input type="submit" name="submit" value="Save Contact"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>