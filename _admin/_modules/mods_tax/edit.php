<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/State.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";

$author_id = $_REQUEST['id'];

if(isset($_POST['submit'])) {
   
	 $_POST = sanitize($_POST);
	$state = $_POST;
	settype($state,'object');
	State::updateState($state); 
	$success = "Tax Successfully Saved!";
	
}

?>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_author/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_tax/dashboard.php">Back</a></li>
    </ul>    
</div>    
<? $tax = State::findState($_REQUEST['id']);?>
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
    <h3>ACM Tax</h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>Update State</legend>
      <ul>
        <li>
          <label for="name">State : </label>
            <strong><?=$tax->fldStateName?></strong>
          </label>
        </li>
          <li>
          <label for="name">Tax</label>
            <input type="text" name="tax" value="<?=stripslashes($tax->fldStateTax)?>">
          </label>
        </li>        
      </ul>
    </fieldset>
    <ul class="submission">
    	<input type="hidden" name="Id" value="<?=$_REQUEST['id']?>">
      <li><input type="submit" name="submit" value="Save Tax"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>