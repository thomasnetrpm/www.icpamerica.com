<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Category.php";
include   "../../../classes/AdminAction.php";
include   "../../../classes/Products.php";
include   "../../../classes/ProductsOption.php";
include   "../../../classes/Options.php";

include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

//delete photo
if(isset($_REQUEST['delete'])) {
	ProductsOption::deleteProductsOptionByOptions($_REQUEST['product_id']);
	$updates = 'Delete products content';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="store_overview">
    	<ul class="btn">
             <li><a href="<?=$ROOT_URL?>_admin/modules_products/view/">Back</a></li>    		
    	</ul>
    <h3>Options Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="30"> ID </td>        
          <td width="100">Options</td>  
          <td width="100">Amount</td>

              
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
   <?php
	$count_record=ProductsOption::countProductOptionsByProducts($_REQUEST['id']);
	 
	$page = (!isset($_REQUEST['page'])) ? 1 : $_GET[page];
	 
	$pagination = new Pagination;
	$pg = $pagination->page_pagination(20, $count_record, $page, 20);

	$product=ProductsOption::findAll($pg[1],$_REQUEST['id']); ?>
  	<? if($count_record == 0): ?>
			<tr>
				<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
			</tr>
    <? else: ?>
		<? foreach($product as $products):
			$options = Options::findOptions($products->fldProductsOptionMainId); ?>
			<tr>
				<td> <?=$products->fldProductsOptionID?> </td>                  
				<td><?=$options->fldOptionsName?></td>
				<td><?='$'.number_format($products->fldProductsOptionAmount,2)?></td> 
				<td align="center">
					<a href="<?=$ROOT_URL?>_admin/modules_products_options/edit/<?=$products->fldProductsOptionID?>/<?=$_REQUEST['id']?>/">
						<img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/modify.png" width="14" height="16" alt="mod" />
					</a>
					<a href="<?=$ROOT_URL?>_admin/_modules/mods_store/products_options.php?delete=1&product_id=<?=$products->fldProductsOptionID?>&id=<?=$_REQUEST['id']?>" title="Delete Option" onClick="return confirm(&quot;Are you sure you want to completely remove this Products Options from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Products Options.\n&quot;)">
						<img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/delete.gif" width="16" height="16" alt="del" />
					</a>
				</td>
			</tr>
		<? endforeach ?>
	<? endif ?>
    
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2"></dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    <!-- /End Fetching Data Tables -->
  
</div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_acm/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>