<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/NewsCategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

//Delete Category
if (isset($_REQUEST['delete'])) {
  NewsCategory::deleteNewsCategory($_REQUEST['delete']);

  $updates = 'Delete news content';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="blog_overview">
    	<ul class="btn">
            <li><a href="<?=$ROOT_URL?>_admin/_modules/mods_news/create_category.php">Add News Category</a></li>
        </ul>
  
    <h3>News Category Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="100"></td>  
          <td width="300">Category Name</td>   
          <td width="110"></td>
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
        <?php
        $count_record = NewsCategory::countNewsCategory();
				 
				if(!isset($_REQUEST['page']))
					{
						$page = 1;
					}
					else
					{
					$page = $_GET[page];
					}
					$pagination = new Pagination;
					//for display
					$pg = $pagination->page_pagination(20, $count_record, $page, 20);
					$newscat = NewsCategory::findAll($pg[1]);
			?>
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="5" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else { 
					foreach($newscat as $nc) { 
            $nc_id = $nc->nc_id;
            $nc_name = $nc->nc_name;
            // $newsDate = ($nc->fldNewsDate)? date("M d, Y", strtotime($news->fldNewsDate)): '';
			    ?>
            <tr>
              <td><?=$nc_id?></td>
              <td><?php /* <img src="<?=$ROOT_URL?>news_image/_75_<?=$nc->fldNewsImage?>"> */ ?></td>
              <td><?=$nc_name?></td>
              <td><?//=$newsDate?></td>
              <td align="center">
                <a href="<?=$ROOT_URL?>_admin/_modules/mods_news/edit_category.php?cid=<?=$nc_id?>"><img src="<?=$ROOT_URL?>_admin/_modules/mods_news/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard_category.php?delete=<?=$nc_id?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this News Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the News Category.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mods_news/images/delete.gif" width="16" height="16" alt="del" /></a>
                <br> <a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard.php?cid=<?=$nc_id?>"> [ View News ]</a>
              </td>
            </tr>
          <? } }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="5" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2"></dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>