<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/News.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

$categoryID = trim($_REQUEST['cid']);

if(isset($_POST['submit'])) {
	
/*
  include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['image']['name'] != '') {

		$my_upload->upload_dir = "../../../news_image/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		$my_upload->rename_file = true;

		$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
		$my_upload->the_file = $_FILES['image']['name'];
		$my_upload->http_error = $_FILES['image']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		$new_name = 'News'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path = $my_upload->file_copy;
			// ... or do something like insert the filename to the database
			
		 	//resize the image
			//create a thumbnail 683 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_683_".$full_path;
			$obj_img->NewWidth = 683;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
				//create a thumbnail 360 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_360_".$full_path;
			$obj_img->NewWidth = 360;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 235 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_235_".$full_path;
			$obj_img->NewWidth = 235;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 75 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_75_".$full_path;
			$obj_img->NewWidth = 75;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }			
		}
	} else {
		$full_path = "";
	}
*/	
	
  include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['pdf']['name'] != '') {

		$my_upload->upload_dir = "../../../uploads/news_pdf/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".pdf",".PDF"); // specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		// $my_upload->rename_file = true;
		$my_upload->rename_file = false;

		$my_upload->the_temp_file = $_FILES['pdf']['tmp_name'];
		$my_upload->the_file = $_FILES['pdf']['name'];
		$my_upload->http_error = $_FILES['pdf']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		// $new_name = 'news_pdf_'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		// if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
		if ($my_upload->upload($my_upload->the_file)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path = $my_upload->file_copy;
			// ... or do something like insert the filename to the database
		}
	} else {
		$full_path = "";
	}

	$_POST = sanitize($_POST);
	$news = $_POST;
	settype($news,'object');
	News::addNews($news, $categoryID, $full_path); 
	$success = "News Successfully Saved!";

	$updates = 'Add new news content';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_news/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard.php?cid=<?=$categoryID?>">Back</a></li>
    </ul>    
</div>    
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
    <h3>ACM News</h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>New News</legend>
      <ul>
        <li>
          <label for="name">Title</label>
            <input type="text" id="name" name="name">
          </label>
        </li>
        <li>
          <label for="blog.editor">
            <textarea cols="" rows="" id="blog.editor" name="description"></textarea>
          </label>
        </li>

        <li>
          <label for="url">URL</label>
            <input type="text" id="url" name="url">
          </label>
        </li>
        <li>
          <label for="pdf">PDF Upload</label>
            <input type="file" name="pdf">
          </label>
        </li>
        <li>
          <label for="date">Date</label>
            <input type="text" id="date" name="date_news"> eg.. 2012-08-20
          </label>
        </li>
<?php
/*
        <li>
          <label for="image"> Image</label>
            <input type="file" name="image">
          </label>
        </li>
*/
?>
     
      </ul>
    </fieldset>
    <input type="hidden" name="cid" value="<?=$categoryID?>">
    <ul class="submission">
      <li><input type="submit" name="submit" value="Save News"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>