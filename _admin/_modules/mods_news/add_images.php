<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/News.php";
include   "../../../classes/NewsImages.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

if(isset($_POST['submit'])) {
	
  include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['image']['name'] != '') {


	$my_upload->upload_dir = "../../../news_add_image/"; // "files" is the folder for the uploaded files (you have to create this folder)
	$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
	$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
	$my_upload->rename_file = true;

	$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
	$my_upload->the_file = $_FILES['image']['name'];
	$my_upload->http_error = $_FILES['image']['error'];
	$my_upload->replace = 'y';
	//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
	//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

	$new_name = 'News'.GetSID(7);
	//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
	if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
		$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
		$info = $my_upload->get_uploaded_file_info($full_path_name);
		$full_path = $my_upload->file_copy;
		// ... or do something like insert the filename to the database
		
	 	//resize the image
		
	 			//create a thumbnail 683 width
				$obj_img = new thumbnail_images();
				$obj_img->PathImgOld = $full_path_name;
				$obj_img->PathImgNew = "../../../news_add_image/_683_".$full_path;
				$obj_img->NewWidth = 683;
						//$obj_img->NewHeight = 58;
				if (!$obj_img->create_thumbnail_images()) { echo "error"; }
				
				//create a thumbnail 75 width
				$obj_img = new thumbnail_images();
				$obj_img->PathImgOld = $full_path_name;
				$obj_img->PathImgNew = "../../../news_add_image/_75_".$full_path;
				$obj_img->NewWidth = 75;
						//$obj_img->NewHeight = 58;
				if (!$obj_img->create_thumbnail_images()) { echo "error"; }
								
	}


	} else {
			$full_path = "";
	}
	
	  $_POST = sanitize($_POST);
      $news = $_POST;
	  settype($news ,'object');
	  $news->news_id = $_REQUEST['id'];
	  NewsImages::addNewsImages($news ,$full_path); 
	  $success = "News Images Successfully Saved!";
	  
	   $updates = 'Add new news content';
  	  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}


if(isset($_REQUEST['delete'])) {
	NewsImages::deleteNewsImages($_REQUEST['delete']);
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_projects/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
    <? $news = News::findNews($_REQUEST['id']);?>  
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard.php">Back</a></li>
    </ul>    
</div>    

  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
    <h3>ACM News Images</h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>News Images</legend>
      <ul>
        <li>
         <?=stripslashes($news->fldNewsTitle)?>
        </li>
       
        <li>
          <label for="image">Image</label>
            <input type="file" name="image">
          </label>
        </li>
      </ul>
    </fieldset>
    <ul class="submission">
      <li><input type="submit" name="submit" value="Save News Images"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
     <table border="0">
  	<? 
		$image = NewsImages::displayAll($_REQUEST['id']);
		foreach($image as $images) { 
		  $ctr=$ctr+1;
	?>
  	<? if($ctr==1) { ?><tr><? } ?>
    	<td style="padding:5px 5px;" align="center"><img src="<?=$ROOT_URL?>news_add_image/_75_<?=$images->fldNewsImagesImage?>"> <br><a href="add_images.php?id=<?=$_REQUEST['id']?>&delete=<?=$images->fldNewsImagesID?>" onClick="return confirm(&quot;Are you sure you want to completely remove this News Images from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the News Images.\n&quot;)">[ Delete ]</a></td>
    <? if($ctr==4) {$ctr=0; ?></tr><? } ?>
    <? } ?>
  </table>
  </form>
  
 

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>