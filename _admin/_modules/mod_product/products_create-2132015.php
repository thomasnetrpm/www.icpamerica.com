<?php 
ob_start();
session_start();

include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/Products.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');
$ROOT_URL = "http://www.icpamerica.com/";
$product_id = $_REQUEST['Id'];

if ($_REQUEST['err']==1) {
  $msg_error = "Please complete the product category and subcategory below";
}


if(isset($_POST['submit'])) {
  
  if ($_REQUEST['cid']!='') {
    if ($_REQUEST['cid']!=15) {
      if ($_REQUEST['scid']=='') {
        echo $msg_error = "Please complete the product category and subcategory below1";
        // header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_create.php?Id=".$product_id."&err=1");
        die();
      }
    }
  }
  if ($_REQUEST['scid']!='') {
    if ($_REQUEST['sc2id']=='') {
      echo $msg_error = "Please complete the product category and subcategory below2";
      // header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_create.php?Id=".$product_id."&err=1");
      die();
    }
  }
  /*
  if ($_REQUEST['sc2id']!='') {
    if ($_REQUEST['sc3id']=='') {
      echo $msg_error = "Please complete the product category and subcategory below3";
      // header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_create.php?Id=".$product_id."&err=1");
      die();
    }
  }
  */

  if ($msg_error=='') {
    include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

    $max_size = 1024*250; // the max. size for uploading

    $my_upload = new file_upload;

    // ****************** MAIN IMAGE - Start
    if($_FILES['image']['name'] != '') {

      $my_upload->upload_dir = "../../../uploads/product_image/".$product_id.""; // "files" is the folder for the uploaded files (you have to create this folder)
      $my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
      // $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
      $my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
      $my_upload->rename_file = true;

      $my_upload->the_temp_file = $_FILES['image']['tmp_name'];
      $my_upload->the_file = $_FILES['image']['name'];
      $my_upload->http_error = $_FILES['image']['error'];
      $my_upload->replace = 'y';
      //$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
      //$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

      $new_name = 'main_img_'.GetSID(7);
      //$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
      if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
        $full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
        $info = $my_upload->get_uploaded_file_info($full_path_name);
        $full_path_main = $my_upload->file_copy;
        // ... or do something like insert the filename to the database

        // Resize to 590 width
        $obj_img = new thumbnail_images();
        $obj_img->PathImgOld = $full_path_name;
        $obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_590_".$full_path_main;
        $obj_img->NewWidth = 590;
        //$obj_img->NewHeight = 58;
        if (!$obj_img->create_thumbnail_images()) { echo "error"; }

        //create a thumbnail 290 width
        $obj_img = new thumbnail_images();
        $obj_img->PathImgOld = $full_path_name;
        $obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_290_".$full_path_main;
        $obj_img->NewWidth = 290;
        //$obj_img->NewHeight = 58;
        if (!$obj_img->create_thumbnail_images()) { echo "error"; }

        //create a thumbnail 190 width
        $obj_img = new thumbnail_images();
        $obj_img->PathImgOld = $full_path_name;
        $obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_190_".$full_path_main;
        $obj_img->NewWidth = 190;
        //$obj_img->NewHeight = 58;
        if (!$obj_img->create_thumbnail_images()) { echo "error"; }

        //create a thumbnail 75 width
        $obj_img = new thumbnail_images();
        $obj_img->PathImgOld = $full_path_name;
        $obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_75_".$full_path_main;
        $obj_img->NewWidth = 75;
        //$obj_img->NewHeight = 58;
        if (!$obj_img->create_thumbnail_images()) { echo "error"; }

      }     
    } else {
      $full_path_main = "";
    }
    // ****************** MAIN IMAGE - End


    if(isset($_POST['isAvailable'])) {
      $isAvailable = 1;
    } else {
      $isAvailable = 0;
    }
    
    if(isset($_POST['isFeatured'])) {
      $isFeatured = 1;
    } else {
      $isFeatured = 0;
    }
    
    $_POST = sanitize($_POST);
    $products = $_POST;
    settype($products,'object');
    $products->isAvailable = $isAvailable;
    $products->isFeatured = $isFeatured;

    Products::updateProducts($products, $full_path_main, $full_path_pdf); 
    $success = "Product Successfully Added.";

    $updates = 'Add New Product.';
    AdminAction::addAdminAction($_SESSION['admin_name'],$updates);

    $product_id = Products::insertNewProduct();
  // } else {
  //   if (!isset($_REQUEST['Id'])) {
  //     $product_id = Products::insertNewProduct();
  //   } else {
  //     $product_id = $_REQUEST['Id'];
  //   }
  // }

// }
  }

} else {
  if (!isset($_REQUEST['Id'])) {
    $product_id = Products::insertNewProduct();
  } else {
    $product_id = $_REQUEST['Id'];
  }
}

// Create product folder
if (file_exists("../../../uploads/product_image/".$product_id)){
  // Do not re-create folder
} else {
  $oldumask = umask(0);
  mkdir("../../../uploads/product_image/".$product_id, 0777);
  umask($oldumask); 
}


if(isset($_REQUEST['cid'])) {
  $cid = $_REQUEST['cid'];
  Products::updateMainCategory($cid, $product_id);
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$cid;
}
if(isset($_REQUEST['scid'])) {
  $scid = $_REQUEST['scid'];
  Products::updateSubCategory($scid, $product_id);
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$cid.'&scid='.$scid;
}
if(isset($_REQUEST['sc2id'])) {
  $sc2id = $_REQUEST['sc2id'];
  Products::updateSubCategory2($sc2id, $product_id);
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$cid.'&scid='.$scid.'&sc2id='.$sc2id;
}
if(isset($_REQUEST['sc3id'])) {
  $sc3id = $_REQUEST['sc3id'];
  Products::updateSubCategory3($sc3id, $product_id);
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$cid.'&scid='.$scid.'&sc2id='.$sc2id.'&sc3id='.$sc3id;
}




if ($backLink=='') {
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
}


$products = Products::findProducts($product_id); 
if($products->fldProductsMainCategoryID > 0) {
    $backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$products->fldProductsMainCategoryID;
    if ($products->fldProductsMainCategoryID==15) { // Medical Parts
      $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
    }
}
if($products->fldProductsSubcategoryID > 0) {
    $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID;
}
if($products->fldProductsSubcategory2ID > 0) {
    $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID.'&sc2id='.$products->fldProductsSubcategory2ID;
}
if($products->fldProductsSubcategory3ID > 0) {
    $backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID.'&sc2id='.$products->fldProductsSubcategory2ID.'&sc3id='.$products->fldProductsSubcategory3ID;
}
if ($backLink=='') {
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/select2/select2.css" /> 
  <link rel="stylesheet" type="text/css" href="<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.css">

  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mod_product/js/tiny.mods.js"></script>
</head>

<body>
<? if(isset($success)) { ?>
	<div class="alert"> <?=$success?> </div>
<? } ?>
<div id="store_overview">
	<ul class="btn">
		<li><a href="<?=$backLink?>">Back</a></li>
	</ul>
</div>        
  <form id="store_page" action="<?=$ROOT_URL.'_admin/_modules/mod_product/products_create.php?cid='.$cid.'&scid='.$scid?>" method="post" enctype="multipart/form-data">
    <h3>ACM Simple E-Cart (Products)</h3>
    <fieldset style="width:1065px;">

      <? if(isset($msg_error)): ?>
        <div class="alert alert-error">
          <?=$msg_error?>
        </div>
      <? endif; ?>

      <legend>Products Panel</legend>
      <ul>
        <li>
          <label for="Id">Product ID: <?=$product_id?></label>
        </li>
        <li>
          <b>Note:</b> Select Category first before proceeding to Product name and other details.
        </li>
         <li>
          <label for="title">Category</label>
          <br>
          <select name="category_id" onChange="javascript:goToPage(options[selectedIndex].value)">
            <option value="products_create.php?Id=<?=$product_id?>&cid=0">Choose Category</option>
            <? 
              $category = Category::displayAllCategory(0);
              foreach($category as $categories) {
                if($categories->fldCategoryID == $products->fldProductsMainCategoryID) {
                  $selected = "selected='selected'";
                } else {
                  $selected = "";
                }
            ?>
                <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$categories->fldCategoryID?>" <?=$selected?> ><?=$categories->fldCategoryName?></option>
            <? } ?>
          </select>

          <?php
          if ($products->fldProductsMainCategoryID > 0) {
          $subcat = Subcategory::displayAll($products->fldProductsMainCategoryID);
          if (!empty($subcat)) {
            ?>
                  &#62; 
                  <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
                    <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=0">Choose Sub-Category</option>
                    <? 
                foreach($subcat as $subcatR) {
                  if($subcatR->fldSubcategoryID == $products->fldProductsSubcategoryID) {
                    $selected = "selected='selected'";
                  } else {
                    $selected = "";
                  }
              ?>
                    <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$subcatR->fldSubcategoryID?>" <?=$selected?> ><?=$subcatR->fldSubcategoryName?></option>
                <? } ?>
              </select>
              <?php
            }
          }
          ?>

          <?php
          if ($products->fldProductsSubcategoryID > 0) {
          $subcat2 = Subcategory::displayAll2($products->fldProductsSubcategoryID);
          if (!empty($subcat2)) {
                ?>
                &#62; 
                <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=0">Choose Sub-Category</option>
                  <? 
              foreach($subcat2 as $subcat2R) {
                if($subcat2R->fldSubcategoryID == $products->fldProductsSubcategory2ID) {
                  $selected = "selected='selected'";
                } else {
                  $selected = "";
                }
            ?>
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$subcat2R->fldSubcategoryID?>" <?=$selected?> ><?=$subcat2R->fldSubcategoryName?></option>
              <? } ?>
            </select>
            <?php 
            }
          }
          ?>

          <?php
          if ($products->fldProductsSubcategory2ID > 0) {
          $subcat3 = Subcategory::displayAll3($products->fldProductsSubcategory2ID);
          if (!empty($subcat3)) {
                ?>
                &#62; 
                <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$products->fldProductsSubcategory2ID?>&sc3id=0">Choose Sub-Category</option>
                  <? 
              foreach($subcat3 as $subcat3R) {
                if($subcat3R->fldSubcategoryID == $products->fldProductsSubcategory3ID) {
                  $selected = "selected='selected'";
                } else {
                  $selected = "";
                }
            ?>
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$products->fldProductsSubcategory2ID?>&sc3id=<?=$subcat3R->fldSubcategoryID?>" <?=$selected?> ><?=$subcat3R->fldSubcategoryName?></option>
              <? } ?>
            </select>
            <?php 
            }
          }
          ?>
        </li>


        <li>
          <label for="title">Product Name</label>
          <input type="text" id="title" name="name" placeholder="New Product">
        </li>
        <li>
          <label for="code">Product Code</label>
          <input type="text" id="code" name="code">
        </li>
        <li>
          <label for="url">URL</label>
          	<br>
			<small><strong>Example</strong>:
			<br>
			for http://icpamerica.com/products/<strong><i>single_board_computers/picmg_13/server_grade/
testprod001.html</i>
			<br>
			Enter &nbsp;&nbsp;&nbsp;<strong><i>single_board_computers/picmg_13/server_grade/
testprod001.html</i></strong>
			&nbsp;&nbsp;&nbsp;&nbsp;
			</strong></small><br>

          <input type="text" id="url" name="url">
        </li>
        <li>
          <label for="title">Product Price ($)</label>
            <br>Basic
            <input type="text" id="title" name="price1" > - <input type="text" id="title" name="price1text" value="">
            <br>Corporate
            <input type="text" id="title" name="price2" > - <input type="text" id="title" name="price2text" value="">
            <br>Customer Type 3
            <input type="text" id="title" name="price3" > - <input type="text" id="title" name="price3text" value="">
        </li>

       <li>
          <label for="title">Product Weight</label>
          <input type="text" id="title" name="weight">
        </li>
        <li>
          	<label for="cover">Upload Main Image
          		<br><small><strong>Formats</strong>: gif | jpg | JPG | jpeg | png &bull; 
					<strong>Max Size</strong>: 1MB &bull; 
					<strong>Dimension</strong>: 800px x 700px</small>
      		</label>
          <input type="file" id="cover" name="image">
        </li>
        <li>
          <label for="image2">Upload Alternate Image/s <small><strong>UP TO 2 IMAGES</strong></small>
              <br><small><strong>Formats</strong>: gif | jpg | JPG | jpeg | png &bull; 
					<strong>Max Size</strong>: 1MB &bull; 
					<strong>Dimension</strong>: 800px x 700px</small>
          </label>
            <div id="queue"></div>
            <input id="file_upload" name="file_upload" type="file" multiple>
        </li>

          <li>
            <label for="cover">Upload PDF 
                <br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
            </label>
            <div id="queue"></div>
            <input id="pdf_upload" name="pdf_upload" type="file" multiple>
            <?php
            if($products->fldProductsPDF != '') { 
             $pdf = $products->fldProductsPDF;
            ?>
            <a href='<?=$ROOT_URL?>uploads/product_pdf/<?=$pdf?>' target='_new'>Click to view PDF</a>
            <?php } ?>
        </li>

        <li>
          <label for="cover">Upload User Manual PDF 
              <br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
          </label>
          <div id="queue"></div>
          <input id="usermanual_upload" name="usermanual_upload" type="file" multiple>
              <?php
              if($products->fldProductsUserManual != '') { 
            $usermanual = $products->fldProductsUserManual;
            ?>
              <a href='<?=$ROOT_URL?>uploads/product_usermanual/<?=$usermanual?>' target='_new'>Click to view User Manual</a>
              <?php } ?>
        </li>

        <li>
          <label for="cover">Upload Quick Guide PDF 
              <br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
          </label>
          <div id="queue"></div>
          <input id="quickguide_upload" name="quickguide_upload" type="file" multiple>
              <?php
              if($products->fldProductsQuickGuide != '') { 
            $quickguide = $products->fldProductsQuickGuide;
            ?>
              <a href='<?=$ROOT_URL?>uploads/product_quickguide/<?=$quickguide?>' target='_new'>Click to view Quick Guide</a>
              <?php } ?>
        </li>

        <li>
          <label for="overview.editor">Product Overview
            <textarea cols="" rows="" id="overview.editor" name="overview"> </textarea>
          </label>
        </li>
        <li>
          <label for="features.editor">Product Features
            <textarea cols="" rows="" id="features.editor" name="features"> </textarea>
          </label>
        </li>
        <li>
          <label for="techspec.editor">Technical Specification
           <textarea rows="" cols="" id="techspec.editor" name="techspec"></textarea>
          </label>
        </li>
        <li>
          <label for="orderinfo.editor">Ordering Info
           <textarea rows="" cols="" id="orderinfo.editor" name="orderinfo"></textarea>
          </label>
        </li>

        <li>
          <label for="title">is Available?</label>
          <input type="checkbox" name="isAvailable" style="width:15px !important;">
        </li>
        <li>
          <label for="title">is New?</label>
          <input type="checkbox" name="isNew" value="1" style="width:15px !important;">
        </li>
        <li>
          <label for="title">is Special Deal?</label>
          <input type="checkbox" name="isSpecialDeal" value="1" style="width:15px !important;">
        </li>
         <li>
          <label for="position">Position</label>
          <input type="text" id="position" name="position">
        </li>
	</ul>
  </fieldset>

  <fieldset style="width:1065px;">
    <legend>Meta Information</legend>
    <ul>
      <li><label for="meta_title"> Meta Title </label>
        <textarea cols="80" rows="10" id="meta_title" name="meta_title"></textarea></li>
      <?php /* <li><label for="meta_keywords"> Meta Keywords </label>
        <textarea cols="80" rows="10" id="meta_keywords" name="meta_keywords"><?=stripslashes($page->fldPagesMetaKeywords)?></textarea></li>
      <li><label for="meta_description"> Meta Description </label>
        <textarea cols="80" rows="10" id="meta_description" name="meta_description"><?=stripslashes($page->fldPagesMetaDescription)?></textarea></li> */ ?>
    </ul>
  </fieldset>

    <ul class="submission">
          <input type="hidden" name="Id" value="<?=$product_id?>">
          <input type="hidden" name="cid" value="<?=$products->fldProductsMainCategoryID?>">
          <input type="hidden" name="scid" value="<?=$products->fldProductsSubcategoryID?>">
          <input type="hidden" name="sc2id" value="<?=$products->fldProductsSubcategory2ID?>">
          <input type="hidden" name="sc3id" value="<?=$products->fldProductsSubcategory3ID?>">

      <li><input type="submit" name="submit" value="Add Product"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/select2/select2.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="<?=$ROOT_URL?>_admin/plugins/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $('#pdf_upload-button span.uploadify-button-text').text('Select PDF');

  <?php $timestamp = time();?>
  $(function() {
    $('#file_upload').uploadify({
      'buttonText' : 'SELECT IMAGES',
      'formData'     : {
        'timestamp'   : '<?php echo $timestamp;?>',
        'token'       : '<?php echo md5('unique_salt' . $timestamp);?>',
        'product_id'  : '<?=$products->fldProductsId?>'
      },
      'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
      'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
        'onUploadSuccess' : function(file, data, response) {
            alert('File: ' + file.name + ' ' + data);
        }
    });
  });

  $(function() {
    $('#pdf_upload').uploadify({
      'buttonText' : 'SELECT PDF',
      'formData'     : {
        'timestamp'   : '<?php echo $timestamp;?>',
        'pdf'         : '1',
        'token'       : '<?php echo md5('unique_salt' . $timestamp);?>',
        'product_id'  : '<?=$products->fldProductsId?>'
      },
      'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
      'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
        'onUploadSuccess' : function(file, data, response) {
            alert('File: ' + file.name + ' ' + data);
        }
    });
  });

  $(function() {
    $('#usermanual_upload').uploadify({
      'buttonText' : 'SELECT User Manual',
      'formData'     : {
        'timestamp'   : '<?php echo $timestamp;?>',
        'pdf'       : '2',
        'token'       : '<?php echo md5('unique_salt' . $timestamp);?>',
            'product_id'    : '<?=$products->fldProductsId?>'
      },
      'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
      'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
        'onUploadSuccess' : function(file, data, response) {
            alert('File: ' + file.name + ' ' + data);
        }
    });
  });

  $(function() {
    $('#quickguide_upload').uploadify({
      'buttonText' : 'SELECT Quick Guide',
      'formData'     : {
        'timestamp'   : '<?php echo $timestamp;?>',
        'pdf'       : '3',
        'token'       : '<?php echo md5('unique_salt' . $timestamp);?>',
            'product_id'    : '<?=$products->fldProductsId?>'
      },
      'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
      'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
        'onUploadSuccess' : function(file, data, response) {
            alert('File: ' + file.name + ' ' + data);
        }
    });
  });

</script>

</body>
</html>