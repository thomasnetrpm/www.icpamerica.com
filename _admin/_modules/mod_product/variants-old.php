<?php 
session_start();
ob_start();

include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

include   "../../../classes/Products.php";
include   "../../../classes/Category.php";
include   "../../../classes/AdminAction.php";
include   "../../../classes/ProductVariant.php";

$cid    = trim($_REQUEST['cid']);
$scid   = trim($_REQUEST['scid']);
$sc2id  = trim($_REQUEST['sc2id']);

$product_id = trim($_REQUEST['pid']);

$prod = Products::findProducts($product_id);
$product_name = $prod->fldProductsName;


if(isset($_POST['add_variant'])) {
  $_POST = sanitize($_POST);
  $variant = $_POST;
  settype($variant,'object');
 
  ProductVariant::addVariant($variant); 
  $success = "Product Variant Successfully Added!";  

  $updates = 'Add new product option variant';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

if(isset($_REQUEST['delete'])) {
  ProductVariant::removeVariant($_REQUEST['delete']); 
  $success = "Product Variant Successfully Removed!";  

  $updates = 'Removed product variant';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);

  $link = $ROOT_URL.'_admin/_modules/mod_product/variants.php?cid='.$cid.'&scid='.$scid.'&sc2id='.$sc2id.'&pid='.$product_id.'';
  header("Location: ".$link);
  exit();
}

if(isset($_POST['add_option'])) {
  $_POST  = sanitize($_POST);
  $option = $_POST;
  settype($option,'object');
 
  ProductVariant::addProductOption($option); 
  $success = "New Product Option Successfully Added!";  

  $updates = 'Add new product option';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

if(isset($_POST['update_variant'])) {
  $_POST = sanitize($_POST);
  $variant = $_POST;
  settype($variant,'object');
 
  // ProductVariant::updateVariant($variant); 
  ProductVariant::updateAllVariantPrice($variant); 
  $success = "Product Variant Successfully updated!";  

  $updates = 'Updated product variant';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/tiny.mods.js"></script>
</head>

<body>
<? if(isset($success)) { ?>
	<div class="alert"> <?=$success?> </div>
<? } ?>
<div id="store_overview">
    	<ul class="btn">
  			<li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/dashboard.php?cid=<?=$cid?>&scid=<?=$scid?>&sc2id=<?=$sc2id?>">Back</a></li>
    	</ul>
</div>        
    <form id="store_page" action="" method="post" name="form_variant" enctype="multipart/form-data">
    <h3>ACM Simple E-Cart (Category)</h3>
   
    <fieldset style="width:800px;">
      <legend>Product Variants for <?=$product_name?></legend>
      <table class="variants">
        <tr>
          <td class="fix100">ID</td>
          <td>Ordering Information</td>
          <td>Part Number</td>
          <td>Price $</td>
          <td>Action</td>
        </tr>
        <tr>
          <td class="fix100">&nbsp;</td>
          <td class="variant-td">
            <select name="product_option_id" class="prod-select" required>
              <option value="">Select..</option>
              <?
              $option = ProductVariant::displayAllOptionByProductID($product_id);
              foreach($option as $opt) {
              ?>
                <option value="<?=$opt->product_option_id?>"><?=$opt->product_option?></option>
              <? } ?>
            </select>
          </td>
          <td  class="variant-td">
            <input type="text" name="variant" class="partnum-input" required>
          </td>
          <td  class="variant-td"><input type="text" name="variant_price" class="price-input" required></td>
          <td  class="variant-td">
            <input type="hidden" name="product_id" class="variant-btn" value="<?=$product_id?>">
            <input type="hidden" name="cid" class="variant-btn" value="<?=$cid?>">
            <input type="hidden" name="scid" class="variant-btn" value="<?=$scid?>">
            <input type="hidden" name="sc2id" class="variant-btn" value="<?=$sc2id?>">
            <input type="submit" name="add_variant" class="variant-btn" value="ADD">
          </td>
        </tr>
    </form>
        <tr>
          <td class="fix100">&nbsp;</td>
          <td  class="variant-td" colspan="3"><textarea name="variant_description"></textarea></td>
          <td  class="variant-td">&nbsp;</td>
        </tr>
        <?php
        // Get all variants grouped by options
        $variant = ProductVariant::findAllVariantByOption($product_id);
        foreach ($variant as $var) {
          $variant_id = $var->product_variant_id;
          $option_id  = $var->product_option_id;
          $variant_name = $var->product_variant;
          $description = $var->product_variant_description;
          $price      = $var->price;

          $option = ProductVariant::findOption($option_id);
          $option_name = $option->product_option;
          ?>
          <form method="POST">
          <tr>
            <td class="fix100 color-td"><?=$variant_id?></td>
            <td class="color-td"><?=$option_name?></td>
            <td class="color-td"><?=$variant_name?><br><small><?=substr($description, 0, 50)?>...</small></td>
            <td class="color-td">Price $ <input type="text" name="variant_price" value="<?=$price?>"></td>
            <td class="color-td">
              <input type="hidden" name="variant_id" value="<?=$variant_id?>">
              <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/variants.php?cid=<?=$cid?>&scid=<?=$scid?>&sc2id=<?=$sc2id?>&pid=<?=$product_id?>&delete=<?=$variant_id?>" title="Delete Variant" onClick="return confirm(&quot;Are you sure you want to completely remove this Product variant from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Product Variant.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif" width="16" height="16" alt="del" /> <small>REMOVE</small></a>
              <input type="hidden" name="variant_name" value="<?=$variant_name?>">
              <input type="submit" name="update_variant" value="UPDATE">
            </td>
          </tr>
          </form>
          <?php
        }
        ?>
      </table>
    </fieldset>

    <form id="store_page" action="" method="post" name="form_option" enctype="multipart/form-data">
    <fieldset style="width:800px;">
      <legend>Ordering Information</legend>
      <ul class="variant-list">
      	<li> <label class="float-left" for="title">Add Ordering Information</label></li>
        <li class="input-left">
          <input type="text" id="title" name="product_option" required></li>
          <li class="input-right"><input type="hidden" class="variant-btn" name="product_id" value="<?=$product_id?>">
          <input type="hidden" class="variant-btn" name="cid" value="<?=$cid?>">
          <input type="hidden" class="variant-btn" name="scid" value="<?=$scid?>">
          <input type="hidden" class="variant-btn" name="sc2id" value="<?=$sc2id?>">
          <input type="submit" class="variant-btn" name="add_option" value="Submit">
        </li>
      </ul>

      <ul class="variant-list">
        <li> <label class="float-left" for="title">&nbsp;</label></li>
        <li class="input-left">
          <?php
          $getOptions = ProductVariant::displayAllOptionByProductID($product_id);
          foreach ($getOptions as $opt) {
            $opt_id = $opt->product_option_id;
            $opt_name = $opt->product_option;
            ?>
            <?=$opt_name?><br>
            <?php
          }
          ?>
        </li>
      </ul>

    </fieldset>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>