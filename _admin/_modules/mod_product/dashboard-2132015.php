<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Products.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   


$categoryID     = trim($_REQUEST['cid']);
$subcategoryID  = trim($_REQUEST['scid']);
$subcategory2ID = trim($_REQUEST['sc2id']);
$subcategory3ID = trim($_REQUEST['sc3id']);

if ($subcategory2ID=='') { // 2 levels for example category > subcategory > products (no $subcategory2ID)
  $condition = "AND fldProductsSubcategoryID='$subcategoryID'";
  $addButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/products_create.php?cid='.$categoryID.'&scid='.$subcategoryID.'">Add New Product</a></li>';
  $backButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$categoryID.'&scid='.$subcategoryID.'">Back</a></li>';

  if ($categoryID==15) { // Medical Parts
    $condition = "";
    $addButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/products_create.php?cid='.$categoryID.'">Add New Product</a></li>';
    $backButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php">Back</a></li>';
  }
} else { 
  if ($subcategory3ID!='') { // 4 levels for example backplanes
    $condition = "AND fldProductsSubcategoryID='$subcategoryID' AND fldProductsSubcategory2ID='$subcategory2ID' AND fldProductsSubcategory3ID='$subcategory3ID'";
    $addButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/products_create.php?cid='.$categoryID.'&scid='.$subcategoryID.'&sc2id='.$subcategory2ID.'&sc3id='.$subcategory3ID.'">Add New Product</a></li>';
    $backButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/subcategory3_dashboard.php?sc2id='.$subcategory2ID.'">Back</a></li>';
  } else { // 3 levels for example category > subcategory > subcategory2 > products
    $condition = "AND fldProductsSubcategoryID='$subcategoryID' AND fldProductsSubcategory2ID='$subcategory2ID'";
    $addButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/products_create.php?cid='.$categoryID.'&scid='.$subcategoryID.'&sc2id='.$subcategory2ID.'">Add New Product</a></li>';
    $backButton = '<li><a href="'.$ROOT_URL.'_admin/_modules/mod_product/subcategory2_dashboard.php?cid='.$categoryID.'&scid='.$subcategoryID.'&sc2id='.$subcategory2ID.'">Back</a></li>';
  }
}

// DELETE Product
if(isset($_REQUEST['delete'])) {
  Products::deleteProducts($_REQUEST['delete']);

  $updates = 'Delete products content';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

if(isset($_REQUEST['submit'])) {
  // $condition = "AND fldProductsSubcategoryID='$subcategoryID' AND fldProductsSubcategory2ID='$subcategory2ID'";
  $product = Products::displayAllByCondition($categoryID, $condition);
  foreach($product as $prod) {
    $position = $_POST['position_'.$prod->fldProductsId];
    Products::updateProductsByPosition($position, $prod->fldProductsId);
  }
  $success = "Product Position Successfully Saved!";
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">
<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
	<div id="blog_overview">
    	<ul class="btn">
        <?=$addButton?>
        <?=$backButton?>
<!--        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_create.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>">Add New Product</a></li>
         <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/subcategory2_dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>">Back</a></li>
 -->      </ul>
  
   <?php
    // $condition = "AND fldProductsSubcategoryID='$subcategoryID' AND fldProductsSubcategory2ID='$subcategory2ID'";
    $count_record = Products::countAllByCondition($categoryID, $condition);
     
    if(!isset($_REQUEST['page'])) {
      $page = 1;
    } else {
      $page = $_GET[page];
    }
    $pagination = new Pagination;
    //for display
    $pg = $pagination->page_pagination(20, $count_record, $page, 20);
    $product = Products::displayAllByCondition2($categoryID, $condition, $pg[1]);
  ?>

   <h3>Product Overview</h3>

  <form method="post" action="">
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="100">Image</td> 
          <td width="410">Product Name</td>
          <td width="100">Order</td>   
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else {
					foreach($product as $prod) { 
            $productID      = $prod->fldProductsId;
            $productName    = $prod->fldProductsName;
            $productImage   = $prod->fldProductsImage;
            $productPosition= $prod->fldProductsPosition;
        ?>
            <tr id="<?=$productID?>_<?=$productPosition?>">
              <td><?=$productID?></td>
              <td>
                <? 
                if($prod->fldProductsImage!='') { ?>
                  <img src="<?=$ROOT_URL?>uploads/product_image/<?=$productID?>/_75_<?=$productImage?>" >
                <? } ?>
              </td>
              <td>
                <small>
                  <?=$prod->fldProductsName?><br><i><?=$prod->fldProductsCode?></i>
                  <br><i><?=$ROOT_URL.'products/'.$prod->fldProductsURL?></i>
                </small>
              </td>

              <td>
               	<input type="text" size="5" value="<?=$prod->fldProductsPosition?>" name="position_<?=$productID?>">
              </td>

              <td align="center">
                <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?Id=<?=$productID?>">
                <img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/modify.png" width="14" height="16" alt="mod" />
                </a>
                <?php $link = str_replace('delete=', '', basename($_SERVER["REQUEST_URI"])); ?>
                <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/<?=$link?>&delete=<?=$productID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Products from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Products.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif" width="16" height="16" alt="del" /></a><br>
                <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/variants.php?pid=<?=$productID?>&cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>">Edit Variant</a>
              </td>
<!--               <td align="center">
                <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?id=<?=$productID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/dashboard.php?delete=<?=$productID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Product from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Product.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif" width="16" height="16" alt="del" /></a>
              </td> -->
            </tr>
        <? } }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2">
              <!-- <input type="submit" name="submit" value="Update Positions"> -->
            </dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
     <input type="submit" name="submit" value="Update Position">
    </form>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>
<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.tablednd.js"></script>


<script type="text/javascript">
	Cufon.replace('h3');
	
	 $(document).ready(function(){	
	  		
			$('#page_manager').tableDnD({
				onDrop: function(table, row) {
					$.ajax({	
							type: "POST",
							// url: "<?=$ROOT_URL?>_admin/_modules/mod_product/update.php?action=update_position_drop_products&category_id=<?=$subcategoryID?>",
              url: "<?=$ROOT_URL?>_admin/_modules/mod_product/update.php?action=update_position_drop_products&cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>",
							data: $.tableDnD.serialize(),
							cache: false,
							success: function(){
								// document.location.href= "<?=$ROOT_URL?>_admin/_modules/mod_product/dashboard.php?scid=<?=$subcategoryID?>";														
                document.location.href= "<?=$ROOT_URL?>_admin/_modules/mod_product/dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcategory2ID?>";
							}
							
						});	
				}
			});
	});		
</script>

</body>
</html>