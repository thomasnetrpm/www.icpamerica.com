<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/Products.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

/*
//delete photo
if(isset($_REQUEST['delete'])) {
	Category::deleteCategory($_REQUEST['delete']);
	
		 $updates = 'Delete product category content';
  	  		AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
*/
$categoryID     = trim($_REQUEST['cid']);

$count_record = Subcategory::countSubcategory($categoryID);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="store_overview">
    	<ul class="btn">
		<?php
		if ($count_record > 0) {
			?>
        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_create.php?cid=<?=$categoryID?>">Add New Product</a></li>
			<?php
		}
		?>
        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/category_dashboard.php">Back</a></li>
    	</ul>
    <h3>Category Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="10%">ID</td>
          <td width="50%">Category Name</td>
          <td width="20%">Position</td>
          <td width="20%" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
       <?php
        $count_record = Subcategory::countSubcategory($categoryID);
				 
				if(!isset($_REQUEST['page']))
					{
						$page = 1;
					}
					else
					{
					$page = $_GET[page];
					}
					$pagination = new Pagination;
					//for display
					$pg = $pagination->page_pagination(20, $count_record, $page, 20);
          $subCat = Subcategory::displayAll($categoryID);
			?>
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  <td colspan="4" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                </tr>
            <? } else { 
					foreach($subCat as $sc) { // Main Category
            $subcategoryID  = $sc->fldSubcategoryID;
            $subcatName     = $sc->fldSubcategoryName;
            $subcatPosition = $sc->fldSubcategoryPosition;
			    ?>		
                <tr>
                  <td><?=$subcategoryID?></td>
                  <td><?=$subcatName?></td>
                  <td><?=$subcatPosition?></td>
                  <td align="center">
                    <? /* <a href="<?=$ROOT_URL?>_admin/modules_category/edit/<?=$categories->fldCategoryID?>/"><img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/modules_category/delete/<?=$categories->fldCategoryID?>/" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Category.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/delete.gif" width="16" height="16" alt="del" /></a> */ ?>
                  <?php
                  // Check for subcategory or list of products
                  $subCat2 = Subcategory::displayAll2($subcategoryID);
                  if (empty($subCat2)) {
                    // Check if products are available
                    $condition = "AND fldProductsSubcategoryID='$subcategoryID' ";
                    $product = Products::displayAllByCondition($categoryID, $condition);
                    if (empty($product)) {
                      echo 'Empty';
                    } else {
                      // echo 'View Products';
                      ?>
                      <a href="dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>">View Products</a>
                      <?php
                    }

                  } else {
                    ?>
                    <a href="subcategory2_dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>">View Subcategory</a>
                    <?php
                  }
                  ?>
                  </td>
                </tr>
        <? } } ?>
        
      </tbody>
      
      <tfoot>
      <th colspan="4" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2"></dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    <!-- /End Fetching Data Tables -->
    
    
  
  </div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_acm/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>