<?php 
ob_start();
session_start();

include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/Products.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');


$product_id = $_REQUEST['Id'];

if (file_exists("../../../uploads/product_image/".$product_id)){
	// Do not re-create folder
} else {
	$oldumask = umask(0);
	mkdir("../../../uploads/product_image/".$product_id, 0777);
	umask($oldumask); 
}

// Delete Alternate Image
if(isset($_GET['delImg'])) {
	Products::deleteProductImage($_GET['delImg']);

	$success = "Product Image Deleted!";
	$updates = 'Deleted product image';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}


// Delete Datasheet
if(isset($_GET['del_datasheet'])) {
	Products::deleteDatasheet($_GET['del_datasheet']);
	$success = "Datasheet (PDF) Deleted!";
	$updates = 'Deleted Datasheet (PDF)';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_edit.php?Id=".$_GET['del_datasheet']."");
	exit();
}
// Delete User Manual
if(isset($_GET['del_usermanual'])) {
	Products::deleteUsermanual($_GET['del_usermanual']);
	$success = "User Manual (PDF) Deleted!";
	$updates = 'Deleted User Manual (PDF)';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_edit.php?Id=".$_GET['del_usermanual']."");
	exit();
}
// Delete Quick Guide
if(isset($_GET['del_quickguide'])) {
	Products::deleteQuickguide($_GET['del_quickguide']);
	$success = "Quick Guide (PDF) Deleted!";
	$updates = 'Deleted Quick Guide (PDF)';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	header("Location: ".$ROOT_URL."_admin/_modules/mod_product/products_edit.php?Id=".$_GET['del_quickguide']."");
	exit();
}


if(isset($_GET['cid'])) {
	Products::updateMainCategory($_GET['cid'], $product_id);
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$_GET['cid'];

    if ($_GET['cid']==15) { // Medical Parts
      $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
    }
}
if(isset($_GET['scid'])) {
	Products::updateSubCategory($_GET['scid'], $product_id);
  	// $backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory2_dashboard.php?cid='.$_GET['cid'].'&scid='.$_GET['scid'];
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$_GET['cid'].'&scid='.$_GET['scid'];
}
if(isset($_GET['sc2id'])) {
	Products::updateSubCategory2($_GET['sc2id'], $product_id);
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$_GET['cid'].'&scid='.$_GET['scid'].'&sc2id='.$_GET['sc2id'];
}
if(isset($_GET['sc3id'])) {
	Products::updateSubCategory3($_GET['sc3id'], $product_id);
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$_GET['cid'].'&scid='.$_GET['scid'].'&sc2id='.$_GET['sc2id'].'&sc3id='.$_GET['sc3id'];
}


if(isset($_POST['submit'])) {

	include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	// ****************** MAIN IMAGE - Start
	if($_FILES['image']['name'] != '') {

    	$my_upload->upload_dir = "../../../uploads/product_image/".$product_id.""; // "files" is the folder for the uploaded files (you have to create this folder)
		// $my_upload->upload_dir = "../../../uploads/product_image/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		$my_upload->rename_file = true;

		$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
		$my_upload->the_file = $_FILES['image']['name'];
		$my_upload->http_error = $_FILES['image']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		$new_name = 'main_img_'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path_main = $my_upload->file_copy;
			// ... or do something like insert the filename to the database

			// Resize to 590 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_590_".$full_path_main;
			$obj_img->NewWidth = 590;
			// $obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }

			//create a thumbnail 290 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_290_".$full_path_main;
			$obj_img->NewWidth = 290;
			//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }

			//create a thumbnail 190 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_190_".$full_path_main;
			$obj_img->NewWidth = 190;
			//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }

			//create a thumbnail 75 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/product_image/".$product_id."/_75_".$full_path_main;
			$obj_img->NewWidth = 75;
			//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }

		}			
	} else {
		$full_path_main = "";
	}
	// ****************** MAIN IMAGE - End


	if(isset($_POST['isAvailable'])) {
		$isAvailable = 1;
	} else {
		$isAvailable = 0;
	}
	
	if(isset($_POST['isFeatured'])) {
		$isFeatured = 1;
	} else {
		$isFeatured = 0;
	}
	
	$_POST = sanitize($_POST);
	$products = $_POST;
	settype($products,'object');
	$products->isAvailable = $isAvailable;
	$products->isFeatured = $isFeatured;

	Products::updateProducts($products, $full_path_main, $full_path_pdf); 
	$success = "Product Successfully Changed!";

	$updates = 'Update products content';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

$products = Products::findProducts($product_id); 

if($products->fldProductsMainCategoryID > 0) {
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/subcategory_dashboard.php?cid='.$products->fldProductsMainCategoryID;

    if ($products->fldProductsMainCategoryID==15) { // Medical Parts
      $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
    }
}
if($products->fldProductsSubcategoryID > 0) {
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID;
}
if($products->fldProductsSubcategory2ID > 0) {
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID.'&sc2id='.$products->fldProductsSubcategory2ID;
}
if($products->fldProductsSubcategory3ID > 0) {
  	$backLink = $ROOT_URL.'_admin/_modules/mod_product/dashboard.php?cid='.$products->fldProductsMainCategoryID.'&scid='.$products->fldProductsSubcategoryID.'&sc2id='.$products->fldProductsSubcategory2ID.'&sc3id='.$products->fldProductsSubcategory3ID;
}
if ($backLink=='') {
  $backLink = $ROOT_URL.'_admin/_modules/mod_product/category_dashboard.php';
}
?>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/select2/select2.css" /> 
  <link rel="stylesheet" type="text/css" href="<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.css">

  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mod_product/js/tiny.mods.js"></script>
</head>

<body>
<? if(isset($success)) { ?>
	<div class="alert"> <?=$success?> </div>
<? } ?>
<div id="store_overview">
	<ul class="btn">
		<li><a href="<?=$backLink?>">Back</a></li>
	</ul>
</div>        
  <form id="store_page" action="<?=$ROOT_URL.'_admin/_modules/mod_product/products_edit.php?Id='.$product_id?>" method="post" enctype="multipart/form-data">
    <h3>ACM Simple E-Cart (Products)</h3>
    <fieldset style="width:1065px;">
      <legend>Products Panel</legend>
      <ul>
        <li>
          <b>Note:</b> Select Category first before proceeding to Product name and other details.
        </li>
         <li>
          <label for="title">Category</label>
          <br>
          <select name="category_id" onChange="javascript:goToPage(options[selectedIndex].value)">
          	<option value="products_edit.php?Id=<?=$product_id?>&cid=0">Choose Category</option>
          	<? 
				$category = Category::displayAllCategory(0);
				foreach($category as $categories) {
					if($categories->fldCategoryID == $products->fldProductsMainCategoryID) {
						$selected = "selected='selected'";
					} else {
						$selected = "";
					}
			?>
            		<option value="products_edit.php?Id=<?=$product_id?>&cid=<?=$categories->fldCategoryID?>" <?=$selected?> ><?=$categories->fldCategoryName?></option>
            <? } ?>
          </select>

          <?php
          if ($products->fldProductsMainCategoryID > 0) {
			$subcat = Subcategory::displayAll($products->fldProductsMainCategoryID);
			if (!empty($subcat)) {
				?>
	          	&#62; 
		          <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
		          	<option value="products_edit.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=0">Choose Sub-Category</option>
		          	<? 
						foreach($subcat as $subcatR) {
							if($subcatR->fldSubcategoryID == $products->fldProductsSubcategoryID) {
								$selected = "selected='selected'";
							} else {
								$selected = "";
							}
					?>
		            		<option value="products_edit.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$subcatR->fldSubcategoryID?>" <?=$selected?> ><?=$subcatR->fldSubcategoryName?></option>
		            <? } ?>
		          </select>
		          <?php
			}
          }
          ?>

          <?php
          if ($products->fldProductsSubcategoryID > 0) {
			$subcat2 = Subcategory::displayAll2($products->fldProductsSubcategoryID);
			if (!empty($subcat2)) {
          	?>
          	&#62; 
	          <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
	          	<option value="products_edit.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=0">Choose Sub-Category</option>
	          	<? 
					foreach($subcat2 as $subcat2R) {
						if($subcat2R->fldSubcategoryID == $products->fldProductsSubcategory2ID) {
							$selected = "selected='selected'";
						} else {
							$selected = "";
						}
				?>
	            		<option value="products_edit.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$subcat2R->fldSubcategoryID?>" <?=$selected?> ><?=$subcat2R->fldSubcategoryName?></option>
	            <? } ?>
	          </select>
          	<?php	
          	}
          }
          ?>

          <?php
          if ($products->fldProductsSubcategory2ID > 0) {
          $subcat3 = Subcategory::displayAll3($products->fldProductsSubcategory2ID);
          if (!empty($subcat3)) {
                ?>
                &#62; 
                <select name="subcat_id" onChange="javascript:goToPage(options[selectedIndex].value)">
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$products->fldProductsSubcategory2ID?>&sc3id=0">Choose Sub-Category</option>
                  <? 
              foreach($subcat3 as $subcat3R) {
                if($subcat3R->fldSubcategoryID == $products->fldProductsSubcategory3ID) {
                  $selected = "selected='selected'";
                } else {
                  $selected = "";
                }
            ?>
                  <option value="products_create.php?Id=<?=$product_id?>&cid=<?=$products->fldProductsMainCategoryID?>&scid=<?=$products->fldProductsSubcategoryID?>&sc2id=<?=$products->fldProductsSubcategory2ID?>&sc3id=<?=$subcat3R->fldSubcategoryID?>" <?=$selected?> ><?=$subcat3R->fldSubcategoryName?></option>
              <? } ?>
            </select>
            <?php 
            }
          }
          ?>

        </li>

        <li>
          <label for="title">Product Name</label>
          <input type="text" id="title" name="name" value="<?=htmlspecialchars($products->fldProductsName)?>">
        </li>
        <li>
          <label for="code">Product Code</label>
          <input type="text" id="code" name="code" value="<?=htmlspecialchars($products->fldProductsCode)?>">
        </li>
       	<li>
          <label for="url">URL</label>
          	<br>
			<small><strong>Example</strong>:
			<br>
			for http://icpamerica.com/products/<strong><i>single_board_computers/picmg_13/server_grade/
testprod001.html</i>
			<br>
			Enter &nbsp;&nbsp;&nbsp;<strong><i>single_board_computers/picmg_13/server_grade/
testprod001.html</i></strong>
			&nbsp;&nbsp;&nbsp;&nbsp;
			</strong></small><br>

          <input type="text" id="url" name="url" value="<?=htmlspecialchars($products->fldProductsURL)?>">
        </li>

         <li>
          <label for="title">Product Price ($)</label>
          	<br>Basic
          	<input type="text" id="title" name="price1" value="<?=$products->fldProductsPrice1?>"> - <input type="text" id="title" name="price1text" value="<?=$products->fldProductsPrice1Text?>">
          	<br>Corporate
          	<input type="text" id="title" name="price2" value="<?=$products->fldProductsPrice2?>"> - <input type="text" id="title" name="price2text" value="<?=$products->fldProductsPrice2Text?>">
          	<br>Customer Type 3
          	<input type="text" id="title" name="price3" value="<?=$products->fldProductsPrice3?>"> - <input type="text" id="title" name="price3text" value="<?=$products->fldProductsPrice3Text?>">
        </li>
        <li>
          <label for="title">Product Weight</label>
          <input type="text" id="title" name="weight" value="<?=$products->fldProductsWeight?>">
        </li>
        
        <li>
          <label for="cover">Upload Image Main Image
	  		<br><small><strong>Formats</strong>: gif | jpg | JPG | jpeg | png &bull; 
				<strong>Max Size</strong>: 1MB &bull;
				<strong>Dimension</strong>: 800px x 700px</small>
          </label>
          <input type="file" id="cover" name="image">
          <? if($products->fldProductsImage != '') { 
		  		$image = $products->fldProductsImage;
		  ?>
          		<img src="<?=$ROOT_URL?>uploads/product_image/<?=$product_id?>/_75_<?=$image?>" >
          <? } ?>
        </li>

        <li>
          <label for="image2">Upload Alternate Image/s <small><strong>UP TO 2 IMAGES</strong></small></label>
	  		<br><small><strong>Formats</strong>: gif | jpg | JPG | jpeg | png &bull; 
				<strong>Max Size</strong>: 1MB &bull;
				<strong>Dimension</strong>: 800px x 700px</small>
		<div id="queue"></div>
		<input id="file_upload" name="file_upload" type="file" multiple="true">
		<?php
		$images = Products::displayAllProductImages($product_id);
		foreach ($images as $imgR) {
			$otherImages = $imgR->fldProductImage;
			$imgID = $imgR->fldProductImageID;
			?>
        	<img src="<?=$ROOT_URL?>uploads/product_image/<?=$product_id?>/_75_<?=$otherImages?>" >
			<a href='<?=$_SERVER['PHP_SELF']?>?delImg=<?=$imgID?>&Id=<?=$product_id?>' onClick="return confirm(&quot;Are you sure you want to completely remove this Product Image from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Image.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif"></a>			
			<?php
		}
		?>
        </li>

        <li>
          <label for="cover">Upload Datasheet PDF 
          		<br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
          </label>
		<div id="queue"></div>
		<input id="pdf_upload" name="pdf_upload" type="file" multiple="false">
        <?php
        if($products->fldProductsPDF != '') { 
			$pdf = $products->fldProductsPDF;
			?>
 		  	<a href='<?=$ROOT_URL?>uploads/product_pdf/<?=$pdf?>' target='_new'>Click to view Datasheet</a>   --   
 		  	<a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?del_datasheet=<?=$products->fldProductsId?>" title="Delete Datasheet" onClick="return confirm(&quot;Are you sure you want to completely remove this Datasheet from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Datasheet.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif"><b><small><font color="red"> DELETE</font></small></b></a>
        <?php } ?>
        </li>

        <li>
          <label for="cover">Upload User Manual PDF 
          		<br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
          </label>
		<div id="queue"></div>
		<input id="usermanual_upload" name="usermanual_upload" type="file" multiple="false">
        <?php
        if($products->fldProductsUserManual != '') { 
			$usermanual = $products->fldProductsUserManual;
			?>
 		  	<a href='<?=$ROOT_URL?>uploads/product_usermanual/<?=$usermanual?>' target='_new'>Click to view User Manual</a>   --   
 		  	<a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?del_usermanual=<?=$products->fldProductsId?>" title="Delete User Manual" onClick="return confirm(&quot;Are you sure you want to completely remove this User Manual from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the User Manual.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif"><b><small><font color="red"> DELETE</font></small></b></a>
        <?php } ?>
        </li>

        <li>
          <label for="cover">Upload Quick Guide PDF 
          		<br><small><strong>Formats</strong>: pdf &bull; <strong>Max Size</strong>: 2MB</small>
          </label>
		<div id="queue"></div>
		<input id="quickguide_upload" name="quickguide_upload" type="file" multiple="false">
        <?php
        if($products->fldProductsQuickGuide != '') { 
			$quickguide = $products->fldProductsQuickGuide;
			?>
 		  	<a href='<?=$ROOT_URL?>uploads/product_quickguide/<?=$quickguide?>' target='_new'>Click to view Quick Guide</a>   --   
 		  	<a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?del_quickguide=<?=$products->fldProductsId?>" title="Delete Quick Guide" onClick="return confirm(&quot;Are you sure you want to completely remove this Quick Guide from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Quick Guide.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif"><b><small><font color="red"> DELETE</font></small></b></a>
        <?php } ?>
        </li>

        <li>
          <label for="overview.editor">Overview
          <textarea cols="" rows="" id="overview.editor" name="overview"><?=stripslashes($products->fldProductsOverview)?></textarea>
          </label>
        </li>
        <li>
          <label for="features.editor">Features
          <textarea cols="" rows="" id="features.editor" name="features"><?=stripslashes($products->fldProductsFeatures)?></textarea>
          </label>
        </li>
         <li>
          <label for="techspec.editor">Technical Specification</label>
           <textarea cols="" rows="" id="techspec.editor" name="techspec"><?=stripslashes($products->fldProductsTechspecs)?></textarea>
        </li>
         <li>
          <label for="orderinfo.editor">Ordering Info</label>
           <textarea cols="" rows="" id="orderinfo.editor" name="orderinfo"><?=stripslashes($products->fldProductsOrderinfo)?></textarea>
        </li>
        
        <li>
          <label for="title">is Available?</label>
          <?
		  		if($products->fldProductsAvailability ==1) {
					$checked = "checked";
				} else {
					$checked = "";
				}
		  ?>
          <input type="checkbox" name="isAvailable" style="width:15px !important;" <?=$checked?>>
        </li>

        <li>
          <label for="title">is New?</label>
          <?
	  		if ($products->isNew ==1) { $checked = "checked"; }
	  			else { $checked = ""; }
		  ?>
          <input type="checkbox" name="isNew" value="1" style="width:15px !important;" <?=$checked?>>
        </li>
        <li>
          <label for="title">is Special Deal?</label>
          <?
	  		if ($products->isSpecialDeal ==1) { $checked = "checked"; }
	  			else { $checked = ""; }
		  ?>
          <input type="checkbox" name="isSpecialDeal" value="1" style="width:15px !important;" <?=$checked?>>
        </li>

        <li>
          <label for="position">Position</label>
          <input type="text" id="position" name="position" value="<?=stripslashes($products->fldProductsPosition)?>">
        </li>
         
      </ul>
    </fieldset>

	<fieldset style="width:1065px;">
	<legend>Meta Information</legend>
	<ul>
		<li><label for="meta_title"> Meta Title </label>
			<textarea cols="80" rows="10" id="meta_title" name="meta_title"><?=stripslashes($products->fldProductMetaTitle)?></textarea></li>
		<?php /* <li><label for="meta_keywords"> Meta Keywords </label>
			<textarea cols="80" rows="10" id="meta_keywords" name="meta_keywords"><?=stripslashes($page->fldPagesMetaKeywords)?></textarea></li>
		<li><label for="meta_description"> Meta Description </label>
			<textarea cols="80" rows="10" id="meta_description" name="meta_description"><?=stripslashes($page->fldPagesMetaDescription)?></textarea></li> */ ?>
	</ul>
	</fieldset>

    <ul class="submission">
    <input type="hidden" name="Id" value="<?=$products->fldProductsId?>">
      <li><input type="submit" name="submit" value="Update Product"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/select2/select2.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="<?=$ROOT_URL?>_admin/plugins/jquery-ui/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?=$ROOT_URL?>_admin/plugins/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$('#pdf_upload-button span.uploadify-button-text').text('Select PDF');

	<?php $timestamp = time();?>
	$(function() {
		$('#file_upload').uploadify({
			'buttonText' : 'SELECT IMAGES',
			'formData'     : {
				'timestamp' 	: '<?php echo $timestamp;?>',
				'token'     	: '<?php echo md5('unique_salt' . $timestamp);?>',
      			'product_id'    : '<?=$products->fldProductsId?>'
			},
			'swf'      : 'http://icpamerica.com/_admin/plugins/uploadify/uploadify.swf',
			'uploader' : 'http://icpamerica.com/_admin/plugins/uploadify/uploadify.php',
		    'onUploadSuccess' : function(file, data, response) {
		        alert('File: ' + file.name + ' ' + data);
		    }
		});
	});

	$(function() {
		$('#pdf_upload').uploadify({
			'buttonText' : 'SELECT Datasheet',
			'formData'     : {
				'timestamp' 	: '<?php echo $timestamp;?>',
				'pdf' 			: '1',
 				'token'     	: '<?php echo md5('unique_salt' . $timestamp);?>',
      			'product_id'    : '<?=$products->fldProductsId?>'
			},
			'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
			'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
		    'onUploadSuccess' : function(file, data, response) {
		        alert('File: ' + file.name + ' ' + data);
		    }
		});
	});

	$(function() {
		$('#usermanual_upload').uploadify({
			'buttonText' : 'SELECT User Manual',
			'formData'     : {
				'timestamp' 	: '<?php echo $timestamp;?>',
				'pdf' 			: '2',
 				'token'     	: '<?php echo md5('unique_salt' . $timestamp);?>',
      			'product_id'    : '<?=$products->fldProductsId?>'
			},
			'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
			'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
		    'onUploadSuccess' : function(file, data, response) {
		        alert('File: ' + file.name + ' ' + data);
		    }
		});
	});

	$(function() {
		$('#quickguide_upload').uploadify({
			'buttonText' : 'SELECT Quick Guide',
			'formData'     : {
				'timestamp' 	: '<?php echo $timestamp;?>',
				'pdf' 			: '3',
 				'token'     	: '<?php echo md5('unique_salt' . $timestamp);?>',
      			'product_id'    : '<?=$products->fldProductsId?>'
			},
			'swf'      : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.swf',
			'uploader' : '<?=$ROOT_URL?>_admin/plugins/uploadify/uploadify.php',
		    'onUploadSuccess' : function(file, data, response) {
		        alert('File: ' + file.name + ' ' + data);
		    }
		});
	});

</script>

</body>
</html>