<?php 
session_start();
ob_start();

include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

include   "../../../classes/Products.php";
include   "../../../classes/Category.php";
include   "../../../classes/AdminAction.php";
include   "../../../classes/ProductVariant.php";


if(isset($_REQUEST['delOrdInf'])) {
  ProductVariant::removeOrderingInformation($_REQUEST['delOrdInf']); 
  $success = "Ordering Information Successfully Removed!";  

  $updates = 'Removed ordering information';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);

  $link = $ROOT_URL.'_admin/_modules/mod_product/variants.php?cid='.$cid.'&scid='.$scid.'&sc2id='.$sc2id.'&pid='.$product_id.'';
  header("Location: ".$link);
  exit();
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/tiny.mods.js"></script>
</head>

<body>
<? if(isset($success)) { ?>
	<div class="alert"> <?=$success?> </div>
<? } ?>
<div id="store_overview">
    	<ul class="btn">
  			<li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/dashboard.php?cid=<?=$cid?>&scid=<?=$scid?>&sc2id=<?=$sc2id?>">Back</a></li>
    	</ul>
</div>        
    <form id="store_page" action="" method="post" name="form_variant" enctype="multipart/form-data">
    <h3>ACM Simple E-Cart (Category)</h3>
   

  </form>

      <form id="store_page" action="" method="post" name="form_option" enctype="multipart/form-data">
    <fieldset style="width:800px;">
    </form>

        <?php
        $product = Products::displayAll();
        foreach ($product as $prod) {
          $product_id = $prod->fldProductsId;
          $product_name = $prod->fldProductsName;

          $ordering_info = ProductVariant::displayAllOptionByProductID($product_id);
          foreach ($ordering_info as $ord) {
            $ord_id = $ord->product_option_id;
            $ord_name = $ord->product_option;

            $countProdOption = ProductVariant::countProductOption($ord_id);
            // echo $countProdOption.'<br>';
            if ($countProdOption==0) {
              echo "DELETE FROM cms_tblProductOption WHERE product_option_id='".$ord_id."';";
              echo '<br>';
              ?>
              Product Name: <?=$product_id.': '.$product_name?> -->
              Ordering Info: <?=$ord_id.': '.$ord_name?>
              <br>
              <?php
            }
          }

        }
        ?>
    </fieldset>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>