<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    

include   "../../../classes/Cart.php";
include   "../../../classes/Products.php";
include   "../../../classes/Client.php";
include   "../../../classes/Coupon.php";
include   "../../../classes/Tax.php";
include   "../../../classes/Shipping.php";
include   "../../../classes/Billing.php";
include   "../../../classes/Tracking.php";
include   "../../../classes/ProductVariant.php";
//include   "../../../classes/ShippingRate.php";

include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

// UPDATE Transaction Number
if (isset($_POST['updateTracking'])) {
  
  $_POST = sanitize($_POST);
  $trk = $_POST;
  settype($trk,'object');
  $update = Tracking::updateTracking($trk);

  if ($update==true) {
    $message = 'Tracking Updated.';
    AdminAction::addAdminAction($_SESSION['admin_name'], $message);
  } else {
    $message = 'Error in updating Tracking.';
  }
}


$myCart = Cart::findCartByOrderID($_REQUEST['id']);
$clients = Client::findClient($myCart->fldCartClientID);
$clienttype = $clients->fldClientType;

$shipping = Shipping::findShippingClient($myCart->fldCartClientID);
$billing = Billing::findBillingClient($myCart->fldCartClientID);

// $shipping = Shipping::findShippingClient($clients->fldClientID);
// $billing = Billing::findBillingClient($clients->fldClientID);
// $shippingRate = ShippingRate::findShippingRateByOrderCode($_REQUEST['id']);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="blog_overview">
    	<ul class="btn">
		  <li><a href="<?=$ROOT_URL?>_admin/_modules/mods_order/dashboard.php">Back</a></li>
        </ul>
  
    <h3>Order Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td colspan="5" style="padding:5px 5px">Order No: <?=$myCart->fldCartOrderNo?><br>
            Order Date: <?=date('F d, Y',strtotime($myCart->fldCartDate))?>&nbsp;&nbsp;&nbsp;&nbsp;<?=$myCart->fldCartTime?><br>
<?php /*             Transaction No : <?//=$myCart->fldCartTransactionNo?> */ ?>
            <br>
            <form name='trackingForm' method='POST' action='' >
            Status : 
            <select name="status">
              <option value="new" <?=($myCart->fldCartStatus=='new')? 'SELECTED': '';?>>NEW</option>
              <option value="in process" <?=($myCart->fldCartStatus=='in process')? 'SELECTED': '';?>>IN PROCESS</option>
              <option value="completed" <?=($myCart->fldCartStatus=='completed')? 'SELECTED': '';?>>COMPLETED</option>
            </select>
            <br>
            Tracking No : <input type='text' name='trackingNo' value='<?=$myCart->fldCartTrackingNumber?>'> 
            <input type='hidden' name='id' value='<?=$myCart->fldCartOrderNo?>' >
            <br>
            <input type='submit' name='updateTracking' value='UPDATE' > <?=$message?>
            </form>
            </td>
        </tr>
        <tr class="headers">
          <td colspan="5">
          		<table width="100%" border="0">
                	<tr>
                    	<td width="50%">
                        	<table border="0" width="100%">
                            	<tr>
                                	<td height="25" colspan="3" style="padding:5px 5px"><strong>Billing Information</strong></td>
                                </tr>
                                <tr>
                                	<td width="29%" height="25" style="padding:5px 5px">Name</td>
                                    <td width="2%" height="25" style="padding:5px 5px">:</td>
                                    <td width="69%" height="25" style="padding:5px 5px"><?=$billing->fldBillingFirstName . ' ' . $billing->fldBillingLastname?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Address</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$billing->fldBillingAddress . ' ' . $billing->fldBillingAddress1 . ' ' . $billing->fldBillingCity . ' ' . $billing->fldBillingState . ' ' . $billing->fldBillingCountry . ' ' .$billing->fldBillingZip?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Email Address</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$billing->fldBillingEmail?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Contact No</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$billing->fldBillingPhoneNo?></td>
                                </tr>
                            </table>
                            
                        </td>
                        <td width="50%">
                        	<table border="0" width="100%">
                            	<tr>
                                	<td height="25" colspan="3" style="padding:5px 5px"><strong>Shipping Information</strong></td>
                                </tr>
                                <tr>
                                	<td width="33%" height="25" style="padding:5px 5px">Name</td>
                                    <td width="2%" height="25" style="padding:5px 5px">:</td>
                                    <td width="65%" height="25" style="padding:5px 5px"><?=$shipping->fldShippingFirstName . ' ' . $shipping->fldShippingLastname?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Address</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$shipping->fldShippingAddress . ' ' . $shipping->fldShippingAddress1 . ' ' . $shipping->fldShippingCity . ' ' . $shipping->fldShippingState . ' ' . $shipping->fldShippingCountry . ' ' . $shipping->fldShippingZip?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Email Address</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$shipping->fldShippingEmail?></td>
                                </tr>
                                <tr>
                                	<td height="25" style="padding:5px 5px">Contact No</td>
                                    <td height="25" style="padding:5px 5px">:</td>
                                    <td height="25" style="padding:5px 5px"><?=$shipping->fldShippingPhoneNo?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
          </td>          
        </tr>
        <tr class="headers">
          <td></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="headers">         
          <td width="250"></td>   
          <td width="970" style="padding:5px 5px">Product Name</td>        
          <td width="210" style="padding:5px 5px">Quantity</td>
          <td width="210" style="padding:5px 5px">Price</td>   
          <td width="410" style="padding:5px 5px">Amount</td>                   
        </tr>
      </thead>
    
      <tbody id="alter_rows">
        <?php
				$order_id = $_REQUEST['id'];
				 $count_record=Cart::counCartByOrderNo($order_id);
				 
				if(!isset($_REQUEST['page']))
					{
						$page = 1;
					}
					else
					{
					$page = $_GET[page];
					}
					$pagination = new Pagination;
					//for display
					$pg = $pagination->page_pagination(20, $count_record, $page, 20);
					$cart=Cart::displayAllByOrders($order_id);
			?>
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Orders Found</td>
                  </tr>
            <? } else { 
					foreach($cart as $carts) { 
					  $products = Products::findProducts($carts->fldCartProductID);
					  $total = $carts->fldCartQuantity  * $carts->fldCartProductPrice;
					  $subtotal = $subtotal + $total;

            // Product Option + Variant
            $variant_id = $carts->fldCartProductVariant;
            if (!empty($variant_id)) {
              $variant = ProductVariant::findVariant($variant_id);
              $variant_name = $variant->product_variant;
              $option_id = $variant->product_option_id;
              $option = ProductVariant::findOption($option_id);
              // $option_name = $option->product_option;
              $option_name = ($option->product_option=="Product Options")? "": $option->product_option.' | ';
            }
			?>
      
              <tr>
                <td style="padding:5px 5px">
                <?php // Show no image to accessories
                if (strtolower($option->product_option)!='accessories') {
                  ?>
                  <img src="<?=$ROOT_URL?>uploads/product_image/<?=$carts->fldCartProductID?>/_75_<?=$products->fldProductsImage?>" width="75">
                  <?
                }
                ?>
                </td>

                <td style="padding:5px 5px">
                  <?=stripslashes($carts->fldCartProductName)?>
                  <?php // if ($productPriceText) { echo "<br><b>** ".$productPriceText." **</b>"; }?>
                  <?php if ($variant_id) { 
                        echo "<br>".$option_name.$variant_name." "; 
                      if ($carts->fldCartProductPriceText != "") {
                        echo "<b>** ".$carts->fldCartProductPriceText." **</b>";
                      }
                      if ($clienttype==2) {
                        $corporate_price_text = $products->fldProductsPrice2Text;
                        echo "<br>** ".$corporate_price_text." **";
                      }

                  }?>
                </td>
                <td style="padding:5px 5px"><?=$carts->fldCartQuantity?></td>
                <td style="padding:5px 5px">$<?=number_format($carts->fldCartProductPrice,2)?></td>
                <td style="padding:5px 5px">$<?=number_format($total,2)?></td>
              </tr>
        <? } } ?>

          <?php
          $shipping = 0;
          $coupon   = 0;

          $tax = Tax::findTaxByOrderCode($order_id);
          $taxAmount  = $tax->fldTaxValue;

          $coup = Cart::getCartXCoupon($order_id);
          $couponAmount = $coup->fldCXCCouponAmount;
          // $couponCode = $coup->fldCXCCoupon;
          $couponCode = ($couponAmount > 0)? $coup->fldCXCCoupon : '';

          $subtotalBeforeTax = $subtotal + $shipping;
          $grandtotal = $subtotalBeforeTax + $taxAmount - $couponAmount;
          ?>
          <tr><td colspan="5" align="right" style="padding:5px 5px"><strong>Sub-Total : $<?=number_format($subtotal,2)?></strong></td></tr>
          <tr><td colspan="5" align="right" style="padding:5px 5px"><strong>Tax : $<?=($taxAmount > 0)? number_format($taxAmount,2): '0.00';?></strong></td></tr>
          <tr><td colspan="5" align="right" style="padding:5px 5px"><strong>Coupon (<?=$couponCode?>) : $<?=number_format($couponAmount,2)?></strong></td></tr>
        	<tr><td colspan="5" align="right" style="padding:5px 5px"><strong>GRAND TOTAL : $<?=number_format($grandtotal,2)?></strong></td></tr>
      </tbody>
      
     
    </table>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>