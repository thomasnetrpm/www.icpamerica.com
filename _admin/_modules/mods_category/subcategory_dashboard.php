<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

$categoryID = $_REQUEST['cid'];
$cat = Category::findCategory($categoryID);
$catName = $cat->fldCategoryName;


// Delete Sub-category
if(isset($_REQUEST['submit'])) {
  $subcat2 = Subcategory::displayAll($categoryID);
  foreach($subcat2 as $subcat2R) {
    $position = $_POST['position_'.$subcat2R->fldSubcategoryID];
    Subcategory::updateSubcategoryByPosition($position, $subcat2R->fldSubcategoryID);
  }
  $success = "Subcategory Position Successfully Saved!";
}

if(isset($_REQUEST['delete'])) {
  Subcategory::deleteSubcategory($_REQUEST['delete']);

  $updates = 'Delete Subcategory content';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
  header("Location: subcategory_dashboard.php?cid=$categoryID&update=delete");
  exit();
}
if ($_GET['update']=='delete') {
  $success = 'Subcategory Deleted!';
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">
<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
	<div id="blog_overview">
    	<ul class="btn">
            <li>
              <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory_create.php?cid=<?=$categoryID?>">Add NEW Sub-Category</a>
            </li>
            <li>
              <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/dashboard.php">Back</a>
            </li>
        </ul>
  
  <?php
   $count_record = Subcategory::countSubcategory($categoryID);
   
  if(!isset($_REQUEST['page']))
    {
      $page = 1;
    }
    else
    {
    $page = $_GET[page];
    }
    $pagination = new Pagination;
    //for display
    $pg = $pagination->page_pagination(20, $count_record, $page, 20);
    //$result_prod = mysql_query($query_Recordset2.$pg[1]);
    $subcat = Subcategory::findAll($categoryID, $pg[1]);
  ?>

<!--   <h3>Sub-Category Overview</h3> -->
  <h3><?=$catName?></h3>

  <form method="post" action="">
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="100">Image</td> 
          <td width="410">Sub-Category Name</td>
          <td width="100">Order</td>   
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else {
					foreach($subcat as $subcatR) { 
            $subcatID = $subcatR->fldSubcategoryID;
        ?>
            <tr id="<?=$subcatID?>_<?=$subcatR->fldSubcategoryPosition?>">
              <td><?=$subcatID?></td>
              <td><img src="<?=$ROOT_URL?>uploads/category/_75_<?=$subcatR->fldSubcategoryImage?>" width="75"></td>
              <td><?=$subcatR->fldSubcategoryName?></td>

              <td><input type="text" size="5" value="<?=$subcatR->fldSubcategoryPosition?>" name="position_<?=$subcatID?>"></td>

              <td align="center"><a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory_edit.php?id=<?=$subcatR->fldSubcategoryID?>"><img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory_dashboard.php?cid=<?=$categoryID?>&delete=<?=$subcatR->fldSubcategoryID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Sub-Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Sub-Category.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/delete.gif" width="16" height="16" alt="del" /></a>
                <br>
                <a href='subcategory2_dashboard.php?cid=<?=$subcatR->fldCategoryID?>&scid=<?=$subcatR->fldSubcategoryID?>'>Sub-Category 2</a>
              </td>
            </tr>
        <? } }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2">
              <!-- <input type="submit" name="submit" value="Update Positions"> -->
            </dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
     <input type="submit" name="submit" value="Update Position">
    </form>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>
<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.tablednd.js"></script>
  
<script type="text/javascript">
  Cufon.replace('h3');
    $(document).ready(function(){ 
        
      $('#page_manager').tableDnD({
        onDrop: function(table, row) {
          $.ajax({  
              type: "POST",
              url: "<?=$ROOT_URL?>_admin/_modules/mods_category/update.php?action=update_position_drop_subcat&cid=<?=$categoryID?>",
              data: $.tableDnD.serialize(),
              cache: false,
              success: function(){
                document.location.href= "<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory_dashboard.php?cid=<?=$categoryID?>";
              }
              
            }); 
        }
      });
  });   
</script>

</body>
</html>