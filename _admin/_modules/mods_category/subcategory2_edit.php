<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');


if(isset($_POST['submit'])) {
	
  include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['image']['name'] != '') {

		$my_upload->upload_dir = "../../../uploads/category/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		$my_upload->rename_file = true;

		$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
		$my_upload->the_file = $_FILES['image']['name'];
		$my_upload->http_error = $_FILES['image']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		$new_name = 'Subcategory_'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path = $my_upload->file_copy;
			// ... or do something like insert the filename to the database
			
		 	//resize the image
			
 			//create a thumbnail 230 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/category/_230_".$full_path;
			$obj_img->NewWidth = 230;
			// $obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 75 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/category/_75_".$full_path;
			$obj_img->NewWidth = 75;
			// $obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
								
		}


	} else {
		$full_path = "";
	}
	
	$_POST = sanitize($_POST);
	$subcat = $_POST;
	settype($subcat,'object');
	Subcategory::updateSubcategory2($subcat, basename($full_path)); 
	$success = "Product Sub-category Successfully Updated!";

	$updates = 'Update Product Subcategory content';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

$subcat2 = Subcategory::findSubcategory($_REQUEST['id']);

$subcategoryID2	= $subcat2->fldSubcategoryID;
$categoryID 	= $subcat2->fldCategoryID;
$subcategoryID 	= $subcat2->subcategoryID;
$subcatName2 	= $subcat2->fldSubcategoryName;

$cat = Category::findCategory($categoryID);
$catName = $cat->fldCategoryName;

$subcat1 = Subcategory::findSubcategory($subcategoryID);
$subcatName1 = $subcat1->fldSubcategoryName;
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_category/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory2_dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>">Back</a></li>
    </ul>    
</div>    
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
<!--     <h3>ACM Product Category</h3> -->
    <h3><?=$catName?> >> <?=$subcatName1?> >> <?=$subcatName2?></h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>Update Product Sub-Category</legend>
      <ul>
        <li>
          <label for="name">Name</label>
            <input type="text" id="name" name="name" value="<?=stripslashes($subcat2->fldSubcategoryName)?>">
          </label>
        </li>
        <li>
          <label for="description.editor">Description
          <textarea cols="" rows="" id="description.editor" name="description"><?=stripslashes($subcat2->fldSubcategoryDescription)?></textarea>
          </label>
        </li>
        <li>
          <label for="image">Image</label>
          	<br><small><strong>Formats</strong>: gif | jpg | JPG | jpeg | png &nbsp; &bull;
				<strong>Max Size</strong>: 2MB &nbsp; &bull;
				<strong>Dimension</strong>: 260px x 130px &nbsp;</small>
			<br>
            <input type="file" name="image">
            <? if($subcat2->fldSubcategoryImage != "") { ?>
            	<img src="<?=$ROOT_URL?>uploads/category/_75_<?=$subcat2->fldSubcategoryImage?>">
            <? } ?>
          </label>
        </li>
        <li>
          <label for="url">URL</label>
          	<br>
			<small><strong>Example</strong>:
			<br>
			for http://icpamerica.com/products/<strong><i>single_board_computers/default.html</i>
			<br>
			Enter &nbsp;&nbsp;&nbsp;<strong><i>single_board_computers/default.html</i></strong>&nbsp;&nbsp;&nbsp;&nbsp;
			</strong></small><br>

            <input type="text" id="url" name="url" value="<?=stripslashes($subcat2->fldSubcategoryURL)?>">
          </label>
        </li>
        <li>
          <label for="position">Position</label>
            <input type="text" id="position" name="position" value="<?=$subcat2->fldSubcategoryPosition?>"> eg.. 1,2,3,4
          </label>
        </li>
      </ul>
    </fieldset>
              
	<fieldset style="width:1050px;">
	<legend>Meta Information</legend>
	<ul>
		<li><label for="meta_title"> Meta Title </label>
			<textarea cols="80" rows="2" id="meta_title" name="meta_title"><?=stripslashes($subcat2->fldSubcategoryMetaTitle)?></textarea></li>
		<?php /* <li><label for="meta_keywords"> Meta Keywords </label>
			<textarea cols="80" rows="10" id="meta_keywords" name="meta_keywords"><?=stripslashes($subcat2->fldSubcategoryMetaKeywods)?></textarea></li> */ ?>
		<li><label for="meta_description"> Meta Description </label>
			<textarea cols="80" rows="10" id="meta_description" name="meta_description"><?=stripslashes($subcat2->fldSubcategoryMetaDescription)?></textarea></li>
	</ul>
	</fieldset>

    <ul class="submission">
    <input type="hidden" name="Id" value="<?=$_REQUEST['id']?>">
      <li><input type="submit" name="submit" value="Update Sub-Category"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>