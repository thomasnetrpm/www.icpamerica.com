<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
$categoryID = "";
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
<? include 'includes/sidepanel.php'; ?>
<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li class="active">Products</li>
    </ul>
    <h1>Products</h1>

		<div class="panel-pc">
			<ul class="unstyled clearfix">
				<?php
				// $category = Category::displayAllCategory();
				$category = Category::displayAllCategoryNotHidden();
				foreach ($category as $catR) {
					$categoryName 	= $catR->fldCategoryName;
					$categoryImage 	= $catR->fldCategoryImage;
					$categoryContent= substr(strip_tags($catR->fldCategoryDescription), 0, 100);
					$categoryNameURL= $catR->fldCategoryURL;
					$isOutsideURL 	= $catR->fldCategoryIsUrl;

					if ($isOutsideURL==1) {
						?>
						<li>
							<h3>
								<a href="<?=$categoryNameURL?>" target="_blank"><?=$categoryName?></a>
							</h3>
							<a class="img" href="<?=$categoryNameURL?>" target="_blank">
								<img src="<?=$root?>uploads/category/_230_<?=$categoryImage?>" alt="<?=$categoryName?>">
							</a>
							<p><?=$categoryContent?>... <br>
							<a href="<?=$categoryNameURL?>" target="_blank">see more</a></p>
						</li>
						<?
					} else {
						?>
						<li>
							<h3>
								<a href="<?=$root?>products/<?=$categoryNameURL?>"><?=$categoryName?></a>
							</h3>
							<a class="img" href="<?=$root?>products/<?=$categoryNameURL?>">
								<img src="<?=$root?>uploads/category/_230_<?=$categoryImage?>" alt="<?=$categoryName?>">
							</a>
							<p><?=$categoryContent?>... <br>
							<a href="<?=$root?>products/<?=$categoryNameURL?>">see more</a></p>
						</li>
						<?php
					}
				}
				?>

 			</ul>
		</div>
	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>
<? endblock(); ?>