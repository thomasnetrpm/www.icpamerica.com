<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><link rel="shortcut icon" href="index_files/favicon.ico"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title>ICP Mobile PC Solutions - Automotive PCs, Industrial PDAs, Mobile Computers, and Tablet PCs</title>
<meta content="text/html; charset=iso-8859-1" />

<meta name="Keywords" content="Automobile computers,Automobile PCs,Driver Assistant Operating Panels,Fleet Management Systems,industrial handheld products,Mini KIOSKs,mobile Access Control,mobile Data Collection,mobile embedded solution,Pilot Assistant Operating Panels,Price Verifiers,Remote data collections,Remote RFID Data Collections,Vehicle PC,Vehicle Power,Vehicle Tracker,Rugged handheld computer,rugged handheld,mobile computer equipment products,handheld devices,rugged mobile devices,mobile PCs,rugged PCs,rugged laptops,rugged vehicle mounted computers,vehicle mount computers,mobile productivity,ergonomics,handheld computer,data collection,mobile data collection,Tablet PC,UMPC,Ultra-Mobile PC,UltraMobile PCs,slate pc,mobile clinical assistant,MCA,Eo,MediSlate,Medi Slate,Sahara Slate PC,NetSlate,Kiosk,Sahara Tablet,Tablet PCs,TabletPCs,TufTab,ToughTab,ToughTablet,ToughTablet,Rugged">

<meta name="Description" content="ICP Mobile is a leading supplier of PC based Mobile Computers, Tablet PC's, and Industrial PDA's"/>


<meta name="Keywords" content="Automobile computers,Automobile PCs,Driver Assistant Operating Panels,Fleet Management Systems,industrial handheld products,Mini KIOSKs,mobile Access Control,mobile Data Collection,mobile embedded solution,Pilot Assistant Operating Panels,Price Verifiers,Remote data collections,Remote RFID Data Collections,Vehicle PC,Vehicle Power,Vehicle Tracker,Rugged handheld computer,rugged handheld,mobile computer equipment products,handheld devices,rugged mobile devices,mobile PCs,rugged PCs,rugged laptops,rugged vehicle mounted computers,vehicle mount computers,mobile productivity,ergonomics,handheld computer,data collection,mobile data collection,Tablet PC,UMPC,Ultra-Mobile PC,UltraMobile PCs,slate pc,mobile clinical assistant,MCA,Eo,MediSlate,Medi Slate,Sahara Slate PC,NetSlate,Kiosk,Sahara Tablet,Tablet PCs,TabletPCs,TufTab,ToughTab,ToughTablet,ToughTablet,Rugged">

<meta name="robots" content="INDEX,FOLLOW" />
<link rel="stylesheet" type="text/css" href="index_files/cm0263a3.css" media="all" />
<link rel="stylesheet" type="text/css" href="index_files/cmu5h9u3.css" media="all" />
<link rel="stylesheet" type="text/css" href="index_files/cmpe5mu3.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/strap-core.css" />
<script language="JavaScript" src="index_files/cm0263a3.js" type="text/javascript"></script>
<script language="JavaScript" src="index_files/cmxy6nt3.js" type="text/javascript"></script>
<script language="JavaScript" src="index_files/cmfrtx14.js" type="text/javascript"></script>

<!-- Hiding

defaultStatus="© 2013 ICP Advanced Digital Signage"

//-->
</script>
<script type="text/javascript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<style type="text/css">
<!--
.style10 {
	color: #2e2e2e
}
-->
</style>

</head>
<body id="ctl00_ctl03_Body1" class="default UAgecko UAff" onload="if (typeof _onLoad=='function') _onLoad()">
<a href="http://icpamerica.com/custom_quote/default.html" class="custom-quote"></a>
<div class="wrapper">
	<a name="top" id="top"></a>
    	<form id="search-panel" method="post" action="http://www.icpamerica.com/search/<?=$_POST['top_keyword']?>">
			<ul class="unstyled clearfix">
	            <li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>
	        	<!-- <li><a href="http://www.icpamerica.com/registration.html">Sign Up</a> <a href="http://www.icpamerica.com/login.html">Login</a></li>	 -->
				<li style="top:-2px;">
					<input type="text" placeholder="Type your keywords..." value="" name="top_keyword">
					<input type="submit" value="&nbsp;" name="top_search">
				</li>
				<li>
					<a target="_blank" href="http://www.facebook.com/icpa1"><img width="24" height="24" alt="FB" src="http://www.icpamerica.com/assets/images/icon-facebook.png"></a> 
					<a target="_blank" href="http://www.linkedin.com/company/icp-america-inc."><img width="24" height="24" alt="LI" src="http://www.icpamerica.com/assets/images/icon-linkedin.png"></a> 
					<a target="_blank" href="http://twitter.com/icpamerica"><img width="24" height="24" alt="TT" src="http://www.icpamerica.com/assets/images/icon-twitter.png"></a> 
	                <a target="_blank" rel="publisher" href="https://plus.google.com/100124929247060898549"><img width="24" alt="" src="http://www.icpamerica.com/assets/images/icon-google_plus.png"></a>
				</li>
			</ul>
		</form>
    <header>
    	<div class="container">
    	<div class="logo">
        <a href="http://www.icpmobile.com/" title="ICP Mobile Solutions - Automotive PCs, Industrial PDAs, and Tablet PCs"><img alt="ICP Mobile Solutions - Automotive PCs, Industrial PDAs, and Tablet PCs" src="index_files/icpmobilelogo.bmp" /></a></div>
        <nav>
        	<ul class="unstyled">
            	<li><a href="http://icpamerica.com/" id="banner_current" title="ICP Mobile PC Solutions">Home</a></li>
        		<li><a href="http://icpamerica.com/about_us/default.html" title="About ICP Mobile PC Solutions">About Us</a></li>
        		<li><a href="http://icpamerica.com/products/default.html" title="Products of ICP Mobile Solutions">Products</a></li>
        		<li><a href="http://icpamerica.com/contact_us/default.html" title="Contact ICP Mobile PC Solutions">Contact Us</a></li>
        		<li><a href="http://icpamerica.com/news/default.html" title="News from ICP Mobile PC Solutions">News</a></li>
        		<li><a href="http://icpamerica.com/support/default.html" title="Support for ICP Mobile PC Solutions">Support</a></li>
        		<li><a href="http://icpamerica.com/partners/default.html" title="Partners for ICP Mobile PC Solutions">Partners</a></li>
    			<li><a href="http://icpamerica.com/services/default.html" title="Services from ICP Mobile PC Solutions">Services</a></li>
    			<!--<li><a href="http://icpamerica.com/site_map/default.html" title="Site Map for ICP Mobile PC Solutions">Site Map</a></li>-->
        	</ul>
        </nav>
        </div>
        <div class="clearfix"></div>
    </header>

<? 
	include "../includes/config2.php";
	$page = Pages::findPages(22);

?>



   	<div class="banner">
        <script language="JavaScript">
			showImage();
		</script>	
    </div>



<a name="top" id="top"></a>


<div class="container">
    <div id="tableless">
        <div id="container" class="content">
       	<div class="products">
        <ul>
        	<li>
            	<h1><a href="<?=$page->fldProductUrl6?>"><?=$page->fldProductName6?></a></h1>
        		<a href="<?=$page->fldProductUrl6?>" title="<?=$page->fldProductName6?>"><img alt="<?=$page->fldProductName6?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage6?>" class="cmx-FloatLeft" width="70" height="46" /></a>
            	<p><?=$page->fldProductDescription6?></p>
			</li>
			<li>
            	<h1><a href="<?=$page->fldProductUrl1?>"><?=$page->fldProductName1?></a></h1>
        		<a href="<?=$page->fldProductUrl1?>" title="<?=$page->fldProductName1?>"><img alt="<?=$page->fldProductName1?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage1?>" class="cmx-FloatLeft" width="70" height="46" /></a>
            	<p><?=$page->fldProductDescription1?></p>
			</li>
			<li>
            	<h1><a href="<?=$page->fldProductUrl2?>"><?=$page->fldProductName2?></a></h1>
        		<a href="<?=$page->fldProductUrl2?>" title="<?=$page->fldProductName2?>"><img alt="<?=$page->fldProductName2?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage2?>" class="cmx-FloatLeft" width="70" height="46" /></a>
            	<p><?=$page->fldProductDescription2?></p>
			</li>
        </ul>
        </div><!--products -->
    </div><!--content -->


     <div class="new-products">
   	    	<!--<img src="index_files/vtt-1000-vehicle-tracking-terminal.jpg" width="1050" height="250" /> -->
			<img src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner2?>" width="1050" height="250" />
            <a href="<?=$page->fldUrl?>" class="link1">Learn more</a>
        </div>
    <div class="before-footer">
        <h2>See More Products</h2>
        <ul class="unstyled">
            <li>
                <h3><a title="<?=$page->fldProductName3?>" href="<?=$page->fldProductUrl3?>" target="_blank"><?=$page->fldProductName3?></a></h3>
        		<a href="<?=$page->fldProductUrl3?>" target="_blank" title="<?=$page->fldProductName3?>"><img alt="<?=$page->fldProductName3?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage3?>" class="cmx-FloatLeft" width="72" height="40" /></a>
                <p><?=$page->fldProductDescription3?></p>
            </li>
			<li>
                <h3><a title="<?=$page->fldProductName4?>" href="<?=$page->fldProductUrl4?>" target="_blank"><?=$page->fldProductName4?></a></h3>
        		<a href="<?=$page->fldProductUrl4?>" target="_blank" title="<?=$page->fldProductName4?>"><img alt="<?=$page->fldProductName4?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage4?>" class="cmx-FloatLeft" width="72" height="40" /></a>
                <p><?=$page->fldProductDescription4?></p>
            </li>
			<li>
                <h3><a title="<?=$page->fldProductName5?>" href="<?=$page->fldProductUrl5?>" target="_blank"><?=$page->fldProductName5?></a></h3>
        		<a href="<?=$page->fldProductUrl5?>" target="_blank" title="<?=$page->fldProductName5?>"><img alt="<?=$page->fldProductName2?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage5?>" class="cmx-FloatLeft" width="72" height="40" /></a>
                <p><?=$page->fldProductDescription5?></p>
            </li>
        </ul>
    </div><!--before-footer -->
</div>
</div><!--container -->

<footer id="footer">
	<p>
		<span>&copy; 2014 ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="http://icpamerica.com/privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="http://icpamerica.com/site_map/default.html">Site Map</a></span>
		<br/>
		<small><a target="_blank" href="http://dogandrooster.com">Website Design by Dog and Rooster, Inc.</a></small>
	</p>	
</footer>

</div><!--wrapper -->

<script language="JavaScript">function DoValidate_email_form(){return MM_validateForm('First Name','','R','Last Name','','R','Company Name','','R','Email','','RisEmail','Product Name','','R')}</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37142973-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body></html>
