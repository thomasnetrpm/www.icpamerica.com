function trim(s) 
{
	var n = s.length;
	var x = 0;
	while (n > 0 && s.charAt(n - 1) == " ")
		n--;
	while (n > x && s.charAt(x) == " ")	
		x++;
	return s.substr(x, n - x);
}

function shift(a) // not native in iemac)
{
	if (a.length)
	{
		var r = a[0];
		for (var n = 1;n < a.length; n++)
			a[n - 1] = a[n];
		a.length = a.length - 1;
		return r;
	}
	else return null;
}

function setSelect(sl, code, def) 
{
	for (var n = 0; n < sl.options.length; n++)
		if (code == sl.options[n].value)
		{
		  sl.selectedIndex = n;
		  return;
		}
	if (def == null || def < 0 || def >= sl.options.length)
	  sl.selectedIndex = -1;
	else
	  sl.selectedIndex = def;
}

function _findTag(o, tag, klass)
{
  if (o.className) {
    var ka = o.className.toLowerCase().split(' ');
    for(var n=0; n<ka.length; n++)
      if (trim(ka[n]) == klass)
      //{ tag.push(o); break; }
      { tag[tag.length]=o; break; }
  }
  if (o.childNodes)
    for(n=0;n<o.childNodes.length;n++)
      _findTag(o.childNodes[n], tag, klass);
}

function findTag(klass)
{
  var tag = [];
  _findTag(document.body, tag, klass.toLowerCase());
  if (tag.length==0) return null;
  if (tag.length==1) return tag[0];
  return tag;
}

function OpenPopUp(url,winwidth,winheight,options) 
{
	if (!options) options = "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbar=yes,scrollbars=yes,resizable=no,copyhistory=no";
  var w=window.open(url,'popup_'+winwidth+'_'+winheight,options+',width='+winwidth+',height='+winheight);
  w.focus(); DemoWindow=w;
}
function LoadPage(page)
{
	if (page) location.href = page;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_nameOf(o) {
  if (o.tagName) return o.name; else return MM_nameOf(o[0]);
}

function MM_valueOf(o) {
  if (o.tagName) return trim(o.value); 
  else if (o[0].type=='radio') {
    for(var i=0;i<o.length;i++)	if(o[i].checked) return trim(o[i].value);	return "";}
	else {
		var val="";	i=0; while (o[i])	{
			if (val) val+=","; val+= MM_valueOf(o[i]); i++;}
		return trim(val);	}
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
		if (val) { nm=MM_nameOf(val).replace(/_/g, ' '); while (nm.substr(0,1)==' ') nm=nm.substr(1);
		if ((val=MM_valueOf(val))!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
  return document.MM_returnValue;
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_jumpMenuGo(selName,targ,restore){ //v3.0
  var selObj = MM_findObj(selName); if (selObj) MM_jumpMenu(targ,selObj,restore);
}

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function __onKeyup()
{
	if (event.srcElement.type == "textarea") __Autosize(event.srcElement);
}

function __Autosize(o, min, max)
{
	var scrollHeight = o.scrollHeight;
	var autosize = o.getAttribute("autosize");
	if (autosize != null)
	{
		if (min == null) min = o.autosizemin;
		if (max == null) max = o.autosizemax;
		if ((min == null) || (max == null))
			if (autosize != "") // parse and test
			{
				var bounds = autosize.split(",");
				for (var n = 0; n < 2; n++)
				{
					if (n >= bounds.length)
					  //bounds.push("");
					  bounds[bounds.length]="";
					bounds[n] = parseInt(bounds[n]);
					if (isNaN(bounds[n]) || (bounds[n] < 0))
					  bounds[n] = null;
				}
				if (min != null) bounds[0] = null;
				if (max != null) bounds[1] = null;
				if ((bounds[0] != null) || (bounds[1] != null)) // test
				{
					var save = o.value;
					o.value = "";
					var h1 = o.scrollHeight;
					o.value = "\r\n";
					var h2 = o.scrollHeight;
					o.value = save;
					var x1 = h2 - h1;
					var x0 = h1 - x1;
					if ((min == null) && (bounds[0] != null))
						min = bounds[0] * x1 + x0;
					if ((max == null) && (bounds[1] != null))
						max = bounds[1] * x1 + x0;
				}
			}
		o.autosizemin = min;
		o.autosizemax = max;
		if (min && (scrollHeight < min)) scrollHeight = min;
		if (max && (scrollHeight > max)) scrollHeight = max;
		o.style.height = (scrollHeight - 2).toString() + "px"; 
		if (typeof __Resize == "function") __Resize();
	}
}

function __AutosizeInit(o)
{
  if (o && o.type == "textarea") __Autosize(o);
  if (o.childNodes)
		for (var n = 0; n < o.childNodes.length; n++)
			__AutosizeInit(o.childNodes[n]);
}

function Autosize()
{
	document.onkeyup=__onKeyup;
	__AutosizeInit(document.body);
}

function GetCmxCookies()
{
	var cookies = new Object();
	var allcookies = document.cookie;
	if (allcookies != "")
	{
		var start = allcookies.indexOf("cmx=");
		if (start != -1)
		{
			start += 4;
			var end = allcookies.indexOf(';', start);
			if (end == -1) end = allcookies.length;
			cmxcookie = allcookies.substring(start, end);
			var a = cmxcookie.split('&');
			for (var i = 0; i < a.length; i++) 
			{
				a[i] = a[i].split('=');
				cookies[a[i][0]] = unescape(a[i][1]);
			}
		}
	}
	return cookies;
}

function SetCmxCookies(cookies)
{
	cmxcookie = ""
	for (var prop in cookies)
		if ((prop.charAt(0) != '$') && (typeof cookies[prop] != "function"))
		{
			if (cmxcookie) cmxcookie += "&";
			cmxcookie += prop + '=' + escape(cookies[prop]);
		}
	cmxcookie = "cmx=" + cmxcookie;
	var date = new Date();
	date.setFullYear(date.getFullYear() + 1);
	cmxcookie += "; expires=" + date.toGMTString();
	cmxcookie += "; path=/";
	document.cookie = cmxcookie;
}

function GetCookies()
{
}

function GetCookieList()
{
	return [];
}

function FindFormField(name)
{
	for (var n = 0; n < document.forms.length; n++)
	{
		var form = document.forms[n];
		for (var m = 0; m < form.elements.length; m++)
			if (form.elements[m].name == name)
				return form.elements[m];
	}
	return null;
}

function DoCookies(getCookies, cookieList)
{
	var cookies = GetCmxCookies();
	if (getCookies)
		for (var n = 0; n < cookieList.length; n += 2)
		{
			var field = FindFormField(cookieList[n]);
			if (field) field.value = cookies[cookieList[n+1]] || "";
		}
	else
	{
		for (var n = 0; n < cookieList.length; n += 2)
		{
			var field = FindFormField(cookieList[n]);
			if (field) cookies[cookieList[n+1]] = field.value;
		}
		SetCmxCookies(cookies);
	}
}

function QueryString(name)
{
	var re = new RegExp("(?:\\?|&)" + name + "\\=([^&]*)(?:&|$)","i");
	var q = location.search.match(re);
	return q ? unescape(q[1]) : null;
	/*
	var a = unescape(location.search.substring(1)).split('&');
	name += '=';
	var nlen = name.length;
	for (var n = 0; n < a.length; n++)
		if (a[n].substr(0, nlen) == name)
			return a[n].substr(nlen);
	return null;
	*/
}

function Validate(formname)
{ 
	var ok = true;
	fn = "DoValidate_" + formname;
  if (eval("typeof " + fn) == "function") ok = eval(fn + "()");
  if (ok && (typeof OnSubmitContactForm == "function")) ok = OnSubmitContactForm(formname) != false;
  if (ok) DoCookies(false, GetCookieList());
	return ok;
}

var IsStaging = false;
					
function __b2t(o1,o2,o3)
{
	o1=document.getElementById(o1);
	o2=document.getElementById(o2);
	var b1=o2.offsetTop-o1.offsetTop;
	if (o3)
	{
		o3=document.getElementById(o3);
		var b2=o3.offsetTop+o3.offsetHeight;
		if (b2>b1) b1=b2
	}
	o1.style.height=b1+"px";
}

function __t2b(o1)
{
  o1=document.getElementById(o1);
  var top = 0;
  for (var n=1; n < arguments.length; n++)
  {
		var o = document.getElementById(arguments[n]);
		if (o)
		{
			var t = o.offsetTop+o.offsetHeight;
			if (t > top) top = t;
		}
  }
  o1.style.top=top+"px";
}

function _onLoad(print)
{
	Autosize();
	GetCookies();
	if (typeof onLoad == "function")
	  onLoad();
	if (__onLoad.fns)
	  for (var n=0;n<__onLoad.fns.length;n++)
		{
			var fn = __onLoad.fns[n];
			if (typeof fn == "function") __onLoad.fns[n]();
			else eval(fn);
		}
	if (print)
		window.print();
}

function __onLoad(fn)
{
	if (!__onLoad.fns) __onLoad.fns = [];
	__onLoad.fns[__onLoad.fns.length]=fn;
}

function FixCheckboxFields(form)
{
	// so unchecked box will be sent
  fields = [];
  for (var n = 0; n < form.length; n++)
  {
    var field = form[n];
    if (field.type == "checkbox"
      && !field.checked)
      if (form[field.name].length && form[field.name].type != "select-one") // an array
      {
				var a = form[field.name];
				if (a[0] == field) // only do if first entry
				{
					for (var m = 1; m < a.length; m++)
						if (a[m].type != "checkbox" || a[m].checked)
							break
					if (m >= a.length) // all unchecked checkboxes
						fields[fields.length] = field.name;
				}
      }
      else fields[fields.length] = field.name;
  }
  for (var n = 0; n < fields.length; n++)
  {
		var done = false;
		try
		{	
			var e = document.createElement("INPUT");
			e.type = "hidden"; // fails macie
			e.name = fields[n];
			e.value = "";
			form.appendChild(e);
			done = true;
		} 
		catch(e) {}
		if (!done) 
		{
			if (form[fields[n]].length) field = form[fields[n]][0];
			else field = form[fields[n]];
			field.style.position = "relative";
			field.style.left = "-1000px";
			field.checked = true;
			field.value = "";
		}
	}
}

function setValue(o, value, noradio)
{
	if (o)
		if (o.length && o.type != "select-one") // array
		{
			var v = null;
			var v1 = value;
			var pass2needed = 0; // 0 = not needed, 1 = a true checkbox, 2 = a subsequent true radio
			for (var n = -1; n < o.length; n++)
			{
				var field = n < 0 ? null : o[n];
				if (field)
				{
					setValue(field, v);
					if (field.type == "checkbox" || field.type == "radio")
						if (field.value == v)
							switch (pass2needed)
							{
								case 0:
									if (field.type == "checkbox") pass2needed = 1;
									break;
								case 1:
									if (field.type == "radio") pass2needed = 2;
									break;
							}
						else continue;
				}
				var pos = value.indexOf(',');
				if (pos < 0)
				{
					v = value;
					value = "";
				}
				else
				{
					v = value.substr(0, pos);
					value = value.substr(pos + 1);
				}
			}
			if (pass2needed == 2 && !noradio)
				setValue(o, v1, true);
		}
		else switch (o.type)
		{
			case "checkbox":
			case "radio":
				if (o.type != "radio" || !noradio)
					o.checked = value == o.value;
				break;
				
			case "text":
			case "textarea":
			case "password":
				o.value = value || "";
				break;
				
			case "select-one":
				setSelect(o, value);
				break;
		}
}

function _normalizeUrl(url)
{
  var dot = url.lastIndexOf('.');
  var slash = url.lastIndexOf('/');
  if (dot <= slash)
  {
		if (url.charAt(url.length - 1) != '/')
			url += '/';
			url += "default.html";
  }
  return url;
}

function cmxPrint()
{
	if (QueryString("_cmxprint") != null)
		window.print();
	else
	{
		var url = _normalizeUrl(location.href);
		url += (url.indexOf('?') >= 0) ? '&' : '?';
		url += "_cmxprint=1";
		window.open(url, "_blank").moveBy(40, 40);
	}
}

function cmxDownload(file)
{
	var url = "/download.cm?";
	if (location.protocol == "https:")
	{
		var pos = location.pathname.indexOf('/', 1);
		if (pos >= 0)
			url += "sitecode=" + location.pathname.substr(1, pos - 1) + '&';
	}
	url += "file=" + file;
	location.href = url;
}

function _registration(pg, folder, signOut)
{
	var wind = window.parent || window;
  var thisPage = wind.location.pathname.toLowerCase();
  var sitecode = "";
  if (wind.location.protocol == "https:")
  {
		var pos = thisPage.indexOf('/', 1);
		if (pos >= 0)
		{
			sitecode = thisPage.substr(1, pos - 1);
			thisPage = thisPage.substr(pos);
		}
  }
  if (folder.toLowerCase() + '/' == thisPage.substr(0, folder.length + 1))
  { // registration folder, transfer page parameter
		var page = _pageQuery();
		if (page)	pg += '?page=' + page;
  }
  else pg += "?page=" + thisPage;
  if (wind.location.protocol == "https:")
		if (signOut)
			pg += "&sitecode=" + sitecode;
		else
			pg = '/' + sitecode + pg;
  wind.location.href = pg;
}

function Register(folder)
{
	_registration(folder + "/default.html", folder, false);
}

function SignIn(folder, signOut)
{
	_registration(signOut ? "/SignOut.cm" : 
		folder + "/Sign_In.html", folder, signOut);
}

function javascript(s)
{
	var e, a = s.match(/^javascript\s*:\s*(.*)$/,'i');
	if (a)
	  try { return eval(a[1]); }
	  catch (e) { return ""; }
	else
	  return s;
}

function RadioIndex(o)
{
	for (var n = o.length - 1; n >= 0; n--)
	  if (o[n].checked) break;
	return n;
}

function RadioSet(o, inx)
{
	if (isString(inx))
	{
		inx = inx.toLowerCase();
		for (var n = o.length - 1; n >= 0; n--)
		  if (o[n].value.toLowerCase() == inx) break;
		inx = n;
	}
	for (var n = o.length - 1; n >= 0; n--)
	  o[n].checked = n == inx;
}

function RadioValue(o)
{
	var n = RadioIndex(o);
	return (n >= 0) ? o[n].value : null;
}
