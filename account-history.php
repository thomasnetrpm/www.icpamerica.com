<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
include 'classes/Tax.php';

if(isset($_SESSION['client_id'])) {
	$client_id = $_SESSION['client_id'];		
} else {
	header("Location: index.php");
}

$client = Client::findClient( $client_id );
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=$root?>account-information.html">My Account</a> <span class="divider">/</span></li>
      <li class="active">Purchase History</li>
    </ul>

		<h2>Purchase History</h2>

		<?php
		$order = Cart::findAllClient($page, $client->fldClientID);

		if (empty($order)) {
		    echo 'No Transaction/s found.';
		} else {

			foreach ($order as $orderR) {

				$cartID 	= $orderR->fldCartID;
				$orderNo 	= $orderR->fldCartOrderNo;
				$trackingNo = $orderR->fldCartTrackingNumber;
				$status 	= $orderR->fldCartStatus;
				$cartDate 	= $orderR->fldCartDate;

				$formattedDate = date("M d, Y", strtotime($cartDate));
				?>
				<div class="accordion purchase-history" id="accordion<?=$cartID?>">
					<div class="accordion-group">
						<div class="accordion-heading">
							<!-- Entry Tab -->
							<table width="100%" cellspacing="0" cellpadding="5">
								<tbody>
									<tr>
										<td width="85%">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?=$cartID?>" href="#collapseOne<?=$cartID?>">
											<table width="100%" class="tab-panel">
												<tbody>
													<tr>
														<td width="20%">Order Number:</td>
														<td><?=$orderNo?></td>
													</tr>
													<tr>
														<td>Order Date:</td>
														<td><?=$formattedDate?></td>
													</tr>
												</tbody>
											</table> 
											</a>
										</td>
										<td width="15%" align="center" class="status">
											<?=$status?>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- Entry Tab -->
						</div>
						<div id="collapseOne<?=$cartID?>" class="accordion-body collapse">
							<div class="accordion-inner">
								<!-- Entry Content -->
								<table class="table history-panel">
									<thead>
										<tr>
											<td class="item">Item No.</td>
											<td class="desc">Description</td>
											<td class="qty">Qty</td>
											<td class="amount">Amount</td>
										</tr>
									</thead>
									<tbody>
										<?php
										$cart = Cart::displayAllByOrders($orderNo);
										foreach ($cart as $cartR) {
											$prodID 	= $cartR->fldCartProductID;
											// $prodCode 	= $cartR->fldCartProductName;
											$prodQty 	= $cartR->fldCartQuantity;
											$prodPrice 	= $cartR->fldCartProductPrice;

											$prodRowPrice = number_format($prodQty * $prodPrice, 2);
											
											$prod = Products::findProducts($prodID);
											$prodCode 	= $prod->fldProductsCode;
											$prodName 	= $prod->fldProductsName;

											$variant_id = $cartR->fldCartProductVariant;
											if (!empty($variant_id)) {
												$variant = ProductVariant::findVariant($variant_id);
												$variant_name = $variant->product_variant;
												$option_id = $variant->product_option_id;
												$option = ProductVariant::findOption($option_id);
												$option_name = ($option->product_option=="Product Options")? "": $option->product_option.' | ';
											}
											?>
											<tr>
												<td><?=$prodCode?></td>
												<td><?=$prodName?><br><?=$option_name.$variant_name?></td>
												<td><?=$prodQty?></td>
												<td>$ <?=$prodRowPrice?></td>
											</tr>
											<?php
											$subtotal += $prodRowPrice;
										}

										$shipping = 0;
										$coupon   = 0;

							            // TAX
										$tax = Tax::findTaxByOrderCode($orderNo);
										$taxAmount 	= $tax->fldTaxValue;

										$coup = Cart::getCartXCoupon($orderNo);
										$coupon   	= $coup->fldCXCCouponAmount;
										$couponCode = $coup->fldCXCCoupon;

										$subtotalBeforeTax = $subtotal + $shipping;
										$grandtotal = $subtotalBeforeTax + $taxAmount - $coupon;
										?>
										<tr>
											<td>
												<strong>Track this Shipment:</strong>
												<br><?=$trackingNo?>
											</td>
											<td colspan="3" class="subtotal">
												<table width="100%">
													<tbody>
														<tr>
															<td>Item(s) Subtotal:</td>
															<td>$ <?=number_format($subtotal, 2)?></td>
														</tr>
														<tr>
															<td>Shipping & Handling:</td>
															<td>$ <?=number_format($shipping, 2)?></td>
														</tr>
														<tr>
															<td>Total before tax:</td>
															<td>$ <?=number_format($subtotalBeforeTax, 2)?></td>
														</tr>
														<tr>
															<td>Sales Tax:</td>
															<td>$ <?=number_format($taxAmount, 2)?></td>
														</tr>
														<tr>
															<td>Coupon (<?=$couponCode?>):</td>
															<td>$ <?=$coupon?></td>
														</tr>
														<tr class="total">
															<td>Grand Total:</td>
															<td>$ <?=number_format($grandtotal, 2)?></td>
														</tr>
													</tbody>
												</table>			  										
											</td>
										</tr>
									</tbody>
								</table>
								<!-- Entry Content -->
							</div>
						</div>
					</div>

					<?php
					$subtotal 			= '';
					$subtotalBeforeTax 	= '';
					$grandtotal 		= '';
				}
		}
		?>


	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>
<? endblock(); ?>