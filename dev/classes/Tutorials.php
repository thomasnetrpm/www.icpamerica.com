<?
   
class Tutorials{
   
    function addTutorials($tutorials){
        global $adb;
		global $table_prefix;
		
		$youtube = addslashes($tutorials->youtube);
		$name = addslashes($tutorials->name);
		
	
        $query="INSERT INTO ".$table_prefix."_tblTutorials SET ".
            "fldTutorialsTitle='$name',".
			"fldTutorialsYouTube='$youtube'";				
        $adb->query($query);
        return true;
    }
	
	function updateTutorials($tutorials) {
		 global $adb;
		 global $table_prefix;
		 
		$youtube = addslashes($tutorials->youtube);
		$name = addslashes($tutorials->name);
		
		
			$query="UPDATE ".$table_prefix."_tblTutorials SET ".
				 "fldTutorialsTitle='$name',".
				"fldTutorialsYouTube='$youtube'".
           		 " WHERE fldTutorialsID=$tutorials->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTutorials";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTutorials ORDER BY fldTutorialsID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countTutorials() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTutorials";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findTutorials($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTutorials WHERE fldTutorialsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteTutorials($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblTutorials WHERE fldTutorialsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
