<?
class Client {

    function addClient($client){

        global $adb;
		global $table_prefix;		

		$date = date('Y-m-d');

		$username 	= $client->username;
		$password 	= $client->confirm_password;
		$firstname 	= addslashes($client->firstname);
		$lastname 	= addslashes($client->lastname);
		$member_type= $client->member_type;
		$company 	= $client->company;
		$phone 		= $client->phone;
		$address1 	= $client->address1;
		$address2 	= $client->address2;
		$city 		= $client->city;
		$state 		= $client->state;
		$country 	= addslashes($client->country);
		$zip 		= $client->zip;
		$email 		= $client->email;

        $query = "INSERT INTO ".$table_prefix."_tblClient SET ".
            "fldClientUsername='$username',". 
			"fldClientPassword='$password',". 
			"fldClientFirstName='$firstname',". 
			"fldClientLastname='$lastname',". 
			"fldClientType='$member_type',".
			"fldClientCompany='$company',".	
			"fldClientPhoneNo='$phone',".	
			"fldClientAddress='$address1',".	
			"fldClientAddress1='$address2',".	
			"fldClientCity='$city',".	
			"fldClientState='$state',".	
			"fldClientCountry='$country', ".
			"fldClientZip='$zip',".	
			"fldClientEmail='$email',".	
			"fldClientRegDate='$date'";
        $adb->query($query);

        return mysql_insert_id();
    }

	function updateClient($client) {
		global $adb;
		global $table_prefix;
		
		$client_id 	= $client->fldClientID;
		$password1 	= $client->password;
		$password 	= $client->fldClientPassword;
		$firstname 	= addslashes($client->firstname);
		$lastname 	= addslashes($client->lastname);
		$company 	= $client->company;
		$phone 		= $client->phone;
		$address1 	= $client->address1;
		$address2 	= $client->address2;
		$city 		= $client->city;
		$state 		= $client->state;
		$country 	= addslashes($client->country);
		$zip 		= $client->zip;
		$email 		= $client->email;

		$query = "UPDATE ".$table_prefix."_tblClient SET ";
		if ($password) { $query .= "fldClientPassword='$password',"; }
		$query .= "fldClientFirstName='$firstname',". 
			"fldClientLastname='$lastname',". 
			"fldClientCompany='$company',".	
			"fldClientPhoneNo='$phone',".	
			"fldClientAddress='$address1',".	
			"fldClientAddress1='$address2',".	
			"fldClientCity='$city',".	
			"fldClientState='$state',".	
			"fldClientCountry='$country', ".
			"fldClientZip='$zip',".	
			"fldClientEmail='$email' ".
            "WHERE fldClientID='$client_id' ";
	    $adb->query($query);
        return true;
	}	

	function addClientFromAdmin($client) {
		global $adb;
		global $table_prefix;
		
/*
		$firstname 		= addslashes($client->firstname);
		$lastname 		= addslashes($client->lastname);
		$email 			= addslashes($client->email_address);
		$username 		= addslashes($client->username);
		$password 		= addslashes($client->password);
		$member_type 	= addslashes($client->member_type);
		$bill_address 	= addslashes($client->bill_address);
		$bill_address1 	= addslashes($client->bill_address1);
		$bill_city 		= addslashes($client->bill_city);
		$bill_state 	= addslashes($client->bill_state);
		$bill_country 	= addslashes($client->bill_country);
		$bill_zip 		= addslashes($client->bill_zip);
		$bill_phone 	= addslashes($client->bill_phone);
		$ship_address 	= addslashes($client->ship_address);
		$ship_address1 	= addslashes($client->ship_address1);
		$ship_city 		= addslashes($client->ship_city);
		$ship_state 	= addslashes($client->ship_state);
		$ship_country 	= addslashes($client->ship_country);
		$ship_zip 		= addslashes($client->ship_zip);
		$ship_phone 	= addslashes($client->ship_phone);

		$query = "INSERT INTO ".$table_prefix."_tblClient SET ".	 
			"fldClientLastname='{$lastname}', ".
			"fldClientFirstName='{$firstname}', ".
			"fldClientEmail='{$email}', ".
			"fldClientUsername='{$username}', ".
			"fldClientPassword='{$password}', ".

			"fldClientType='{$member_type}', ".
			"fldClientBillAddress='{$bill_address}', ".
			"fldClientBillAddress1='{$bill_address1}', ".
			"fldClientBillCity='{$bill_city}', ".
			"fldClientBillState='{$bill_state}', ".
			"fldClientBillCountry='{$bill_country}', ".
			"fldClientBillZip='{$bill_zip}', ".
			"fldClientBillPhone='{$bill_phone}', ".

			"fldClientShipAddress='{$ship_address}', ".
			"fldClientShipAddress1='{$ship_address1}', ".
			"fldClientShipCity='{$ship_city}', ".
			"fldClientShipState='{$ship_state}', ".
			"fldClientShipCountry='{$ship_country}', ".
			"fldClientShipZip='{$ship_zip}', ".
			"fldClientShipPhone='{$ship_phone}' ";
*/
		$firstname 		= addslashes($client->firstname);
		$lastname 		= addslashes($client->lastname);
		$email 			= addslashes($client->email_address);
		$username 		= addslashes($client->username);
		$password 		= addslashes($client->password);
		$member_type 	= addslashes($client->member_type);
		$address1 		= addslashes($client->address1);
		$address2 		= addslashes($client->address2);
		$city 			= addslashes($client->city);
		$state 			= addslashes($client->state);
		$country 		= addslashes($client->country);
		$zip 			= addslashes($client->zip);
		$phone 			= addslashes($client->phone);
		$company 		= addslashes($client->company);

		$query = "INSERT INTO ".$table_prefix."_tblClient SET ".	 
			"fldClientLastname='{$lastname}', ".
			"fldClientFirstName='{$firstname}', ".
			"fldClientEmail='{$email}', ".
			"fldClientUsername='{$username}', ".
			"fldClientPassword='{$password}', ".
			"fldClientType='{$member_type}', ".
			"fldClientAddress='{$address1}', ".
			"fldClientAddress1='{$address2}', ".
			"fldClientCity='{$city}', ".
			"fldClientState='{$state}', ".
			"fldClientCountry='{$country}', ".
			"fldClientZip='{$zip}', ".
			"fldClientPhoneNo='{$phone}', ".
			"fldClientCompany='{$company}' ";
	    $adb->query($query);
        return true;
	}	

	function updateClientFromAdmin($client) {
		global $adb;
		global $table_prefix;
/*		
		$firstname 		= addslashes($client->firstname);
		$lastname 		= addslashes($client->lastname);
		$email 			= addslashes($client->email_address);
		$username 		= addslashes($client->username);
		$password 		= addslashes($client->password);
		$member_type 	= addslashes($client->member_type);
		$bill_address 	= addslashes($client->bill_address);
		$bill_address1 	= addslashes($client->bill_address1);
		$bill_city 		= addslashes($client->bill_city);
		$bill_state 	= addslashes($client->bill_state);
		$bill_country 	= addslashes($client->bill_country);
		$bill_zip 		= addslashes($client->bill_zip);
		$bill_phone 	= addslashes($client->bill_phone);
		$ship_address 	= addslashes($client->ship_address);
		$ship_address1 	= addslashes($client->ship_address1);
		$ship_city 		= addslashes($client->ship_city);
		$ship_state 	= addslashes($client->ship_state);
		$ship_country 	= addslashes($client->ship_country);
		$ship_zip 		= addslashes($client->ship_zip);
		$ship_phone 	= addslashes($client->ship_phone);

		$query = "UPDATE ".$table_prefix."_tblClient SET ".	 
			"fldClientLastname='{$lastname}', ".
			"fldClientFirstName='{$firstname}', ".
			"fldClientEmail='{$email}', ".
			"fldClientUsername='{$username}', ".
			"fldClientPassword='{$password}', ".

			"fldClientType='{$member_type}', ".
			"fldClientBillAddress='{$bill_address}', ".
			"fldClientBillAddress1='{$bill_address1}', ".
			"fldClientBillCity='{$bill_city}', ".
			"fldClientBillState='{$bill_state}', ".
			"fldClientBillCountry='{$bill_country}', ".
			"fldClientBillZip='{$bill_zip}', ".
			"fldClientBillPhone='{$bill_phone}', ".

			"fldClientShipAddress='{$ship_address}', ".
			"fldClientShipAddress1='{$ship_address1}', ".
			"fldClientShipCity='{$ship_city}', ".
			"fldClientShipState='{$ship_state}', ".
			"fldClientShipCountry='{$ship_country}', ".
			"fldClientShipZip='{$ship_zip}', ".
			"fldClientShipPhone='{$ship_phone}' ".

            "WHERE fldClientID={$client->Id} ";
*/
		$firstname 		= addslashes($client->firstname);
		$lastname 		= addslashes($client->lastname);
		$email 			= addslashes($client->email_address);
		$username 		= addslashes($client->username);
		$password 		= addslashes($client->password);
		$member_type 	= addslashes($client->member_type);
		$address1 		= addslashes($client->address1);
		$address2 		= addslashes($client->address2);
		$city 			= addslashes($client->city);
		$state 			= addslashes($client->state);
		$country 		= addslashes($client->country);
		$zip 			= addslashes($client->zip);
		$phone 			= addslashes($client->phone);
		$company 		= addslashes($client->company);

		$query = "UPDATE ".$table_prefix."_tblClient SET ".	 
			"fldClientLastname='{$lastname}', ".
			"fldClientFirstName='{$firstname}', ".
			"fldClientEmail='{$email}', ".
			"fldClientUsername='{$username}', ".
			"fldClientPassword='{$password}', ".
			"fldClientType='{$member_type}', ".
			"fldClientAddress='{$address1}', ".
			"fldClientAddress1='{$address2}', ".
			"fldClientCity='{$city}', ".
			"fldClientState='{$state}', ".
			"fldClientCountry='{$country}', ".
			"fldClientZip='{$zip}', ".
			"fldClientPhoneNo='{$phone}', ".
			"fldClientCompany='{$company}' ".
            "WHERE fldClientID={$client->Id} ";
	    $adb->query($query);
        return true;
	}	

	function changePassword($client) {
		global $adb;
		global $table_prefix;
		$password = $client->password;
			$query="UPDATE ".$table_prefix."_tblClient SET ".
			"fldClientPassword='{$client->fldClientPassword}'". 
            " WHERE fldClientID={$client->fldClientID}";

	    $adb->query($query);
        return true;
	}    

	function findAll($pg) {
		global $adb;
		global $table_prefix;
		$query = "SELECT * FROM ".$table_prefix."_tblClient ORDER BY fldClientFirstName";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAll() {
		global $adb;
		global $table_prefix;
		$query = "SELECT * FROM ".$table_prefix."_tblClient ORDER BY fldClientFirstName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	

	function countClient() {
		global $adb;
		global $table_prefix;	
		$query = "SELECT * FROM ".$table_prefix."_tblClient";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function countClientEmail($email) {
		global $adb;
		global $table_prefix;
		$query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientEmail='$email'";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function countClientUsername($username) {
		global $adb;
		global $table_prefix;
		$query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientUsername='$username'";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function findClientByEmail($email){
        global $adb;
		global $table_prefix;
        $query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientEmail LIKE '{$email}'";
        $result = $adb->query($query);
        return $result->fetch_object();
    }

	function checkLogin($client) {	
		global $adb;
		global $table_prefix;
		$username  = $client->username;
		$password  = $client->password;	
		$query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientUsername='$username' AND fldClientPassword='$password'";
		$result = $adb->query($query);
		return $result->db_num_rows();		
	}
	
	function getInfo($client){
        global $adb;
		global $table_prefix;
        $username  = $client->username;
		$password  = $client->password;
		echo $query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientUsername='$username' AND fldClientPassword='$password'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	

    function findClient($id){
        global $adb;
		global $table_prefix;
        $query = "SELECT * FROM ".$table_prefix."_tblClient WHERE fldClientID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
    function deleteClient($id){        
        global $adb;
		global $table_prefix;
        $query = "DELETE FROM ".$table_prefix."_tblClient WHERE fldClientID='$id'";
        $adb->query($query);
        return true;
    }

    /**
    * checks if the username is unique
    */
    function isUniqueUsername($username, $current_user_id)
    {
	    global $adb;
		global $table_prefix;
        $query = "SELECT * FROM ".$table_prefix."_tblClient ".
        	"WHERE fldClientUsername LIKE '{$username}' ".
        	"AND fldClientId != {$current_user_id}";
        $result = $adb->query($query);

        return ($result->num_rows() == 0);
    }

    /**
    * checks if the email is unique
    */
    function isUniqueEmail($email, $current_user_id)
    {
	    global $adb;
		global $table_prefix;
        $query = "SELECT * FROM ".$table_prefix."_tblClient ".
        	"WHERE fldClientEmail LIKE '{$email}' ".
        	"AND fldClientId != {$current_user_id}";
        $result = $adb->query($query);

        return ($result->num_rows() == 0);
    }

}

?>