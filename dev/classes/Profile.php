<?
  
class Profile{
   
    
    function addProfile($profile,$image){
        global $adb;
		global $table_prefix;
		
		$firstname = addslashes($profile->firstname);
		$lastname = addslashes($profile->lastname);
		$email = addslashes($profile->email);
		$address1 = addslashes($profile->address1);
		$address2 = addslashes($profile->username);
		$city = addslashes($profile->city);
		$state = addslashes($profile->state);
		$zip = addslashes($profile->zip);
		$phone = addslashes($profile->phone);
		$password = addslashes($profile->password);
		$date = date('Y-m-d');
		
        $query="INSERT INTO ".$table_prefix."_tblProfile SET ".
			"fldProfileClientID='$profile->client_id',". 
			"fldProfileFirstname='$firstname',".
			"fldProfileLastname='$lastname',".
			"fldProfileEmail='$email',". 
			"fldProfileAddress1='$address1',". 
			"fldProfileAddress2='$address2',". 
			"fldProfileCity='$city',".
			"fldProfilePhone='$phone',".
			"fldProfileState='$state',".
			"fldProfileZip='$zip',".
			"fldProfilePassword='$password',".
			"fldProfileImage='$image',".
			"fldProfileRegDate='$date'";             
        $adb->query($query);
        return mysql_insert_id();
    }
	
	function updateProfile($profile,$image) {
		 global $adb;
		 global $table_prefix;
		
		$firstname = addslashes($profile->firstname);
		$lastname = addslashes($profile->lastname);
		$email = addslashes($profile->email);
		$address1 = addslashes($profile->address1);
		$address2 = addslashes($profile->username);
		$city = addslashes($profile->city);
		$state = addslashes($profile->state);
		$zip = addslashes($profile->zip);
		$password = addslashes($profile->password);
		$phone = addslashes($profile->phone);

		if($password != "") { 
			$query="UPDATE ".$table_prefix."_tblProfile SET ".
			"fldProfileClientID='$profile->client_id',". 
			"fldProfileEmail='$email',". 
			"fldProfileFirstname='$firstname',".
			"fldProfileLastname='$lastname',".
			"fldProfileAddress1='$address1',". 
			"fldProfileAddress2='$address2',". 
			"fldProfileCity='$city',".
			"fldProfilePhone='$phone',".
			"fldProfileState='$state',".
			"fldProfileZip='$zip',".
			"fldProfilePassword='$password'".		
      
            " WHERE fldProfileID=$profile->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblProfile SET ".
			"fldProfileClientID='$profile->client_id',". 
			"fldProfileEmail='$email',". 
			"fldProfileFirstname='$firstname',".
			"fldProfileLastname='$lastname',".
			"fldProfileAddress1='$address1',". 
			"fldProfilePhone='$phone',".
			"fldProfileAddress2='$address2',". 
			"fldProfileCity='$city',".
			"fldProfileState='$state',".
			"fldProfileZip='$zip'".      
            " WHERE fldProfileID=$profile->Id";
		}
		 $adb->query($query);
		 
		if($image != "") {
			$query="UPDATE ".$table_prefix."_tblProfile SET ".			
			"fldProfileImage='$image'".      
            " WHERE fldProfileID=$profile->Id";
			 $adb->query($query);
		}
	   
        return true;
	}
	
	
	
	
	function changePassword($profile) {
		 global $adb;
		 global $table_prefix;
		 
		$password = $profile->password;
			$query="UPDATE ".$table_prefix."_tblProfile SET ".          
			"fldProfilePassword='$password'".              
            " WHERE fldProfileID=$profile->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProfile";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProfile";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function countProfile() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProfile";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	function countProfileEmail($email) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProfile WHERE fldProfileEmail='$email'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	
	 function findProfile($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProfile WHERE fldProfileID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findProfileByClient($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProfile WHERE fldProfileClientID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findProfileByEmail($email){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProfile WHERE fldProfileEmail='$email'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	
	
	   
    function deleteProfile($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblProfile WHERE fldProfileID='$id'";
        $adb->query($query);
        return true;
    }
    
    
}
?>
