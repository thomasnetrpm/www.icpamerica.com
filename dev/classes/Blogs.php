<?
   
class Blogs{
   
    function addBlogs($blogs){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($blogs->description);
		$name = addslashes($blogs->name);
		$author = addslashes($blogs->author);
		$category = addslashes($blogs->category);
		$tags = addslashes($blogs->tags);
		//$date = date('Y-m-d');
        $query="INSERT INTO ".$table_prefix."_tblBlogs SET ".
            "fldBlogsName='$name',".
			"fldBlogsDescription='$description',".	
			"fldBlogsAuthor='$author',".	
			"fldBlogsCategory='$category',".	
			"fldBlogsTags='$tags',".	
            "fldBlogsDate='$blogs->date'";
        $adb->query($query);
        return true;
    }
	
	function updateBlogs($blogs) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($blogs->description);
		$name = addslashes($blogs->name);
		$author = addslashes($blogs->author);
		$category = addslashes($blogs->category);
		$tags = addslashes($blogs->tags);
		
			$query="UPDATE ".$table_prefix."_tblBlogs SET ".
				"fldBlogsName='$name',".
				"fldBlogsDescription='$description',".	
				"fldBlogsAuthor='$author',".	
				"fldBlogsCategory='$category',".	
				"fldBlogsDate='$blogs->date',".
				"fldBlogsTags='$tags'".	           
           		 " WHERE fldBlogsID=$blogs->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs ORDER BY fldBlogsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllID($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs WHERE fldBlogsID='$id' ORDER BY fldBlogsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllHome() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs ORDER BY fldBlogsDate DESC LIMIT 3";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllCategory($category) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs WHERE fldBlogsCategory='$category' ORDER BY fldBlogsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countBlogs() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblBlogs";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findBlogs($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblBlogs WHERE fldBlogsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteBlogs($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblBlogs WHERE fldBlogsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
