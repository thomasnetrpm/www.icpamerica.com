<?php

require_once '../PHPToolkit/NetSuiteService.php';

$service = new NetSuiteService();

$service->setSearchPreferences(false, 20);

// "email" => "SearchStringField",
$emailSearchField = new SearchStringField();
$emailSearchField->operator = "startsWith";
// $emailSearchField->searchValue = "j";
$emailSearchField->searchValue = "a";

// class_exists
$search = new EmployeeSearchBasic();
// "email" => "SearchStringField",
$search->email = $emailSearchField;

$request = new SearchRequest();
$request->searchRecord = $search;

$searchResponse = $service->search($request);

if (!$searchResponse->searchResult->status->isSuccess) {
    echo "SEARCH ERROR";
} else {
    $count = $searchResponse->searchResult->totalRecords;

    echo "SEARCH SUCCESS, records found: " . $count."<br>";
    for ($i=0; $i < $count; $i++) { 
		$employee_firstname = $searchResponse->searchResult->recordList->record[$i]->firstName;
		echo 'firstname: '.$employee_firstname.'<br>';    	
    }
}

// echo "<br><br><br>";
// print_r($searchResponse);
// echo "<br><br><br>";
// echo "This: ";
// echo $searchResponse->searchResult->recordList->record[0]->firstName;
// echo $employee->firstName;

$employee = $searchResponse->searchResult->recordList->record;
foreach( $employee as $empRow){
	echo "Name: ".$empRow->firstName."<br />";
}
?>