<?
	//delete page
	if(isset($_REQUEST['delete'])) {
		Pages::deletePages($_REQUEST['delete']);
		
		
		$updates = 'Delete page management content';
		AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
	
	if(isset($_REQUEST['delete_banner'])) {
		Pages::deletePagesBanner($_REQUEST['delete_banner']);
	}
?>
          
          <table id="page_manager">
          
        	<thead>
          	<tr class="headers">
            	<td width="70"> ID </td>
               
              <td width="200">Date Modified</td>
              <td width="650">Page Name</td>
              <td width="150">Action</td>
            </tr>
          </thead>
       
        	<tbody id="alter_rows">
         
		  	  <?php
					$page=Pages::displayAll();					  	
					foreach($page as $pages) {
            $pageID = $pages->fldPagesID; 
            if ($pageID > 1) { // Hide Index page - Metafields only
			    ?>
            <tr>
              <td><?=$pages->fldPagesID?></td>
              <td><?=$pages->fldPagesDateModified?></td>
              <td><?=$pages->fldPagesName?></td>
              <td align="center"><a href="<?=$ROOT_URL?>_admin/_manager/includes/page_mod.php?id=<?=$pages->fldPagesID?>" rel="shadowbox;width=1110;"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/page.php?delete=<?=$pages->fldPagesID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Page from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Page.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/delete.gif" width="16" height="16" alt="del" /></a> </td>
            </tr>
          <?php
            }
          } //foreach 
          ?>
          	
          </tbody>
          
          <tfoot>
          	<th colspan="4" align="right" height="30">
            	<dl>
              	<dt class="col1"></dt>
                <dd class="col2"></dd>
              </dl>
            </th>
          </tfoot>
        
        </table>
        <!-- /End Fetching Data Tables -->
