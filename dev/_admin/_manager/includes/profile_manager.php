<?
	//delete page
	if(isset($_REQUEST['delete'])) {
		Administrator::deleteAdministrator($_REQUEST['delete']);
		
		
		$updates = 'Delete Administrator content';
		AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
?>
        <table id="page_manager">        
        	<thead>
          	<tr class="headers">
            	<td width="70">ID</td>
              <td width="200">Real Name</td>
              <td width="410">Username</td>
              <td width="410">Email Address</td>
              <td width="150">Action</td>
            </tr>
          </thead>
       
        	<tbody id="alter_rows">
          
		  	  <?php
          if ($adminLevel==1) { // Super Admin displays all admin and sub-admin
            $admin = Administrator::displayAll();
          } else { // Sub Admin displays co-sub admin
            $condition = "fldAdministratorLevel <> 1 ";
            $admin = Administrator::findAllAdministratorByCondition($condition);
          }
					
          foreach($admin as $admins) {
            $isSuperAdmin = $admins->fldAdministratorLevel;
			    ?>
                    <tr>
                      <td><?=$admins->fldAdministratorID?></td>
                      <td><?=$admins->fldAdministratorRealName?><?=($isSuperAdmin==1)? "<br>&nbsp;<small>SUPER ADMIN</small>": "";?></td>
                      <td><?=$admins->fldAdministratorusername?></td>
                      <td><?=$admins->fldAdministratorEmail?></td>
                      <td align="center"><a href="<?=$ROOT_URL?>_admin/settings_mod.php?id=<?=$admins->fldAdministratorID?>"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/settings.php?delete=<?=$admins->fldAdministratorID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Administrator from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Administrator.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_assets/images/icons/delete.gif" width="16" height="16" alt="del" /></a> </td>
                    </tr>
           <? } //foreach ?>
          
          	
          </tbody>
          
          <tfoot>
          	<th colspan="5" align="right" height="30">
            	<dl>
              	<dt class="col1"><?=$pg[0]?></dt>
                <dd class="col2"></dd>
              </dl>
            </th>
          </tfoot>
        
        </table>
        <!-- /End Fetching Data Tables -->
