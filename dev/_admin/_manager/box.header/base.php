<?php
ob_start();
session_start();
?>

<?php 
require_once 'php_inheritance.php';
include   "../classes/Database.php";
include   "../classes/Connection.php";
include_once "../includes/bootstrap.php";    
include   "../classes/Administrator.php";
include   "../classes/Contact.php";
include   "../classes/Newsletter.php";
include   "../classes/HomePage.php";
include   "../classes/Pages.php";
include   "../classes/Modules.php";
include   "../classes/AdminAction.php";
include   "../includes/security.funcs.inc";
include   "../includes/Pagination.php";

if(!isset($_SESSION['admin_id'])) {
	header("Location: index.php");
}

$adminLevel = $_SESSION['admin_level'];
?>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>  
  <? include '_manager/box.header/emulator.php'; ?>
  <meta name="Keywords" content="" />
  <meta name="Description" content="" />
  <title> Welcome to <?=$SITE_NAME?> Admin Control Panel </title>
  <!--[if IE]>
    <script src="<?=$ROOT_URL?>_admin/_assets/js/html5.js"></script>
    <script language="javascript" type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/excanvas.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core2.css" />

  <? emptyblock('headercodes') ?>
  <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico" />
</head>

<body>
<div id="wrapper">
	
	<!-- /Template Start Here -->
  <figure>
  	<header>
    	<h1 title="Welcome to Dog and Rooster Admin Panel"><!-- Dog and Rooster Logo --></h1>
      <strong>Welcome Administrator!</strong>
      
      <nav id="menunav1">
      	<div id="menu">
        	<ul class="panel1">
          	<li><a href="<?=$ROOT_URL?>_admin/modules.php"></a></li>
          </ul>
        	<ul class="panel2">
          	<li><a href="<?=$ROOT_URL?>_admin/settings.php"></a></li>
          </ul>
        	<ul class="panel3">
          	<li><a href="<?=$ROOT_URL?>_admin/logout.php"></a></li>
          </ul>
        </div>
      </nav>
      <nav id="menunav2">
      	<ul>
          <li><a href="<?=$ROOT_URL?>_admin/page.php">Page Management</a></li>
          <li><a href="<?=$ROOT_URL?>_admin/modules.php">Modules Management</a></li>      
        </ul>
      </nav>
    </header>
    
    
    <section>
    	<div class="stop"></div>
      <!-- /Inter-Section Start Block -->

			<? emptyblock('section') ?>
      
      <!-- /Inter-Section End Block -->
			<div class="sbottom"></div>
    </section>  
  
  
  </figure>
	<!-- /Template End Here -->

  <footer>
    <small>Copyright <?=date('Y')?>. Dog and Rooster, Inc.</small>
  </footer>

<!--#extracodes#-->
<? emptyblock('extracodes') ?>
<!--#extracodes#-->
</div>
<? include '_manager/box.header/ie6update.php'; ?>
</body>
</html>
