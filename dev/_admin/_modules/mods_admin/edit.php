<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Administrator.php";
include   "../../../classes/State.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include   "../../../functions/myFunctions.php";

if(isset($_POST['submit'])) {
	$ctr="";
	if($ctr=="") { 
	 $_POST = sanitize($_POST);
      $admin = $_POST;
	  settype($admin,'object');
	  Administrator::updateAdministrator($admin); 
	  $success = "Administrator Successfully Saved!";
	  
	   $updates = 'update administrator content';
  	  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_contact/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_admin/dashboard.php">Back</a></li>
    </ul>    
</div>    
<? $admin = Administrator::findAdministrator($_REQUEST['id']); ?>
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post">
    <h3>ACM Administrator</h3>
    <fieldset style="width:1050px;">
      <legend>Administrator Panel</legend>
      <ul>
        <li>
          <label for="title">User name</label>
           <input type="text" id="title" name="username" value="<?=stripslashes($admin->fldAdministratorusername)?>">   
         
        </li>
        <li>
          <label for="password">Password</label>
           <input type="password" id="password" name="password">   
        </li>
        <li>
          <label for="email">Email Address</label>
           <input type="text" id="email" name="email" value="<?=stripslashes($admin->fldAdministratorEmail)?>">   
           
        </li>
         <li>
          <label for="real_name">Real Name</label>
           <input type="text" id="real_name" name="real_name" value="<?=stripslashes($admin->fldAdministratorRealName)?>">   
        </li>
       
        
               
       
  
      </ul>
    </fieldset>
    <ul class="submission">
    <input type="hidden" name="Id" value="<?=$admin->fldAdministratorID?>">
      <li><input type="submit" name="submit" value="Save Administrator"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>