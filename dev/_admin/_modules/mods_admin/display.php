<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Contact.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";  


?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/tiny.mods.js"></script>
</head>

<body>
<? if(isset($success)) { ?>
	<div class="alert"> <?=$success?> </div>
<? } ?>
<div id="store_overview">
    	<ul class="btn">
  			<li><a href="<?=$ROOT_URL?>_admin/modules_contact/view/">Back</a></li>    		
    	</ul>
</div>        
<? 
	$contact  = Contact::findContact($_REQUEST['id']);
?>
 
    <h3>ACM Contact</h3>
    <span></span>
    
      <ul>
         
       <li>
          <strong>Name :</strong> <?=$contact->fldContactName?><br><br>
          <strong>Phone :</strong> <?=$contact->fldContactPhoneNo?><br><br>
          <strong>Address :</strong> <?=$contact->fldContactAddress?><br><br>
          <strong>Fax :</strong> <?=$contact->fldContactFax?><br><br>
          <strong>Mobile :</strong> <?=$contact->fldContactMobile?><br><br>
          <strong>Where did you hear About us :</strong> <?=$contact->fldContactMarketing?><br><br>
          <strong>Email :</strong> <?=$contact->fldContactEmail?><br><br>
          <strong>Comments :</strong> <?=$contact->fldContactMessage?>
        </li>
       
        
      </ul>
      
        
    
    <ul class="submission">
    </ul>


<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>