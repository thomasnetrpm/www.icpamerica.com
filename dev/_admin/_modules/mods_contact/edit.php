<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Contact.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

if(isset($_POST['submit'])) {
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$email = $_POST['email'];
	
	if($firstname == "" || $lastname == "" ){$name_error = "Please enter name";$ctr=1;}
	
	if($ctr=="") { 
	 $_POST = sanitize($_POST);
      $contact = $_POST;
      settype($contact,'object');
      Contact::updateContact($contact); 
      $success = "Contacts Successfully Saved!";

      $updates = 'Add new contacts content';
      AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_event/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_contact/dashboard.php">Back</a></li>
    </ul>    
</div>    
<? $contact = Contact::findContact($_REQUEST['id']);?>
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post">
    <h3>ACM Contacts</h3>
    <fieldset style="width:1050px;">
      <legend>Contacts Panel</legend>
      <ul>
        <li>
          <label for="title">First Name</label>
           <input type="text" id="title" name="firstname" value="<?=stripslashes($contact->fldContactFirstName)?>">   
           <? if(isset($name_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$name_error?></b></div>
           <? } ?>
        </li>
        <li>
          <label for="title">Last Name</label>
           <input type="text" id="title" name="lastname" value="<?=stripslashes($contact->fldContactLastName)?>">   
           <? if(isset($name_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$name_error?></b></div>
           <? } ?>
        </li>
        <li>
          <label for="company">Company name</label>
           <input type="text" id="company" name="company" value="<?=stripslashes($contact->fldContactCompany)?>">   
        </li>
        <li>
          <label for="phone">Phone</label>
           <input type="text" id="phone" name="phone" value="<?=stripslashes($contact->fldContactPhoneNo)?>">         
        </li>
        <li>
          <label for="fax">Fax</label>
           <input type="text" id="fax" name="fax" value="<?=stripslashes($contact->fldContactFax)?>">         
        </li>
        <li>
          <label for="email">Email</label>
           <input type="text" id="email" name="email" value="<?=stripslashes($contact->fldContactEmail)?>">   
               
        </li>
        <li>
          <label for="subject">Address 1</label>
           <input type="text" id="address" name="address1" value="<?=stripslashes($contact->fldContactAddress1)?>">         
        </li>  
        <li>
          <label for="subject">Address 2</label>
           <input type="text" id="address" name="address2" value="<?=stripslashes($contact->fldContactAddress2)?>">         
        </li>  
        <li>
          <label for="city">City </label>
           <input type="text" id="city" name="city" value="<?=stripslashes($contact->fldContactCity)?>">         
        </li> 
         <li>
          <label for="state">State </label>
           <input type="text" id="state" name="state" value="<?=stripslashes($contact->fldContactState)?>">         
        </li>  
         <li>
          <label for="zip">ZIP</label>
           <input type="text" id="zip" name="zip" value="<?=stripslashes($contact->fldContactZip)?>">         
        </li>       
        <li>
          <label for="website">How Did You Find Us?</label>
           <input type="text" id="find" name="find" value="<?=stripslashes($contact->fldContactHowDidYouFindUs)?>">         
        </li>
          <li>
          <label for="interest">Inquiry / Comment</label>
           <input type="text" id="comment" name="comment" value="<?=stripslashes($contact->fldContactMessage)?>">         
        </li>
        <li>
          <label for="interest">Date Received: </label> 
          <?php
            echo $dateReceived = ($contact->fldContactDate)? date("M d, Y", strtotime($contact->fldContactDate)): '';
          ?>
        </li>
        
<!--          <li>
          <label for="interest">Email Updates</label>
-->          <? 
/*		  	if($contact->fldContactEmailUpdates == "Yes") { 
				$checked = "checked='checked'";
			} else {
				$checked = "";
			}
*/		  ?>
<!--			<input type="checkbox" name="email_updates" value="Yes" width="10px;" <?=$checked?>>
        </li>
-->  
      </ul>
    </fieldset>
    <ul class="submission">
    <input type="hidden" name="Id" value="<?=$_REQUEST['id']?>">
      <li><input type="submit" name="submit" value="Save Contact"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>