<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/NewsCategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

if(isset($_POST['submit'])) {
	
/*
  include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['image']['name'] != '') {


		$my_upload->upload_dir = "../../../news_image/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		$my_upload->rename_file = true;

		$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
		$my_upload->the_file = $_FILES['image']['name'];
		$my_upload->http_error = $_FILES['image']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		$new_name = 'News'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path = $my_upload->file_copy;
			// ... or do something like insert the filename to the database
			
		 	//resize the image
					
			//create a thumbnail 683 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_683_".$full_path;
			$obj_img->NewWidth = 683;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
				//create a thumbnail 360 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_360_".$full_path;
			$obj_img->NewWidth = 360;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 235 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_235_".$full_path;
			$obj_img->NewWidth = 235;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 75 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../news_image/_75_".$full_path;
			$obj_img->NewWidth = 75;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
		}

	} else {
		$full_path = "";
	}

	$_POST = sanitize($_POST);
	$news = $_POST;
	settype($news,'object');
	NewsCategory::updateNewsCategory($news,$full_path); 
	$success = "News Successfully Saved!";
*/	
	
	$_POST = sanitize($_POST);
	$newscat = $_POST;
	settype($newscat,'object');
	NewsCategory::updateNewsCategory($newscat); 
	$success = "News Category Successfully Saved!";

	$updates = 'update news category content';
	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

$categoryID = trim($_REQUEST['cid']);
$nc = NewsCategory::findNewsCategory($categoryID);

$nc_id 			= $nc->nc_id;
$nc_name 		= stripslashes($nc->nc_name);
$nc_description = stripslashes($nc->nc_description);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_news/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_news/dashboard_category.php">Back</a></li>
    </ul>    
</div>    

  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
    <h3>ACM News</h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>Edit News Category</legend>
      <ul>
        <li>
          <label for="name">Title</label>
            <input type="text" id="name" name="name" value="<?=$nc_name?>">
          </label>
        </li>
<?php        
/*
        <li>
          <label for="name">Page Title (for SEO URL)</label>
          	<br>
          	<small>Sample: This is for the SEO Url  -->  <i>This-is-for-the-SEO-Url</i></small>
            <input type="text" id="url" name="url" value="<?=stripslashes($news->fldNewsSEOURL)?>">
          </label>
        </li>
*/
?>
        <li>
          <label for="blog.editor">
            <textarea cols="" rows="" id="blog.editor" name="description"><?=$nc_description?></textarea>
          </label>
        </li>
<?php
/*
           <li>
          <label for="date">Date</label>
            <input type="text" id="date" name="date_news" value="<?=$news->fldNewsDate?>"> eg.. 2012-08-20
          </label>
        </li>
        
        <li>
          <label for="image"> Image </label>
            <input type="file" name="image">
            <? if($news->fldNewsImage != "") { ?>
            	<br>
				<img src="<?=$ROOT_URL?>news_image/_235_<?=$news->fldNewsImage?>">
            <? } ?>
          </label>
        </li>
*/
?>
      </ul>
    </fieldset>

<?php        
/*
	<fieldset>
		<legend>Meta Information</legend>
		<ul>
			<li><label for="meta_title"> Meta Title </label>
			<textarea cols="80" rows="10" id="meta_title" name="metatitle"><?=stripslashes($news->fldNewsMetaTitle)?></textarea></li>
			<li><label for="meta_keywords"> Meta Keywords </label>
			<textarea cols="80" rows="10" id="meta_keywords" name="metakeywords"><?=stripslashes($news->fldNewsMetaKeywords)?></textarea></li> 
			<li><label for="meta_description"> Meta Description </label>
			<textarea cols="80" rows="10" id="meta_description" name="metadesc"><?=stripslashes($news->fldNewsMetaDescription)?></textarea></li>    
		</ul>
	</fieldset>
*/
?>

        <ul class="submission">
    	<input type="hidden" name="Id" value="<?=$categoryID?>">
    	<input type="hidden" name="cid" value="<?=$categoryID?>">
      <li><input type="submit" name="submit" value="Save News Category"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>