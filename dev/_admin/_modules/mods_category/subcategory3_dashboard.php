<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

$subcat2ID = trim($_REQUEST['sc2id']);
$sc2 = Subcategory::findSubcategory($subcat2ID);

$mainCatID    = $sc2->fldCategoryID;
$c            = Category::findCategory($mainCatID);
$mainCatName  = $c->fldCategoryName; // Single Board

$subcatID     = $sc2->subcategoryID;
$sc           = Subcategory::findSubcategory($subcatID);
$subcatName   = $sc->fldSubcategoryName; // Backplane

$subcat2Name  = $sc2->fldSubcategoryName; // Picmg13

// $breadcrumb = ''.$mainCatID.': '.$mainCatName.' >> '.$subcatID.': '.$subcatName.' >> '.$subcat2ID.': '.$subcat2Name.'';
$breadcrumb = ''.$mainCatName.' >> '.$subcatName.' >> '.$subcat2Name.'';


// Delete Sub-category2
if(isset($_REQUEST['delete'])) {
  Subcategory::deleteSubcategory($_REQUEST['delete']);
  $updates = 'Delete Subcategory content ID: '.$_REQUEST['delete'];
  
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
  $link = "subcategory3_dashboard.php?sc2id=".$subcat2ID."&update=delete";

  header("Location: ".$link."");
  exit();
}
if ($_GET['update']=='delete') {
  $success = 'Subcategory Deleted!';
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">
<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
	<div id="blog_overview">
    	<ul class="btn">
            <li>
              <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory3_create.php?sc2id=<?=$subcat2ID?>">Add NEW Sub-Category</a>
            </li>
            <li>
              <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory2_dashboard.php?cid=<?=$mainCatID?>&scid=<?=$subcatID?>">Back</a>
            </li>
        </ul>

  <?php
  $count_record = Subcategory::countSubcategory3($subcat2ID);
   
  if(!isset($_REQUEST['page']))
    {
      $page = 1;
    }
    else
    {
    $page = $_GET[page];
    }
    $pagination = new Pagination;
    //for display
    $pg = $pagination->page_pagination(20, $count_record, $page, 20);
    //$result_prod = mysql_query($query_Recordset2.$pg[1]);
    $subcat = Subcategory::findAll3($subcat2ID, $pg[1]);
  ?>

  <h3><?=$breadcrumb?></h3>
  <form method="post" action="">
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="100">Image</td> 
          <td width="410">Sub-Category Name</td>
          <td width="100">Order</td>   
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else {
					foreach($subcat as $subcatR) { 
            $subcatID = $subcatR->fldSubcategoryID;
        ?>
            <tr id="<?=$subcatID?>_<?=$subcatR->fldSubcategoryPosition?>">
              <td><?=$subcatID?></td>
              <td><img src="<?=$ROOT_URL?>uploads/category/_75_<?=$subcatR->fldSubcategoryImage?>"></td>
              <td><?=$subcatR->fldSubcategoryName?></td>

              <td><input type="text" size="5" value="<?=$subcatR->fldSubcategoryPosition?>" name="position_<?=$subcatID?>"></td>

              <td align="center">
                <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory3_edit.php?id=<?=$subcatR->fldSubcategoryID?>">
                  <img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/modify.png" width="14" height="16" alt="mod" /></a>
                <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory3_dashboard.php?sc2id=<?=$subcat2ID?>&delete=<?=$subcatR->fldSubcategoryID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Sub-Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Sub-Category.\n&quot;)">
                  <img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/delete.gif" width="16" height="16" alt="del" /></a>
<!--                 <br>
                <a href='subcategory3_dashboard.php?scid=<?=$subcatR->fldSubcategoryID?>'>Sub-Category 3</a> -->
              </td>
            </tr>
        <? } }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2">
              <!-- <input type="submit" name="submit" value="Update Positions"> -->
            </dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
     <input type="submit" name="submit" value="Update Position">
    </form>
    <!-- /End Fetching Data Tables -->
    
</div>
<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.tablednd.js"></script>
  
<script type="text/javascript">
  Cufon.replace('h3');
  //   $(document).ready(function(){ 
        
  //     $('#page_manager').tableDnD({
  //       onDrop: function(table, row) {
  //         $.ajax({  
  //             type: "POST",
  //             url: "<?=$ROOT_URL?>_admin/_modules/mods_category/update.php?action=update_position_drop_subcat2&cid=<?=$categoryID?>&scid=<?=$subcategoryID?>",
  //             data: $.tableDnD.serialize(),
  //             cache: false,
  //             success: function(){
  //               document.location.href= "<?=$ROOT_URL?>_admin/_modules/mods_category/subcategory2_dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>";
  //             }
              
  //           }); 
  //       }
  //     });
  // });   
</script>

</body>
</html>