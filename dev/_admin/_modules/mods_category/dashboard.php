<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Category.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

// Delete Category
if(isset($_REQUEST['delete'])) {
	Category::deleteCategory($_REQUEST['delete']);
	
	$updates = 'Deleted Category';
  AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}

if(isset($_REQUEST['submit'])) {
  $category = Category::displayAll();
  foreach($category as $categories) {
    $position = $_POST['position_'.$categories->fldCategoryID];
    Category::updateCategoryByPosition($position, $categories->fldCategoryID);
  }
  
  $success = "Category Position Successfully Saved!";
}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">
<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
	<div id="blog_overview">
    	<ul class="btn">
            <li><a href="<?=$ROOT_URL?>_admin/_modules/mods_category/create.php">Add New Category</a></li>
        </ul>
  
  <h3>Category Overview</h3>
    
  <?php
   // $count_record = Category::countCategory();
   $count_record = Category::countCategoryNotHidden();
   
  if(!isset($_REQUEST['page']))
    {
      $page = 1;
    }
    else
    {
    $page = $_GET[page];
    }
    $pagination = new Pagination;
    //for display
    $pg = $pagination->page_pagination(20, $count_record, $page, 20);
    //$result_prod = mysql_query($query_Recordset2.$pg[1]);
    // $category = Category::findAll($pg[1]);
    $category = Category::findAllNotHidden($pg[1]);
  ?>

  <form method="post" action="">
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="100">Image</td> 
          <td width="310">Category Name</td>
          <td width="100">Is Featured</td>
          <td width="100">Order</td>   
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="6" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else {
					foreach($category as $catR) {
            $categoryID   = $catR->fldCategoryID;
            $isOutsideURL = $catR->fldCategoryIsUrl;
            $isFeatured   = $catR->fldCategoryIsFeatured;
        ?>
            <tr id="<?=$categoryID?>_<?=$catR->fldCategoryPosition?>">
              <td><?=$catR->fldCategoryID?></td>
              <td><img src="<?=$ROOT_URL?>uploads/category/_75_<?=$catR->fldCategoryImage?>"></td>
              <td>
                <?php
                echo $catR->fldCategoryName;
                if ($isOutsideURL==1) {
                  ?>
                   - <i><?=$catR->fldCategoryURL?></i>
                  <?php
                }
                ?>
              </td>
              <td>
                <?php
                if ($isFeatured == 1) {
                  echo "Yes";
                }
                ?>
              </td>

              <td><input type="text" size="5" value="<?=$catR->fldCategoryPosition?>" name="position_<?=$categoryID?>"></td>

              <td align="center"><a href="<?=$ROOT_URL?>_admin/_modules/mods_category/edit.php?id=<?=$catR->fldCategoryID?>"><img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/_modules/mods_category/dashboard.php?delete=<?=$catR->fldCategoryID?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Category.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mods_category/images/delete.gif" width="16" height="16" alt="del" /></a> 
              <?php
              if ($isOutsideURL!=1) {
                ?>
              <br>
              <a href='subcategory_dashboard.php?cid=<?=$catR->fldCategoryID?>'>Sub-Category</a>
                <?
              }
              ?>
              
              </td>
            </tr>
        <? } }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2">
            </dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
     <input type="submit" name="submit" value="Update Position">
    </form>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>
<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.tablednd.js"></script>
  
<script type="text/javascript">
  Cufon.replace('h3');
    $(document).ready(function(){ 
        
      $('#page_manager').tableDnD({
        onDrop: function(table, row) {
          $.ajax({  
              type: "POST",
              // url: "<?=$ROOT_URL?>_admin/_modules/mods_category/update.php?action=update_position_drop&category_id=<?=$category_id?>",
              url: "<?=$ROOT_URL?>_admin/_modules/mods_category/update.php?action=update_position_drop",
              data: $.tableDnD.serialize(),
              cache: false,
              success: function(){
                // document.location.href= "<?=$ROOT_URL?>_admin/_modules/mods_category/dashboard.php?category_id=<?=$category_id?>";
                document.location.href= "<?=$ROOT_URL?>_admin/_modules/mods_category/dashboard.php";
              }
              
            }); 
        }
      });
  });   
</script>

</body>
</html>