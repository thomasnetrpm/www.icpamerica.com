<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Client.php";
include   "../../../classes/State.php";
include   "../../../classes/Country.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include   "../../functions/myFunctions.php";

include   "../../../classes/Billing.php";
include   "../../../classes/Shipping.php";


if(isset($_POST['submit'])) {
	$firstname = $_POST['firstname'];
	$lastname  = $_POST['lastname'];
	$email     = $_POST['email_address'];
  $username  = $_POST['username'];
  $password  = $_POST['password'];
  $type      = $_POST['member_type'];
	
	if ($firstname == "") { $firstname_error = "Please enter First name";$ctr=1;}
	if ($lastname == "") { $lastname_error = "Please enter Last name";$ctr=1;}
  if ($username == "") { $username_error = "Please enter Username";$ctr=1;}
  if ($password == "") { $pw_error = "Please enter Password";$ctr=1;}
  if ($type == "") { $type_error = "Please select customer type";$ctr=1;}

	if (!checkEmail($email)) {$email_error = "Please enter your valid email address";$ctr=1;}
	
	if($ctr=="") { 
	  $_POST = sanitize($_POST);
    $client = $_POST;
	  settype($client,'object');
	  Client::addClientFromAdmin($client); 
	  $success = "Customer Successfully Added!";
	  
	  $updates = 'Add new customer content';
  	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}
}

?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_event/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_client/dashboard.php">Back</a></li>
    </ul>    
</div>    
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post">
    <h3>ACM Customer</h3>
    <fieldset style="width:1050px;">
      <legend>Customer Panel</legend>
      <ul>
        <li>
          <label for="title">First name</label>
           <input type="text" id="title" name="firstname">   
           <? if(isset($firstname_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$firstname_error?></b></div>
           <? } ?>
        </li>
         <li>
          <label for="title">Last name</label>
           <input type="text" id="title" name="lastname">   
           <? if(isset($lastname_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$lastname_error?></b></div>
           <? } ?>
        </li>

        <li>
          <label for="username">Username</label>
           <input type="text" id="username" name="username">   
            <? if(isset($username_error)) { ?>
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$username_error?></b></div>
           <? } ?>
        </li>
        <li>
          <label for="password">Password</label>
           <input type="text" id="password" name="password">   
            <? if(isset($pw_error)) { ?>
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$pw_error?></b></div>
           <? } ?>
        </li>

        <li>
          <label for="email">Email</label>
           <input type="text" id="email" name="email_address">   
            <? if(isset($email_error)) { ?>      
           		<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$email_error?></b></div>
           <? } ?>      
        </li>

        <li>
          <label for="member_type">Type</label>
            <? if(isset($type_error)) { ?>      
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$type_error?></b></div>
            <? } ?>
            <select name='member_type'>
              <option value=''>Choose</option>
              <option value='1'>Basic</option>
              <option value='2'>Corporate</option>
              <option value='3'>Type 3</option>
            </select>
        </li>
        <li>
          <label for="company">Company</label>
           <input type="text" id="company" name="company">         
        </li>
        <li>
          <label for="phone">Telephone Number</label>
           <input type="text" id="phone" name="phone">         
        </li>
        <li>
          <label for="address1">Address 1</label>
           <input type="text" id="address1" name="address1">         
        </li>
        <li>
          <label for="address2">Address 2</label>
           <input type="text" id="address2" name="address2">         
        </li>
         <li>
          <label for="city">City</label>
           <input type="text" id="city" name="city">         
        </li>
        <li>
          <label for="state">State</label>

            <select name="state" class="select_state">
            <? 
            $state = State::findAll();
            foreach($state as $states) {
              ?>
              <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
              <?php
            } ?>
            </select>
        </li>
        <li>
          <label for="country">Country</label>
              <select name ="country" class="select_text">                 
              <? 
              $country = Country::findAll();
              foreach($country as $countries) {
                $defaultCountry = "US";
              ?>
                <option value="<?=$countries->country_code?>" <?=($countries->country_code==$defaultCountry)? "selected": "";?> ><?=$countries->country_name?></option>
              <? } ?>
              </select>
        </li>
          <li>
          <label for="zip">Zip Code</label>
           <input type="text" id="zip" name="zip">         
        </li>

      </ul>
    </fieldset>
    <ul class="submission">
      <li><input type="submit" name="submit" value="Save Customer"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>