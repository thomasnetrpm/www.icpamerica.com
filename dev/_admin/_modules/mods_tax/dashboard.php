<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/State.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   



?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">
<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>  
	<div id="blog_overview">
  
  <h3>Tax Overview</h3>
    
  <?php
  
    $state = State::findAll($pg[1]);
  ?>

  <form method="post" action="">
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="70">ID</td>
          <td width="410">Name</td>
          <td width="410">Tax</td>
          <td width="150" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
		  <?          
				foreach($state as $states) { 
         ?>
            <tr>
              <td><?=$states->fldStateID?></td>            
              <td><?=$states->fldStateName?></td>
              <td><?=$states->fldStateTax?></td>
             
              <td align="center"><a href="<?=$ROOT_URL?>_admin/_modules/mods_tax/edit.php?id=<?=$states->fldStateID?>"><img src="<?=$ROOT_URL?>_admin/_modules/mods_tax/images/modify.png" width="14" height="16" alt="mod" /></a> 
              
              </td>
            </tr>
        <? }?>
       
      </tbody>
      
      <tfoot>
      <th colspan="6" align="right" height="30">
          <dl>
            <dt class="col1"></dt>
            <dd class="col2">
              <!-- <input type="submit" name="submit" value="Update Positions"> -->
            </dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    </form>
    <!-- /End Fetching Data Tables -->
    
   
  
</div>
<script type="text/javascript">
  function goToPage(page) {
    window.location = page;
  } 
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>