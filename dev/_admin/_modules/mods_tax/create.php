<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Authors.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include_once('thumbnail/thumbnail_images.class.php');
include_once('functions/myFunctions.php');

if(isset($_POST['submit'])) {
    if(isset($_POST['isFeatured'])) {
		$isFeatured = 1;
	} else {
		$isFeatured = 0;
	}
    $_POST = sanitize($_POST);
	$authors = $_POST;
	settype($authors,'object');
	$authors->isFeatured = $isFeatured;
	$author_id = Authors::addAuthors($authors); 
	$success = "Author Successfully Saved!";
	
	if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/uploads/authors/'.$author_id."/")) {
		mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads/authors/'.$author_id."/",0777);
	}
	
   include ("upload_class.php"); //classes is the map where the class file is stored (one above the root)

	$max_size = 1024*250; // the max. size for uploading

	$my_upload = new file_upload;

	if($_FILES['image']['name'] != '') {

		$my_upload->upload_dir = "../../../uploads/authors/".$author_id."/"; // "files" is the folder for the uploaded files (you have to create this folder)
		$my_upload->extensions = array(".jpeg", ".gif", ".bmp",".jpg",".png");// specify the allowed extensions here
		// $my_upload->extensions = "de"; // use this to switch the messages into an other language (translate first!!!)
		$my_upload->max_length_filename = 50; // change this value to fit your field length in your database (standard 100)
		$my_upload->rename_file = true;

		$my_upload->the_temp_file = $_FILES['image']['tmp_name'];
		$my_upload->the_file = $_FILES['image']['name'];
		$my_upload->http_error = $_FILES['image']['error'];
		$my_upload->replace = 'y';
		//$my_upload->replace = (isset($_POST['replace'])) ? $_POST['replace'] : "n"; // because only a checked checkboxes is true
		//$my_upload->do_filename_check = (isset($_POST['check'])) ? $_POST['check'] : "n"; // use this boolean to check for a valid filename

		$new_name = 'Authors'.GetSID(7);
		//$new_name = (isset($_POST['name'])) ? $_POST['name'] : "";
		if ($my_upload->upload($new_name)) { // new name is an additional filename information, use this to rename the uploaded file
			$full_path_name = $my_upload->upload_dir.$my_upload->file_copy;
			$info = $my_upload->get_uploaded_file_info($full_path_name);
			$full_path = $my_upload->file_copy;
			// ... or do something like insert the filename to the database
			
		 	//resize the image
			
			// create a thumbnail 320 width
			// $obj_img = new thumbnail_images();
			// $obj_img->PathImgOld = $full_path_name;
			// $obj_img->PathImgNew = "../../../uploads/category/_320_".$full_path;
			// $obj_img->NewWidth = 320;
			// 		//$obj_img->NewHeight = 58;
			// if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 75 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/authors/".$author_id."/_75_".$full_path;
			$obj_img->NewWidth = 75;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
			
			//create a thumbnail 230 width
			$obj_img = new thumbnail_images();
			$obj_img->PathImgOld = $full_path_name;
			$obj_img->PathImgNew = "../../../uploads/authors/".$author_id."/_230_".$full_path;
			$obj_img->NewWidth = 230;
					//$obj_img->NewHeight = 58;
			if (!$obj_img->create_thumbnail_images()) { echo "error"; }
								
		}

	} else {
		$full_path = "";
	}
	
	Authors::updateAuthorsImage($full_path,$author_id);
	
}

?>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
  
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_author/js/tiny.mods.js"></script>
  
</head>

<body>
	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
    <? } ?>    
<div id="blog_overview">
	<ul class="btn">
	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_author/dashboard.php">Back</a></li>
    </ul>    
</div>    
  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post" enctype="multipart/form-data">
    <h3>ACM Authors</h3>
    <span></span>
    <fieldset style="width:1050px;">
      <legend>New Authors</legend>
      <ul>
        <li>
          <label for="name">Name</label>
            <input type="text" id="name" name="name">
          </label>
        </li>
          <li>
          <label for="name">Quote</label>
            <textarea name="quote"></textarea>
          </label>
        </li>
        <li>
          <label for="image">Image</label>
            <input type="file" name="image">
          </label>
        </li>
        <li>
          <label for="position">isFeatured</label>
            <input type="checkbox" name="isFeatured" style="width:15px !important;">
          </label>
        </li>
        <li>
          <label for="title">Author Information</label>
          <label for="store.editor">
          		<textarea cols="" rows="" id="blog.editor" name="description"> </textarea>
          </label>
        </li>
      </ul>
    </fieldset>
    <ul class="submission">
      <li><input type="submit" name="submit" value="Save Authors"></li>
      <li><input type="reset" value="Clear Forms"></li>
    </ul>
  </form>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>