<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/Products.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

$categoryID     = trim($_REQUEST['cid']);
$subcategoryID  = trim($_REQUEST['scid']);
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="store_overview">
    	<ul class="btn">
        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_create.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>">Add New Product</a></li>
        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/subcategory_dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>">Back</a></li>
    	</ul>
    <h3>Category Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="10%">ID</td>
          <td width="50%">Category Name</td>
          <td width="20%">Position</td>
          <td width="20%" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
       <?php
        $count_record = Subcategory::countSubcategory2($subcategoryID);
				 
				if(!isset($_REQUEST['page']))
					{
						$page = 1;
					}
					else
					{
					$page = $_GET[page];
					}
					$pagination = new Pagination;
					//for display
					$pg = $pagination->page_pagination(20, $count_record, $page, 20);
          $subCat2 = Subcategory::findAll2($subcategoryID, $pg[1]);
			?>
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="4" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else { 
					foreach($subCat2 as $sc2) { // Main Category
            $subcat2ID = $sc2->fldSubcategoryID;
            $subcat2Name = $sc2->fldSubcategoryName;
            $subcat2Position = $sc2->fldSubcategoryPosition;
			    ?>		
                <tr>
                  <td><?=$subcat2ID?></td>
                  <td><?=$subcat2Name?></td>
                  <td><?=$subcat2Position?></td>
                  <td align="center">
                    <? /* <a href="<?=$ROOT_URL?>_admin/modules_category/edit/<?=$categories->fldCategoryID?>/"><img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/modify.png" width="14" height="16" alt="mod" /></a> <a href="<?=$ROOT_URL?>_admin/modules_category/delete/<?=$categories->fldCategoryID?>/" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Category from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Category.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mods_store/images/delete.gif" width="16" height="16" alt="del" /></a> */ ?>
                  <?php
                  // Check for list of products
                  // $subCat2 = Subcategory::displayAll2($subcategoryID);

                  // $condition = "AND fldProductsSubcategory2ID='$subcat2ID' ";
                  // $product = Products::displayAllByCondition($categoryID, $condition);

                  // if (empty($product)) {
                  //   echo 'Empty';
                  // } else {
                  //   ?>
<!--                     <a href="dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcat2ID?>">View Products</a> -->
                    <?php
                  // }
                  ?>


                  <?php
                  // Check for subcategory or list of products
                  $subCat2 = Subcategory::displayAll3($subcat2ID);
                  if (empty($subCat2)) {
                    // Check if products are available
                    $condition = "AND fldProductsSubcategory2ID='$subcat2ID' ";
                    $product = Products::displayAllByCondition($categoryID, $condition);
                    if (empty($product)) {
                      echo 'Empty';
                    } else {
                      // echo 'View Products';
                      ?>
                      <a href="dashboard.php?cid=<?=$categoryID?>&scid=<?=$subcategoryID?>&sc2id=<?=$subcat2ID?>">View Products</a>
                      <?php
                    }

                  } else {
                    ?>
                    <a href="subcategory3_dashboard.php?sc2id=<?=$subcat2ID?>">View Subcategory</a>
                    <?php
                  }
                  ?>

                  </td>
                </tr>

        <? } } ?>
        
      </tbody>
      
      <tfoot>
      <th colspan="4" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2"></dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    <!-- /End Fetching Data Tables -->
    
    
  
  </div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_acm/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>