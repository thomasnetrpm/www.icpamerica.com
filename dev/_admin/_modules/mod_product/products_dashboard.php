<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";
include   "../../../classes/Category.php";
include   "../../../classes/Subcategory.php";
include   "../../../classes/AdminAction.php";
include   "../../../classes/Products.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   

//delete photo
if(isset($_REQUEST['delete'])) {
	Products::deleteProducts($_REQUEST['delete']);
	
	$updates = 'Delete products content';
  	    AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
}
?>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 
</head>

<body onLoad="javascript:alternatecolor('alter_rows');">

	<div id="store_overview">
    	<ul class="btn">
        <li><a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_create.php">Add New Product</a></li>           
    		
    	</ul>
    <h3>Products Overview</h3>
  
    <table id="page_manager">
    
      <thead>
        <tr class="headers">
          <td width="30">ID</td>
          <td width="75"></td>
          <td width="225">Product Name</td>  
          <td width="50">Product Basic Price</td>
          <td width="200">Category</td>
          <td width="50">Position</td>
          <td width="50" align="center">Action</td>
        </tr>
      </thead>
    
      <tbody id="alter_rows">
       <?php
				$count_record=Products::countProducts();
				 
				if(!isset($_REQUEST['page'])) {
					$page = 1;
				} else {
					$page = $_GET[page];
				}
				$pagination = new Pagination;
				//for display
				$pg = $pagination->page_pagination(20, $count_record, $page, 20);
				//$result_prod = mysql_query($query_Recordset2.$pg[1]);
				$product=Products::findAll($pg[1]);
			?>
		  	<? if($count_record == 0) { ?>
            	  <tr>
                  	<td colspan="7" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00; font-weight:bold">No Record Found</td>
                  </tr>
            <? } else { 
					foreach($product as $products) { 
						$image = $products->fldProductsImage;
			?>		
                <tr>
                  <td> <?=$products->fldProductsId?> </td>
                  <td>
                    <? 
                    if($products->fldProductsImage!='') { ?>
                    <img src="<?=$ROOT_URL?>uploads/product_image/<?=$products->fldProductsId?>/_75_<?=$image?>" width="75">
                    <? } ?>
                  </td>
                  <td>
                    <small>
                      <?=$products->fldProductsName?><br><i><?=$products->fldProductsCode?></i>
                      <br><i><?=$ROOT_URL.'products/'.$products->fldProductsURL?></i>
                    </small>
                  </td>
                  <td><?='$'.number_format($products->fldProductsPrice1,2)?></td> 
                  <td><small>
                    <?
                    $category   = Category::findCategory($products->fldProductsMainCategoryID);
                    $subcat     = Subcategory::findSubcategory($products->fldProductsSubcategoryID);
                    $subcat2    = Subcategory::findSubcategory($products->fldProductsSubcategory2ID);
                    echo $category->fldCategoryName;
                    if ($subcat->fldSubcategoryName) { echo ' / <br>'.$subcat->fldSubcategoryName;}
                    if ($subcat2->fldSubcategoryName) { echo ' / <br>'.$subcat2->fldSubcategoryName;}
                    ?></small>
                  </td>
                  <td><?=$products->fldProductsPosition?></td>
                      
                  <td align="center">
                    <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_edit.php?Id=<?=$products->fldProductsId?>">
                    <img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/modify.png" width="14" height="16" alt="mod" />
                    </a>
                    <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/products_dashboard.php?delete=<?=$products->fldProductsId?>" title="Delete Page" onClick="return confirm(&quot;Are you sure you want to completely remove this Products from the database?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without deleting the Products.\n&quot;)"><img src="<?=$ROOT_URL?>_admin/_modules/mod_product/images/delete.gif" width="16" height="16" alt="del" /></a>
                    <!-- <a href="<?=$ROOT_URL?>_admin/_modules/mod_product/options/<?=$products->fldProductsId?>/"> [ Options Amount ] </a> -->
                  </td>
                </tr>
        <? } } ?>
        
      </tbody>
      
      <tfoot>
      <th colspan="7" align="right" height="30">
          <dl>
            <dt class="col1"><?=$pg[0]?></dt>
            <dd class="col2"></dd>
          </dl>
        </th>
      </tfoot>
    
    </table>
    <!-- /End Fetching Data Tables -->
    
    
  
  </div>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/alternate_color.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_acm/_assets/js/cufon.js"></script>
<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>
<script type="text/javascript">
	Cufon.replace('h3');
</script>

</body>
</html>