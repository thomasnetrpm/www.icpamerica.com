<? 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Subcategory.php";
include   "../../../classes/Category.php";
include   "../../../classes/Products.php";

$action = $_REQUEST['action'];

if($action == "update_position_drop_products") {
	foreach($_REQUEST['page_manager'] as $pageManager) {
		$values =  explode("_",$pageManager);
		$id = $values[0];
		$position = $values[1];
		if($ctr>0) { 
			Products::updateProductsByPosition($ctr,$id);
		}
		$ctr=$ctr+1;
	}

} else if($action == "update_position_drop") {
	foreach($_REQUEST['page_manager'] as $pageManager) {
		$values =  explode("_",$pageManager);
		$id = $values[0];
		$position = $values[1];
		if($ctr>0) { 
	 		Category::updateCategoryByPosition($ctr,$id);
		}
		$ctr=$ctr+1;
	}

} else if($action == "update_position_drop_subcat") {
	foreach($_REQUEST['page_manager'] as $pageManager) {
		$values =  explode("_",$pageManager);
		$id = $values[0];
		$position = $values[1];
		if($ctr>0) { 
	 		Subcategory::updateSubcategoryByPosition($ctr,$id);
		}
		$ctr=$ctr+1;
	}

} else if($action == "update_position_drop_subcat2") {
	foreach($_REQUEST['page_manager'] as $pageManager) {
		$values =  explode("_",$pageManager);
		$id = $values[0];
		$position = $values[1];
		if($ctr>0) { 
	 		Subcategory::updateSubcategory2ByPosition($ctr,$id);
		}
		$ctr=$ctr+1;
	}
}
?>

