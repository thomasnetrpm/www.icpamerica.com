<?php 
include   "../../../classes/Database.php";
include   "../../../classes/Connection.php";
include_once "../../../includes/bootstrap.php";    
include   "../../../classes/Coupon.php";
include   "../../../classes/AdminAction.php";
include   "../../../includes/security.funcs.inc";
include_once "../../../includes/Pagination.php";   
include   "../../functions/myFunctions.php";

if(isset($_POST['submit'])) {

	$name = $_POST['name'];
	$code = $_POST['code'];
  $cpnAmount      = $_POST['amount'];
  $cpnPercent     = $_POST['percent'];
  $expiryDate = $_POST['expiry'];
  $cpnFreeShipping= $_POST['isFreeShipping'];

	if($name == ""){$name_error = "Please enter your coupon name";$ctr=1;}
	if($code == ""){$code_error = "Please enter your coupon code";$ctr=1;}
  if (($cpnAmount == "") && ($cpnPercent == "")) { $discount_error = "Please enter Coupon Discount."; $ctr=1;}
  if($expiryDate == ""){$expiry_error = "Please enter your expiry date";$ctr=1;}


  if ($cpnAmount > 0) {
    $_POST['percent'] = '';
    $_POST['isFreeShipping'] = '';
  } else {
    if ($cpnPercent > 0) {
      $_POST['isFreeShipping'] = '';
      $isFreeShipping = 0;
    }
  }


	if($ctr=="") { 

	  if ($_POST['isFreeShipping']) {
		  $isFreeShipping = 1;
	  } else {
		  $isFreeShipping = 0;
	  }

  	$_POST = sanitize($_POST);
    $coupons = $_POST;
    settype($coupons,'object');

    // $expiryDate = $_POST['expiry'];
    $date = explode('/', $expiryDate);
    $expiryDate = $date[2].'-'.$date[0].'-'.$date[1];

    $coupons->expiryDate = $expiryDate;
    $coupons->isFreeShipping = $isFreeShipping;
    Coupon::addCoupon($coupons);
    $success = "Coupon Successfully Saved!";

    $updates = 'Add new coupon content';
  	AdminAction::addAdminAction($_SESSION['admin_name'],$updates);
	}

}
?>
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/core3.css" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$ROOT_URL?>_admin/_assets/css/modules.css" /> 

  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_manager/tinymce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?=$ROOT_URL?>_admin/_modules/mods_event/js/tiny.mods.js"></script>
  
  <link rel="stylesheet" href="<?=$ROOT_URL?>_admin/plugins/jquery-ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" />
</head>

<body>

	<? if(isset($success)) { ?>
		<div class="alert"> <?=$success?> </div>
  <? } ?>    

<div id="blog_overview">

	<ul class="btn">

	  	<li><a href="<?=$ROOT_URL?>_admin/_modules/mods_coupon/dashboard.php">Back</a></li>

    </ul>    

</div>    

  <form id="blog_page" action="<? $PHP_SELF; ?>" method="post">

    <h3>ACM Coupon Code</h3>

    <fieldset style="width:1050px;">

      <legend>Coupon Code Panel</legend>

      <ul>

        <li>
          <label for="title">Coupon name</label>
           <? if(isset($name_error)) { ?>
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$name_error?></b></div>
           <? } ?>
           <input type="text" id="title" name="name">   
        </li>

        <li>
          <label for="title">Coupon Code</label>
           <? if(isset($code_error)) { ?>      
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$code_error?></b></div>
           <? } ?>
           <input type="text" id="title" name="code">   
        </li>

        <? if(isset($discount_error)) { ?>
        <br><div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$discount_error?></b></div>
        <? } ?>
        <li>
          <label for="amount">Amount</label>
          $<input type="text" id="amount" name="amount">   
        </li>
        <li>
          <label for="percent">Percent %</label>
          <input type="text" id="percent" name="percent">
        </li>

        <li>
          <label for="expiry">Expiry Date</label>
           <? if(isset($expiry_error)) { ?>
              <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#F00"><b><?=$expiry_error?></b></div>
           <? } ?>
          <input type="text" name="expiry" value="" id="datepicker">
        </li>

<?php /*          <li>
          <label for="shipping">Free Shipping</label>
          <input type="checkbox" name="isFreeShipping" <?=$checked?> >               
        </li> */ ?>

      
      </ul>

    </fieldset>

    <ul class="submission">

      <li><input type="submit" name="submit" value="Save Coupon Code"></li>

      <li><input type="reset" value="Clear Forms"></li>

    </ul>

  </form>



<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/jquery.js"></script>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script>
$(function() {
  $( "#datepicker" ).datepicker();
});
</script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon.js"></script>

<script type="text/javascript" src="<?=$ROOT_URL?>_admin/_assets/js/cufon_font.js"></script>

<script type="text/javascript">

	Cufon.replace('h3');

</script>



</body>

</html>