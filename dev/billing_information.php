<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
include 'classes/Coupon.php';
include 'classes/Tax.php';
include 'classes/Administrator.php';
?>
<?php
// if(isset($_POST['update'])) {
if ( (isset($_POST['update'])) || (isset($_POST['state'])) ) {
    //update the member information
    $_POST = sanitize($_POST);
    $client = $_POST;
    settype($client,'object');
    if($_POST['Id']=="") {
        Billing::addBilling($client);
        $shipping = Shipping::addShipping($client);         
    } else {
        Billing::updateBilling($client);
        $shipping = Shipping::updateShipping($client); 
    }
}

if(isset($_SESSION['client_id'])) {
  $client_id = $_SESSION['client_id'];

  $client   = Client::findClient($client_id);
  $billing  = Billing::findBillingClient($client_id);
  $shipping = Shipping::findShippingClient($client_id);

    // Check if cart has content
    $countCart = Tempcart::countTempcartbyClient($client_id);
    if ($countCart >= 1) {
      // Do nothing
    } else {
      header("Location: ".$root."shopping-cart.html");
      die();
    }
} else {
  header("Location: index.php");
}

if ($_REQUEST['continue_checkout']==1) {
  header("Location: ".$root."order-confirmation.html");
  die();
}

if ($_REQUEST['back_to_cart']==1) {
  header("Location: ".$root."shopping-cart.html");
  die();
}

/*
if(isset($_POST['submit_bill'])) {
  //update the member information
  $_POST = sanitize($_POST);
  $client = $_POST;
  settype($client,'object');
  if($_POST['Id']=="") {
    Billing::addBilling($client);
    $shipping = Shipping::addShipping($client);     
  } else {
    Billing::updateBilling($client);
    $shipping = Shipping::updateShipping($client); 
  }
  
  $date = date('Y-m-d');
  $order_code = $client_id .'-'.date('Ymd').'-'.rand(1,400);
  $status = 'New';
  $condition = "fldTempCartClientID='$client_id' AND fldTempCartDate='$date'";
        
  $cart = Tempcart::findTempcartByCondition($condition);
  foreach($cart as $carts) {
    //get all info of tempcart
    $_POST = sanitize($_POST);
    $myCart = $_POST;
    settype($myCart,'object');
    
    $myCart->fldCartProductID = $carts->fldTempCartProductID;
    $myCart->fldCartClientID = $carts->fldTempCartClientID;
    $myCart->fldCartProductName = $carts->fldTempCartProductName;
    $myCart->fldCartProductVariant = $carts->fldTempCartProductVariant;
    $myCart->fldCartProductPrice = $carts->fldTempCartProductPrice;
    $myCart->fldCartProductPriceText = $carts->fldTempCartProductPriceText;
    $myCart->fldCartQuantity =$carts->fldTempCartQuantity;
    $myCart->fldCartOrderNo = $order_code;        
    $myCart->fldCartStatus = $status;
    //save to cart
    Cart::addCart($myCart);               
  }
  
  //save tax
  $stateTax = State::findState($_POST['state']);           
  $taxAmount = $_POST['subtotal'] * ($stateTax->fldStateTax);
  // $total = $_POST['subtotal'] + $taxAmount;

  $tax = $taxAmount;
  Tax::addTax($order_code,$tax);
  // $tax = $_POST['tax'];
  // Tax::addTax($order_code,$tax);

  // Update Cart with Coupon
  Cart::updateCartXCoupon($order_code, $client);
  
  // remove order from tempcart
  Tempcart::deleteTempCartByCondition($condition);
  
  $clients = Billing::findBillingClient($client_id);
  $shipping = Shipping::findShippingClient($client_id);
  
  //send email order to CLIENT
  require_once("includes/email_receipt.php");
  
  $links = $root."thank_you/default.html";
  header("Location: $links");
  exit();
}
*/

$bill_lastname = ($billing->fldBillingLastname)? $billing->fldBillingLastname: $client->fldClientLastname;
$bill_firstname = ($billing->fldBillingFirstName)? $billing->fldBillingFirstName: $client->fldClientFirstName;
$bill_address1 = ($billing->fldBillingAddress)? $billing->fldBillingAddress: $client->fldClientAddress;
$bill_address2 = ($billing->fldBillingAddress1)? $billing->fldBillingAddress1: $client->fldClientAddress1;
$bill_city = ($billing->fldBillingCity)? $billing->fldBillingCity: $client->fldClientCity;
$bill_state = ($billing->fldBillingState)? $billing->fldBillingState: $client->fldClientState;
$bill_country = ($billing->fldBillingCountry)? $billing->fldBillingCountry: $client->fldClientCountry;
$bill_zip = ($billing->fldBillingZip)? $billing->fldBillingZip: $client->fldClientZip;
$bill_phone = ($billing->fldBillingPhoneNo)? $billing->fldBillingPhoneNo: $client->fldClientPhoneNo;
$bill_email = ($billing->fldBillingEmail)? $billing->fldBillingEmail: $client->fldClientEmail;

$ship_lastname = ($shipping->fldShippingLastname)? $shipping->fldShippingLastname: $client->fldClientLastname;
$ship_firstname = ($shipping->fldShippingFirstName)? $shipping->fldShippingFirstName: $client->fldClientFirstName;
$ship_address1 = ($shipping->fldShippingAddress)? $shipping->fldShippingAddress: $client->fldClientAddress;
$ship_address2 = ($shipping->fldShippingAddress1)? $shipping->fldShippingAddress1: $client->fldClientAddress1;
$ship_city = ($shipping->fldShippingCity)? $shipping->fldShippingCity: $client->fldClientCity;
$ship_state = ($shipping->fldShippingState)? $shipping->fldShippingState: $client->fldClientState;
$ship_country = ($shipping->fldShippingCountry)? $shipping->fldShippingCountry: $client->fldClientCountry;
$ship_zip = ($shipping->fldShippingZip)? $shipping->fldShippingZip: $client->fldClientZip;
$ship_phone = ($shipping->fldShippingPhoneNo)? $shipping->fldShippingPhoneNo: $client->fldClientPhoneNo;
$ship_email = ($shipping->fldShippingEmail)? $shipping->fldShippingEmail: $client->fldClientEmail;
?>
<script language="javascript">        
    function chkClickInfo() {
      if($("#chkShip").is(':checked')) {
        document.getElementById('shipping_firstname').value = document.getElementById('firstname').value;
        document.getElementById('shipping_lastname').value = document.getElementById('lastname').value;
        document.getElementById('shipping_address').value = document.getElementById('address').value;
        document.getElementById('shipping_address1').value = document.getElementById('address1').value;
        document.getElementById('shipping_city').value = document.getElementById('city').value;
        document.getElementById('shipping_state').value = document.getElementById('state').value;
        document.getElementById('shipping_country').value = document.getElementById('country').value;
        document.getElementById('shipping_zip').value = document.getElementById('zip').value;
        document.getElementById('shipping_phone').value = document.getElementById('phone').value;
        document.getElementById('shipping_email').value = document.getElementById('email').value;
      } else {
        document.getElementById('shipping_firstname').value = "<?=stripslashes($ship_lastname)?>";
        document.getElementById('shipping_lastname').value = "<?=stripslashes($ship_firstname)?>";
        document.getElementById('shipping_address').value = "<?=stripslashes($ship_address1)?>";
        document.getElementById('shipping_address1').value = "<?=stripslashes($ship_address2)?>";
        document.getElementById('shipping_city').value = "<?=stripslashes($ship_city)?>";
        document.getElementById('shipping_state').value = "<?=stripslashes($ship_state)?>";
        document.getElementById('shipping_country').value = "<?=stripslashes($ship_country)?>";
        document.getElementById('shipping_zip').value = "<?=$ship_zip?>";
        document.getElementById('shipping_phone').value = "<?=$ship_phone?>";
        document.getElementById('shipping_email').value = "<?=$ship_email?>";
      }
    }
  </script>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li>Shopping Cart</li>
    </ul>
		<!-- <h2>Shopping Cart</h2> -->

    <form action="" method="post" class="row-fluid" name="bill_form">
      <div class="span12">   
      <ul class="unstyled">
        <li class="span6">
          <legend class="text-success">Billing Information</legend>
          <table class="table fieldy">
            <tbody>
              <tr>
                <td>
                  <label for="lastname">Last Name</label>
                  <input type="text" name="lastname" id="lastname" class=":required :only_on_submit input-block-level" value="<?=stripslashes($bill_lastname)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="firstname">First name</label>
                  <input type="text" name="firstname" id="firstname" class=":required input-block-level"  value="<?=stripslashes($bill_firstname)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="address">Address</label>
                  <input type="text" name="address" id="address" class=":required input-block-level" value="<?=stripslashes($bill_address1)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="address1">Address 2</label>
                  <input type="text" name="address1" id="address1" class="input-block-level" value="<?=stripslashes($bill_address2)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="city">City</label>
                  <input type="text" name="city" id="city" class=":required input-block-level" value="<?=stripslashes($bill_city)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="state">State</label>
                  <select name="state" id="state" data-placeholder="Select Select" onchange="this.form.submit();">
                    <option></option>
                    <? 
                      $state = State::findAll();
                      foreach($state as $states) {
                        if($bill_state == "") {
                          $stateVal = "CA";
                        } else {
                          $stateVal = $bill_state;
                        }
                        if($states->fldStateID == $stateVal) { 
                    ?>                        
                      <option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
                      <? } else { ?>
                      <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
                      <? } ?>
                    <? } ?>                      
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  <label for="country">Country</label>
                  <select name="country" id="country" data-placeholder="Select Country">
                    <option></option>
                    <? 
                      $country = Country::findAll();
                      foreach($country as $countries) {
                        if($bill_country == "") {
                          $countryVal = "US";
                        } else {
                          $countryVal = $bill_country;
                        }
                        if($countries->country_code == $countryVal) { 
                    ?>
                      <option value="<?=$countries->country_code?>" selected="selected"><?=$countries->country_name?></option>
                      <? } else { ?>
                      <option value="<?=$countries->country_code?>"><?=$countries->country_name?></option>
                      <? } ?>
                    <? } ?>                      
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  <label for="zip">Postal / Zip Code</label>
                  <input type="text" name="zip" id="zip" class=":required input-block-level" value="<?=$bill_zip?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="phone">Phone number</label>
                  <input type="text" name="phone" id="phone" class=":required input-block-level" value="<?=$bill_phone?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="email">Email Address</label>
                  <input type="text" name="email" id="email" class=":required :email input-block-level" value="<?=$bill_email?>">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="checkbox" id="chkShip" name="chkShip" value="1" style="margin-right:15px;float:left;" onclick="chkClickInfo()">
                  <small>Click here if your shipping information is the same in your billing information</small>
                </td>
              </tr>
            </tbody>  
          </table>
        </li>
        <li class="span6">
          <legend class="text-success">Shipping Information</legend>
          <table class="table fieldy">
            <tbody>
              <tr>
                <td>
                  <label for="shipping_lastname">Last Name</label>
                  <input type="text" name="shipping_lastname" id="shipping_lastname" class=":required input-block-level" value="<?=stripslashes($ship_lastname)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_firstname">First name</label>
                  <input type="text" name="shipping_firstname" id="shipping_firstname" class=":required input-block-level" value="<?=stripslashes($ship_firstname)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_address">Address</label>
                  <input type="text" name="shipping_address" id="shipping_address" class=":required input-block-level" value="<?=stripslashes($ship_address1)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_address1">Address 2</label>
                  <input type="text" name="shipping_address1" id="shipping_address1" class="input-block-level" value="<?=stripslashes($ship_address2)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_city">City</label>
                  <input type="text" name="shipping_city" id="shipping_city" class=":required input-block-level" value="<?=stripslashes($ship_city)?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_state">State</label>
                  <select name="shipping_state" id="shipping_state" data-placeholder="Select Select">
                    <option></option>
                    <? 
                      $state = State::findAll();
                      foreach($state as $states) {
                        if($ship_state == "") {
                          $stateVal = "CA";
                        } else {
                          $stateVal = $ship_state;
                        }
                        if($states->fldStateID == $stateVal) { 
                    ?>                        
                      <option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
                      <? } else { ?>
                      <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
                      <? } ?>
                    <? } ?>                      
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_country">Country</label>
                  <select name="shipping_country" id="shipping_country" data-placeholder="Select Country">
                    <option></option>
                    <? 
                      $country = Country::findAll();
                      foreach($country as $countries) {
                        if($ship_country == "") {
                          $countryVal = "US";
                        } else {
                          $countryVal = $ship_country;
                        }
                        if($countries->country_code == $countryVal) { 
                    ?>
                      <option value="<?=$countries->country_code?>" selected="selected"><?=$countries->country_name?></option>
                      <? } else { ?>
                      <option value="<?=$countries->country_code?>"><?=$countries->country_name?></option>
                      <? } ?>
                    <? } ?>                      
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_zip">Postal / Zip Code</label>
                  <input type="text" name="shipping_zip" id="shipping_zip" class=":required input-block-level" value="<?=$ship_zip?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_phone">Phone number</label>
                  <input type="text" name="shipping_phone" id="shipping_phone" class=":required input-block-level" value="<?=$ship_phone?>">
                </td>
              </tr>
              <tr>
                <td>
                  <label for="shipping_email">Email Address</label>
                  <input type="text" name="shipping_email" id="shipping_email" class=":required input-block-level" value="<?=$ship_email?>">
                </td>
              </tr>
            </tbody>
          </table>
        </li>
      </ul>
      </div>
      <!-- END OF FIELDS -->

      <input type="hidden" name="Id" value="<?=$billing->fldBillingID?>">
      <input type="hidden" name="client_id" value="<?=$_SESSION['client_id']?>">

      <button class="btn_submit" type="submit" name="continue_checkout" value="1">Continue</button>
    </form>
    
  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<link rel="stylesheet" href="<?=$root?>plugins/select/select2.css">
<style>
  legend {font-size:16px;}
  .table.fieldy {margin-top:20px;}
  .table.fieldy td {
    padding:8px 0;
    border-top:0;
  }
  .table.fieldy td input,
  .table.fieldy td select,
  .table.fieldy td textarea {margin-bottom:0;border-radius:0;box-shadow:none;}
  .select2-choice {padding:3px 8px!important;}
  .select2-search-choice-close {top:10px!important;}
</style>
<? endblock() ?>


<? startblock('extracodes') ?>
<script src="<?=$root?>assets/js/jvanadium.js"></script>
<script src="<?=$root?>plugins/select/select2.min.js"></script>
<script>
$(document).ready(function(){
  $("#state,#country,#shipping_state,#shipping_country").select2({
    allowClear: true,
    width: "390px"
  });
});
</script>
<? endblock() ?>
