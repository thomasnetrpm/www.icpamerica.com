<?php
# EXECUTE YOUR CSS
$css = 'page';

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

if(isset($_POST['submit_contact'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$phone 		= trim($_POST['phone']);
	$email 		= trim($_POST['email']);

	if ( ($firstname=='') || ($lastname=='') || ($phone=='') || ($email=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_contactus.php");

		$_POST = sanitize($_POST);
		$contact = $_POST;
		settype($contact,'object');
		Contact::addContact($contact);

		$msg_success = "Contact us form submitted.";
	}
}

if(isset($_POST['submit_support'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$company 	= trim($_POST['company']);
	$email 		= trim($_POST['email']);
	$distributor= trim($_POST['distributor']);
	$position 	= trim($_POST['position']);
	$fax 		= trim($_POST['fax']);
	$country 	= trim($_POST['country']);
	
	if ( ($firstname=='') || ($lastname=='') || ($company=='') || ($email=='') || ($distributor=='') || ($position=='') || ($fax=='') || ($country=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_support.php");
		$msg_success = "Support form submitted.";
	}
}

if(isset($_POST['submit_customquote'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$email 		= trim($_POST['email']);
	$product_name = trim($_POST['product_name']);
	$phone 		= trim($_POST['phone']);
	
	if ( ($firstname=='') || ($lastname=='') || ($email=='') || ($product_name=='') || ($phone=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_customquote.php");
		$msg_success = "Custom Quote form submitted.";
	}
}
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li class="active"><?=$pageName?></li>
    </ul>

    	<?php
    	if (isset($pageID)) {
			if ($pageTitle=='services') { $pageName = 'Industrial Computer Services'; }
    		?>
			<h1><?=$pageName?></h1>

			<? if(isset($msg_error)): ?>
		    <div class="alert alert-error">
		    	<button type="button" class="close" data-dismiss="alert">&times;</button>
		    	<?=$msg_error?>
		    </div>
			<? endif; ?>

			<? if(isset($msg_success)): ?>
		    <div class="alert alert-info">
		    	<button type="button" class="close" data-dismiss="alert">&times;</button>
		    	<?=$msg_success?>
		    </div>
			<? endif; ?>

			<p><?=$pageDesc?></p>

			<?php
			if ($pageTitle=='contact us') {
				?>
				<!-- CONTACT US FORM -->
				<form action="" method="post" class="class-form">
					<dl>
						<dt>First Name *</dt>
						<dd><input type="text" name="firstname" placeholder="First Name" value="<?=sanitize($_POST['firstname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Last Name *</dt>
						<dd><input type="text" name="lastname" placeholder="Last Name" value="<?=sanitize($_POST['lastname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Company Name</dt>
						<dd><input type="text" name="company" placeholder="Company Name" value="<?=sanitize($_POST['company'])?>"></dd>
					</dl>
					<dl>
						<dt>Phone * / Fax / Email *</dt>
						<dd>
							<input compact type="text" name="phone" placeholder="Phone" value="<?=sanitize($_POST['phone'])?>" required>
							<input compact type="text" name="fax" placeholder="Fax" value="<?=sanitize($_POST['fax'])?>">
							<input compact type="email" name="email" placeholder="Email" value="<?=sanitize($_POST['email'])?>" required>
						</dd>
					</dl>
					<dl>
						<dt>Address 1</dt>
						<dd><input type="text" name="address1" placeholder="Address 1" value="<?=sanitize($_POST['address1'])?>"></dd>
					</dl>
					<dl>
						<dt>Address 2</dt>
						<dd><input type="text" name="address2" placeholder="Address 2" value="<?=sanitize($_POST['address2'])?>"></dd>
					</dl>
					<dl>
						<dt>City / State / Zip</dt>
						<dd>
							<input compact type="text" name="city" placeholder="City" value="<?=sanitize($_POST['city'])?>">
							<input compact type="text" name="state" placeholder="State" value="<?=sanitize($_POST['state'])?>">
							<input compact type="text" name="zip" placeholder="Zip" value="<?=sanitize($_POST['zip'])?>">
						</dd>
					</dl>
					<dl>
						<dt>How did you find us</dt>
						<dd><input type="text" name="find" placeholder="How did you find us" value="<?=sanitize($_POST['find'])?>"></dd>
					</dl>
					<dl>
						<dt>Inquiry / Comments</dt>
						<dd>
							<textarea cols="" rows="" name="comment" placeholder="Comments"><?=sanitize($_POST['comment'])?></textarea>
						</dd>
					</dl>
					<dl>
						<dt>&nbsp;</dt>
						<dd>&nbsp;(* Fields Required)</dd>
					</dl>
					<dl>
						<dt>&nbsp;</dt>
						<dd><button class="submit-btn" type="submit" name="submit_contact" value="1">Submit Information</button></dd>
					</dl>
				</form>
				<?php
			}
			if ($pageTitle=='support') {
				?>
				<!-- SUPPORT FORM -->
				<form action="" method="post" class="class-form">
					<dl>
						<dt>First Name *</dt>
						<dd><input type="text" name="firstname" placeholder="" value="<?=sanitize($_POST['firstname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Last Name *</dt>
						<dd><input type="text" name="lastname" placeholder="" value="<?=sanitize($_POST['lastname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Company *</dt>
						<dd><input type="text" name="company" placeholder="" value="<?=sanitize($_POST['company'])?>" required></dd>
					</dl>
					<dl>
						<dt>Email *</dt>
						<dd><input type="text" name="email" placeholder="" value="<?=sanitize($_POST['email'])?>" required></dd>
					</dl>
					<dl>
						<dt>Distributor *</dt>
						<dd><input type="text" name="distributor" placeholder="" value="<?=sanitize($_POST['distributor'])?>" required></dd>
					</dl>
					<dl>
						<dt>Position/Title *</dt>
						<dd><input type="text" name="position" placeholder="" value="<?=sanitize($_POST['position'])?>" required></dd>
					</dl>
					<dl>
						<dt>Fax *</dt>
						<dd><input type="text" name="fax" placeholder="" value="<?=sanitize($_POST['fax'])?>" required></dd>
					</dl>
					<dl>
						<dt>Country *</dt>
						<dd><input type="text" name="country" placeholder="" value="<?=sanitize($_POST['country'])?>" required></dd>
					</dl>
					<h4>System Information</h4>
					<dl>
						<dt>CPU Brand</dt>
						<dd><input type="text" name="cpu_brand" placeholder="e.g. Intel, AMD" value="<?=sanitize($_POST['cpu_brand'])?>"></dd>
					</dl>
					<dl>
						<dt>CPU Model</dt>
						<dd><input type="text" name="cpu_model" placeholder="e.g. 586" value="<?=sanitize($_POST['cpu_model'])?>"></dd>
					</dl>
					<dl>
						<dt>CPU Speed</dt>
						<dd><input type="text" name="cpu_speed" placeholder="e.g. 66Mhz" value="<?=sanitize($_POST['cpu_speed'])?>"></dd>
					</dl>
					<dl>
						<dt>Main Board</dt>
						<dd><input type="text" name="main_board" placeholder="" value="<?=sanitize($_POST['main_board'])?>"></dd>
					</dl>
					<dl>
						<dt>Bios Version</dt>
						<dd><input type="text" name="bios_version" placeholder="" value="<?=sanitize($_POST['bios_version'])?>"></dd>
					</dl>
					<dl>
						<dt>DRAM</dt>
						<dd><input type="text" name="dram" placeholder="" value="<?=sanitize($_POST['dram'])?>"></dd>
					</dl>
					<dl>
						<dt>Operating System</dt>
						<dd><input type="text" name="os" placeholder="" value="<?=sanitize($_POST['os'])?>"></dd>
					</dl>

					<dl>
						<dt>Hard Disk Brand</dt>
						<dd><input type="text" name="hd_brand" placeholder="e.g. IBM" value="<?=sanitize($_POST['hd_brand'])?>"></dd>
					</dl>
					<dl>
						<dt>Hard Disk Size</dt>
						<dd><input type="text" name="hd_size" placeholder="e.g. 100Gb" value="<?=sanitize($_POST['hd_size'])?>"></dd>
					</dl>
					<h4>Product Information</h4>
					<dl>
						<dt>Product Brand</dt>
						<dd><input type="text" name="prod_brand" placeholder="e.g. Compact Chasis" value="<?=sanitize($_POST['prod_brand'])?>"></dd>
					</dl>
					<dl>
						<dt>Product Model</dt>
						<dd><input type="text" name="prod_model" placeholder="" value="<?=sanitize($_POST['prod_model'])?>"></dd>
					</dl>
					<dl>
						<dt>Product Version</dt>
						<dd><input type="text" name="prod_version" placeholder="" value="<?=sanitize($_POST['prod_version'])?>"></dd>
					</dl>
					<dl>
						<dt>Peripheral & <br>Environment Description</dt>
						<dd>
							<textarea cols="" rows="" name="peripheral" placeholder=""><?=sanitize($_POST['peripheral'])?></textarea>
						</dd>
					</dl>
					<dl>
						<dt>Problem Description</dt>
						<dd>
							<textarea cols="" rows="" name="problem" placeholder=""><?=sanitize($_POST['problem'])?></textarea>
						</dd>
					</dl>
					<dl>
						<dt>&nbsp;</dt>
						<dd><button class="submit-btn" type="submit" name="submit_support" value="1">Submit Information</button></dd>
					</dl>
				</form>
				<?php
			}
			?>
			<?php
			if ($pageTitle=='custom quote') {
				?>
				<!-- CUSTOM QUOTE FORM -->
				<form action="" method="post" class="class-form">
					<dl>
						<dt>First Name *</dt>
						<dd><input type="text" name="firstname" placeholder="First Name" value="<?=sanitize($_POST['firstname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Last Name *</dt>
						<dd><input type="text" name="lastname" placeholder="Last Name" value="<?=sanitize($_POST['lastname'])?>" required></dd>
					</dl>
					<dl>
						<dt>Company Name</dt>
						<dd><input type="text" name="company" placeholder="Company Name" value="<?=sanitize($_POST['company'])?>"></dd>
					</dl>
					<dl>
						<dt>ICP Product Name: *</dt>
						<dd><input type="text" name="product_name" placeholder="Product Name" value="<?=sanitize($_POST['product_name'])?>" required></dd>
					</dl>
					<dl>
						<dt>Phone * / Fax / Email *</dt>
						<dd>
							<input compact type="text" name="phone" placeholder="Phone" value="<?=sanitize($_POST['phone'])?>" required>
							<input compact type="text" name="fax" placeholder="Fax" value="<?=sanitize($_POST['fax'])?>">
							<input compact type="email" name="email" placeholder="Email" value="<?=sanitize($_POST['email'])?>" required>
						</dd>
					</dl>
					<dl>
						<dt>Inquiry / Comments</dt>
						<dd>
							<textarea cols="" rows="" name="comment" placeholder="Comments"><?=sanitize($_POST['comment'])?></textarea>
						</dd>
					</dl>
					<dl>
						<dt>&nbsp;</dt>
						<dd><button class="submit-btn" type="submit" name="submit_customquote" value="1">Submit Information</button></dd>
					</dl>
				</form>
				<?php
			}
			?>
    		<?php
    	} else {
    		// Show 404 page
    		include "includes/pages/404.php";
    	}
    	?>
	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b22acdb2-b960-43a7-a566-b857f357f33e"});</script>
<? endblock(); ?>