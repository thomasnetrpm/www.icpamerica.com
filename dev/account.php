<?php 
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

if( (isset($_SESSION['logged_in'])) && ($_SESSION['logged_in'] == true) ) {
	// echo 'logged_in: '.$_SESSION['logged_in'];
	$links = $root . 'account-information.php';
	header("Location: $links");	
} else {
	$client_id = session_id();
	// echo 'not logged_in: '.$client_id;
}	
	
if(isset($_POST['login'])) {			
	//check if the login information is valid
	$_POST = sanitize($_POST);
	$client = $_POST;
	settype($client,'object');
	if(Client::checkLogin($client)==1) {
		$_SESSION['logged_in'] = true;
	    $clients = Client::getInfo($client);			
		$_SESSION['client_id']	 = $clients->fldClientID;
		$_SESSION['client_type'] = $clients->fldClientType;
		$_SESSION['client_firstname'] = $clients->fldClientFirstName;
		$xclient = session_id();
		$condition = "fldTempCartClientID='$xclient'";
		if(TempCart::countTempcartbyCondition($condition)>=1) {
			//change the client id
			TempCart::updateTempcartClient($_SESSION['client_id'],$xclient);
			$links = $root.'billing-info.html';
			header("Location: $links");					
		} else {
			// $links = $root . 'account-information.php';
			// error_log('redirecting to ' . $links);
			// header("Location: $links");			
			if ($_POST['from_page']) {
				echo $links = $_POST['from_page'];
				if ($links==''.$root.'thankyou.html') {
					$links = $root . 'account-information.php';
				}
			} else {
				// $links = $root . 'account-information.php';
				$links = $root;
				// error_log('redirecting to ' . $links);
			}
			header("Location: $links");
		}
	} else {		
		$error = "Invalid username or password. Please try again.";
	}
}
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Login or Create Account</li>
    </ul>

    <hgroup>
      <h2>Login or Create Account</h2>
    </hgroup>

			<form action="" method="post" class="row-fluid account-login">
			<? if(isset($error)): ?>
		    <div class="alert alert-error">
		    	<button type="button" class="close" data-dismiss="alert">&times;</button>
		    	Sorry, <?=$error?>
		    </div>
			<? endif; ?>

				<ul class="unstyled span12">
					<li class="span6 new-account">
						<h4>New Customers</h4>
						<p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                        <div style="clear:both; margin-bottom:90px;"></div>
						<!-- <a href="<?=$root?>registration.html" class="btn_submit"> Create an account</a> -->
					</li>
					<li class="span6 exist-account">
						<h4>Registered Customers</h4>
						<p>If you have an account with us, please log in.</p>
						<div class="formbox">
							<fieldset class="form-inline">
								<label for="username" style="width:100px">Username</label>
								<input type="text" name="username" id="username" required>
							</fieldset>
							<fieldset class="form-inline">
								<label for="password" style="width:100px">Password</label>
								<input type="password" name="password" id="password" required>
							</fieldset>
							<fieldset class="form-inline">
								<?php $from_page = $_SERVER['HTTP_REFERER']; ?>
								<input type="hidden" name="from_page" value="<?=$from_page?>">
								<button type="submit" name="login" style="margin-top:20px;" class="btn_submit">Login</button>
								<a href="<?=$root?>forgot-password.html" class="pull-right forgot-password">Forgot Username / Password?</a>
							</fieldset>
						</div>
					</li>
				</ul>
			</form>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>

<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock() ?>

<? startblock('script') ?>
<? endblock() ?>