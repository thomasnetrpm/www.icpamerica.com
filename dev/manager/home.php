<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";

// $pageID 	= 1; // Default page ID for Index page
$pageID 	= 19; // Default page ID for Index page

$content = Pages::findPages($pageID);
$pageName 	= $content->fldPagesName;
$pageDesc 	= $content->fldPagesDescription;

$pageMetatitle	= $content->fldPagesMetaTitle;
$pageMetadesc	= $content->fldPagesMetaDescription;
$pageMetakey	= $content->fldPagesMetaKeywords;
?>
<!DOCTYPE HTML>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<?php if ($pageMetaAuthor) { ?>
<meta name="author" content="<?=$pageMetaAuthor?>">
<? } ?>
<?php if ($pageMetakey) { ?>
<meta name="keyword" content="<?=$pageMetakey?>">
<? } ?>
<?php if ($pageMetadesc) { ?>
<meta name="description" content="<?=$pageMetadesc?>">
<? } ?>
<meta content="width=1280, maximum-scale=1" name="viewport">
<meta name="google-site-verification" content="oMzJkYw8s3z4JSvDK-HIv0jIp6hpc0qmrCiCyD8K16w" />﻿
<link rel="stylesheet" type="text/css" media="all" href="<?=$root?>assets/css/print.css">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<?=$root?>assets/js/modernizr.js"></script>
<?=emptyblock('head');?>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"QcIFg1awAe00w8", domain:"icpamerica.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script><!-- End Alexa Certify Javascript -->
</head>

<body>
<div class="wrap-body">
<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix">
	<form action="<?=$root?>search/default.html" method="post" id="search-panel">
		<ul class="unstyled clearfix">
            <? // if(!isset($_SESSION['client_id'])) { ?>
        	<? if(isset($_SESSION['logged_in'])) { ?>
			<li>Welcome Back <?=$_SESSION['client_firstname']?></li>
			<li><a href='<?=$root?>logout.php'>Logout</a></li>
			<li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>			 
			<li>
		    <div class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account</a>
			    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			    	<div><a href="<?=$root?>account-information.html">My Account</a></div>
			    	<div><a href="<?=$root?>account-history.html">Purchase History</a></div>
			    	<div><a href="<?=$root?>faqs/default.html">FAQs</a></div>
			    </div>
		    </div>
			</li>
			<? } else { ?>
				<li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>
                <li><a href="<?=$root?>registration.html">Sign Up</a> <a href="<?=$root?>login.html">Login</a></li>	
			<? } ?>	


			<li>
				<input type="text" name="top_keyword" value="" placeholder="Type your keywords...">
				<input type="submit" name="top_search" value="&nbsp;">
			</li>
			<li>
				<a href="http://www.facebook.com/icpa1" target="_blank"><img src="assets/images/icon-facebook.png" width="24" height="24" alt="FB"></a> 
				<a href="http://www.linkedin.com/company/icp-america-inc." target="_blank"><img src="assets/images/icon-linkedin.png" width="24" height="24" alt="LI"></a> 
				<a href="http://twitter.com/icpamerica" target="_blank"><img src="assets/images/icon-twitter.png" width="24" height="24" alt="TT"></a> 
                <a href="https://plus.google.com/100124929247060898549" rel="publisher" target="_blank"><img src="<?=$root?>assets/images/icon-google_plus.png" width="24" alt=""></a>
			</li>
		</ul>
	</form>
	<header>
		<a id="hdr-logo" href="<?=$root?>" title="go back to Homepage"></a>
		<button type="button" class="media-btn btn">
			<i class="icon-align-justify"></i>
		</button>

		<?php include "includes/pages/top-nav.php"; ?>

	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<a href="http://icpamerica.com/custom_quote/default.html" class="custom-quote"></a>
	<?=emptyblock('content');?>
</div>
<!--/ CONTENT -->
<?php include "includes/footer.php"; ?>
<? /*
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
*/ ?>
<!--/ FOOTER -->
<div class="bg-panel"></div>
</div>
<!--/ BACKGROUND PANEL  -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<script src="<?=$root?>assets/js/placeholder.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];  
    _gaq.push(['_setAccount', 'UA-48231208-1']);  
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>﻿
<?=emptyblock('script');?>
</body>
</html>