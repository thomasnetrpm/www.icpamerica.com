<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";
?>
<?php
$filename = basename($_SERVER['PHP_SELF']);

$prodURL = trim($_REQUEST['url']);

if (substr($prodURL, -12) == 'default.html') { // Category / Subcategory

    // CATEGORY
    // Check if its main category
    $cat = Category::findByURL($prodURL);
    $catID = $cat->fldCategoryID;

    if ($catID >= 1) { // cms_tblCategory
        $cond1 = 'subcategoryID = 0';
        $subcat = Subcategory::findByID($catID, $cond1);
        $showCategory = 1; // Display category table

        // For Medical Products OVERRIDE - Just one category then display product right away
        if ($prodURL == 'medical_products/default.html') {
            // echo 'display medical list with products<br>';
            $condition = "AND (fldProductsURL LIKE 'medical_products/%' OR fldProductsURL LIKE 'medical_products/%') ";
            $prod = Products::displayAllByCondition($catID, $condition);
            $countProd = Products::countAllByCondition($catID, $condition);

            $showCategory = '';
            $showProductList = 1; // Display product table
        }

    } else { // cms_tblSubcategory
        $subcat = Subcategory::findByURL($prodURL);
        $catID = $subcat->fldCategoryID;
        $subcatID = $subcat->fldSubcategoryID;

        $cond1 = "subcategoryID = '".$subcatID."'";
        // if ($prodURL=='single_board_computer/backplane/default.html') { $cond1 .= " AND subcategory2ID IS NULL"; } // For Backplane only
        if ($prodURL=='backplane/default.html') { $cond1 .= " AND subcategory2ID IS NULL"; } // For Backplane only
        $subcat = Subcategory::findByID($catID, $cond1);
        
        $countSubcat = Subcategory::countByID($catID, $cond1);
        $countNull = Subcategory::countNullByID($catID, $cond1);

        if ($countNull >= 1) { // Category list
            // echo 'display sub-category list with products<br>';
            $showCategoryList = 1; // Display subcategory list and products
        } else {
            if ($countSubcat >= 1) { // Category list
                // echo 'sub-category list<br>';
                $showCategory = 1; // Display subcategory table
            } else { // Products list

                // echo 'product list<br>';
                $showProductList = 1; // Display product table
                $condition = "AND fldProductsURL LIKE '".substr($prodURL, 0, -12)."%'";

                // This is for video_capture / IVC only - Start
                if (substr($prodURL, 0, -12)=='accessories/video_capture/') {
                    $condition = "AND (fldProductsURL LIKE 'accessories/video_capture/%' OR fldProductsURL LIKE 'accessories/IVC/%') ";
                }
                // This is for video_capture / IVC only - End

                // This is for BACKPLANE only - Start
                // echo 'substr produrl: '.substr($prodURL, 0, 9).'<br>';
                if (substr($prodURL, 0, 9)=='backplane') {
                    $countcat = count(explode('/', $prodURL));
                    // echo 'countcat: '.$countcat.'<br>';
                    // echo 'subcatID: '.$subcatID.'<br>';
                    // if (in_array($subcatID, array('90', '195', '194'), true)) { // for PCI/PICOe, PISA and ISA Backplanes
                    //     echo $subcatID." was found in array \n";
                    // }
                    if ($countcat==3) {
                        // $cond1 = "subcategory2ID = '".$subcatID."'";
                        // $subcat = Subcategory::findByID($catID, $cond1);
        
                        // $showCategory = 1; // Display subcategory table
                        
                        if ($subcatID == '194') { // for backplanes / ISA Backplanes
                            $condition .= "AND fldProductsSubcategory2ID='$subcatID'";
                        } else {
                            $cond1 = "subcategory2ID = '".$subcatID."'";
                            $subcat = Subcategory::findByID($catID, $cond1);
            
                            $showCategory = 1; // Display subcategory table
                        }
                    } elseif ($countcat==4) {
                        $condition .= "AND fldProductsSubcategory3ID='$subcatID'";
                    }
                }
                // This is for BACKPLANE only - End

                $prod = Products::displayAllByCondition($catID, $condition);
                $countProd = Products::countAllByCondition($catID, $condition);

                /*
                // This is for BACKPLANE only - Start
                if (substr($prodURL, 0, 9)=='backplane') {
                    $cond1 = "subcategory2ID = '".$subcatID."'";
                    $subcat = Subcategory::findByID($catID, $cond1);
    
                    $showCategory = 1; // Display subcategory table
                }
                // This is for BACKPLANE only - End
                */
            }
        }

    }

    /*
    // For Medical Products OVERRIDE -- Old version - displays Subcategory text and below lists products in one page
    if ($prodURL == 'medical_products/default.html') {
        // echo 'display medical list with products<br>';
        $showCategory = '';
        // $showCategoryList = 1; // Display subcategory list and products
        $showProductList = 1; // Display product table
    }
    */

} else { // Product Details

    if ($filename == "product-detail.php") { $showProductDetails = 1; } // For Product Details page
    
    $prod = Products::findProductByURL($prodURL);
    $productID          = $prod->fldProductsId;
	if (empty($productID)) {
		header('Location: '.$root.'products/default.html');
		exit();
	}

    $productName        = $prod->fldProductsName;
    $productImage       = $prod->fldProductsImage;
    $productCode        = $prod->fldProductsCode;
    $productPDF         = $prod->fldProductsPDF;
    $productUM          = $prod->fldProductsUserManual;
    $productQG          = $prod->fldProductsQuickGuide;
    $productOverview    = $prod->fldProductsOverview;
    $productInfo        = $prod->fldProductsInformation;
    $productDesc        = $prod->fldProductsDescription;
    $productFeatures    = $prod->fldProductsFeatures;
    $productTechspecs   = $prod->fldProductsTechspecs;
    $productOrderinfo   = $prod->fldProductsOrderinfo;
    $productMetaTitle   = $prod->fldProductMetaTitle;

    $catID              = $prod->fldProductsMainCategoryID;
    $subcatID           = $prod->fldProductsSubcategoryID;
    $subcat2ID          = $prod->fldProductsSubcategory2ID;

    // Tiered Pricing
    if ($_SESSION['client_type'] == 1) { $productPrice = $prod->fldProductsPrice1; $productPriceText = $prod->fldProductsPrice1Text; } 
    elseif ($_SESSION['client_type'] == 2) { $productPrice = $prod->fldProductsPrice2; $productPriceText = $prod->fldProductsPrice2Text; } 
    elseif ($_SESSION['client_type'] == 3) { $productPrice = $prod->fldProductsPrice3; $productPriceText = $prod->fldProductsPrice3Text; }

}

$categoryID = $catID;
$cat = Category::findCategory($categoryID);
$category_name = $cat->fldCategoryName;
$category_url = $cat->fldCategoryURL;
$category_desc = $cat->fldCategoryDescription;
$category_metatitle = $cat->fldCategoryMetaTitle;
$pageMetatitle = ($category_metatitle)? $category_metatitle: '';

$subcategoryID = $subcatID;
$subcategory = Subcategory::findSubcategory($subcategoryID);
$subcategory_name = $subcategory->fldSubcategoryName;
$subcategory_url = $subcategory->fldSubcategoryURL;
$subcategory_desc = $subcategory->fldSubcategoryDescription;
$subcategory_metatitle = $subcategory->fldSubcategoryMetaTitle;
$pageMetatitle = ($subcategory_metatitle)? $subcategory_metatitle: $pageMetatitle;

$subcategory2ID = $subcat2ID;
$subcategory2 = Subcategory::findSubcategory($subcategory2ID);
$subcategory2_name = $subcategory2->fldSubcategoryName;
$subcategory2_url = $subcategory2->fldSubcategoryURL;
$subcategory2_desc = $subcategory2->fldSubcategoryDescription;
$subcategory2_metatitle = $subcategory2->fldSubcategoryMetaTitle;
$pageMetatitle = ($subcategory2_metatitle)? $subcategory2_metatitle: $pageMetatitle;

if ($showProductDetails == 1) { $pageMetatitle = $productMetaTitle; }
// if ($showCategory == 1) { $pageMetatitle = $category_metatitle; }


// **** Revised Breadcrumbs
$brk_url = explode('/', $_REQUEST['url']);
$bread_cat1 = trim(str_replace(array('_', '-'), ' ', $brk_url[0]));
    if ($bread_cat1=='backplane') { $category_url = "backplane/default.html"; }
$bread_cat2 = trim(str_replace(array('_', '-'), ' ', $brk_url[1]));
$bread_cat3 = trim(str_replace(array('_', '-'), ' ', $brk_url[2]));
// echo 'bread: '.$bread_cat1.' / '.$bread_cat2.' / '.$bread_cat3.'<br>';

    if ($bread_cat1 == 'LCD products') { $bread_cat1 = 'LCD Monitors, Panel PC and Display Kits'; }
$bread_cat1 = (strpos($bread_cat1,'.html') === false)? $bread_cat1: '';
$bread_cat2 = (strpos($bread_cat2,'.html') === false)? $bread_cat2: '';
$bread_cat3 = (strpos($bread_cat3,'.html') === false)? $bread_cat3: '';
$bread_cat4 = ($productCode)? $productCode: '';
// echo 'bread: '.$bread_cat1.' / '.$bread_cat2.' / '.$bread_cat3.' / '.$bread_cat4.'<br>';


// Pages from CMS > Page Management
$pageTitle      = str_replace('_', ' ', trim($_REQUEST['p']));
if ($pageTitle) {
    $content = Pages::findPageByTitle($pageTitle);
    $pageID     = $content->fldPagesID;
    $pageName   = $content->fldPagesName;
    $pageDesc   = $content->fldPagesDescription;

    $pageMetatitle  = $content->fldPagesMetaTitle;
    $pageMetadesc   = $content->fldPagesMetaDescription;
    $pageMetakey    = $content->fldPagesMetaKeywords;
}

if ($pageMetatitle == "") { $pageMetatitle = "ICP America"; }

// For other pages
$filename = basename($_SERVER['PHP_SELF']);
switch ($filename) {
    case 'news.php':
        $pageMetatitle = "News | ICP America";
        break;
    case 'account.php':
        $pageMetatitle = "Login | ICP America";
        break;
    case 'account-registration.php':
        $pageMetatitle = "Registration | ICP America";
        break;
    case 'account-information.php':
        $pageMetatitle = "My Account | ICP America";
        break;
    case 'account-billship-information.php':
        $pageMetatitle = "Edit Billing and Shipping Information | ICP America";
        break;
    case 'product-category.php':
        $pageMetatitle = "Industrial Computers and Computer Products from ICP America";
        break;
    case 'account-history.php':
        $pageMetatitle = "Account History | ICP America";
        break;
    case 'shopping_cart.php':
        $pageMetatitle = "Shopping Cart | ICP America";
        break;
    case 'billing_information.php':
        $pageMetatitle = "Shipping and Billing Information | ICP America";
        break;
    case 'forgot-password.php':
        $pageMetatitle = "Forgot Password | ICP America";
        break;
}
?>
<!doctype html>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<?php if ($pageMetaAuthor) { ?>
<meta name="author" content="<?=$pageMetaAuthor?>">
<? } ?>
<?php if ($pageMetakey) { ?>
<meta name="keyword" content="<?=$pageMetakey?>">
<? } ?>
<?php if ($pageMetadesc) { ?>
<meta name="description" content="<?=$pageMetadesc?>">
<? } ?>
<meta content="width=1280, maximum-scale=1" name="viewport">
<meta name="google-site-verification" content="oMzJkYw8s3z4JSvDK-HIv0jIp6hpc0qmrCiCyD8K16w" />﻿
<link rel="stylesheet" type="text/css" media="print" href="<?=$root?>assets/css/print.css">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?=emptyblock('head');?>
<script src="<?=$root?>assets/js/modernizr.js"></script>

<?php /* <script src="lightbox/js/jquery-1.10.2.min.js"></script> */ ?>
<script src="<?=$root?>plugins/lightbox/js/lightbox-2.6.min.js"></script>
<link href="<?=$root?>plugins/lightbox/css/lightbox.css" rel="stylesheet" />

</head>
<body>

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"QcIFg1awAe00w8", domain:"icpamerica.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=QcIFg1awAe00w8" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix">
	<form action="<?=$root?>search/default.html" method="post" id="search-panel">
		<ul class="unstyled clearfix">
        <? if(isset($_SESSION['logged_in'])) { ?>
            <li>Welcome Back <?=$_SESSION['client_firstname']?></li>
			<li><a href='<?=$root?>logout.php'>Logout</a></li>
			<li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>			 
			<li>
		    <div class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account</a>
			    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			    	<div><a href="<?=$root?>account-information.html">My Account</a></div>
			    	<div><a href="<?=$root?>account-history.html">Purchase History</a></div>
			    	<div><a href="<?=$root?>faqs/default.html">FAQs</a></div>
			    </div>
		    </div>
			</li>
			<? } else { ?>
				<li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>
                <li><a href="<?=$root?>registration.html">Sign Up</a> <a href="<?=$root?>login.html">Login</a></li>	
			<? } ?>	
			<li>
				<input type="text" name="top_keyword" value="" placeholder="Type your keywords...">
				<input type="submit" name="top_search" value="&nbsp;">
			</li>
			<li>
				<a href="http://www.facebook.com/icpa1" target="_blank"><img src="<?=$root?>assets/images/icon-facebook.png" width="24" alt=""></a> 
				<a href="http://www.linkedin.com/company/icp-america-inc." target="_blank"><img src="<?=$root?>assets/images/icon-linkedin.png" width="24" alt=""></a> 
				<a href="http://twitter.com/icpamerica" target="_blank"><img src="<?=$root?>assets/images/icon-twitter.png" width="24" alt=""></a> 
                <a href="https://plus.google.com/100124929247060898549" rel="publisher" target="_blank"><img src="<?=$root?>assets/images/icon-google_plus.png" width="24" alt=""></a>
			</li>
		</ul>
	</form>
	<header>
		<a id="hdr-logo" href="<?=$root?>" title="go back to Homepage"></a>
		<button type="button" class="media-btn btn">
			<i class="icon-align-justify"></i>
		</button>

		<?php include "includes/pages/top-nav.php"; ?>

	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<a href="http://icpamerica.com/custom_quote/default.html" class="custom-quote"></a>
	<?=emptyblock('content');?>
</div>
<!--/ CONTENT -->
<?php include "includes/footer.php"; ?>
<? /*
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
*/ ?>
<!--/ FOOTER -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<script src="<?=$root?>assets/js/placeholder.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];  
    _gaq.push(['_setAccount', 'UA-48231208-1']);  
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>﻿
<?=emptyblock('script');?>
</body>
</html>