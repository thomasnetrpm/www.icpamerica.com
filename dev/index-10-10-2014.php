<?
# EXECUTE YOUR CSS
$css = 'home';

# IMPORT YOUR BASE TEMPLATE
include 'manager/home.php';
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<?
if(isset($_POST['login'])) {			
	//check if the login information is valid
	$_POST = sanitize($_POST);
	$client = $_POST;
	settype($client,'object');
	if(Client::checkLogin($client)==1) {
		$_SESSION['logged_in'] = true;
	    $clients = Client::getInfo($client);
		$_SESSION['client_id']	 = $clients->fldClientID;
		$_SESSION['client_type'] = $clients->fldClientType;
		$_SESSION['client_firstname'] = $clients->fldClientFirstName;
		$xclient = session_id();

		$condition = "fldTempCartClientID='$xclient'";
		if(TempCart::countTempcartbyCondition($condition)>=1) {
			//change the client id
			TempCart::updateTempcartClient($_SESSION['client_id'],$xclient);
			$links = $root.'billing-info.html';
			header("Location: $links");					
		} else {
			// $links = $root . 'account-information.html';
			$links = $root;
			error_log('redirecting to ' . $links);
			header("Location: $links");			
		}
	} else {		
		$error = "Invalid username or password";
	}
}
?>

<form action="" method="post" id="header-panel">

	<img src="temp/slogan/home-slogan.png" alt="">
	<?php
	if (!$_SESSION['logged_in']) {
		// echo "logged out";
		?>
		<fieldset>
			<h3>Sign In to your Account</h3>
			<? if(isset($error)): ?>
				<div class="alert alert-error">
		    		<button type="button" class="close" data-dismiss="alert">&times;</button>
		    		<?=$error?>
		    	</div>
			<? endif; ?>
			<div class="user-field">
				<input type="text" name="username" placeholder="Username" required>
				<input type="password" name="password" placeholder="Password" required>
			</div>

			<ul class="unstyled clearfix">
				<li class="pull-left">
					<input type="checkbox"> Remember Me
				</li>
				<li class="pull-right">
					<button type="submit" class="btn_submit" name="login"> <b class="icon-lock icon-white"></b> Sign In</button>	
				</li>
			</ul>

			<ul class="user-access">
				<li><a href="<?=$root?>forgot-password.html">Forgot your ID or Password?</a></li>
				<li><a href="<?=$root?>registration.html">New Customer? Click here to create an account.</a></li>
			</ul>

		</fieldset>
		<?php
	} else {
		// echo "logged in";
		?>
		<?php
	}
	?>
</form>


<article>
	<section>
		<ul class="unstyled clearfix product-list">
			<?php
			// Get Top 9 Featured Category
			$category = Category::getFeaturedCategory(9);
			$iCat = 1;
			foreach ($category as $cat) {
				$categoryID 	= $cat->fldCategoryID;
				$categoryName 	= $cat->fldCategoryName;
				$categoryURL 	= $cat->fldCategoryURL;
				$categoryImage 	= $cat->fldCategoryImage;
				$isURL 			= $cat->fldCategoryIsUrl;

				if ($isURL==1) {
					$link = $categoryURL;
				} else {
					$link = $root.'products/'.$categoryURL;
				}

				$outsideLink = array(9, 11, 14);
				if (in_array($categoryID, $outsideLink)) { $target = "target='_blank'"; }
				?>
				<li>
					<a href="<?=$link?>" <?=$target?> >
					<h2><?=$categoryName?></h2>
					<img src="<?=$root?>uploads/category/_320_<?=$categoryImage?>" width="336" height="150" alt="" style="border-left: solid 1px #CCC;">
					</a>
				</li>
				<?php
				if ($iCat == '6') { // Show inbetween info
					?>
				</ul>
			</section>
		</article>

		<article class="company-info">
			<section>
				<h2>Serving the Industrial Market for more than 25 years.</h2>
				<p>In an industry that is constantly re-inventing itself, ICP America has remained one step ahead of the competition for more than 25 years.  Originally founded as a product development company, ICP America quickly made a name for itself with computer innovated designs.  <a href="<?=$root?>about_us/default.html">Read More</a></p>
			</section>
		</article>

		<article>
			<section>
				<ul class="unstyled clearfix product-list">
					<?php
				}
			$iCat++;
			}
			?>
		</ul>
	</section>
</article>

<article>
	<section>
		<table class="table">
			<thead>
				<tr>
					<td><h3><a href="<?=$root?>products/new/default.html">New Products</a></h3></td>
					<td><h3><a href="<?=$root?>products/special-deal/default.html">Special Deals</a></h3></td>
					<td><h3>Contact Us</h3></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<ul class="unstyled">
							<?php
    						// Display All isNew
							$isNew = Products::displayAllIsNew(5);
							foreach ($isNew as $new) {
								$product_name 	= $new->fldProductsName;
								$product_url 	= $new->fldProductsURL;
								?>
							<li><a href="<?=$root?>products/<?=$product_url?>"><?=$product_name?></a></li>
								<?php
							}
							?>
						</ul>
						<a href="<?=$root?>products/new/default.html">see more</a>
					</td>
					<td>
						<ul class="unstyled">
							<?php
    						// Display All isSpecialDeal
							$isSpecialDeal = Products::displayAllIsSpecialDeal(5);
							foreach ($isSpecialDeal as $isS) {
								$product_name 	= $isS->fldProductsName;
								$product_url 	= $isS->fldProductsURL;
								?>
							<li><a href="<?=$root?>products/<?=$product_url?>"><?=$product_name?></a></li>
								<?php
							}
							?>
						</ul>
						<a href="<?=$root?>products/special-deal/default.html">see more</a>
					</td>
					<td>
						<p>ICP America’s Sales Department is available to assist you in selecting the right product for your project.  To speak with a member of our Sales Team please call Monday-Friday 7:00AM – 4:30PM. </p>
						<table style="width:100%">
							<tbody>
								<tr>
									<td>Call Us today</td>
									<td>: 1-877-293-2000</td>
								</tr>
								<tr>
									<td>From Outside US</td>
									<td>: 760-598-2176</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a></td>
								</tr>
								<tr>
									<td>Sales</td>
									<td>: <a href="mailto:sales@icpamerica.com">sales@icpamerica.com</a></td>
								</tr>
								<tr>
									<td>Technical Support</td>
									<td>: <a href="mailto:support@icpamerica.com">support@icpamerica.com</a></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>

		<table class="table">
			<thead>
				<tr>
					<td colspan="4"><h3> <!-- <a href="<?=$root?>">Site Map></a> --> </h3></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width:25%">
						<ul class="unstyled">
							<li><a href="<?=$root?>">ICP America Industrial Computer</a></li>
							<li><a href="<?=$root?>about_us/default.html">About ICP America</a></li>
							<li><a href="<?=$root?>contact_us/default.html">Contact Us</a></li>
							<li><a href="<?=$root?>news/default.html">Industrial Computer News<br />from ICP America</a></li>
							<li><a href="<?=$root?>support/default.html">Support</a></li>
						</ul>
					</td>
					<td style="width:25%">
						<ul class="unstyled">
							<li><a href="<?=$root?>partners/default.html">Partners</a></li>
							<li><a href="<?=$root?>services/default.html">Industrial Computer Services<br />from ICP America</a></li>
							<li><a href="<?=$root?>custom_quote/default.html">Request a Custom Quote</a></li>
							<li><a href="mailto:info@icpamerica.com">Email Us</a></li>
							<li><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></li>
						</ul>						
					</td>
					<td style="width:25%">
						<ul class="unstyled">
							<? /* <li><a href="<?=$root?>custom_quote/default.html">Build a Custom Part</a></li> */ ?>
							<li><a href="<?=$root?>faqs/default.html">FAQs</a></li>
							<li><a href="<?=$root?>products/default.html">Industrial Computer Products</a>
								<ul>
									<li><a href="<?=$root?>products/single_board_computers/default.html">Single Board Computers</a></li>
									<li><a href="<?=$root?>products/LCD_products/default.html">LCD Products & Panel PC</a></li>
									<li><a href="<?=$root?>products/chassis/default.html">Computer Chassis</a></li>
									<li><a href="<?=$root?>products/power_supply/default.html">Power Supplies</a></li>
								</ul>
							</li>
						</ul>						
					</td>
					<td style="width:25%">
						<ul class="unstyled">
							<li><a href="<?=$root?>products/default.html">Industrial Computer Products (cont.)</a>
								<ul>
									<li><a href="<?=$root?>products/medical_products/default.html">Medical Products</a></li>
									<li><a href="<?=$root?>products/accessories/default.html">Computer Accessories</a></li>
									<li><a href="http://www.mcsi1.com/" target="_blank">Embedded Division</a></li>
									<li><a href="http://www.icpads.com/" target="_blank">Digital Signage Division</a></li>
									<li><a href="http://www.icpmobile.com/" target="_blank">ICP Mobile</a></li>
								</ul>
							</li>
						</ul>						
					</td>
				</tr>
			</tbody>
		</table>
	</section>
</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT PLUGINS BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<? endblock(); ?>