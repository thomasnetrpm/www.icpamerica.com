<?
		
	# EXECUTE YOUR CSS
	$css = 'page';

	# CHECK USER CREDENDTIALS
	$user = $_GET['user'];

	# IMPORT YOUR BASE TEMPLATE
	include 'manager/page.php';

?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li class="active">my account</li>
    </ul>

		<h2>My Account</h2>

		<form action="" class="myaccount">
			
			<table class="table">
				<tbody>
					<tr>
						<td width="20%">First Name</td>
						<td width="80%"><input type="text" class="span6"></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type="text" class="span6"></td>
					</tr>
					<tr>
						<td>Company Name</td>
						<td><input type="text" class="span6"></td>
					</tr>
					<tr>
						<td>Telephone Number</td>
						<td><input type="text" class="span5"></td>
					</tr>
					<tr>
						<td>Address 1</td>
						<td><input type="text" class="span5"></td>
					</tr>
					<tr>
						<td>Address 2</td>
						<td><input type="text" class="span5"></td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type="text" class="span4"></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type="text" class="span4"></td>
					</tr>
					<tr>
						<td>Zip Code</td>
						<td><input type="text" class="span4"></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" class="span5"></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" class="span4"></td>
					</tr>
					<tr>
						<td>Re-Password</td>
						<td><input type="password" class="span4"></td>
					</tr>
				</tbody>
			</table>



		</form>

	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>
<? endblock(); ?>