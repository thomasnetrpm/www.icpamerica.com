<?php

#####################################
#Begin Sending Email
#####################################
include 'class.phpmailer.php';
include 'classes/Administrator.php';

$firstname 	  = $_POST['firstname'];
$lastname     = $_POST['lastname'];
$company		  = $_POST['company'];
$email 			  = $_POST['email'];
$distributor  = $_POST['distributor'];
$position 		= $_POST['position'];
$fax		      = $_POST['fax'];
$country      = $_POST['country'];

// System Info
$cpu_brand    = $_POST['cpu_brand'];
$cpu_model    = $_POST['cpu_model'];
$cpu_speed    = $_POST['cpu_speed'];
$main_board   = $_POST['main_board'];
$bios_version = $_POST['bios_version'];
$dram         = $_POST['dram'];
$os           = $_POST['os'];
$hd_brand     = $_POST['hd_brand'];
$hd_size      = $_POST['hd_size'];

// Product Information
$prod_brand   = $_POST['prod_brand'];
$prod_model   = $_POST['prod_model'];
$prod_version = $_POST['prod_version'];
$peripheral   = nl2br(stripslashes(strip_tags($_POST['peripheral'])));
$problem      = nl2br(stripslashes(strip_tags($_POST['problem'])));

$message = '
<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">  
<head>  
  <style type="text/css" media="screen">
    * { margin:0; padding:0; }
    body { font-family:Arial, Helvetica, sans-serif; color:#666; margin:10px 0; }
    body p { font-size:11px; margin:0 0 10px; }
    body a { color:#666; text-decoration:none; }
    body a:hover { color:#006ab6; }
    
    #parentframe { position:relative; font-size:13px; width:798px !important; margin:auto; border-collapse:collapse; border-spacing:0; border:solid 1px #999; }
    thead { background:#006ab6; color:#FFF; }
    thead	td { padding:10px; }
    tbody td { vertical-align:top; padding:10px; }
    tfoot { background:#EFEFEF; font-size:11px !important; color:#666; }
    tfoot	td { padding:5px; }
    
    h3 { font:bold 15px Arial; color:#006ab6; border-bottom:solid 1px #CCC; }
    
    #childframe { position:relative; font-size:13px; line-height:18px; margin:0; border-collapse:collapse; border-spacing:0; }
		#childframe .label { width:25%; }
		#childframe .label-inputs { width:60%; }
    #childframe td { padding:0 0 5px; }
	</style>
  <style type="text/css" media="print">
		* { margin:0; padding:0; }
    body { font-family:Arial, Helvetica, sans-serif; color:#666; margin:10px 0; }
		
    #parentframe { font-size:13px; width:100% !important; margin:10px; }
		.label { float:left; width:30%; }
		.label-inputs { float:left; width:70%; }
	</style>
</head>

<body>

  <table id="parentframe" width="75%">
    <thead>
      <tr>
        <td align="left">You have an Information Request from <strong>'.$name.'</strong>...</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><h3>ICP America - Support</h3></td>
      </tr>
      
      <tr>
        <td>
          <!-- Start Message Content -->
          <table id="childframe" width="100%">
            <tr>
              <td class="label" width="40%">Full Name</td>
              <td class="label-inputs" width="60%">'.$firstname.' '.$lastname.'</td>
            </tr>
            <tr>
              <td class="label">Company Name</td>
              <td class="label-inputs">'.$company.'</td>
            </tr>
            <tr>
              <td class="label">Email Address</td>
              <td class="label-inputs">'.$email.'</td>
            </tr>
            <tr>
              <td class="label">Distributor</td>
              <td class="label-inputs">'.$distributor.'</td>
            </tr>
            <tr>
              <td class="label">Position / Title</td>
              <td class="label-inputs">'.$position.'</td>
            </tr>
            <tr>
              <td class="label">Fax</td>
              <td class="label-inputs">'.$fax.'</td>
            </tr>
            <tr>
              <td class="label">Country</td>
              <td class="label-inputs">'.$country.'</td>
            </tr>

            <tr>
              <td colspan="2"><b>System Information</b></td>
            </tr>
            <tr>
              <td class="label">CPU Brand</td>
              <td class="label-inputs">'.$cpu_brand.'</td>
            </tr>
            <tr>
              <td class="label">CPU Model</td>
              <td class="label-inputs">'.$cpu_model.'</td>
            </tr>
            <tr>
              <td class="label">CPU SPeed</td>
              <td class="label-inputs">'.$cpu_speed.'</td>
            </tr>
            <tr>
              <td class="label">Main Board</td>
              <td class="label-inputs">'.$main_board.'</td>
            </tr>
            <tr>
              <td class="label">BIOS Version</td>
              <td class="label-inputs">'.$bios_version.'</td>
            </tr>
            <tr>
              <td class="label">DRAM</td>
              <td class="label-inputs">'.$dram.'</td>
            </tr>
            <tr>
              <td class="label">Operating System</td>
              <td class="label-inputs">'.$os.'</td>
            </tr>
            <tr>
              <td class="label">Hard Disk Brand</td>
              <td class="label-inputs">'.$hd_brand.'</td>
            </tr>
            <tr>
              <td class="label">Hard Disk Size</td>
              <td class="label-inputs">'.$hd_size.'</td>
            </tr>

            <tr>
              <td colspan="2"><b>Product Information</b></td>
            </tr>
            <tr>
              <td class="label">Product Brand</td>
              <td class="label-inputs">'.$prod_brand.'</td>
            </tr>
            <tr>
              <td class="label">Product Model</td>
              <td class="label-inputs">'.$prod_model.'</td>
            </tr>
            <tr>
              <td class="label">Product Version</td>
              <td class="label-inputs">'.$prod_version.'</td>
            </tr>
            <tr>
              <td class="label">Peripheral & Environment Description</td>
              <td class="label-inputs">'.$peripheral.'</td>
            </tr>
            <tr>
              <td class="label">Problem Description</td>
              <td class="label-inputs">'.$problem.'</td>
            </tr>
          </table>
          <!-- End Message Content -->
        </td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td align="right"><small>&copy; '.date("Y").' Powered by <a href="http://www.dogandrooster.com" target="_blank">Dog and Rooster, Inc.</a></small></td>
      </tr>
    </tfoot>
  </table>

</body>
</html>
';

$mail = new PHPMailer();
$mail->IsMail(); 

// send via Mail

//origin of sender
$mail->From     = ''.$email.'';

//concat name to fullname
$mail->FromName = ''.$firstname.' '.$lastname.'';

//Recipient
/*
$admin = Administrator::findAll('LIMIT 0 , 30');
foreach ($admin as $adm) {
  $mail->AddAddress($adm->fldAdministratorEmail); 
}
$mail->AddAddress('apuglisi@icpamerica.com');
$mail->AddAddress('ktinsley@icpamerica.com');
$mail->AddAddress('sales@icpamerica.com'); 
*/
$mail->AddAddress('test1@dogandrooster.net');
$mail->AddBCC('dennis@dogandrooster.com');

$mail->IsHTML(true); // send as HTML

$mail->Subject  =  'ICP America - Support';
$mail->Body     =  $message;
$mail->AltBody  =  $message;

if(!$mail->Send()){
 echo "Message was not sent <p>";
 echo "Mailer Error: " . $mail->ErrorInfo;
 //exit;
}

#####################################
#End of Sending Email
#####################################

// echo "<div class=alertfiles> Your information has been sent successfully! <br> Please allow us to review your request and we will get back to you soon... </div>";
?>