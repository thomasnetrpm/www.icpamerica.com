<!-- 
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
-->
<?php
// Get Footer
$footerID = 20;
$ftr = Pages::findPages($footerID);
$footerContent = stripslashes($ftr->fldPagesDescription)
?>

<div class="wrap footer">
	<footer class="clearfix">
		<?=$footerContent?>
	</footer>
</div>