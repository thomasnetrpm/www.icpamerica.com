<?php
// echo 'logged_in: '.$_SESSION['logged_in'];
// echo 'client_id: '.$_SESSION['client_id'];
// echo 'client_type: '.$_SESSION['client_type'];
// echo 'client_firstname: '.$_SESSION['client_firstname'];
if (isset($_REQUEST['add2cart'])) { //Add to cart from product details page

    if ($_REQUEST['product_option']!='') {
        $product_option = $_REQUEST['product_option'];

        $variant1 = ProductVariant::findAllVariantByOption2($product_option);

        $client_id  = $_SESSION['client_id'];
        
        $product_id = $_POST['product_id'];
        $name       = $_POST['product_name'];
        $price      = $_POST['product_price'];
        $price_text = $_POST['product_price_text'];
        $quantity   = $_POST['product_qty'];

        // Product Option & Variant
        $product_option = $_POST['product_option'];

        if ( ($_REQUEST['part_number']=='') && ($_REQUEST['addtocart']!='') ) {
            $msg_err = "Please select Part number.";
        }

    } else {
        $msg_err = "Please select Ordering Information.";
    }
    // echo 'product_option: '.$_REQUEST['product_option'].'<br>';
    // echo 'part_number: '.$_REQUEST['part_number'].'<br>';
    // echo 'add2cart: '.$_REQUEST['add2cart'].'<br>';
    // echo 'addtocart: '.$_REQUEST['addtocart'].'<br>';

    if (!$msg_err) {
        $client_id  = $_SESSION['client_id'];
        
        $product_id = $_POST['product_id'];
        $name       = $_POST['product_name'];
        $price      = $_POST['product_price'];
        $price_text = $_POST['product_price_text'];
        $quantity   = $_POST['product_qty'];

        // Product Option & Variant
        $product_option = $_POST['product_option'];

        $variant_id     = $_POST['part_number'];
        if ($variant_id >= 1) {
            $variant = ProductVariant::findVariant($variant_id);
            $price = $variant->price;
            // if ($_SESSION['client_id']==1) {
            if ($_SESSION['client_type']==2) {
                $price = '0.00';
            } else {
                $price = $variant->price;
            }
            unset($price_text);
        }

        if ($quantity >= 1) {
            $qty = $quantity;
        } else {
            $qty = 1;
        }
        
        //get the products information
        $products = Products::findProducts($product_id);
        
        ///code for options
        //$option =  var_dump($_POST['options']);
        if(isset($_POST['options'])) {
            foreach($_POST['options'] as $option1) {
                $array = array_values($option1);
                $options .= $array[0] . ', ';
            }   
        }
        

        $options = substr($options,0,strlen($options)-2);
        //end code for options  
             
        if ($_REQUEST['addtocart']!='') {
            $condition = "fldTempCartClientID='$client_id' AND fldTempCartProductID='$product_id' AND fldTempCartDate='$date' ";
            if (Tempcart::countTempcartbyCondition($condition)==1) {
                // echo "update the quantiy of the products";
                $temp_cart = Tempcart::displayTempcart($condition);
                $qty = $qty + $temp_cart->fldTempCartQuantity;
                Tempcart::updateTempcart($qty,$temp_cart->fldTempCartID);
            } else {
                // echo "save the products to temporary cart";
                $_POST = sanitize($_POST);
                $tempcart = $_POST;
                settype($tempcart,'object');
                $tempcart->product_id   = $product_id;
                $tempcart->quantity     = $qty;
                $tempcart->product_name = $name;
                $tempcart->price        = $price;
                $tempcart->price_text   = $price_text;
                $tempcart->client_id    = $client_id;
                $tempcart->options      = $options;
                if ($variant_id >= 1) {
                    $tempcart->product_variant   = $variant_id;
                }

                //$tempcart->options = $options;
                Tempcart::addTempcart($tempcart); 
            }
            // header("Location: ".$root."shopping-cart.html");
            // exit();
        } 
    }

}

?>
<article class="products clearfix">

	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
 		<li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
		<li><a href="<?=$root?>products/default.html">Products</a> <span class="divider">/</span></li>
		<li><a href="<?=$root?>products/<?=$category_url?>"><?=$bread_cat1?></a> <span class="divider">/</span></li>
		<?php if ($bread_cat2) { ?> <li><a href="<?=$root?>products/<?=$subcategory_url?>"><?=$bread_cat2?></a> <span class="divider">/</span></li> <?php } ?>
		<?php if (($bread_cat3) && (strtolower($bread_cat3)!='rackmount workstation')) { ?> 
            <li><a href="<?=$root?>products/<?=$subcategory2_url?>"><?=$bread_cat3?></a> <span class="divider">/</span></li> <?php } ?>
		<li class="active"><?=$productCode?></li>

    </ul>

    <div class="product-detail">
    	<ul class="unstyled clearfix">
    		<li class="pull-left img-gallery">
    			<dl>
    				<dt id="image">
    					<a href="<?=$root?>uploads/product_image/<?=$productID?>/_590_<?=$productImage?>" data-lightbox="product-images" title="<?=$productName?>">
    						<img src="<?=$root?>uploads/product_image/<?=$productID?>/_290_<?=$productImage?>" alt="<?=$productName?>">
    					</a>
    				</dt>
    				<dd>
							<a href="<?=$root?>uploads/product_image/<?=$productID?>/_75_<?=$productImage?>" rel="<?=$root?>uploads/product_image/<?=$productID?>/_290_<?=$productImage?>" big="<?=$root?>uploads/product_image/<?=$productID?>/_590_<?=$productImage?>" class="image">
								<img src="<?=$root?>uploads/product_image/<?=$productID?>/_75_<?=$productImage?>" alt="<?=$productImage?>" class="thumb">
							</a>
							<?php
								$images = Products::displayAllProductImages($productID);
								foreach ($images as $imgR) {
									$otherImages = $imgR->fldProductImage;
							?>
							<a href="<?=$root?>uploads/product_image/<?=$productID?>/_75_<?=$otherImages?>" rel="<?=$root?>uploads/product_image/<?=$productID?>/_290_<?=$otherImages?>" big="<?=$root?>uploads/product_image/<?=$productID?>/_590_<?=$otherImages?>" class="image">
								<img src="<?=$root?>uploads/product_image/<?=$productID?>/_75_<?=$otherImages?>" alt="" class="thumb">
							</a>
							<?php
								}
							?>
							<script>
								$(function() {
									$(".image").click(function() {
										var image = $(this).attr("rel");
										var larger = $(this).attr("big");
										$('#image').hide();
										$('#image').fadeIn('slow');
										// $('#image').html('<img src="' + image + '"/>');
										$('#image').html('<a href="' + larger + '" data-lightbox="product-images" ><img src="' + image + '"/></a>');
										return false;
									});
								});
							</script>
    				</dd>
    			</dl>
    		</li>

    		<li class="pull-right product-description">
                <? if(isset($msg_err)): ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?=$msg_err?>
                </div>
                <? endif; ?>

    			<h3 style="font-weight:600"><?=$productName?><small><?=$prodCode?></small></h3>
<!--     			<form action="<?=$root?>shopping-cart.html" method='POST'> -->
                <form action="" method='POST'>
    				<? if($_SESSION['logged_in']!= '1') { ?>
    				<fieldset class="form-inline log-user">
    					<a href="<?=$root?>registration.html" class="signup">sign up</a> <a href="<?=$root?>login.html" class="login">login</a>
     				</fieldset>
   					<? } else { ?>
    				<fieldset class="form-inline select-inline">
    					<?php
    					// Check for Text before Price
    					if ($productPriceText!='') { // Show Price Text
    						?>
    						<span class="price-tag pull-left"><?=$productPriceText?></sup> </span>
    						<?php
    						$productPrice = "0.00";
    					} else { // Show Price
                            $priceDisplay = "<font size='4'>Choose options below:</font>";
                            if ($_REQUEST['part_number']) { // show variant price
                                $var = ProductVariant::findVariant($_REQUEST['part_number']);
                                $productPrice = $var->price;
                                $priceDisplay = '$ '.$productPrice; 
                            }
                            /* <span class="price-tag pull-left"> $ <?=$productPrice?></sup> </span> */
                            ?>
	    					<span class="price-tag pull-left"><?=$priceDisplay?></sup></span>
    						<?php
    					}
    					?>

    					<div class="pull-right">
		    				<label for="qty">Quantity:</label>
                			<input type="text" name="product_qty" id="qty" value="1" required>
		    				<input type='hidden' name='product_id' value='<?=$productID?>'>
		    				<input type='hidden' name='product_name' value='<?=$productName?>'>
		    				<input type='hidden' name='product_price' value='<?=$productPrice?>'>
		    				<input type='hidden' name='product_price_text' value='<?=$productPriceText?>'>
                            <button type="submit" name="addtocart" value="add to cart">add to cart</button>
    					</div>

    					<?php
    					// if ($_SESSION['client_id']==1) {
                            $count_option = ProductVariant::countOption($productID);
                            if ($count_option>=1) {
    						?>
    					<div class="clear"></div>
    					<div class="product-option">
    					<p class="option-left">
    						Ordering Information
    					</p>
                            <input type='hidden' name='add2cart' value='1'>
    						<select name="product_option" onchange="this.form.submit()" required>
    							<option value="">Select..</option>
								<?
								$option = ProductVariant::displayAllOptionByProductID($productID);
								foreach($option as $opt) {
                                    $selected = ($opt->product_option_id == $_REQUEST['product_option'])? 'SELECTED': '';
								?>
									<option value="<?=$opt->product_option_id?>" <?=$selected?> ><?=$opt->product_option?></option>
								<? } ?>
    						</select>
    					</div>
    					<?php if (!empty($variant1)) { ?>
    					<div class="part-number">
    					<p class="option-right">
    						Part Number
    					</p>
    						<select name="part_number" onchange="this.form.submit()" required>
    							<option value="">Select..</option>
    							<?php
    							foreach ($variant1 as $var) {
    								$variant_id 	= $var->product_variant_id;
    								$variant_name 	= $var->product_variant;
                                    if ($_SESSION['client_type']==2) {
                                        $variant_price  = '0.00';
                                    } else {
                                        $variant_price  = $var->price;
                                        $variant_price_display = ' - $ '.$variant_price;
                                    }

                                    $selected = ($variant_id == $_REQUEST['part_number'])? 'SELECTED': '';
    								?>
	    							<option value="<?=$variant_id?>" <?=$selected?> ><?=$variant_name?><?=$variant_price_display?></option>
    								<?php
    							}
    							?>
    						</select>
    					</div>
    					<?php } ?>
    						<?
                            } // end if $count_option
    					// } // end if session_client_id
    					?>
    				</fieldset>
    				<? } ?>
                        </form>

    				<fieldset class="product-info">
    					<?php
                        // echo 'request_part_number: '.$_REQUEST['part_number'].'<br>';
                        if ($_REQUEST['part_number']) {
                            $var = ProductVariant::findVariant($_REQUEST['part_number']);
                            // echo stripcslashes($var->product_variant_description);
                            echo $var->product_variant_description;
                        } else {
                            echo $productOverview;
                        }
                        ?>
    				</fieldset>

    				<fieldset class="product-media clearfix">
    					<?php 
    					if ($productPDF) { echo '<a href="'.$root.'uploads/product_pdf/'.$productPDF.'" class="pdf-download" target="_new">Click here to download Datasheet</a>'; }
    					if ($productUM) { echo '<a href="'.$root.'uploads/product_usermanual/'.$productUM.'" class="pdf-download" target="_new">Click here to download User Manual</a>'; }
    					if ($productQG) { echo '<a href="'.$root.'uploads/product_quickguide/'.$productQG.'" class="pdf-download" target="_new">Click here to download Quick Guide</a>'; }
    					?>
    				</fieldset>
   
                
                <div class="social-media">
								<span class='st_facebook_hcount' displayText='Facebook'></span>
								<span class='st_twitter_hcount' displayText='Tweet'></span>
								<span class='st_googleplus_hcount' displayText='Google +'></span>
								<span class='st_sharethis_hcount' displayText='ShareThis'></span>
    					</div>
                
    		</li>
    	</ul>
    </div>

    <div class="product-features">
	    <ul class="nav nav-tabs" id="myTab">
		    <li class="active"><a href="#features">Features</a></li>
		    <li><a href="#specifications">Specifications</a></li>
		    <li><a href="#ordering-info">Ordering Info</a></li>
	    </ul>
	     
	    <div class="tab-content">
		    <div class="tab-pane active" id="features">
   					<?=$productFeatures?>
		    </div>

		    <div class="tab-pane" id="specifications">
   				<?=$productTechspecs?>
		    </div>

		    <div class="tab-pane" id="ordering-info">
				<?=$productOrderinfo?>
		    </div>
	    </div>
    </div>
	</section>
	<!-- End of Content Panel -->
	
</article>