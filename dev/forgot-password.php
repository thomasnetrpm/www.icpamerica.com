<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

$client = Client::findClient( $client_id );
?>

<?
$mail_status = null;
if(isset($_POST['email']))
{
    // retrieve password by username
    $email = $_POST['email'];
    $client = Client::findClientByEmail($email);

    $subject = "Password request for ICPAmerica.com";
    $login_url = $ROOT_URL . "login.html";
    $message = "<html>
        <head>
          <title>{$subject}</title>
        </head>
        <body>
          <p>Hi {$client->fldClientFirstName} {$client->fldClientLastname}</p>
          <table>
            <tr>
              <td>Your Username:</td>
              <td><b>{$client->fldClientUsername}</b></td>
            </tr>
            <tr>
              <td>Your Password:</td>
              <td><b>{$client->fldClientPassword}</b></td>
            </tr>
          </table>
          <p>Login here: <a href='{$login_url}'>{$login_url}</a>. Once logged in you can change your password </p>
          <p>Thanks,</p>
          <p>ICPAmerica.com Site Admin</p>
          <p>This is an automated response, please do not reply.</p>
        </body>
        </html>";

    if(!$client) {
        $mail_status = "Invalid email address.";
    } else {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: ' . $client->fldClientFirstName . ' ' . $client->fldClientLastname . ' <' . $email . '>' . "\r\n";
        $headers .= 'From: ICPAmerica.com <noreply@icpamerica.com>' . "\r\n";

        $mail_status = (mail($email, $subject, $message, $headers) )
            ?
            "Please check your inbox, <b>{$email}</b> to retrieve your password."
            :
            "Failed to send the email, please try again later.";
    }
}
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Forgot Password</li>
    </ul>

    <hgroup>
    <h2>Forgot Username or Password?</h2>
    </hgroup>

<? if(isset($mail_status)): ?>
    <br /><br />
    <div id='messages'>
        <span><?= $mail_status ?></span>
    </div><br />
<? endif ?>

    <div class='account_info col1'>
        <form action="forgot-password.html" method="post">
        <p>Please enter your email below and we'll send you your username / password.</p>
        <p>
            <label>Emaill Address *</label> <br>
            <input type="text" name="email" class=":required :email" size="50">
        </p>
        <p>
		<!-- <input type="submit" class="btn_submit" name="send_msg" value="Remind Password"> -->
		<button class="btn_submit" type="submit" name="send_msg" value="Remind Password">Remind Password</button>
		</p>
        </form>
    </div>
</article>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>


<?
// Clean Session
unset($_SESSION['errors']);
unset($_SESSION['message']);
unset($_SESSION['update_flag']);
?>

<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('headercodes') ?>
<? endblock() ?>

<? startblock('extracodes') ?>
<? endblock() ?>