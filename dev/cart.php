<?
		
	# EXECUTE YOUR CSS
	$css = 'page';

	# IMPORT YOUR BASE TEMPLATE
	include 'manager/page.php';

?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<aside>
		<dl id="category" class="side-framebox clearfix">
			<dt>Category</dt>
			<dd><span>Single Board Computers</span></dd>
			<dd class="active"><span class="on">LCD Products and Panel PC</span>
				<ul class="unstyled">
					<li><a href="#">Industrial panel pc</a></li>
					<li><a href="#">embedded panel pc</a></li>
					<li><a href="#">risc-based panel pc</a></li>
					<li><a href="#">multi-media act panel pc</a></li>
					<li><a href="#">workstation panel pc</a></li>
					<li><a href="#">rackmount lcd drawer</a></li>
					<li><a href="#">industrial lcd displays</a></li>
					<li><a href="#">industrial lcd kits</a></li>
					<li><a href="#">touch screens</a></li>
				</ul>
			</dd>
			<dd><span>Computer Chasis</span></dd>
			<dd><span>Medical Products</span></dd>
			<dd><span>Computer Accessories</span></dd>
			<dd><span>Digital Signage Division</span></dd>
			<dd><span>ICP Mobile</span></dd>
			<dd><span>Power Supplies</span></dd>
			<dd><span>Embedded Division</span></dd>
		</dl>
	</aside>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="#">Home</a> <span class="divider">/</span></li>
	    <li class="active"> Shopping Cart </li>
    </ul>

    <table class="table table-bordered cart-order-list">
    	<thead>
    		<tr class="cart-hdr">
					<th class="hdr-panel5"> <i class="icon-trash icon-white"></i> </th>
	    		<th class="hdr-panel1">Item Name</th>
	    		<th class="hdr-panel2">Price</th>
	    		<th class="hdr-panel3">QTY</th>
	    		<th class="hdr-panel4">Item Total</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td class="tp5"> <a href="#"><i class="icon-remove icon-red"></i></a> </td>
    			<td class="tp1">Sample Product Name 1</td>
    			<td class="tp2">149.99</td>
    			<td class="tp3"><input type="text" name="" value="1"></td>
    			<td class="tp4">149.99</td>
    		</tr>
    		<tr>
    			<td class="tp5"> <a href="#"><i class="icon-remove icon-red"></i></a> </td>
    			<td class="tp1">Sample Product Name 2</td>
    			<td class="tp2">149.99</td>
    			<td class="tp3"><input type="text" name="" value="1"></td>
    			<td class="tp4">149.99</td>
    		</tr>
    		<tr>
    			<td class="tp5"> <a href="#"><i class="icon-remove icon-red"></i></a> </td>
    			<td class="tp1">Sample Product Name 3</td>
    			<td class="tp2">149.99</td>
    			<td class="tp3"><input type="text" name="" value="1"></td>
    			<td class="tp4">149.99</td>
    		</tr>
    	</tbody>
    	<tfoot>
    		<tr class="cart-ftr">
    			<td colspan="4" align="right">Sub Total</td>
    			<td class="total-cart-price">$ 0.00</td>
    		</tr>
    		<tr>
    			<td colspan="2" align="right">Discount Code <br> <small>*You can only use one discount code at a time</small></td>
    			<td colspan="2"> <input type="text" name="" value=""> </td>
    			<td class="total-cart-price">$ 0.00</td>
    		</tr>
    		<tr>
    			<td colspan="4" align="right"><strong>Grand Total</strong></td>
    			<td class="total-cart-price payment">$ 0.00</td>
    		</tr>
    		<tr>
    			<td colspan="5" class="cart-functions">
    				<div class="btn-group">
	    				<button type="submit" name="" class="btn btn-small">Continue Shopping</button>
	    				<button type="submit" name="" class="btn btn-small">Update Shopping Cart</button>
	    				<button type="submit" name="" class="btn btn-small btn-success">Checkout Item(s)</button>
    				</div>
    			</td>
    		</tr>
    	</tfoot>
    </table>
	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<? endblock(); ?>