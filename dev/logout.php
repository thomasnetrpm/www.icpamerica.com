<?
ob_start();
session_start();

// Unset all of the session variables.
session_unset();
session_destroy();
unset($_SESSION['client_id']);

$LOGOUT_URL = "http://icpamerica.com/login.html";

header("Location: ".$LOGOUT_URL."");
?>