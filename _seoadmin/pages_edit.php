<?php 
ob_start();
session_start();

include "../classes/Connection.php";
include "../classes/Database.php";
include_once "../includes/bootstrap.php";    
include "../classes/Administrator.php";
include "../classes/Pages.php";
include "../includes/security.funcs.inc";
?>
<?php include_once("lib/fckeditor/fckeditor.php") ;?>

<?php		
  if(!isset($_SESSION['admin_id'])) {
  		header("Location: index.php");	
  } else {
  	$id = $_SESSION['admin_id'];
  	$admin_access = Administrator::findAdministrator($id);
  }
     
// Save Meta fields
if (isset($_POST['submit'])) {
	$_POST = sanitize($_POST);
  	$pages = $_POST;
  	settype($pages,'object');

	Pages::updatePagesMeta($pages);
	$success= "Meta Information Successfully Changed";
}
?>
<?php include("html/header.php"); ?>
		<!-- Header Here -->
		
  <tr>
    <td height="384" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="body">
      <tr>
        <td width="200" height="384" valign="top" bgcolor="#DDDDDD">
		<!-- Menu Here-->
		
		<?php
		      include("html/menu.php");
		
		 ?>
			
		</td>
    <td width="1000" valign="top"><table width="100%" border="0" cellpadding="10">
      <tr>
        <td valign="top" class="body_text"><span style="font-weight: bold">Welcome</span>&nbsp; <?=$_SESSION['real_name']?>,
          <br />
		<br />
		<table width="971" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333">
          <tr>
            <td height="25" class="table_header">Update Pages</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
            <form action="<?php $PHP_SELF; ?>" method="post">
            <table width="910" border="0" cellspacing="1" cellpadding="1">
            	<?php if(isset($success)) { ?>
              <tr>
                <td height="25" colspan="3" align="center" class="error"><?php echo $success; ?></td>
                </tr>
                <?php } ?>
                
                <?php
      					$id = $_REQUEST['id'];
      			    $pages = Pages::findPages($id);
        				?>
              <tr>
                <td width="163" valign="top" class="body_text">Name</td>
                <td width="10" valign="top" class="body_text">:</td>
                <td width="727"><?=$pages->fldPagesName?></td>
              </tr>              
              <tr>
                <td>Meta Title </td>
                <td>:</td>
                <td><textarea name="meta_title" cols="55" rows="7" id="meta_title"><?=stripslashes($pages->fldPagesMetaTitle)?></textarea></td>
              </tr>
              <tr>
                <td>Meta Keywords </td>
                <td>:</td>
                <td><textarea name="meta_keywords" cols="55" rows="7" id="meta_keywords"><?=stripslashes($pages->fldPagesMetaKeywords)?></textarea></td>
              </tr>
              <tr>
                <td>Meta Description </td>
                <td>:</td>
                <td><textarea name="meta_description" cols="55" rows="7" id="meta_description"><?=stripslashes($pages->fldPagesMetaDescription)?></textarea></td>
              </tr>
              <input type="hidden" name="Id" value="<?=$pages->fldPagesID?>" />
              <tr>
                <td colspan="3" align="center"><input type="submit" name="submit" id="submit" value="Update Page" /></td>
                </tr>
            </table>
            </form>
            </td>
          </tr>
        </table>
		<br />
		<!-- for status of client -->
		<br />
		        <br />
		        <!-- End Status--></td>
      </tr>
        </table>
		  </td>
      </tr>
    </table></td>
  </tr>
    
  
  	<!-- Footer Here -->
	<?php include("html/footer.php"); ?>



