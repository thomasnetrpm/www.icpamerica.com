<?php 
ob_start();
session_start();

include "../classes/Connection.php";
include "../classes/Database.php";
include_once "../includes/bootstrap.php";    
include "../classes/Administrator.php";
include "../includes/security.funcs.inc";

include "../classes/Pages.php";
include "../classes/Footer.php";
?>
<?php include_once("lib/fckeditor/fckeditor.php") ;?>

<?php		
  if(!isset($_SESSION['admin_id'])) {
  		header("Location: index.php");	
  } else {
  	$id = $_SESSION['admin_id'];
  	$admin_access = Administrator::findAdministrator($id);
  }
     
// Update Footer
if (isset($_POST['submit'])) {
	$_POST = sanitize($_POST);
  	$footer = $_POST;
  	settype($footer,'object');

	Footer::updateFooter($footer);
	$success= "Footer Successfully Changed";
}
?>
<?php include("html/header.php"); ?>
		<!-- Header Here -->
		
  <tr>
    <td height="384" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="body">
      <tr>
        <td width="200" height="384" valign="top" bgcolor="#DDDDDD">
		<!-- Menu Here-->
		
		<?php
		      include("html/menu.php");
		
		 ?>
			
		</td>
    <td width="1000" valign="top"><table width="100%" border="0" cellpadding="10">
      <tr>
        <td valign="top" class="body_text"><span style="font-weight: bold">Welcome</span>&nbsp; <?=$_SESSION['real_name']?>,
          <br />
		<br />
		<table width="971" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333">
          <tr>
            <td height="25" class="table_header">Update Footer</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
            <form action="<?php $PHP_SELF; ?>" method="post">
            <table width="910" border="0" cellspacing="1" cellpadding="1">
            	<?php if(isset($success)) { ?>
              <tr>
                <td height="25" colspan="3" align="center" class="error"><?php echo $success; ?></td>
                </tr>
                <?php } ?>
                
                <?php
      			$id = 20;
      			$pages = Pages::findPages($id);
        		?>
              <tr>
                <td colspan="3">
<textarea cols="" rows="" id="page.editor" name="footer_content"><?=stripslashes($pages->fldPagesDescription)?></textarea>
</td>
              </tr>
              <tr>
                <td colspan="3" align="center"><input type="submit" name="submit" id="submit" value="Update Footer" /></td>
                </tr>
            </table>
            </form>
            </td>
          </tr>
        </table>
		<br />
		<!-- for status of client -->
		<br />
		        <br />
		        <!-- End Status--></td>
      </tr>
        </table>
		  </td>
      </tr>
    </table></td>
  </tr>
    
  
  	<!-- Footer Here -->
	<?php include("html/footer.php"); ?>



