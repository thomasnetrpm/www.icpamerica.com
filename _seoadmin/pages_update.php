<?php 
include "../classes/Connection.php";
include "../classes/Database.php";
include_once "../includes/bootstrap.php";    
include "../classes/Administrator.php";
include "../classes/Pages.php";
include_once "../includes/Pagination.php";
?>
<?php
	session_start();	
  if(!isset($_SESSION['admin_id'])) {
  		header("Location: index.php");	
  } else {
  	$id = $_SESSION['admin_id'];
  	$admin_access = Administrator::findAdministrator($id);
  }
     
// Remove from Meta
if(isset($_REQUEST['delete'])) {
 	$page_id = $_REQUEST['delete'];
	
	Pages::removeSEO($page_id);
	$success= "Page successfully removed from SEO list";
}
?>
<?php include("html/header.php"); ?>
		<!-- Header Here -->
		
  <tr>
    <td height="384" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="body">
      <tr>
        <td width="200" height="384" valign="top" bgcolor="#DDDDDD">
		<!-- Menu Here-->
		
		<?php
		      include("html/menu.php");
		
		 ?>
			
		</td>
    <td width="1000" valign="top"><table width="100%" border="0" cellpadding="10">
      <tr>
        <td valign="top" class="body_text"><span style="font-weight: bold">Welcome</span>&nbsp; <?=$_SESSION['real_name']?>,
          <br />
		<br />
		<table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td height="25">
            <?php
				 // $count_pages=pages::countPages();
				 $count_pages=pages::countPagesSEO();
				if(!isset($_REQUEST['page']))
					{
						$page = 1;
					}
					else
					{
					$page = $_GET[page];
					}
					$pagination = new Pagination;
					//for display
					$pg = $pagination->page_pagination(20, $count_pages, $page, 20);

					$id = $_REQUEST['id'];
					// $pages=pages::findAll($pg[1]);
					$pages=pages::findAllSEO($pg[1]);
			?>
            <?=$pg[0]?>
            </td>
          </tr>
          
          <tr>
            <td height="25"><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333">
              <tr>
                <td height="25" class="table_header">Id</td>
                <td height="25" class="table_header">Name</td>
                <td height="25" class="table_header">&nbsp;</td>
              </tr>
              <?php if($count_pages==0) { ?>
              <tr>
              	<td colspan="3" align="center" bgcolor="#FFFFFF" class="error" height="25">No Record Found</td>
              </tr>
      			  <?php } else {
      			  	 foreach($pages as $page) {
      			  ?>
              <tr>
                <td height="20" bgcolor="#FFFFFF"><?=$page->fldPagesID?></td>
                <td height="20" bgcolor="#FFFFFF"><a href="pages_edit.php?id=<?=$page->fldPagesID?>"><?=$page->fldPagesName?></a></td>
                <td height="20" align="center" bgcolor="#FFFFFF">
                	<a href="pages_edit.php?id=<?=$page->fldPagesID?>">
                	<img src="images/edit_1.gif" width="22" height="17" border="0" /></a>
                    <a href="pages_update.php?delete=<?=$page->fldPagesID?>" onClick="return confirm(&quot;Are you sure you want to completely remove this Page from the list?\n\nPress 'OK' to delete.\nPress 'Cancel' to go back without removing the Page.\n&quot;)">
                    <img src="images/delete.gif" width="16" height="16" border="0" /></a>
                  </td>
              </tr>
              <?php } } ?>
            </table></td>
          </tr>
          <tr>
            <td height="25"><?=$pg[0]?></td>
          </tr>
        </table>
		<br />
		<!-- for status of client -->
		<br />
		        <br />
		        <!-- End Status--></td>
      </tr>
        </table>
		  </td>
      </tr>
    </table></td>
  </tr>
    
  
  	<!-- Footer Here -->
	<?php include("html/footer.php"); ?>



