<?php 
include "../classes/Connection.php";
include "../classes/Database.php";
include_once "../includes/bootstrap.php";    
include "../classes/Administrator.php";
include "../includes/security.funcs.inc";
?>
<?php
	if(isset($_POST['submit'])) {
		$_POST = sanitize($_POST);
	    $admin = $_POST;    
    	settype($admin,'object');
		$password = md5($admin->password);
    $condition="fldAdministratorusername='$admin->username' and fldAdministratorPassword='$password'";
    // $condition="username='$admin->username' and password='$password'";
		$administrator = Administrator::findAdministratorByCondition($condition);
		if(!empty($administrator)) {
			session_start();
			$_SESSION['admin_id']=$administrator->fldAdministratorID;
			$_SESSION['real_name']=$administrator->fldAdministratorRealName;
      // $_SESSION['admin_id']=$administrator->Id;
      // $_SESSION['real_name']=$administrator->real_name;
			header("Location: welcome.php");						
		} else {
			$error = "Invalid Username or Password";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ICP AMERICA  - Administrator</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="350" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#333333">
  <tr>
    <td height="25" class="table_header">ICP AMERICA - Administrator</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">
    <form method="post" action="<?php $PHP_SELF; ?>">
    <table width="100%" border="0" cellspacing="1" cellpadding="1">
    <?php if(isset($error)) { ?>
      <tr>
        <td height="25" colspan="3" align="center" class="error"><?php echo $error; ?></td>
        </tr>
     <?php } ?>   
      <tr>
        <td class="body_text">Username</td>
        <td class="body_text">:</td>
        <td><input type="text" name="username" id="username" /></td>
      </tr>
      <tr>
        <td class="body_text">Password</td>
        <td class="body_text">:</td>
        <td><input name="password" type="password" id="password" value="" /></td>
      </tr>
      <tr>
        <td colspan="3" align="center"><input type="submit" name="submit" id="button" value="Login" /></td>
        </tr>
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
