<html>
  <head>
    <title>ICP America - Order Receipt</title>
  </head>
  <body style="font-family:sans-serif;text-shadow:1px 1px 1px rgba(0,0,0,0.15);margin:0;background:#FFF;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td >
          <div align="center" style="width:650px;margin:auto;border-bottom:solid 1px #a0a0a0;box-shadow:0 1px 0 0 #f0f0f0;">
            <table border="0" cellpadding="0" cellspacing="0" width="650" style="font-size:12px;">
              <tr>
                <td colspan="2" align="center" width="100%"> <a href="http://ponytalepress.com"> <img src="http://icp2go.com/dev/assets/images/hdr-logo.png" width="242" height="100" alt="ICP America"> </a> </td>
              </tr>
              <tr>
                <td width="50%" style="background:#427700;font-weight:600;color:#FFF;text-align:left;padding:10px;"> Order No : 1-20130522-366 </td>
                <td width="50%" style="background:#427700;font-weight:600;color:#FFF;text-align:right;padding:10px;"> Order Date : May 22, 2013 </td>
              </tr>
            </table>
          </div>
          <!-- HEADER PANEL -->

          <div align="center" style="background:rgba(0,0,0,0.10);width:650px;margin:auto;padding:10px 0;border-bottom:solid 1px #a0a0a0;box-shadow:0 1px 0 0 #f0f0f0;">
            <table border="0" cellpadding="0" cellspacing="0" width="650">
              <tr>
                <td width="50%" style="font:600 13px sans-serif;color:#555;text-transform:uppercase;text-shadow:none;padding:10px 20px;"> Billing Information </td>
                <td width="50%" style="font:600 13px sans-serif;color:#555;text-transform:uppercase;text-shadow:none;padding:10px 20px;"> Shipping Information </td>
              </tr>
              <tr>
                <td style="font:300 13px sans-serif;color:#555;text-shadow:none;line-height:18px;padding:10px 20px;">
                  John Doe <br>
                  Oberlin Drive San Diego CA US 92121 <br>
                  <a href="mailto:test2@dogandrooster.net" style="color:#666;text-decoration:none;border-bottom:dotted 1px #333;">test2@dogandrooster.net</a> <br>
                  123 444-3344
                </td>
                <td style="font:300 13px sans-serif;color:#555;text-shadow:none;line-height:18px;padding:10px 20px;">
                  John Doe <br>
                  Oberlin Drive San Diego CA US 92121 <br>
                  <a href="mailto:test2@dogandrooster.net" style="color:#666;text-decoration:none;border-bottom:dotted 1px #333;">test2@dogandrooster.net</a> <br>
                  123 444-3344
                </td>
              </tr>
            </table>
          </div>
          <!-- INFO PANEL -->

          <div align="center" style="width:650px;margin:auto;padding:10px 0;">
            <table border="0" cellpadding="2" cellspacing="2" width="650" style="background:rgba(0,0,0,0.10);">
              <tr>
                <th width="60%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Product Name </th>
                <th width="10%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Amount </th>
                <th widht="10%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> QTY </th>
                <th width="20%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Total </th>
              </tr>
              <tr>
                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">
                  <img src="http://icp2go.com/dev/temp/product/home-product01.jpg" width="35%" alt="Single Board Computer" style="display:inline-block;margin-right:10px;vertical-align:middle;box-shadow:1px 1px 3px 0 rgba(0,0,0,0.75);">
                  Single Board Computers
                </td>
                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">
                  $ 7.99
                </td>
                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">
                  1
                </td>
                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">
                  $ 7.99
                </td>
              </tr>
              
              <tr><td colspan="4"><hr></td></tr>

              <tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  Sub-Total:
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  $ 7.99
                </td>
              </tr>
              <!-- SUB-TOTAL -->

              <tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  TAX:
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  $ 7.99
                </td>
              </tr>
              <!-- TAX -->

              <tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  DISCOUNT CODE:
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  test-092384-2
                </td>
              </tr>
              <!-- DISCOUNT CODE -->

              <tr>
                <td colspan="3" style="background:#427700;font:600 16px sans-serif;color:#FFF;text-align:right;text-transform:uppercase;text-shadow:none;padding:15px 10px;">
                  GRAND TOTAL:
                </td>
                <td style="background:#427700;font:400 14px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:15px 10px;">
                  $ 7.99
                </td>
              </tr>
              <!-- GRAND TOTAL -->
              
            </table>
          </div>
          <!-- ORDERS PANEL -->
        </td>
      </tr>
    </table>
  </body>
</html>
