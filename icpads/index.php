
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><!-- <meta http-equiv="refresh" content="0; URL='http://www.icpamerica.com/products/digital_signage_products/default.html'">-->
<link rel="shortcut icon" href="index_files/favicon.ico"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title>ICP Avanced Digital Signage Products - Players and LCD Signage Displays </title>
<meta content="text/html; charset=iso-8859-1" />
<meta name="Description" content="Industrial computers, single board computers and industrial LCD Products from ICP America.  ICP also offers embedded computers, lcd displays and display kits, panel pc computers, touch screens, flat monitors and more.  You can count on ICP to have the best pricing and service for all your industrial computers and supplies." />
<meta name="Keywords" content="industrial computer,computer power supplies,LCD products,rackmount,Embedded,industrial computers,Industrial Computer Product,single board computer,single board computers,industrial single board computer,industrial single board computers,industrial monitor,industrial monitors,industrial computer monitor,industrial computer monitors,icp,icp america,embedded computer,embedded computers,industrial pc,industrial pc computer,industrial embedded computer,industrial embedded computers,panel pc computer,panel pc computers,computer power supplies,computer power supply,computer touch screen,computer touch screens,embedded industrial computer,embedded industrial computers,embedded single board computer,embedded single board computers,high performance single board computer,high performance single board computers,industrial computer manufacturer,industrial computer manufacturers,industrial computer system,industrial computer systems,industrial flat panel computer,industrial flat panel computers,industrial lcd monitor,industrial lcd monitors,industrial panel pc,industrial pc,industrial rack mount computer,industrial rack mount computers,industrial single board computer,industrial single board computers,lcd display kit,lcd display kits,lcd flat panel monitor,lcd flat panel monitors,lcd kit,lcd kits,raw lcd monitor,raw lcd monitors,lcd monitor,lcd monitors" />
<meta name="robots" content="INDEX,FOLLOW" />
<meta name="viewport" content="width=1600">
<link rel="stylesheet" type="text/css" href="index_files/cm0263a3.css" media="all" />
<link rel="stylesheet" type="text/css" href="index_files/cmu5h9u3.css" media="all" />
<link rel="stylesheet" type="text/css" href="index_files/cmpe5mu3.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/strap-core.css" />
<script language="JavaScript" src="index_files/cm0263a3.js" type="text/javascript"></script>
<script language="JavaScript" src="index_files/cmxy6nt3.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
<!-- Hiding

defaultStatus="Â© 2012 ICP Advanced Digital Signage"

//-->
</script>
<script type="text/javascript">
<!--
// function MM_reloadPage(init) {  //reloads the window if Nav4 resized
//   if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
//     document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
//   else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
// }
// MM_reloadPage(true);
//-->
</script>
<style type="text/css">
/*
.style10 {
	color: #2e2e2e
}
*/</style>
</head>
<body id="ctl00_ctl03_Body1" class="default UAgecko UAff" onload="if (typeof _onLoad=='function') _onLoad()">
<!-- <a href="http://icpamerica.com/custom_quote/default.html" class="custom-quote"></a>
<div class="wrapper">
	<a name="top" id="top"></a>

	<form id="search-panel" method="post" action="http://www.icpamerica.com/search/<?=$_POST['top_keyword']?>">
		<ul class="unstyled clearfix">
            <li>Email: <a href="mailto:info@icpamerica.com">info@icpamerica.com</a> &nbsp;&nbsp; Call: 1-877-293-2000</li>
        	<li><a href="http://www.icpamerica.com/registration.html">Sign Up</a> <a href="http://www.icpamerica.com/login.html">Login</a></li>	
			<li style="top:-2px;">
				<input type="text" placeholder="Type your keywords..." value="" name="top_keyword">
				<input type="submit" value="&nbsp;" name="top_search">
			</li>
			<li>
				<a target="_blank" href="http://www.facebook.com/icpa1"><img width="24" height="24" alt="FB" src="http://www.icpamerica.com/assets/images/icon-facebook.png"></a> 
				<a target="_blank" href="http://www.linkedin.com/company/icp-america-inc."><img width="24" height="24" alt="LI" src="http://www.icpamerica.com/assets/images/icon-linkedin.png"></a> 
				<a target="_blank" href="http://twitter.com/icpamerica"><img width="24" height="24" alt="TT" src="http://www.icpamerica.com/assets/images/icon-twitter.png"></a> 
                <a target="_blank" rel="publisher" href="https://plus.google.com/100124929247060898549"><img width="24" alt="" src="http://www.icpamerica.com/assets/images/icon-google_plus.png"></a>
			</li>
		</ul>
	</form>
	
    <header>
    	<div class="container">
    	<div class="logo">
        <a href="index.htm" title="ICP Avanced Digital Signage Products - Players and LCD Signage Displays"><img alt="ICP Avanced Digital Signage Products - Players and LCD Signage Displays" src="index_files/icpadslogo.gif" /></a>	</div>
        <nav>
        	<ul class="unstyled">
            	<li><a href="http://icpamerica.com/index.php" id="banner_current">Home</a></li>
        		<li><a href="http://icpamerica.com/about_us/default.html" title="About ICP Advanced Digital Signage">About Us</a></li>
        		<li><a href="http://icpamerica.com/products/default.html" title="Products of ICP Advanced Digital Signage">Products</a></li>
        		<li><a href="http://icpamerica.com/contact_us/default.html" title="Contact ICP Advanced Digital Signage">Contact Us</a></li>
        		<li><a href="http://icpamerica.com/news/default.html" title="News from ICP Advanced Digital Signage">News</a></li> 
        		<li><a href="http://icpamerica.com/support/default.html" title="Support for ICP Advanced Digital Signage Products">Support</a></li>
        		<li><a href="http://icpamerica.com/partners/default.html" title="Partners for ICP Advanced Digital Signage">Partners</a></li>
        		<li><a href="http://icpamerica.com/services/default.html" title="Services from ICP Advanced Digital Signage">Services</a></li>
        	</ul>
        </nav>
        </div>
        <div class="clearfix"></div>
    </header> -->
<!--
<? 
	include "../includes/config2.php";
	$page = Pages::findPages(21);

?>
-->
<!-- 
   	<div class="banner"> -->
        <!--<img src="index_files/digitalsignagelg.jpg" width="748" border="0"/>-->
<!-- 		<img src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner1?>" width="748" border="0"/>
    </div>
    <div class="container">
    <div id="tableless">
	<div id="container" class="content">
        <div class="col">
        	<div class="signage-products">
            	<ul>
                	<li>
                    	<h1><a href="<?=$page->fldProductUrl1?>"><?=$page->fldProductName1?></a></h1>
        				<a href="<?=$page->fldProductUrl1?>" title="<?=$page->fldProductName1?>"><img alt="<?=$page->fldProductName1?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage1?>" class="cmx-FloatLeft" width="70" height="46" /></a>
                        <p><?=$page->fldProductDescription1?></p>
                    </li><!--player -->
                    <li>
                    	<h1><a href="<?=$page->fldProductUrl2?>"><?=$page->fldProductName2?></a></h1>
	                    <a href="<?=$page->fldProductUrl2?>" title="<?=$page->fldProductName2?>" ><img alt="<?=$page->fldProductName2?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage2?>" class="cmx-FloatLeft" width="69" height="55" /></a>
    	                <p><?=$page->fldProductDescription2?></p>
                    </li><!--wall controller -->
                </ul>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="new-products">
   	    	<!--<img src="index_files/ivw-ud322-video-wall-controller.jpg" width="1050" height="250" /> -->
			<img src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldBanner2?>" width="1050" height="250" /> 
            <a href="<?=$page->fldUrl?>" class="link1">Learn more</a>
        </div>
        <div class="before-footer">
            <h2>See More Products</h2>
        	<ul class="unstyled">
            	<li>
                	<h3><a href="<?=$page->fldProductUrl3?>" title="<?=$page->fldProductName3?>"><?=$page->fldProductName3?></a></h3>
        			<a href="<?=$page->fldProductUrl3?>"><img alt="<?=$page->fldProductName3?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage3?>" /></a>
                    <p><?=$page->fldProductDescription3?></p>
                </li>
                <li>
                	<h3><a href="<?=$page->fldProductUrl4?>" title="<?=$page->fldProductName4?>" target="_blank"><?=$page->fldProductName4?></a></h3>
       			 	<a href="<?=$page->fldProductUrl4?>" title="<?=$page->fldProductUrl4?>"><img alt="<?=$page->fldProductName4?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage4?>" /></a>
        			<p><?=$page->fldProductDescription4?></p>
                </li>
                <li>
                	<h3><a title="<?=$page->fldProductName5?>" href="<?=$page->fldProductUrl5?>" target="_blank"><?=$page->fldProductName5?></a></h3>
        			<a href="<?=$page->fldProductUrl5?>" target="_blank" title="<?=$page->fldProductName5?>"><img alt="<?=$page->fldProductName5?>" src="<?=$ROOT_URL?>uploads/pages/<?=$page->fldProductImage5?>"/></a>
        			<p><?=$page->fldProductDescription5?></p>  
        		</li>
            </ul>
        </div><!--before-footer -->
	</div><!--container -->
</div>
</div> -->
      

<!--
<div id="footer">
	<a href="http://icpamerica.com/index.php">Homepage</a>&nbsp;| 
	<a href="http://icpamerica.com/about_us/default.html">About Us</a>&nbsp;| 
	<a href="http://icpamerica.com/contact_us/default.html">Contact Us</a>&nbsp;| 
	<a href="http://icpamerica.com/news/default.html">News</a>&nbsp;| 
	<a href="http://icpamerica.com/support/default.html">Support</a>&nbsp;| 
	<a href="http://icpamerica.com/partners/default.html">Partners</a>&nbsp;| 
	<a href="http://icpamerica.com/services/default.html">Services</a>&nbsp;|
    <a href="http://icpamerica.com/privacy_policy/default.html"> Privacy Policy</a> 
	<p class="copyright">Â© &nbsp; 2013 ICP Advanced Digital Signage</p>
</div>
-->

<footer id="footer">
	<p>
		<span>&copy; 2014 ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="http://icpamerica.com/privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="http://icpamerica.com/site_map/default.html">Site Map</a></span>
		<br/>
		<small><a target="_blank" href="http://dogandrooster.com">Website Design by Dog and Rooster, Inc.</a></small>
	</p>	
</footer>


</div><!--wrapper -->


<script language="JavaScript" type="text/javascript">
<!--
 //var PL_Length = 6;
//-->
</script>
<style type="text/css">#menu div {display:none;}</style>
<!-- Backward compatibility hacks -->
	<noscript>
		<style type="text/css">#menu div{display:inline;}</style>
	</noscript>	
	<script language="JavaScript" type="text/javascript">
	<!--
	// if (!document.getElementById && !document.createTextNode){
	// 	document.write('<style type="text/css">#menu div{display:inline;}</style>')
	// }
	//-->
</script>

<!-- Backward compatibility hacks end -->
<script language="JavaScript" type="text/javascript">
<!--
// var url=document.URL;
// var website=url.substring(7,8);
// if (website=="w")
// {
// z=url.substring(35,38);
// }
// else {
// z=url.substring(42,45);
// }
// if (z=="sin")
// { z=0;}
// else if (z=="PIA")
// { z=0;}
// else if (z=="bac")
// { z=0;}
// else if (z=="LCD")
// { z=1;}
// else if (z=="cha")
// { z=2;}
// else if (z=="pow")
// { z=3;}
// else if (z=="acc")
// { z=4;}
// else
// { z=99;}
// try{
//      show(z); 
//     }catch(e){}
//-->
</script>

<script language="JavaScript">function DoValidate_email_form(){return MM_validateForm('First Name','','R','Last Name','','R','Company Name','','R','Product Name','','R','Email','','RisEmail')}</script>	

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37189214-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body></html>

